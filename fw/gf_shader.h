﻿//-----------------------------------------------------------------------------
// File: gf_shader.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_resource.h"
#include "gf_singleton.h"

namespace GF
{

	class IShader {
	public:
		virtual ~IShader(){}
		virtual void Release(void) = 0;

		enum SHADER_TYPE {
			SHADER_VS,
			SHADER_PS,
			SHADER_CS,
			SHADER_PRG,
		};

		virtual const std::string &GetName(void) = 0;
		virtual void *GetShader(void) = 0;
		virtual void *GetCode(void) = 0;
		virtual void Reload(void) = 0;

		static bool CreateShader(SHADER_TYPE type, const std::string &name,
								 const char *entryPoint,
								 const char *hlsl_string, unsigned int hlsl_size);
		static bool CreateShader(SHADER_TYPE type, const std::string &name,
								 const char *entryPoint,
								 const char *filename);

		static bool CheckShader(const std::string &name);
		static unsigned char GetShaderID(const std::string &name);
		static IShader *GetIShader(unsigned char id);
		static void *GetShader(unsigned char id);
	};

	class IShaderManager : public Singleton<IShaderManager> {
	public:
		virtual ~IShaderManager(){}
		virtual void Release(void){}
		virtual void Reload(void){}
	};


}
