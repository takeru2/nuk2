﻿
#include "gf_system.h"
#include "gf_device.h"
#include "gf_file.h"

#include "gf_shader.h"

#if defined(_WINDOWS)
#pragma comment( lib, "d3dcompiler.lib" )
#endif

namespace GF
{

	class ShaderManager : public IShaderManager {
	private:
		std::unordered_map<unsigned char, IShader*> id_shader_lists;
		std::unordered_map<std::string, unsigned char> name_shader_lists;
		bool m_bActive = true;
		static unsigned char uid;

	public:
		virtual ~ShaderManager(){
			m_bActive = true;
		}
		virtual void Release(void){
			m_bActive = false;
			for(std::unordered_map<unsigned char, IShader*>::iterator itr = 
					id_shader_lists.begin();itr != id_shader_lists.end();++itr){
				itr->second->Release();
			}
			m_bActive = true;
			uid = 1;
		}
		unsigned char Add(const std::string &name, IShader *shader){
			unsigned char id = uid++;
			id_shader_lists.insert(std::pair<unsigned char,IShader*>( id, shader ));
			name_shader_lists.insert(std::pair<std::string, unsigned char>( name, id ));

			if(uid == 0xff){
				GF_EXCEPTION("Shader Num Max");
			}
			return id; 
		}
		void Remove(unsigned char id){
			if(!m_bActive)return;
			std::unordered_map<unsigned char, IShader*>::iterator itr = id_shader_lists.find(id);
			if(itr != id_shader_lists.end()){
				id_shader_lists.erase(itr);
			}
		}

		IShader *GetShader(unsigned char id){
			std::unordered_map<unsigned char, IShader*>::iterator itr = 
				id_shader_lists.find( id );
			if(itr != id_shader_lists.end()){
				return itr->second;
			}

			GF_EXCEPTION("GetShader Error\n");
			return NULL;
		}

		bool TestShaderTag(const std::string &name){
			std::unordered_map<std::string, unsigned char>::iterator itr = 
				name_shader_lists.find(name);
			return (itr != name_shader_lists.end())?true:false;
		}

		unsigned char GetShaderID(const std::string &name){
			std::unordered_map<std::string, unsigned char>::iterator itr = 
				name_shader_lists.find(name);
			if(itr != name_shader_lists.end()){
				return itr->second;
			}

			GF_EXCEPTION("Shader ID Error\n");
			return 0;
		}

		virtual void Reload(void){
			for (std::unordered_map<unsigned char, IShader*>::iterator itr = id_shader_lists.begin();
				itr != id_shader_lists.end(); ++itr) {
				itr->second->Reload();
			}
		}

	};
	unsigned char ShaderManager::uid = 1;
	ShaderManager sShaderManager;

	class Shader : public IShader {
	private:
		ID3D11VertexShader *m_pShaderVS = NULL;
		ID3D11PixelShader  *m_pShaderPS = NULL;
		ID3D11ComputeShader*m_pShaderCS = NULL;
		ID3D10Blob *m_pCode = NULL;

		SHADER_TYPE mType;
		std::string mName;
		unsigned char mID;

		std::string mEntryPoint;
		std::string mFilename;
	public:
		Shader(SHADER_TYPE type){
			mType = type;
		}

		virtual ~Shader(){
		}

		virtual const std::string &GetName(void){ return mName; }
		virtual SHADER_TYPE GetType(void){ return mType; }
		virtual void SetFilename(const std::string &filename){ mFilename = filename; }

		virtual void *GetShader(void){
			switch(mType){
			case SHADER_VS:
				return m_pShaderVS;
			case SHADER_PS:
				return m_pShaderPS;
			case SHADER_CS:
				return m_pShaderCS;
			default:
				return NULL;
			}
		}
		virtual void SetID(unsigned char id){ mID = id;}

		virtual void *GetCode(void){
			return m_pCode;
		}

		virtual void Release(void){
			sShaderManager.Remove( mID );

			SAFE_RELEASE( m_pShaderVS );
			SAFE_RELEASE( m_pShaderPS );
			SAFE_RELEASE( m_pShaderCS );
			SAFE_RELEASE( m_pCode );

			delete this;
		}

#if defined(_WINDOWS)
		ID3D10Blob *compile(const char *name, const char *type, 
							const unsigned char *hlsl_string, unsigned int hlsl_size){
			D3D_SHADER_MACRO pDefines[0x10] = { 0 };
			ID3D10Blob *pErrorMsgs;
			mEntryPoint = name;

			GF_DX_RESULT(D3DCompile(hlsl_string,
									hlsl_size,
									NULL,
									NULL,//pDefines,
									NULL,
									name,
									type,
									0, 0,
									&m_pCode,
									&pErrorMsgs), (char*)pErrorMsgs->GetBufferPointer());
			return m_pCode;
		}
#endif

		void createShaderVS(const char *entryPoint, const char *hlsl_string, unsigned int hlsl_size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			ID3D10Blob *pCode = compile(entryPoint, "vs_5_0", (const unsigned char*)hlsl_string, hlsl_size);
			GF_DX_RESULT(device->CreateVertexShader(pCode->GetBufferPointer(),
													pCode->GetBufferSize(),
													NULL,
													&m_pShaderVS), "CreateVertexShader");
		}

		void createShaderPS(const char *entryPoint, const char *hlsl_string, unsigned int hlsl_size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			ID3D10Blob *pCode = compile(entryPoint, "ps_5_0", (const unsigned char*)hlsl_string, hlsl_size);
			GF_DX_RESULT(device->CreatePixelShader(pCode->GetBufferPointer(),
												   pCode->GetBufferSize(),
												   NULL,
												   &m_pShaderPS), "CreatePixelShader");
		}

		void createShaderCS(const char *entryPoint, const char *hlsl_string, unsigned int hlsl_size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
			ID3D10Blob *pCode = compile(entryPoint, "cs_5_0", (const unsigned char*)hlsl_string, hlsl_size);
			GF_DX_RESULT(device->CreateComputeShader(pCode->GetBufferPointer(),
													 pCode->GetBufferSize(),
													 NULL,
													 &m_pShaderCS), "CreateComputeShader");
		}

		void Reload(void){
			if(!mFilename.empty()){
				File shaderFile;
				shaderFile.Load(mFilename);
				const char *hlsl_string = (const char*)shaderFile.GetData();
				unsigned int hlsl_size  = shaderFile.GetSize();

				ID3D11VertexShader *m_pShaderVS_Prev = m_pShaderVS;
				ID3D11PixelShader  *m_pShaderPS_Prev = m_pShaderPS;
				ID3D11ComputeShader*m_pShaderCS_Prev = m_pShaderCS;
				ID3D10Blob *m_pCode_Prev = m_pCode;
				try{
					if(m_pShaderVS != NULL){
						createShaderVS(mEntryPoint.c_str(), hlsl_string, hlsl_size);
					}else if(m_pShaderPS != NULL){
						createShaderPS(mEntryPoint.c_str(), hlsl_string, hlsl_size);
					}else if(m_pShaderCS != NULL){
						createShaderCS(mEntryPoint.c_str(), hlsl_string, hlsl_size);
					}
				}
				catch (const GF::Exception &cause) {
					//失敗したので戻す
					GF::LOG("Shader recompile error %s", mFilename.c_str());
					GF::LOG(cause.what());

					m_pShaderVS = m_pShaderVS_Prev; m_pShaderVS_Prev = NULL;
					m_pShaderPS = m_pShaderPS_Prev; m_pShaderPS_Prev = NULL;
					m_pShaderCS = m_pShaderCS_Prev; m_pShaderCS_Prev = NULL;
					m_pCode = m_pCode_Prev; m_pCode_Prev = NULL;
				}

				SAFE_RELEASE(m_pShaderVS_Prev);
				SAFE_RELEASE(m_pShaderPS_Prev);
				SAFE_RELEASE(m_pShaderCS_Prev);
				SAFE_RELEASE(m_pCode_Prev);
			}
		}
	};

	bool IShader::CreateShader(SHADER_TYPE type, const std::string &name,
							   const char *entryPoint,
							   const char *hlsl_string, unsigned int hlsl_size){

		if(sShaderManager.TestShaderTag(name)){
			return true;
		}

		Shader *pShader = new Shader(type);
		try {
			switch (type) {
			case SHADER_VS:
				pShader->createShaderVS(entryPoint, hlsl_string, hlsl_size);
				break;
			case SHADER_PS:
				pShader->createShaderPS(entryPoint, hlsl_string, hlsl_size);
				break;
			case SHADER_CS:
				pShader->createShaderCS(entryPoint, hlsl_string, hlsl_size);
				break;
			}
		}
		catch (const GF::Exception &cause) {
			GF::LOG("Shader compile error %s", name.c_str());
			GF::LOG(cause.what());
		}

		pShader->SetID(sShaderManager.Add(name, pShader));
		return true;
	}

	bool IShader::CreateShader(SHADER_TYPE type, const std::string &name,
							   const char *entryPoint,
							   const char *filename){
		File shaderFile;
		shaderFile.Load(filename);
		const char *hlsl_string = (const char*)shaderFile.GetData();
		unsigned int hlsl_size  = shaderFile.GetSize();

		Shader *pShader = new Shader(type);
		try {
			switch(type){
			case SHADER_VS:
				pShader->createShaderVS(entryPoint, hlsl_string, hlsl_size);
				break;
			case SHADER_PS:
				pShader->createShaderPS(entryPoint, hlsl_string, hlsl_size);
				break;
			case SHADER_CS:
				pShader->createShaderCS(entryPoint, hlsl_string, hlsl_size);
				break;
			}
		}
		catch (const GF::Exception &cause) {
			GF::LOG("Shader compile error %s", name.c_str());
			GF::LOG(cause.what());
		}
		pShader->SetID(sShaderManager.Add(name, pShader));
		pShader->SetFilename(filename);
		return true;
	}

	bool IShader::CheckShader(const std::string &name){
		return sShaderManager.TestShaderTag(name);
	}

	unsigned char IShader::GetShaderID(const std::string &name){
		return sShaderManager.GetShaderID( name );
	}

	IShader *IShader::GetIShader(unsigned char id){
		if(id == 0)return NULL;
		return sShaderManager.GetShader( id );
	}

	void *IShader::GetShader(unsigned char id){
		if(id == 0)return NULL;
		return sShaderManager.GetShader( id )->GetShader();
	}


}

