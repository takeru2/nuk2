﻿//-----------------------------------------------------------------------------
// File: gf_resource.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <unordered_map>

#include "gf_system.h"

namespace GF
{

template< typename T_, typename E_ > class IResource {
    typedef T_ object_type;
private:
    static object_type *static_pInstance;
    IResource(const IResource&);
    IResource &operator=(const IResource&);

	std::list<E_*> element_lists;
	bool m_bRelease = false;
public:
	IResource(){ static_pInstance = (object_type*)this; }
  	virtual ~IResource(){ static_pInstance = 0; }

	virtual void Release(void){
		m_bRelease = true;
		for(std::list<E_*>::iterator itr = element_lists.begin();
			itr != element_lists.end();++itr){
			(*itr)->Release();
		}
		m_bRelease = false;
		element_lists.clear();
		delete this;
	}
	virtual std::list<E_*> &GetLists(void){return element_lists;}

	virtual void Add(E_ *elem){
		element_lists.push_back( elem );
	}
	virtual void Remove(E_ *elem){
		if(m_bRelease)return;
		std::list<E_*>::iterator itr = find(element_lists.begin(), element_lists.end(), elem);
		if(itr != element_lists.end()){
			element_lists.erase( itr );
		}
	}
    static object_type *GetInstance(void){
		if(static_pInstance == NULL)new IResource<T_, E_>();
		return static_pInstance;
	}
};
template< typename T_, typename E_ > typename IResource< T_, E_ >::object_type* IResource< T_, E_ >::static_pInstance = 0;



template< typename T_ > class IDMapResource {
	std::unordered_map<unsigned int, T_> element_maps;
public:
	IDMapResource(){}
  	virtual ~IDMapResource(){}

	virtual void Release(void){
		for(std::unordered_map<unsigned int, T_>::iterator itr = element_maps.begin();
			itr != element_maps.end();++itr){
			SAFE_RELEASE(itr->second);
		}
		element_maps.clear();
	}
	virtual bool Get(unsigned int id, T_ &elem){
		std::unordered_map<unsigned int, T_>::iterator itr = element_maps.find( id );
		if(itr != element_maps.end()){
			elem = itr->second;
			return true;
		}
		return false;
	}

	virtual void Add(unsigned int id, T_ elem){
		element_maps.insert(std::pair<unsigned int, T_>( id, elem));
	}

	virtual void Remove(unsigned int id){
		std::unordered_map<unsigned int, T_>::iterator itr = element_maps.find( id );
		if(itr != element_maps.end()){
			element_maps.erase( itr );
		}
	}
};

template< typename T_, typename E_ > class MapResource {
	std::unordered_map<T_, E_> element_maps;
public:
	MapResource(){}
	MapResource(T_ key, E_ elem){
		Add(key, elem);
	}
  	virtual ~MapResource(){}

	virtual void Release(void){
		for(std::unordered_map<T_, E_>::iterator itr = element_maps.begin();
			itr != element_maps.end();++itr){
			SAFE_RELEASE(itr->second);
		}
		element_maps.clear();
	}

	virtual void Add(T_ key, E_ elem){
		element_maps.insert(std::pair<T_, E_>( key, elem));
	}

	virtual void Remove(T_ key){
		std::unordered_map<T_, E_>::iterator itr = element_maps.find( key );
		if(itr != element_maps.end()){
			element_maps.erase( itr );
		}
	}

	virtual void Remove(E_ elem){
		for(std::unordered_map<T_, E_>::iterator itr = 
				element_maps.begin(); itr != element_maps.end();++itr){
			if(itr->second == elem){
				element_maps.erase( itr );
				return;
			}
		}
	}

	virtual bool Contain(T_ key) {
		std::unordered_map<T_, E_>::iterator itr = element_maps.find(key);
		return (itr != element_maps.end()) ? true : false;
	}

	virtual E_ &Get(T_ key){
		std::unordered_map<T_, E_>::iterator itr = element_maps.find( key );
		if(itr != element_maps.end()){
			return itr->second;
		}
		GF_EXCEPTION("Map Resource Error!");
		
		static E_ _dummy;
		return _dummy;
	}

};
	
}





