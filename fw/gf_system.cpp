﻿
#include "gf_device.h"
#include "gf_task.h"
#include "gf_input.h"
#include "gf_editvalue.h"

#include "gf_system.h"

namespace GF
{
	void (*CustomLog)(const char*) = NULL;

class SystemCore : public ISystem {
public:
	static SystemCore *s_Instance;
	bool bPause = false;
public:
	SystemCore(){
		s_Instance = this;
	}

	virtual void Release(void){
		ITaskManager::GetInstance()->Release();
		IDevice::GetInstance()->Release();
		IEditValue::GetInstance()->Release();

		delete this;
	}

	virtual void Init(const char *commands){
		IEditValue::CreateInstance();
		ITaskManager::CreateInstance();
		ITaskManager::GetInstance()->Start(commands);
	}

	virtual void Update(void){
		//Input
		IInput::GetInstance()->Update();

		//FPS  ???
		if(bPause){

		}else{
			ITaskManager::GetInstance()->Update();
		}

		//SYNC ???
		IDevice::GetInstance()->Begin();
		{
			ITaskManager::GetInstance()->PreDraw();
			ITaskManager::GetInstance()->Draw();
		}
		IDevice::GetInstance()->Submit();
	}

	virtual void Pause(bool bPause){
		this->bPause = bPause;
	}

	virtual bool IsPause(void){
		return bPause;
	}


};
SystemCore *SystemCore::s_Instance = NULL;

bool ISystem::CreateInstance(void){
	new SystemCore();
	return true;
}

ISystem *ISystem::GetInstance(void){
	return SystemCore::s_Instance;
}


}




