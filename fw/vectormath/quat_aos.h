﻿/* SCE CONFIDENTIAL
* PlayStation(R)4 Programmer Tool Runtime Library Release 03.508.031
* Copyright (C) 2015 Sony Computer Entertainment Inc.
* All Rights Reserved.
*/
#pragma once

#ifndef _SCE_VECTORMATH_SCALAR_QUAT_AOS_CPP_H
#define _SCE_VECTORMATH_SCALAR_QUAT_AOS_CPP_H

//-----------------------------------------------------------------------------
// Definitions

namespace Monstars {
namespace Vectormath {
namespace Scalar {
namespace Aos {

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat()
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(const Quat& quat)
	: mX(quat.mX)
	, mY(quat.mY)
	, mZ(quat.mZ)
	, mW(quat.mW)
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(float _x, float _y, float _z, float _w)
	: mX(_x)
	, mY(_y)
	, mZ(_z)
	, mW(_w)
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(floatInVec_arg _x, floatInVec_arg _y, floatInVec_arg _z, floatInVec_arg _w)
	: mX(_x.getAsFloat())
	, mY(_y.getAsFloat())
	, mZ(_z.getAsFloat())
	, mW(_w.getAsFloat())
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(Vector3_arg xyz, float _w)
	: mX(xyz.getX().getAsFloat())
	, mY(xyz.getY().getAsFloat())
	, mZ(xyz.getZ().getAsFloat())
	, mW(_w)
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(Vector3_arg xyz, floatInVec_arg _w)
	: mX(xyz.getX().getAsFloat())
	, mY(xyz.getY().getAsFloat())
	, mZ(xyz.getZ().getAsFloat())
	, mW(_w.getAsFloat())
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(Vector4_arg vec)
	: mX(vec.getX().getAsFloat())
	, mY(vec.getY().getAsFloat())
	, mZ(vec.getZ().getAsFloat())
	, mW(vec.getW().getAsFloat())
{
}

#if !defined(SCE_VECTORMATH_NO_MATH_DEFS)

SCE_VECTORMATH_INLINE Quat::Quat(Matrix3_arg rotMat)
{
	float trace, radicand, scale, xx, yx, zx, xy, yy, zy, xz, yz, zz, tmpx, tmpy, tmpz, tmpw, qx, qy, qz, qw;
	bool negTrace, ZgtX, ZgtY, YgtX;
	bool largestXorY, largestYorZ, largestZorX;

	xx = rotMat.getCol0().getX().getAsFloat();
	yx = rotMat.getCol0().getY().getAsFloat();
	zx = rotMat.getCol0().getZ().getAsFloat();
	xy = rotMat.getCol1().getX().getAsFloat();
	yy = rotMat.getCol1().getY().getAsFloat();
	zy = rotMat.getCol1().getZ().getAsFloat();
	xz = rotMat.getCol2().getX().getAsFloat();
	yz = rotMat.getCol2().getY().getAsFloat();
	zz = rotMat.getCol2().getZ().getAsFloat();

	trace = ( ( xx + yy ) + zz );

	negTrace = ( trace < 0.0f );
	ZgtX = zz > xx;
	ZgtY = zz > yy;
	YgtX = yy > xx;
	largestXorY = ( !ZgtX || !ZgtY ) && negTrace;
	largestYorZ = ( YgtX || ZgtX ) && negTrace;
	largestZorX = ( ZgtY || !YgtX ) && negTrace;

	if ( largestXorY )
	{
		zz = -zz;
		xy = -xy;
	}
	if ( largestYorZ )
	{
		xx = -xx;
		yz = -yz;
	}
	if ( largestZorX )
	{
		yy = -yy;
		zx = -zx;
	}

	radicand = ( ( ( xx + yy ) + zz ) + 1.0f );
	scale = ( 0.5f * ( 1.0f / ::sqrtf( radicand ) ) );

	tmpx = ( ( zy - yz ) * scale );
	tmpy = ( ( xz - zx ) * scale );
	tmpz = ( ( yx - xy ) * scale );
	tmpw = ( radicand * scale );
	qx = tmpx;
	qy = tmpy;
	qz = tmpz;
	qw = tmpw;

	if ( largestXorY )
	{
		qx = tmpw;
		qy = tmpz;
		qz = tmpy;
		qw = tmpx;
	}
	if ( largestYorZ )
	{
		tmpx = qx;
		tmpz = qz;
		qx = qy;
		qy = tmpx;
		qz = qw;
		qw = tmpz;
	}

	mX = qx;
	mY = qy;
	mZ = qz;
	mW = qw;
}

#endif	/* !defined(SCE_VECTORMATH_NO_MATH_DEFS) */

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(float scalar)
	: mX(scalar)
	, mY(scalar)
	, mZ(scalar)
	, mW(scalar)
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat::Quat(floatInVec_arg scalar)
	: mX(scalar.getAsFloat())
	, mY(scalar.getAsFloat())
	, mZ(scalar.getAsFloat())
	, mW(scalar.getAsFloat())
{
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator =(Quat_arg quat)
{
	mX = quat.mX;
	mY = quat.mY;
	mZ = quat.mZ;
	mW = quat.mW;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setXY(Vector2_arg vec)
{
	mX = vec.getX().getAsFloat();
	mY = vec.getY().getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setXYZ(Vector3_arg vec)
{
	mX = vec.getX().getAsFloat();
	mY = vec.getY().getAsFloat();
	mZ = vec.getZ().getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE const Vector2 Quat::getXY() const
{
	return Vector2( mX, mY );
}

SCE_VECTORMATH_ALWAYS_INLINE const Vector3 Quat::getXYZ() const
{
	return Vector3( mX, mY, mZ );
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setX(float _x)
{
	mX = _x;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setY(float _y)
{
	mY = _y;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setZ(float _z)
{
	mZ = _z;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setW(float _w)
{
	mW = _w;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setX(floatInVec_arg _x)
{
	mX = _x.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setY(floatInVec_arg _y)
{
	mY = _y.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setZ(floatInVec_arg _z)
{
	mZ = _z.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::setW(floatInVec_arg _w)
{
	mW = _w.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec Quat::getX() const
{
	return floatInVec(mX);
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec Quat::getY() const
{
	return floatInVec(mY);
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec Quat::getZ() const
{
	return floatInVec(mZ);
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec Quat::getW() const
{
	return floatInVec(mW);
}

SCE_VECTORMATH_INLINE Quat& Quat::setElem(int idx, float value)
{
	SCE_VECTORMATH_ASSERT_MSG( ( 0 <= idx ) && ( idx < 4 ), "Element Index Out of Range" );
	*(&mX + idx) = value;
	return *this;
}

SCE_VECTORMATH_INLINE Quat& Quat::setElem(int idx, floatInVec_arg value)
{
	SCE_VECTORMATH_ASSERT_MSG( ( 0 <= idx ) && ( idx < 4 ), "Element Index Out of Range" );
	*(&mX + idx) = value.getAsFloat();
	return *this;
}

SCE_VECTORMATH_INLINE const floatInVec Quat::getElem(int idx) const
{
	SCE_VECTORMATH_ASSERT_MSG( ( 0 <= idx ) && ( idx < 4 ), "Element Index Out of Range" );
	return floatInVec(*(&mX + idx));
}

SCE_VECTORMATH_INLINE float & Quat::operator [](int idx)
{
	SCE_VECTORMATH_ASSERT_MSG( ( 0 <= idx ) && ( idx < 4 ), "Element Index Out of Range" );
	return *(&mX + idx);
}

SCE_VECTORMATH_INLINE const floatInVec Quat::operator [](int idx) const
{
	SCE_VECTORMATH_ASSERT_MSG( ( 0 <= idx ) && ( idx < 4 ), "Element Index Out of Range" );
	return floatInVec( *(&mX + idx) );
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator +(Quat_arg quat) const
{
	return Quat(
		( mX + quat.mX ),
		( mY + quat.mY ),
		( mZ + quat.mZ ),
		( mW + quat.mW )
		);
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator -(Quat_arg quat) const
{
	return Quat(
		( mX - quat.mX ),
		( mY - quat.mY ),
		( mZ - quat.mZ ),
		( mW - quat.mW )
		);
}

SCE_VECTORMATH_INLINE const Quat Quat::operator *(Quat_arg quat) const
{
	return Quat(
		( ( ( ( mW * quat.mX ) + ( mX * quat.mW ) ) + ( mY * quat.mZ ) ) - ( mZ * quat.mY ) ),
		( ( ( ( mW * quat.mY ) + ( mY * quat.mW ) ) + ( mZ * quat.mX ) ) - ( mX * quat.mZ ) ),
		( ( ( ( mW * quat.mZ ) + ( mZ * quat.mW ) ) + ( mX * quat.mY ) ) - ( mY * quat.mX ) ),
		( ( ( ( mW * quat.mW ) - ( mX * quat.mX ) ) - ( mY * quat.mY ) ) - ( mZ * quat.mZ ) )
		);
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator *(floatInVec_arg scalar) const
{
	return *this * scalar.getAsFloat();
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator *(float scalar) const
{
	return Quat(
		( mX * scalar ),
		( mY * scalar ),
		( mZ * scalar ),
		( mW * scalar )
		);
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator /(floatInVec_arg scalar) const
{
	return *this / scalar.getAsFloat();
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator /(float scalar) const
{
	return Quat(
		( mX / scalar ),
		( mY / scalar ),
		( mZ / scalar ),
		( mW / scalar )
		);
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator +=(Quat_arg quat)
{
	*this = *this + quat;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator -=(Quat_arg quat)
{
	*this = *this - quat;
	return *this;
}

SCE_VECTORMATH_INLINE Quat& Quat::operator *=(Quat_arg quat)
{
	*this = *this * quat;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator *=(float scalar)
{
	*this = *this * scalar;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator /=(float scalar)
{
	*this = *this / scalar;
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator *=(floatInVec_arg scalar)
{
	*this = *this * scalar.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE Quat& Quat::operator /=(floatInVec_arg scalar)
{
	*this = *this / scalar.getAsFloat();
	return *this;
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::operator -() const
{
	return Quat(
		-mX,
		-mY,
		-mZ,
		-mW
		);
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::identity()
{
	return Quat( 0.0f, 0.0f, 0.0f, 1.0f );
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat Quat::zero()
{
	return Quat( 0.0f, 0.0f, 0.0f, 0.0f );
}

#if	!defined(SCE_VECTORMATH_NO_MATH_DEFS)

SCE_VECTORMATH_INLINE const Vector4 Quat::axisAngle(Quat_arg unitQuat)
{
	const float kEpsilon = 0.0001f;
	float q_w = unitQuat.getW().getAsFloat();

	bool valid = ( _SCE_VECTORMATH_BUILTIN_FABSF( q_w ) <= 1.0f );

	float angle = ::acosf( q_w );
	angle += angle;

	float scale = 1.0f - ( q_w * q_w );
	valid &= ( scale > kEpsilon );
	scale = valid ? scale : 1.0f;
	angle = valid ? angle : 0.0f;

	Vector3 axis = select( Vector3::xAxis(), unitQuat.getXYZ(), valid );
	axis = axis / ::sqrtf( scale );

	Vector4 result = Vector4( axis, angle );
	return result;
}

SCE_VECTORMATH_INLINE const Vector3 Quat::euler(Quat_arg quat, RotationOrder order)
{
	const floatInVec k0(0.0f);
	const floatInVec k1p(+1.0f);
	const floatInVec k1n(-1.0f);
	const floatInVec kEpsilon(0.99999f);

	// Sign masks
	const float kSignMods[ 6 ] = {
		-1.0f,	// kXYZ
		-1.0f,	// kYZX
		-1.0f,	// kZXY
		+1.0f,	// kXZY
		+1.0f,	// kYXZ
		+1.0f 	// kZYX
	};

	// Reordering select masks, used to put in strict order (for asin/atan calls)
	const bool kOrderMasks[6][4] = {
		{ true,  false, false, true  },	// kXYZ
		{ false, true,  false, false },	// kYZX
		{ false, false, true,  false },	// kZXY
		{ true,  false, false, false },	// kXZY
		{ false, true,  true,  false },	// kYXZ
		{ false, false, false, true  },	// kZYX
	};
	const boolInVec isX0 = boolInVec( kOrderMasks[ order ][ 0 ] );
	const boolInVec isY0 = boolInVec( kOrderMasks[ order ][ 1 ] );
	const boolInVec isX1 = boolInVec( kOrderMasks[ order ][ 2 ] );
	const boolInVec isY1 = boolInVec( kOrderMasks[ order ][ 3 ] );
	const boolInVec isX2 = !(isX0 || isX1);
	const boolInVec isY2 = !(isY0 || isY1);

	// Get various sums of squares
	Vector4 vquat = Vector4( quat );
	Vector4 sq_vquat = mulPerElem( vquat, vquat );
	Vector4 mtx0 = mulPerElem( sq_vquat, Vector4( +1.f, -1.f, -1.f, +1.f ) );
	Vector4 mtx1 = mulPerElem( sq_vquat, Vector4( -1.f, +1.f, -1.f, +1.f ) );
	Vector4 mtx2 = mulPerElem( sq_vquat, Vector4( -1.f, -1.f, +1.f, +1.f ) );
	floatInVec mtx00 = sum( mtx0 );
	floatInVec mtx11 = sum( mtx1 );
	floatInVec mtx22 = sum( mtx2 );

	// Replicate terms from quat
	floatInVec qX = quat.getX();
	floatInVec qY = quat.getY();
	floatInVec qZ = quat.getZ();
	floatInVec qW = quat.getW();

	floatInVec qXY = qX * qY;
	floatInVec qXZ = qX * qZ;
	floatInVec qYZ = qY * qZ;
	floatInVec qXW = qX * qW;
	floatInVec qYW = qY * qW;
	floatInVec qZW = qZ * qW;

	// Fetch sign bits, perform modification, multiply with lhs, then add to core value
	const floatInVec signBits( kSignMods[ order ] );
	floatInVec mtx10or01p = signBits * qZW + qXY;
	floatInVec mtx20or02p = signBits * qYW + qXZ;
	floatInVec mtx21or12p = signBits * qXW + qYZ;
	floatInVec mtx10or01n = signBits * qZW - qXY;
	floatInVec mtx20or02n = signBits * qYW - qXZ;
	floatInVec mtx21or12n = signBits * qXW - qYZ;

	// At this point all mtx components should be good to do.. we need to re-order for asin/atan2f calls
	floatInVec a = select( select( mtx10or01p, mtx20or02p, isY1 ), mtx21or12p, isX1 );
	floatInVec b = select( select( mtx10or01n, mtx20or02n, isY2 ), mtx21or12n, isX2 ) * signBits;
	floatInVec d = select( select( mtx10or01n, mtx20or02n, isY0 ), mtx21or12n, isX0 ) * signBits;
	floatInVec f = select( select( mtx10or01p, mtx20or02p, isY0 ), mtx21or12p, isX0 ) * signBits;
	floatInVec c = select( select( mtx22, mtx11, isY0 ), mtx00, isX0 );
	floatInVec e = select( select( mtx22, mtx11, isY2 ), mtx00, isX2 );
	floatInVec g = select( select( mtx22, mtx11, isY1 ), mtx00, isX1 );

	// Scale by 2.0f (so.. that's an add then..)
	a += a;
	b += b;
	d += d;
	f += f;

	// If we are close to limit, we must account for a potential loss of a degree of freedom
	boolInVec limit( ::fabsf( a.getAsFloat() ) >= kEpsilon.getAsFloat() );
	floatInVec scale = select( k1p, select( k1n, k1p, k0 < a ), limit );

	a = clamp( a, k1n, k1p ) * signBits;
	b = select( b, f, limit );
	c = select( c, g, limit );
	d = select( d, k0, limit );
	e = select( e, k0, limit );

	// Do the maths..
	float results[ 3 ];
	results[ 0 ] = ::asinf( a.getAsFloat() );
	results[ 1 ] = ::atan2f( b.getAsFloat(), c.getAsFloat() ) * scale.getAsFloat();
	results[ 2 ] = ::atan2f( d.getAsFloat(), e.getAsFloat() );

	// Now write to X, Y, and Z floats depending on rotation order..
	const int resultLoc[ 6 ][ 3 ] =
	{
		{ 2, 0, 1 }, // kXYZ
		{ 1, 2, 0 }, // kYZX
		{ 0, 1, 2 }, // kZXY
		{ 2, 1, 0 }, // kXZY
		{ 0, 2, 1 }, // kYXZ
		{ 1, 0, 2 }, // kZYX
	};

	Vector3 result(
		results[ resultLoc[ order ][ 0 ] ],
		results[ resultLoc[ order ][ 1 ] ],
		results[ resultLoc[ order ][ 2 ] ] );
	return result;
}

SCE_VECTORMATH_INLINE const Quat Quat::rotation(Vector3_arg radians, RotationOrder order)
{
	const int rotIndex[ 6 ][ 3 ] =
	{
		{ 0, 1, 2 }, // kXYZ
		{ 2, 0, 1 }, // kYZX
		{ 1, 2, 0 }, // kZXY
		{ 0, 2, 1 }, // kXZY
		{ 1, 0, 2 }, // kYXZ
		{ 2, 1, 0 }, // kZYX
	};

	// Build axial rotations
	Quat rotations[ 3 ];
	rotations[ rotIndex[ order ][ 0 ] ] = Quat::rotationX( radians.getX().getAsFloat() );
	rotations[ rotIndex[ order ][ 1 ] ] = Quat::rotationY( radians.getY().getAsFloat() );
	rotations[ rotIndex[ order ][ 2 ] ] = Quat::rotationZ( radians.getZ().getAsFloat() );

	// Generate our quaternion
	Quat result = ( ( rotations[ 2 ] * rotations[ 1 ] ) * rotations[ 0 ] );
	return result;
}

SCE_VECTORMATH_INLINE const Quat Quat::rotation(Vector3_arg unitVec0, Vector3_arg unitVec1)
{
	float cosHalfAngleX2, recipCosHalfAngleX2;
	cosHalfAngleX2 = ::sqrtf( ( 2.0f * ( 1.0f + dot( unitVec0, unitVec1 ).getAsFloat() ) ) );
	recipCosHalfAngleX2 = ( 1.0f / cosHalfAngleX2 );
	return Quat( ( cross( unitVec0, unitVec1 ) * recipCosHalfAngleX2 ), ( cosHalfAngleX2 * 0.5f ) );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotation(floatInVec_arg radians, Vector3_arg unitVec)
{
	return rotation( radians.getAsFloat(), unitVec );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotation(float radians, Vector3_arg unitVec)
{
	float s, c, angle;
	angle = ( radians * 0.5f );
	s = ::sinf( angle );
	c = ::cosf( angle );
	return Quat( ( unitVec * s ), c );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationX(floatInVec_arg radians)
{
	return rotationX( radians.getAsFloat() );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationX(float radians)
{
	float s, c, angle;
	angle = ( radians * 0.5f );
	s = ::sinf( angle );
	c = ::cosf( angle );
	return Quat( s, 0.0f, 0.0f, c );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationY(floatInVec_arg radians)
{
	return rotationY( radians.getAsFloat() );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationY(float radians)
{
	float s, c, angle;
	angle = ( radians * 0.5f );
	s = ::sinf( angle );
	c = ::cosf( angle );
	return Quat( 0.0f, s, 0.0f, c );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationZ(floatInVec_arg radians)
{
	return rotationZ( radians.getAsFloat() );
}

SCE_VECTORMATH_INLINE const Quat Quat::rotationZ(float radians)
{
	float s, c, angle;
	angle = ( radians * 0.5f );
	s = ::sinf( angle );
	c = ::cosf( angle );
	return Quat( 0.0f, 0.0f, s, c );
}

#endif	/* !defined(SCE_VECTORMATH_NO_MATH_DEFS) */

SCE_VECTORMATH_ALWAYS_INLINE const Quat operator *(floatInVec_arg scalar, Quat_arg quat)
{
	return quat * scalar.getAsFloat();
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat operator *(float scalar, Quat_arg quat)
{
	return quat * scalar;
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat conj(Quat_arg quat)
{
	return Quat( -quat.getX().getAsFloat(), -quat.getY().getAsFloat(), -quat.getZ().getAsFloat(), quat.getW().getAsFloat() );
}

SCE_VECTORMATH_INLINE const Vector3 rotate(Quat_arg quat, Vector3_arg vec)
{
	float tmpX, tmpY, tmpZ, tmpW;
	tmpX = ( ( ( quat.getW().getAsFloat() * vec.getX().getAsFloat() ) + ( quat.getY().getAsFloat() * vec.getZ().getAsFloat() ) ) - ( quat.getZ().getAsFloat() * vec.getY().getAsFloat() ) );
	tmpY = ( ( ( quat.getW().getAsFloat() * vec.getY().getAsFloat() ) + ( quat.getZ().getAsFloat() * vec.getX().getAsFloat() ) ) - ( quat.getX().getAsFloat() * vec.getZ().getAsFloat() ) );
	tmpZ = ( ( ( quat.getW().getAsFloat() * vec.getZ().getAsFloat() ) + ( quat.getX().getAsFloat() * vec.getY().getAsFloat() ) ) - ( quat.getY().getAsFloat() * vec.getX().getAsFloat() ) );
	tmpW = ( ( ( quat.getX().getAsFloat() * vec.getX().getAsFloat() ) + ( quat.getY().getAsFloat() * vec.getY().getAsFloat() ) ) + ( quat.getZ().getAsFloat() * vec.getZ().getAsFloat() ) );
	return Vector3(
		( ( ( ( tmpW * quat.getX().getAsFloat() ) + ( tmpX * quat.getW().getAsFloat() ) ) - ( tmpY * quat.getZ().getAsFloat() ) ) + ( tmpZ * quat.getY().getAsFloat() ) ),
		( ( ( ( tmpW * quat.getY().getAsFloat() ) + ( tmpY * quat.getW().getAsFloat() ) ) - ( tmpZ * quat.getX().getAsFloat() ) ) + ( tmpX * quat.getZ().getAsFloat() ) ),
		( ( ( ( tmpW * quat.getZ().getAsFloat() ) + ( tmpZ * quat.getW().getAsFloat() ) ) - ( tmpX * quat.getY().getAsFloat() ) ) + ( tmpY * quat.getX().getAsFloat() ) )
		);
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec dot(Quat_arg quat0, Quat_arg quat1)
{
	float result;
	result  = ( quat0.getX().getAsFloat() * quat1.getX().getAsFloat() );
	result += ( quat0.getY().getAsFloat() * quat1.getY().getAsFloat() );
	result += ( quat0.getZ().getAsFloat() * quat1.getZ().getAsFloat() );
	result += ( quat0.getW().getAsFloat() * quat1.getW().getAsFloat() );
	return floatInVec( result );
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec norm(Quat_arg quat)
{
	float result;
	result  = ( quat.getX().getAsFloat() * quat.getX().getAsFloat() );
	result += ( quat.getY().getAsFloat() * quat.getY().getAsFloat() );
	result += ( quat.getZ().getAsFloat() * quat.getZ().getAsFloat() );
	result += ( quat.getW().getAsFloat() * quat.getW().getAsFloat() );
	return floatInVec( result );
}

SCE_VECTORMATH_ALWAYS_INLINE const floatInVec lengthSqr(Quat_arg quat)
{
	return norm( quat );
}

#if	!defined(SCE_VECTORMATH_NO_MATH_DEFS)

SCE_VECTORMATH_INLINE const floatInVec length(Quat_arg quat)
{
	return floatInVec( ::sqrtf( lengthSqr( quat ).getAsFloat() ) );
}

SCE_VECTORMATH_INLINE const Quat normalize(Quat_arg quat)
{
	float lenInv = 1.0f / length( quat ).getAsFloat();
	return Quat(
		( quat.getX().getAsFloat() * lenInv ),
		( quat.getY().getAsFloat() * lenInv ),
		( quat.getZ().getAsFloat() * lenInv ),
		( quat.getW().getAsFloat() * lenInv )
		);
}

#endif	/* !defined(SCE_VECTORMATH_NO_MATH_DEFS) */

SCE_VECTORMATH_ALWAYS_INLINE const Quat lerp(floatInVec_arg t, Quat_arg quat0, Quat_arg quat1)
{
	return lerp( t.getAsFloat(), quat0, quat1 );
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat lerp(float t, Quat_arg quat0, Quat_arg quat1)
{
	return ( quat0 + ( ( quat1 - quat0 ) * t ) );
}

#if	!defined(SCE_VECTORMATH_NO_MATH_DEFS)

SCE_VECTORMATH_INLINE const Quat slerp(floatInVec_arg t, Quat_arg unitQuat0, Quat_arg unitQuat1, floatInVec_arg tol)
{
	return slerp( t.getAsFloat(), unitQuat0, unitQuat1, tol.getAsFloat() );
}

SCE_VECTORMATH_INLINE const Quat slerp(float t, Quat_arg unitQuat0, Quat_arg unitQuat1, float tol)
{
	Quat start;
	float recipSinAngle, scale0, scale1, cosAngle, angle;
	cosAngle = dot( unitQuat0, unitQuat1 ).getAsFloat();
	if ( cosAngle < 0.0f ) {
		cosAngle = -cosAngle;
		start = ( -unitQuat0 );
	} else {
		start = unitQuat0;
	}
	if ( cosAngle < tol ) {
		angle = ::acosf( cosAngle );
		recipSinAngle = ( 1.0f / ::sinf( angle ) );
		scale0 = ( ::sinf( ( ( 1.0f - t ) * angle ) ) * recipSinAngle );
		scale1 = ( ::sinf( ( t * angle ) ) * recipSinAngle );
	} else {
		scale0 = ( 1.0f - t );
		scale1 = t;
	}
	return ( ( start * scale0 ) + ( unitQuat1 * scale1 ) );
}

SCE_VECTORMATH_INLINE const Quat squad(floatInVec_arg t, Quat_arg unitQuat0, Quat_arg unitQuat1, Quat_arg unitQuat2, Quat_arg unitQuat3)
{
	return squad( t.getAsFloat(), unitQuat0, unitQuat1, unitQuat2, unitQuat3 );
}

SCE_VECTORMATH_INLINE const Quat squad(float t, Quat_arg unitQuat0, Quat_arg unitQuat1, Quat_arg unitQuat2, Quat_arg unitQuat3)
{
	Quat tmp0, tmp1;
	tmp0 = slerp( t, unitQuat0, unitQuat3 );
	tmp1 = slerp( t, unitQuat1, unitQuat2 );
	return slerp( ( ( 2.0f * t ) * ( 1.0f - t ) ), tmp0, tmp1 );
}

#endif	/* !defined(SCE_VECTORMATH_NO_MATH_DEFS) */

SCE_VECTORMATH_ALWAYS_INLINE const Quat select(Quat_arg quat0, Quat_arg quat1, bool select1)
{
	return ( select1 ? quat1 : quat0 );
}

SCE_VECTORMATH_ALWAYS_INLINE const Quat select(Quat_arg quat0, Quat_arg quat1, boolInVec_arg select1)
{
	return ( select1.getAsBool() ? quat1 : quat0 );
}

SCE_VECTORMATH_INLINE void loadXYZW(Quat& quat, const float* fptr)
{
	quat = Quat( fptr[0], fptr[1], fptr[2], fptr[3] );
}

SCE_VECTORMATH_INLINE void storeXYZW(Quat_arg quat, float* fptr)
{
	fptr[0] = quat.getX().getAsFloat();
	fptr[1] = quat.getY().getAsFloat();
	fptr[2] = quat.getZ().getAsFloat();
	fptr[3] = quat.getW().getAsFloat();
}

#ifdef SCE_VECTORMATH_DEBUG

SCE_VECTORMATH_INLINE void print(Quat_arg quat)
{
	sce_vectormath_printf( "( %f %f %f %f )\n", quat.getX().getAsFloat(), quat.getY().getAsFloat(), quat.getZ().getAsFloat(), quat.getW().getAsFloat() );
}

SCE_VECTORMATH_INLINE void print(Quat_arg quat, const char* name)
{
	sce_vectormath_printf( "%s: ( %f %f %f %f )\n", name, quat.getX().getAsFloat(), quat.getY().getAsFloat(), quat.getZ().getAsFloat(), quat.getW().getAsFloat() );
}

#endif

} // namespace Aos
} // namespace Scalar
} // namespace Vectormath
} // namespace Monstars

#endif /* _SCE_VECTORMATH_SCALAR_QUAT_AOS_CPP_H */
