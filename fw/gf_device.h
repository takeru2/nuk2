﻿//-----------------------------------------------------------------------------
// File: gf_device.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#if defined(_WINDOWS)
#include <D3Dcompiler.h>
#include <D3DX11tex.h>
#endif

namespace GF
{

class IDevice {
public:
	virtual ~IDevice(){}
	virtual void Release(void) = 0;
	virtual void Update(void) = 0;

	virtual void Begin(void) = 0;
	virtual void Submit(void) = 0;

	virtual void *GetDevice(void) = 0;
	virtual void *GetContext(void) = 0;

	virtual void GetScreenSize(unsigned short *w, unsigned short *h) = 0;
	virtual void SetScreenSize(unsigned short w, unsigned short h) = 0;
	virtual void SetViewport(unsigned short w, unsigned short h) = 0;
	virtual void Clear(float *clearColor) = 0;

	static void SetInstance(IDevice *inst);
	static IDevice *GetInstance(void);
};


}
