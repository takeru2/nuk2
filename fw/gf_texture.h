﻿//-----------------------------------------------------------------------------
// File: gf_texture.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_resource.h"

namespace GF
{

class ITexture {
public:
	virtual ~ITexture(){}
	virtual void Release(void) = 0;

	virtual unsigned short GetWidth(void) = 0;
	virtual unsigned short GetHeight(void) = 0;
	virtual unsigned short GetDepth(void) = 0;

	virtual ISRV *GetSRV(void) = 0;

	static ITexture *Create(unsigned int format, unsigned short w, unsigned short h, unsigned short depth = 1);
	static ITexture *CreateFromMemory(const void *buffer, unsigned int size);
};

class ITextureResource : public IResource<ITextureResource, ITexture> {virtual ~ITextureResource(){} };


}
