﻿
#include <unordered_map>

#include "gf_system.h"
#include "gf_device.h"
#include "gf_shader.h"
#include "gf_texture.h"
#include "gf_file.h"
#include "gf_pipeline.h"

#include "json.h"

#include "gf_material.h"

namespace GF
{
	class MaterialManager : public IMaterialManager {
	public:
		bool m_bActive = true;
		MapResource<std::string, IMaterial*> map_material;

		std::unordered_map<std::string, int> map_blend {
			{"DISABLE", IPipeline::BLEND_DISABLE},
			{"SA_IA",   IPipeline::BLEND_SA_IA},
			{"ONE",     IPipeline::BLEND_ONE},
			{"ONE_ZERO",IPipeline::BLEND_ONE_ZERO},
			{"ONE_ONE", IPipeline::BLEND_ONE_ONE},
		};
		std::unordered_map<std::string, int> map_depth {
			{"DE_WE", IPipeline::DEPTH_E_WRITE_E},
			{"DE_WD", IPipeline::DEPTH_E_WRITE_D},
			{"DD_WE", IPipeline::DEPTH_D_WRITE_E},
			{"DD_WD", IPipeline::DEPTH_D_WRITE_D},
		};
		std::unordered_map<std::string, int> map_raster {
			{"FS_CN", IPipeline::FILL_S_CULL_N},
			{"FS_CB", IPipeline::FILL_S_CULL_B},
			{"FS_CF", IPipeline::FILL_S_CULL_F},
			{"FW_N",  IPipeline::FILL_W_CULL_N},
		};
		std::unordered_map<std::string, int> map_sampler {
			{"WRAP_WRAP",   IPipeline::ANISO_WRAP_WRAP},
			{"WRAP_CLAMP",  IPipeline::ANISO_WRAP_CLAMP},
			{"CLAMP_WRAP",  IPipeline::ANISO_CLAMP_WRAP},
			{"CLAMP_CLAMP", IPipeline::ANISO_CLAMP_CLAMP},
		};
		std::unordered_map<std::string, int> map_topology {
			{"TRIANGLELIST",   IPipeline::TOPOLOGY_TRIANGLELIST},
			{"TRIANGLESTRIP",  IPipeline::TOPOLOGY_TRIANGLESTRIP},
			{"LINELIST",       IPipeline::TOPOLOGY_LINELIST},
			{"LINESTRIP",      IPipeline::TOPOLOGY_LINESTRIP},
			{"POINTLIST",      IPipeline::TOPOLOGY_POINTLIST},
		};
		std::unordered_map<std::string, int> map_layout {
			{"P3",      IPipeline::LAYOUT_POS},
			{"POS",     IPipeline::LAYOUT_POS},
			{"N3",      IPipeline::LAYOUT_NORMAL},
			{"NORMAL",  IPipeline::LAYOUT_NORMAL},
			{"C8",      IPipeline::LAYOUT_COLOR},
			{"COLOR",   IPipeline::LAYOUT_COLOR},
			{"UV",      IPipeline::LAYOUT_TEXCOORD0},
			{"UV0",     IPipeline::LAYOUT_TEXCOORD0},
			{"UV1",     IPipeline::LAYOUT_TEXCOORD1},
			{"UV2",     IPipeline::LAYOUT_TEXCOORD2},
			{"UV3",     IPipeline::LAYOUT_TEXCOORD3},
			{"T0",      IPipeline::LAYOUT_TEXCOORD0},
			{"T1",      IPipeline::LAYOUT_TEXCOORD1},
			{"T2",      IPipeline::LAYOUT_TEXCOORD2},
			{"T3",      IPipeline::LAYOUT_TEXCOORD3},
			{"TA",      IPipeline::LAYOUT_TANGENT},
			{"BI",      IPipeline::LAYOUT_BINORMAL},
			{"W4",      IPipeline::LAYOUT_WEIGHT4IDX | IPipeline::LAYOUT_WEIGHT4},
			{"IM",      IPipeline::LAYOUT_INST_MATRIX4},
			{"IV",      IPipeline::LAYOUT_INST_VEC},
			{"P3C8",    IPipeline::LAYOUT_P3_C8},
			{"P3C8UV",  IPipeline::LAYOUT_P3_C8_UV},
			{"P3N3C8UV",IPipeline::LAYOUT_P3_N3_C8_UV},
		};
		
	public:
		MaterialManager(){
		}

		virtual ~MaterialManager(){
			
		}

		virtual void Release(void){
			m_bActive = false;
			map_material.Release();
			m_bActive = true;
		}

		IMaterial *GetMaterial(const std::string &name){
			if (map_material.Contain(name)) {
				return map_material.Get(name);
			}
			else {
				return NULL;
			}
		}

		void AddMaterial(const std::string &name, IMaterial *p){
			map_material.Add(name, p);
		}

		void Remove(IMaterial *p){
			if(!m_bActive)return;
			map_material.Remove(p);
		}		

		void Create(const std::string &name,
					GF::IPipeline **pipeline,
					std::vector<GF::ITexture*> &texs){
					
			GF::File matFile;
			std::string cfgFile = name + "/" + name + ".txt";
			matFile.Load(cfgFile.c_str());
			std::string str((const char*)matFile.GetData(),
							(const char*)matFile.GetData() + matFile.GetSize());

			GF::IPipeline::Info info;

			auto jp = json::parse(str);
			info.bs = map_blend[jp.at("BS").get<std::string>()];
			info.ds = map_depth[jp.at("DS").get<std::string>()];
			info.rs = map_raster[jp.at("RS").get<std::string>()];
			try {
				info.constant_size[0] = jp.at("CONSTANT_SIZE").get<int>();
			}catch(...){
				info.constant_size[0] = 0x40;
			}
			try {
				info.constant_size[1] = jp.at("CONSTANT_SIZE_PS").get<int>();
			}
			catch (...) {
				info.constant_size[0] = 0;
			}
			std::vector<std::string> layouts =
				jp.at("LAYOUT").get<std::vector<std::string>>();
			for(std::vector<std::string>::iterator itr = layouts.begin();
				itr != layouts.end();++itr){
				info.layout |= map_layout[*itr];
			}
			info.topology = map_topology[jp.at("TOPOLOGY").get<std::string>()];

			std::string vs = jp.at("VS").get<std::string>();
			std::string ps = jp.at("PS").get<std::string>();

			if(!IShader::CheckShader(vs)){
				std::string fname = name + "/" + vs + ".hlsl";
				GF::IShader::CreateShader(
					GF::IShader::SHADER_VS, 
					vs,
					"VS",
					fname.c_str());
			}
			if(!IShader::CheckShader(ps)){
				std::string fname = name + "/" + ps + ".hlsl";
				GF::IShader::CreateShader(
					GF::IShader::SHADER_PS, 
					ps,
					"PS",
					fname.c_str());
			}
			info.vs = GF::IShader::GetShaderID(vs);
			info.ps = GF::IShader::GetShaderID(ps);

			std::vector<std::string> images =
				jp.at("IMAGE").get<std::vector<std::string>>();
			std::vector<std::string> samplers =
				jp.at("IMAGE_SS").get<std::vector<std::string>>();

			for(std::vector<std::string>::iterator itr = images.begin();
				itr != images.end();++itr){
				GF::File image;
				image.Load(name + "/" + *itr);
				texs.push_back(GF::ITexture::CreateFromMemory(
					image.GetData(), image.GetSize()));
			}

			for(std::vector<std::string>::iterator itr = samplers.begin();
				itr != samplers.end();++itr){
				info.ss[info.ss_num++] = map_sampler[*itr];
			}
			
			*pipeline = GF::IPipeline::GetPipeline(info);
		}

	};
	MaterialManager sMaterialManager;


	class Material : public IMaterial {
	private:
		IPipeline *pipeline;
		std::vector<GF::ITexture*> images;
		ISRV **srv_array = NULL;
		int srv_array_num = 0;
	public:
		Material(){
			pipeline = NULL;
			srv_array = NULL;
			srv_array_num = 0;
		}
		virtual ~Material(){
			SAFE_RELEASE( pipeline );
			SAFE_DELETE_ARRAY(srv_array);
		}

		virtual void Release(void){
			sMaterialManager.Remove( this );
			delete this;
		}
		void createSRV(void){
			if( (srv_array == NULL) && !images.empty()){
				srv_array_num = (int)images.size();
				srv_array = new ISRV*[srv_array_num];
				for(int i=0;i<srv_array_num;i++){
					srv_array[i] = images[i]->GetSRV();
				}
			}
		}
		
		void Create(const std::string &name) {
			sMaterialManager.Create(name, &pipeline, images);
			createSRV();
		}

		virtual void UpdateSRV(int srv_num, const ISRV **srvs){
			if( (srv_array != NULL) || (srv_array_num != srv_num) ){
				if(srv_array != NULL)delete [] srv_array;
				srv_array_num = srv_num;
				srv_array = new ISRV*[srv_array_num];
			}
			for(int i=0;i<srv_array_num;i++){
				srv_array[i] = (ISRV*)srvs[i];
			}
		}
		
		virtual void Set(const void *constantBuffer, const void *constantBufferPS){
			pipeline->SetShaderResource(srv_array, srv_array_num);
			pipeline->SetConstantBuffer(constantBuffer, 0);
			if(constantBufferPS != NULL)pipeline->SetConstantBuffer(constantBufferPS, 0, 1);
			pipeline->Set();
		}

		virtual IPipeline *GetPipeline(void){
			return pipeline;
		}
	};
		
	IMaterial *IMaterial::GetMaterial(const std::string &name){
		IMaterial *mat = sMaterialManager.GetMaterial(name);
		if(mat == NULL){
			Material *material = new Material();
			sMaterialManager.AddMaterial(name, material);
			material->Create(name);
			mat = material;
		}
		return mat;
	}

}


