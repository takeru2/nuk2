﻿//-----------------------------------------------------------------------------
// File: gf_pipeline.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_singleton.h"

namespace GF
{

	class IPipeline {
	public:
		enum {
			BLEND_DISABLE,
			BLEND_SA_IA,
			BLEND_ONE,
			BLEND_ONE_ZERO,
			BLEND_ONE_ONE,
			BLEND_SA_IDA,
			BLEND_SA_SC,
		};
		enum {
			DEPTH_E_WRITE_E,
			DEPTH_E_WRITE_D,
			DEPTH_D_WRITE_E,
			DEPTH_D_WRITE_D,
		};
		enum {
			FILL_S_CULL_N,
			FILL_S_CULL_B,
			FILL_S_CULL_F,
			FILL_W_CULL_N,
		};
		enum {
			ANISO_WRAP_WRAP,
			ANISO_WRAP_CLAMP,
			ANISO_CLAMP_WRAP,
			ANISO_CLAMP_CLAMP,
		};
		enum {
			TOPOLOGY_TRIANGLELIST,
			TOPOLOGY_TRIANGLESTRIP,
			TOPOLOGY_LINELIST,
			TOPOLOGY_LINESTRIP,
			TOPOLOGY_POINTLIST,
		};

		enum { 
			LAYOUT_POS       = (1 << 0),

			LAYOUT_NORMAL    = (1 << 1),
			LAYOUT_hNORMAL   = (1 << 2),

			LAYOUT_COLOR     = (1 << 3),

			LAYOUT_TEXCOORD0 = (1 << 4),
			LAYOUT_TEXCOORD1 = (1 << 5),
			LAYOUT_TEXCOORD2 = (1 << 6),
			LAYOUT_TEXCOORD3 = (1 << 7),

			LAYOUT_hTEXCOORD0 = (1 << 8),
			LAYOUT_hTEXCOORD1 = (1 << 9),
			LAYOUT_hTEXCOORD2 = (1 << 10),
			LAYOUT_hTEXCOORD3 = (1 << 11),

			LAYOUT_TANGENT   = (1 << 12),
			LAYOUT_hTANGENT  = (1 << 13),

			LAYOUT_BINORMAL  = (1 << 14),
			LAYOUT_hBINORMAL = (1 << 15),

			LAYOUT_WEIGHT4IDX = (1 << 16),
			LAYOUT_WEIGHT4    = (1 << 17),
			LAYOUT_hWEIGHT4   = (1 << 18),

			LAYOUT_INST_MATRIX4 = (1 << 19),
			LAYOUT_INST_VEC     = (1 << 20),
		};
		enum {
			LAYOUT_P3_C8       = (LAYOUT_POS | LAYOUT_COLOR),
			LAYOUT_P3_C8_UV    = (LAYOUT_POS | LAYOUT_COLOR | LAYOUT_TEXCOORD0),
			LAYOUT_P3_N3_C8_UV = (LAYOUT_POS | LAYOUT_NORMAL | LAYOUT_COLOR | LAYOUT_TEXCOORD0),
		};

		enum {
			CONSTANT_M16,
		};

		struct Info {
			//kay area
			unsigned char vs = 0;  //sh vs
			unsigned char ps = 0;  //sh ps
			unsigned char cs = 0;  //sh cs
			unsigned char bs  = 0; //blend

			unsigned char rs  = 0; //raster
			unsigned char ds  = 0; //depth
			unsigned char topology = 0;
			unsigned char padding = 0;
			//key area
			unsigned short constant_size[2] = { 64, 0 };
			unsigned char ss[5] = {0}; //sampler lists
			unsigned char ss_num = 0;
			unsigned int  layout = 0; //layout
		};
	public:
		virtual ~IPipeline(){}
		virtual void Release(void) = 0;

		virtual void SetConstantBuffer(const void *ptr, unsigned int size, int type = 0) = 0;
		virtual void SetShaderResource(ISRV **ptr, unsigned int num) = 0;

		virtual void Set(void) = 0;
		virtual void Dispatch(ISRV **srvs, int srvNum, IUAV **uavs, int uavNum,
							  int x, int y, int z) = 0;

		virtual void Reload(void) = 0;
		
		static IPipeline *GetPipeline(const Info &info);
	};


	class IPipelineManager : public Singleton<IPipelineManager> {
	public:
		virtual ~IPipelineManager(){}
		virtual void Release(void){}

		virtual void Reload(void){}
		
	};

}
