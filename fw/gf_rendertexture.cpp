﻿
#include <list>

#include "gf_system.h"
#include "gf_device.h"

#include "gf_rendertexture.h"

#if defined(_WINDOWS)
#include <D3DX11tex.h>
#endif

namespace GF
{
	class RenderTexture : public IRenderTexture {
	public:
		ID3D11Texture2D *m_pTexture      = NULL;
		ID3D11RenderTargetView *m_pRTV   = NULL;
		ID3D11ShaderResourceView *m_pSRV = NULL;

		ID3D11Texture2D *m_pDepthTexture = NULL;
		ID3D11DepthStencilView *m_pDSV   = NULL;
		ID3D11ShaderResourceView *m_pDepthSRV = NULL;

		unsigned short mWidth  = 0;
		unsigned short mHeight = 0;
		unsigned short mDepth  = 0;
		ID3D11DeviceContext *context = NULL;

		D3D11_VIEWPORT tmp_Viewport;
		ID3D11RenderTargetView *tmp_RTV[4];
		ID3D11DepthStencilView *tmp_DSV;
	public:
		RenderTexture(){
			context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

			m_pTexture = NULL;
			m_pSRV = NULL;
			m_pRTV = NULL;

			m_pDepthTexture = NULL;
			m_pDSV = NULL;
			m_pDepthSRV = NULL;

			IRenderTextureResource::GetInstance()->Add( this );
		}

		virtual ~RenderTexture(){
		}

		virtual void Release(void){
			IRenderTextureResource::GetInstance()->Remove( this );

			SAFE_RELEASE(m_pTexture);
			SAFE_RELEASE(m_pSRV);
			SAFE_RELEASE(m_pRTV);

			SAFE_RELEASE(m_pDepthTexture);
			SAFE_RELEASE(m_pDSV);
			SAFE_RELEASE(m_pDepthSRV);

			delete this;
		}

		virtual unsigned short GetWidth(void){return mWidth;}
		virtual unsigned short GetHeight(void){return mHeight;}
		virtual unsigned short GetDepth(void){return mDepth;}
		virtual ISRV *GetSRV(void){return (ISRV*)m_pSRV;}
		virtual ISRV *GetDepthSRV(void){return (ISRV*)m_pDepthSRV;}

		virtual IRTV *GetRTV(void){return (IRTV*)m_pRTV;}

		void BeginVP(void){
			unsigned int vpNum[] = { 1 };
			context->RSGetViewports(vpNum, &tmp_Viewport);
			context->OMGetRenderTargets(1,
				(ID3D11RenderTargetView **)&tmp_RTV, 
				(ID3D11DepthStencilView **)&tmp_DSV);
		}

		void EndVP(void){
			context->RSSetViewports(1, &tmp_Viewport);
			context->OMSetRenderTargets(1, tmp_RTV, tmp_DSV);
		}

		virtual void Begin(void){
			BeginVP();
			
			ID3D11RenderTargetView* rtv = m_pRTV;
			ID3D11DepthStencilView* dsv = m_pDSV;
			context->OMSetRenderTargets(1, &rtv, dsv);

			D3D11_VIEWPORT vp;
			vp.Width = (float)mWidth;
			vp.Height = (float)mHeight;
			vp.MinDepth = 0;
			vp.MaxDepth = 1;
			vp.TopLeftX = 0;
			vp.TopLeftY = 0;
			context->RSSetViewports(1, &vp);
		}
		virtual void End(void){
			EndVP();
		}

		virtual void Clear(const float *clearColor, float depth){
			static const float zero_color[4] = {0,0,0,0};
			if(clearColor == NULL)clearColor = zero_color;

			context->ClearRenderTargetView(m_pRTV, clearColor);
			if(m_pDSV != NULL){
				context->ClearDepthStencilView(m_pDSV,
											   D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
											   depth, 0);
			}
		}

	};


	IRenderTexture *IRenderTexture::Create(unsigned int format, unsigned short w, unsigned short h){
		ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

		if(format == 0){
			format = (unsigned int)DXGI_FORMAT_B8G8R8X8_UNORM;
		}
		
		RenderTexture *pTex = new RenderTexture();
		pTex->mWidth = w;
		pTex->mHeight = h;
		pTex->mDepth  = 1;

		D3D11_TEXTURE2D_DESC desc;
		memset(&desc, 0, sizeof(desc));
		desc.Width  = w;
		desc.Height = h;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		desc.Format = (DXGI_FORMAT)format;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.MiscFlags = 0;
	
		GF_DX_RESULT(device->CreateTexture2D(
						 &desc, NULL, &pTex->m_pTexture),
					 "CreateTexture2D");


		D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
		memset(&rtv_desc, 0, sizeof(rtv_desc));
		rtv_desc.Format = (DXGI_FORMAT)format;
		rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		GF_DX_RESULT(device->CreateRenderTargetView(pTex->m_pTexture, &rtv_desc,
													&pTex->m_pRTV),"CreateRenderTargetView");
					 
		D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
		memset(&srv_desc, 0, sizeof(srv_desc));
		srv_desc.Format = desc.Format;
		srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv_desc.Texture1D.MostDetailedMip = 0;
		srv_desc.Texture1D.MipLevels = 1;
		GF_DX_RESULT(device->CreateShaderResourceView(
						 pTex->m_pTexture, &srv_desc, &pTex->m_pSRV),
					 "CreateShaderResourceView");

		//DEPTH
		desc.Format = DXGI_FORMAT_R32_TYPELESS;// DXGI_FORMAT_R24G8_TYPELESS;
		desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	
		GF_DX_RESULT(device->CreateTexture2D(
						 &desc, NULL, &pTex->m_pDepthTexture),
					 "CreateTexture2D");

		D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
		memset(&dsv_desc, 0, sizeof(dsv_desc));
		dsv_desc.Format = DXGI_FORMAT_D32_FLOAT;// DXGI_FORMAT_D24_UNORM_S8_UINT;
		dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		GF_DX_RESULT(device->CreateDepthStencilView(pTex->m_pDepthTexture,
						&dsv_desc,
						&pTex->m_pDSV),
					"CreateDepthStencilView");

		srv_desc.Format = DXGI_FORMAT_R32_FLOAT;
		srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		GF_DX_RESULT(device->CreateShaderResourceView(
						 pTex->m_pDepthTexture, &srv_desc, &pTex->m_pDepthSRV),
					 "CreateShaderResourceView");

		return pTex;
	}

}



