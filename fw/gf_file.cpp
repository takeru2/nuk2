﻿
#include <windows.h>

#include "gf_file.h"

namespace GF
{

	File::File(){
		mData.first  = NULL;
		mData.second = 0;
	}


	File::~File(){
		if(mData.first != NULL){
			delete [] mData.first;
			mData.first = NULL;
		}
	}

	std::pair<char*,unsigned int> File::Copy(void){
		char *data = mData.first;
		unsigned int size = mData.second;

		mData.first = NULL;
		mData.second = 0;

		return std::pair<char*,unsigned int>(data, size);
	}

	std::pair<char*,unsigned int> &File::Load(const std::string &name){

		mName = name;

#if defined(_WINDOWS)
		HANDLE hFile;
		DWORD read;
		DWORD file_size;
		std::string winName = name;
		size_t si;
		while((si = winName.find("/")) != std::string::npos)winName.replace(si, 1, "\\");

		hFile = CreateFile(winName.c_str(),
						   GENERIC_READ, FILE_SHARE_READ, NULL,
						   OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);

		mData.first  = NULL;
		mData.second = 0;

	

		if(hFile != INVALID_HANDLE_VALUE){
			file_size = GetFileSize(hFile, NULL);

			unsigned int size = (unsigned int)file_size;
			char *pData = new char[size];

			ReadFile(hFile, (LPVOID)pData, file_size, &read, NULL);
			CloseHandle(hFile);
			if(size != read){
				delete [] pData;
			}else{
				mData.first  = pData;
				mData.second = size;
			}
		}
#endif
		return mData;
	}
}



