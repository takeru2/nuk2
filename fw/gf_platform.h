﻿//-----------------------------------------------------------------------------
// File: gf_platform.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

namespace GF
{

class IPlatform {
public:
	virtual ~IPlatform(){}
	virtual void Release(void) = 0;
	
	virtual void Update(void) = 0;
	

	static bool CreateInstance(void);
	static IPlatform *GetInstance(void);
};


}
