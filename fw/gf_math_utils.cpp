﻿
#include "gf_math_utils.h"

namespace GF
{


    static const RandContext scRandCtx = {123456789,362436069,521288629,88675123};
	static RandContext sRandCtx = scRandCtx;


	static inline unsigned int XorShift(RandContext &ctx){
		unsigned int &x = ctx.x;
		unsigned int &y = ctx.y;
		unsigned int &z = ctx.z;
		unsigned int &w = ctx.w;
		unsigned int t;
		t=(x^(x<<11));x=y;y=z;z=w;
		return(w=(w^(w>>19))^(t^(t>>8)));
	}

	float Rand(RandContext *pCtx){
		if(pCtx == NULL){
			return static_cast<float>(XorShift(sRandCtx)) / (0xffffffff - 1);
		}else{
			return static_cast<float>(XorShift(*pCtx)) / (0xffffffff - 1);
		}
	}

	unsigned int Rand(RandContext *pCtx, unsigned int range){
		if(range == 0)return 0;

		if(pCtx == NULL){
			return XorShift(sRandCtx) % range;
		}else{
			return XorShift(*pCtx) % range;
		}
	}

	void RandSeed(RandContext *pCtx, unsigned int seed){
		if(pCtx == NULL)pCtx = &sRandCtx;
		if(seed == 0){
			*pCtx = scRandCtx;
		}else{
			unsigned int s = seed;
			for(int i=0;i<4;i++)reinterpret_cast<unsigned int*>(pCtx)[i] =
									s = 1812433253 * (s^(s >> 30)) + i;
		}
	}


}
