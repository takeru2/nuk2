﻿
#include "gf.h"

#include "json.h"

#include "gf_editvalue.h"

namespace GF
{
	class EditElem {
	public:
		struct Elem {
			enum EDIT {EDIT_NONE, EDIT_INT, EDIT_UINT, EDIT_FLOAT, EDIT_BOOL, EDIT_COLOR, EDIT_STRING};
			EDIT editType;
			void *ptr;
			std::string name;
			union {
				int   value_int;
				float value_float;
				bool  value_bool;
			}prev;
		};
		void *handle;
		std::list<Elem> elems;
		std::string name;
		unsigned int id;
		bool selected = false;
	};

	EditHandle::EditHandle(){
		handle = IEditValue::GetInstance()->Create();
	}

	EditHandle::~EditHandle(){
		IEditValue::GetInstance()->Unregist(handle);
	}

	void EditHandle::SetName(const std::string &name){
		EditElem *ee = (EditElem*)handle;
		ee->name = name;
	}
	void EditHandle::SetValue(const std::string &name, int &value){
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_INT;
		ee->elems.push_back(elem);
	}
	void EditHandle::SetValue(const std::string &name, unsigned int &value) {
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_UINT;
		ee->elems.push_back(elem);
	}
	void EditHandle::SetValue(const std::string &name, bool &value) {
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_BOOL;
		ee->elems.push_back(elem);
	}
	void EditHandle::SetValue(const std::string &name, PColor &value) {
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_COLOR;
		ee->elems.push_back(elem);
	}
	void EditHandle::SetValue(const std::string &name, float &value){
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_FLOAT;
		ee->elems.push_back(elem);
	}
	void EditHandle::SetValue(const std::string &name, std::string &value){
		EditElem *ee = (EditElem*)handle;
		EditElem::Elem elem;
		elem.name = name;
		elem.ptr = (void*)&value;
		elem.editType = EditElem::Elem::EDIT_STRING;
		ee->elems.push_back(elem);
	}

	EditRegister::EditRegister(EditHandle &handle, const std::string &name){
		p_handle = &handle;
		handle.SetName( name );
	}

	EditRegister::~EditRegister(){
		IEditValue::GetInstance()->Update( p_handle->handle );
	}

	class EditValue : public IEditValue {
	private:
		std::list<EDIT_HANDLE> editList;
		std::string set_command;
		static int uid;
	public:
		EditValue(){
		}
		virtual ~EditValue() {
		}
		virtual void Release(void) {
			delete this;
		}
		virtual EDIT_HANDLE Create(void){
			EditElem *ee = new EditElem();
			ee->id = ++uid;
			editList.push_back(ee);
			return (EDIT_HANDLE)ee;
		}
		virtual void Unregist(EDIT_HANDLE h){
			std::list<EDIT_HANDLE>::iterator itr = find(
				editList.begin(), editList.end(), h);

			if(itr != editList.end()){
				EditElem *ee = (EditElem*)*itr;

				UnListCommand(*itr);

				delete ee;
				editList.erase(itr);
			}
		}

		void jsonToString(std::string &value) {
			std::replace(value.begin(), value.end(), '{', '<');
			std::replace(value.begin(), value.end(), '}', '>');
			std::replace(value.begin(), value.end(), '[', '(');
			std::replace(value.begin(), value.end(), ']', ')');
		}

		void stringToJson(std::string &value) {
			std::replace(value.begin(), value.end(), '<', '{');
			std::replace(value.begin(), value.end(), '>', '}');
			std::replace(value.begin(), value.end(), '(', '[');
			std::replace(value.begin(), value.end(), ')', ']');
		}

		void ListCommand(EDIT_HANDLE h){
			EditElem *ee = (EditElem*)h;

			std::string cmds;
			{
				json e;
				e["name"] = ee->name;
				e["id"] = ee->id;
				std::string rep = e.dump();
				jsonToString(rep);

				json j;
				j[EDIT_CMD_TYPE] = "list";
				j[EDIT_CMD_VALUE] = rep;
				cmds = j.dump();
			}

			if (!set_command.empty())set_command += "\n";
			set_command += cmds;
		}

		void UnListCommand(EDIT_HANDLE h) {
			EditElem *ee = (EditElem*)h;

			std::string cmds;
			{
				json e;
				e["name"] = ee->name;
				e["id"] = ee->id;
				std::string rep = e.dump();
				jsonToString(rep);

				json j;
				j[EDIT_CMD_TYPE] = "unlist";
				j[EDIT_CMD_VALUE] = rep;
				cmds = j.dump();
			}

			if (!set_command.empty())set_command += "\n";
			set_command += cmds;
		}

		bool ParamCommand(EDIT_HANDLE h, bool updated){
			EditElem *ee = (EditElem*)h;
			json elist;
			char tmp[0x100];
			std::string cmds;
			bool updateParam = false;

			for(std::list<EditElem::Elem>::iterator itr = ee->elems.begin();
				itr != ee->elems.end();++itr){
				EditElem::Elem &elem = *itr;

				bool res = updateEditElem(elem);
				updateParam |= res;
	
				json e;
				switch(elem.editType){
				case EditElem::Elem::EDIT_UINT:
				case EditElem::Elem::EDIT_INT:
					if(updated | (res && !updated)){
						sprintf(tmp, "%d", *(int*)elem.ptr);
						e["name"] = elem.name;
						e["id"] = 1;
						e["type"] = "int";
						e["value"] = tmp;
						elist["list"] += e;
					}
					break;
				case EditElem::Elem::EDIT_BOOL:
					if(updated | (res && !updated)){
						sprintf(tmp, "%s", (*(bool*)elem.ptr)?"true":"false");
						e["name"] = elem.name;
						e["id"] = 1;
						e["type"] = "bool";
						e["value"] = tmp;
						elist["list"] += e;
					}
					break;
				case EditElem::Elem::EDIT_COLOR:
					if(updated | (res && !updated)){
						sprintf(tmp, "%d", *(int*)elem.ptr);
						e["name"] = elem.name;
						e["id"] = 1;
						e["type"] = "color";
						e["value"] = tmp;
						elist["list"] += e;
					}
					break;
				case EditElem::Elem::EDIT_FLOAT:
					if(updated | (res && !updated)){
						sprintf(tmp, "%f", *(float*)elem.ptr);
						e["name"] = elem.name;
						e["id"] = 1;
						e["type"] = "float";
						e["value"] = tmp;
						elist["list"] += e;
					}
					break;
				case EditElem::Elem::EDIT_STRING:
					if(updated | (res && !updated)){
						e["name"] = elem.name;
						e["id"] = 1;
						e["type"] = "string";
						e["value"] = *(std::string*)elem.ptr;
						elist["list"] += e;
					}
					break;
				}
			}

			if(!updateParam && !updated){
				return false;
			}
			
			std::string rep = elist.dump();
			jsonToString(rep);
			{
				json j;
				j["type"] = "edit";
				j["value"] = rep;
				rep = j.dump();
				cmds += j.dump();
			}

			if (!set_command.empty())set_command += "\n";
			set_command += cmds;
			return true;
		}
		
		virtual void Update(EDIT_HANDLE h){
			ListCommand( h );
		}

		void GetCommand(std::string &command){
			if (!set_command.empty()) {
				command = set_command;
				set_command.clear();
			}
		}

		bool updateEditElem(EditElem::Elem &elem){
			bool result = false;
			switch (elem.editType) {
			case EditElem::Elem::EDIT_FLOAT:
			    {
					float value = *(float*)elem.ptr;
					if(elem.prev.value_float != value){
						elem.prev.value_float = value;
						result = true;
					}
				}
				break;
			case EditElem::Elem::EDIT_INT:
			case EditElem::Elem::EDIT_UINT:
			case EditElem::Elem::EDIT_COLOR:
			    {
					int value = *(int*)elem.ptr;
					if(elem.prev.value_int != value){
						elem.prev.value_int = value;
						result = true;
					}
				}
				break;
			case EditElem::Elem::EDIT_BOOL:
			    {
					bool value = *(bool*)elem.ptr;
					if(elem.prev.value_bool != value){
						elem.prev.value_bool = value;
						result = true;
					}
				}
				break;
			}
			return result;
		}

		void EditedCommand(std::string &command){
			stringToJson(command);
			auto jp = json::parse(command);
			std::string &name  = jp.at("type").get<std::string>();
			std::string &value = jp.at("value").get<std::string>();
			for(std::list<EDIT_HANDLE>::iterator itr_b = editList.begin();
				itr_b != editList.end();++itr_b){
				EditElem *ee = (EditElem*)(*itr_b);
				if (ee->selected) {
					for (std::list<EditElem::Elem>::iterator itr = ee->elems.begin();
						itr != ee->elems.end(); ++itr) {
						EditElem::Elem &elem = *itr;
						if (elem.name == name) {
							switch (elem.editType) {
							case EditElem::Elem::EDIT_FLOAT:
								*(float*)elem.ptr = (float)atof(value.c_str());
								updateEditElem(elem);
								break;
							case EditElem::Elem::EDIT_INT:
							case EditElem::Elem::EDIT_UINT:
								*(int*)elem.ptr = (int)atoi(value.c_str());
								updateEditElem(elem);
								break;
							case EditElem::Elem::EDIT_COLOR:
								{
									PColor rgba;
									*(int*)elem.ptr = (int)rgba.setARGB(atoi(value.c_str()));
									updateEditElem(elem);
								}
								break;
							case EditElem::Elem::EDIT_BOOL:
								*(bool*)elem.ptr = (value.c_str()[0] == 'T') ? true : false;
								updateEditElem(elem);
								break;
							case EditElem::Elem::EDIT_STRING:
								*(std::string*)elem.ptr = value;
								break;
							}
							return;
						}
					}
				}
			}
		}

		void UpdateSelectedCommand(void) {
			try {
				for (std::list<EDIT_HANDLE>::iterator itr = editList.begin();
					itr != editList.end(); ++itr) {
					EditElem *ee = (EditElem*)*itr;
					if (ee->selected) {
						ParamCommand(*itr, false);
						return;
					}
				}
			}
			catch (Exception e) {
				OutputDebugString("SelectedCommand Error\n");
			}
		}

		void SelectedCommand(std::string &command) {
			stringToJson(command);
			try {
				auto jp = json::parse(command);
				int id = jp.at("id").get<int>();
				for (std::list<EDIT_HANDLE>::iterator itr = editList.begin();
					itr != editList.end();++itr){
					EditElem *ee = (EditElem*)*itr;
					if (ee->id == id) {
						if (!ee->selected) {
							ee->selected = true;
							ParamCommand(*itr, true);
						}
					}
					else {
						ee->selected = false;
					}
				}
			}
			catch (Exception e) {
				OutputDebugString("SelectedCommand Error\n");
			}
		}

		void CustomCommand(std::string &sendCommand, const std::string &tag, const std::string &command) {
			json j;
			std::string sc = command;
			jsonToString(sc);
			j[GF::EDIT_CMD_TYPE] = tag;
			j[GF::EDIT_CMD_VALUE] = sc;
			sendCommand = j.dump();
		}


	};
	int EditValue::uid = 0;

	IEditValue *IEditValue::CreateInstance(void){
		return new EditValue();
	}
}

