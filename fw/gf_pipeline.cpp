﻿
#include <unordered_map>

#include "gf_system.h"
#include "gf_device.h"
#include "gf_shader.h"

#include "gf_pipeline.h"

namespace GF
{
	class PipelineManager : public IPipelineManager {
	public:
#if defined(_WINDOWS)
		IDMapResource<ID3D11BlendState*> blendState;
		IDMapResource<ID3D11DepthStencilState*> depthState;
		IDMapResource<ID3D11RasterizerState*> rasterState;
		IDMapResource<ID3D11SamplerState*> samplerState;
		IDMapResource<ID3D11InputLayout*> layouts;
		IDMapResource<ID3D11Buffer*> constants;
#endif
		bool m_bActive = true;
		std::unordered_map<unsigned long long, IPipeline*> map_pipeline;

	public:
		PipelineManager(){
		}

		virtual ~PipelineManager(){
			
		}

		virtual void Release(void){

#if defined(_WINDOWS)
			blendState.Release();
			depthState.Release();
			rasterState.Release();
			samplerState.Release();
			layouts.Release();
			constants.Release();
#endif

			m_bActive = false;
			for(std::unordered_map<unsigned long long, IPipeline*>::iterator itr = 
					map_pipeline.begin();itr != map_pipeline.end();++itr){
				SAFE_RELEASE( itr->second );
			}
			map_pipeline.clear();
			m_bActive = true;
		}

		void createLayout(unsigned int id, void **handle, void *code, unsigned int *stride){
#if defined(_WINDOWS)
			ID3D11InputLayout *pLayout = NULL;
			if(layouts.Get(id, pLayout)){
				*handle = (void*)pLayout;
				return;
			}

			const D3D11_INPUT_ELEMENT_DESC s_layout[] = {
				{ "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  12, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hNORMAL",   0, DXGI_FORMAT_R16G16B16A16_FLOAT, 0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "COLOR",     0, DXGI_FORMAT_B8G8R8A8_UNORM,     0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "hTEXCOORD",  0, DXGI_FORMAT_R16G16_FLOAT,       0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hTEXCOORD",  0, DXGI_FORMAT_R16G16_FLOAT,       0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hTEXCOORD",  0, DXGI_FORMAT_R16G16_FLOAT,       0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hTEXCOORD",  0, DXGI_FORMAT_R16G16_FLOAT,       0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "TANGENT",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hTANGENT",   0, DXGI_FORMAT_R16G16B16A16_FLOAT,    0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "BINORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hBINORMAL",  0, DXGI_FORMAT_R16G16B16A16_FLOAT,    0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				{ "BLENDINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT,   0,  4, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BLENDWEIGHT",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,  16, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "hBLENDWEIGHT",  0, DXGI_FORMAT_R16G16B16A16_FLOAT, 0,  8, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			const D3D11_INPUT_ELEMENT_DESC s_layout_inst[] = {
				{ "MATRIX",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "MATRIX",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			};
			D3D11_INPUT_ELEMENT_DESC send_layout[0x10];
			int send_layout_index  = 0;
			int send_layout_offset = 0;
			for(int i=0;i<(int)(sizeof(s_layout)/sizeof(s_layout[0]));i++){
				if( (1<<i) & id){
					send_layout[send_layout_index] = s_layout[i];
					send_layout[send_layout_index].AlignedByteOffset = send_layout_offset;
					send_layout_offset += s_layout[i].AlignedByteOffset;
					send_layout_index++;
				}
			}
			*stride = send_layout_offset;
			
			send_layout_offset = 0;
			if(id & IPipeline::LAYOUT_INST_MATRIX4){
				for(int i=0;i<4;i++){
					send_layout[send_layout_index] = s_layout_inst[0];
					send_layout[send_layout_index].SemanticIndex = i;
					send_layout[send_layout_index].AlignedByteOffset = send_layout_offset;
					send_layout_offset += s_layout_inst[0].AlignedByteOffset;
					send_layout_index++;
				}
			}
			if(id & IPipeline::LAYOUT_INST_VEC){
				send_layout[send_layout_index] = s_layout_inst[1];
				send_layout[send_layout_index].AlignedByteOffset = send_layout_offset;
				send_layout_offset += s_layout_inst[1].AlignedByteOffset;
				send_layout_index++;
			}

			ID3D10Blob *pCode = (ID3D10Blob*)code;
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			GF_DX_RESULT(device->CreateInputLayout(
							 send_layout, send_layout_index,
							 pCode->GetBufferPointer(),
							 pCode->GetBufferSize(), &pLayout), "CreateInputLayout");

			layouts.Add(id, pLayout);
			*handle = (void*)pLayout;
#endif
		}

		void createConstantBuffer(unsigned int id, void **handle, unsigned int size){
#if defined(_WINDOWS)
			ID3D11Buffer *pcb = NULL;
			if(constants.Get(id, pcb)){
				*handle = (void*)pcb;
				return;
			}

			D3D11_BUFFER_DESC Desc;
			Desc.Usage          = D3D11_USAGE_DYNAMIC;
			Desc.BindFlags      = D3D11_BIND_CONSTANT_BUFFER;
			Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			Desc.MiscFlags      = 0;
			Desc.ByteWidth      = size;

			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
			GF_DX_RESULT(device->CreateBuffer( &Desc, NULL, &pcb ), "CreateBuffer");

			constants.Add(id, pcb );
			*handle = (void*)pcb;
#endif			
		}

		void createBlend(unsigned int id, void **handle){

#if defined(_WINDOWS)
			ID3D11BlendState *bs = NULL;
			if(blendState.Get(id, bs)){
				*handle = (void*)bs;
				return;
			}

			struct BlendParam {
				bool    blend_enable;
				int     src_blend;
				int     dest_blend;
				int     blend_op;
				int     src_blend_alpha;
				int     dest_blend_alpha;
				int     blend_op_alpha;
				int     render_target_write_mask;
			}blend_table[] = {
				{ false, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_COLOR_WRITE_ENABLE_ALL },
				{ true,  D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_INV_SRC_ALPHA, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_COLOR_WRITE_ENABLE_ALL },
				{ true,  D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_ONE, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_COLOR_WRITE_ENABLE_ALL },
				{ true,  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD, 
				  D3D11_COLOR_WRITE_ENABLE_ALL },
				{ true,  D3D11_BLEND_ONE, D3D11_BLEND_ONE, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD, 
				  D3D11_COLOR_WRITE_ENABLE_ALL },
				{ true,  D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_INV_DEST_ALPHA, D3D11_BLEND_OP_ADD,
				  D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD,
				  D3D11_COLOR_WRITE_ENABLE_ALL },
			};
			{
				D3D11_BLEND_DESC desc;
				desc.AlphaToCoverageEnable = false;
				desc.IndependentBlendEnable = false;
				for (int n=0; n<8; ++n) {
					desc.RenderTarget[n].BlendEnable            = blend_table[id].blend_enable;
					desc.RenderTarget[n].SrcBlend               = (D3D11_BLEND)blend_table[id].src_blend;
					desc.RenderTarget[n].DestBlend              = (D3D11_BLEND)blend_table[id].dest_blend;
					desc.RenderTarget[n].BlendOp                = (D3D11_BLEND_OP)blend_table[id].blend_op;
					desc.RenderTarget[n].SrcBlendAlpha          = (D3D11_BLEND)blend_table[id].src_blend_alpha;
					desc.RenderTarget[n].DestBlendAlpha         = (D3D11_BLEND)blend_table[id].dest_blend_alpha;
					desc.RenderTarget[n].BlendOpAlpha           = (D3D11_BLEND_OP)blend_table[id].blend_op_alpha;
					desc.RenderTarget[n].RenderTargetWriteMask  = blend_table[id].render_target_write_mask;
				}

				ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
				GF_DX_RESULT( device->CreateBlendState(&desc, &bs), "CreateBlendState" );

				blendState.Add(id, bs);
				*handle = (void*)bs;
			}
#endif
		}

		void createDepth(unsigned int id, void **handle){

#if defined(_WINDOWS)
			ID3D11DepthStencilState *ds = NULL;
			if(depthState.Get(id, ds)){
				*handle = (void*)ds;
				return;
			}

			struct DepthParam {
				bool                    depth_enable;
				D3D11_DEPTH_WRITE_MASK  write_mask;
				D3D11_COMPARISON_FUNC   comp_func;
				bool                    stencil_enable;
			} depth_table[] = {
				{ true,  D3D11_DEPTH_WRITE_MASK_ALL,  D3D11_COMPARISON_LESS, false },
				{ true,  D3D11_DEPTH_WRITE_MASK_ZERO, D3D11_COMPARISON_LESS, false },
				{ false, D3D11_DEPTH_WRITE_MASK_ALL,  D3D11_COMPARISON_LESS, false },
				{ false, D3D11_DEPTH_WRITE_MASK_ZERO, D3D11_COMPARISON_LESS, false },
			};

			{
				D3D11_DEPTH_STENCIL_DESC desc;
				desc.DepthEnable    = depth_table[id].depth_enable;
				desc.DepthWriteMask = depth_table[id].write_mask;
				desc.DepthFunc      = depth_table[id].comp_func;
				desc.StencilEnable  = depth_table[id].stencil_enable;
				
				ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
				GF_DX_RESULT( device->CreateDepthStencilState(&desc, &ds), "CreateDepthStencilState" );

				depthState.Add(id, ds);
				*handle = (void*)ds;
			}
#endif
		}
		void createRasterizer(unsigned int id, void **handle){

#if defined(_WINDOWS)
			ID3D11RasterizerState *rs = NULL;
			if(rasterState.Get(id, rs)){
				*handle = (void*)rs;
				return;
			}

			struct RasterizerParam {
				int fill_mode;
				int cull_mode;
			} raster_table[] = {
				{ D3D11_FILL_SOLID,       D3D11_CULL_NONE	},
				{ D3D11_FILL_SOLID,       D3D11_CULL_BACK	},
				{ D3D11_FILL_SOLID,       D3D11_CULL_FRONT	},
				{ D3D11_FILL_WIREFRAME,   D3D11_CULL_NONE,	},
			};
			{
				D3D11_RASTERIZER_DESC desc;
				desc.FillMode               = (D3D11_FILL_MODE)raster_table[id].fill_mode;
				desc.CullMode               = (D3D11_CULL_MODE)raster_table[id].cull_mode;
				desc.FrontCounterClockwise  = false;
				desc.DepthBias              = 0;
				desc.DepthBiasClamp         = 0.f;
				desc.SlopeScaledDepthBias   = 0.f;
				desc.DepthClipEnable        = false;
				desc.ScissorEnable          = false;
				desc.MultisampleEnable      = false;
				desc.AntialiasedLineEnable  = false;
 
				ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
				GF_DX_RESULT( device->CreateRasterizerState(&desc, &rs), "CreateRasterizerState" );

				rasterState.Add(id, rs);
				*handle = (void*)rs;
			}
#endif
		}

		void createSampler(unsigned int id, void **handle){

#if defined(_WINDOWS)
			ID3D11SamplerState *ss = NULL;
			if(samplerState.Get(id, ss)){
				*handle = (void*)ss;
				return;
			}

			struct SamplerParam {
				D3D11_FILTER filter;
				D3D11_TEXTURE_ADDRESS_MODE addressU;
				D3D11_TEXTURE_ADDRESS_MODE addressV;
			}sampler_table[] = {
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_WRAP,   D3D11_TEXTURE_ADDRESS_WRAP   },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_WRAP,   D3D11_TEXTURE_ADDRESS_CLAMP  },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_CLAMP,  D3D11_TEXTURE_ADDRESS_WRAP   },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_CLAMP,  D3D11_TEXTURE_ADDRESS_CLAMP  },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_CLAMP,  D3D11_TEXTURE_ADDRESS_MIRROR },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_MIRROR, D3D11_TEXTURE_ADDRESS_CLAMP  },
				{ D3D11_FILTER_ANISOTROPIC,        D3D11_TEXTURE_ADDRESS_MIRROR, D3D11_TEXTURE_ADDRESS_MIRROR },
				{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_WRAP,   D3D11_TEXTURE_ADDRESS_WRAP   },
				{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_WRAP,   D3D11_TEXTURE_ADDRESS_CLAMP  },
				{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_CLAMP,  D3D11_TEXTURE_ADDRESS_WRAP   },
				{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_CLAMP,  D3D11_TEXTURE_ADDRESS_CLAMP  },
				{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MIRROR, D3D11_TEXTURE_ADDRESS_MIRROR },
			};

			{
				D3D11_SAMPLER_DESC desc;
				memset(&desc, 0, sizeof(desc));

				desc.Filter			= sampler_table[id].filter;
				desc.AddressU		= sampler_table[id].addressU;
				desc.AddressV		= sampler_table[id].addressV;
				desc.AddressW		= D3D11_TEXTURE_ADDRESS_WRAP;
				desc.MinLOD			= 0;
				desc.MaxLOD			= FLT_MAX;
				desc.MipLODBias		= 0.f;
				desc.MaxAnisotropy	= 1; //????
				desc.ComparisonFunc	= D3D11_COMPARISON_NEVER;
				desc.BorderColor[0]	= 0.f;
				desc.BorderColor[1]	= 0.f;
				desc.BorderColor[2]	= 0.f;
				desc.BorderColor[3]	= 0.f;

				ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
				GF_DX_RESULT( device->CreateSamplerState(&desc, &ss), "CreateSamplerState" );

				samplerState.Add(id, ss);
				*handle = (void*)ss;
			}
#endif
		}
		void createSampler(unsigned char *ids, void **handle, int num){
			for(int i=0;i<num;i++){
				createSampler(ids[i], &handle[i]);
			}
		}

		unsigned long long getID(const IPipeline::Info &info){
			unsigned long long id[0x10] = { 0 };
			memcpy(id, &info, sizeof(info));
			return id[0];
		}
		
		IPipeline *GetPipeline(const IPipeline::Info &info){
			std::unordered_map<unsigned long long, IPipeline*>::iterator itr = map_pipeline.find( getID( info) );
			if(itr != map_pipeline.end()){
				return itr->second;
			}
			return NULL;
		}

		void AddPipeline(const IPipeline::Info &info, IPipeline *p){
			map_pipeline.insert(std::pair<unsigned long long, IPipeline*>( getID(info), p));
		}

		void Remove(IPipeline *p){
			if(!m_bActive)return;
			for(std::unordered_map<unsigned long long, IPipeline*>::iterator itr = 
					map_pipeline.begin(); itr != map_pipeline.end();++itr){
				if(itr->second == p){
					map_pipeline.erase( itr );
					return;
				}
			}
		}

		void Reload(void){
			for(std::unordered_map<unsigned long long, IPipeline*>::iterator itr = map_pipeline.begin();itr != map_pipeline.end();++itr){
				itr->second->Reload();
			}
		}

	};
	PipelineManager sPipelineManager;


	class Pipeline : public IPipeline {
	private:
		Info mInfo;
#if defined(_WINDOWS)
		ID3D11InputLayout *layout = NULL;
		ID3D11BlendState *blend = NULL;
		ID3D11DepthStencilState *depth = NULL;
		ID3D11RasterizerState *raster = NULL;
		ID3D11VertexShader  *vs = NULL;
		ID3D11PixelShader   *ps = NULL;
		ID3D11ComputeShader *cs = NULL;
		ID3D11DeviceContext *context = NULL;
		ID3D11Buffer *pcb[2] = { NULL, NULL };

		ID3D11SamplerState  *ss[4] = {NULL, NULL, NULL, NULL};
		unsigned int base_stride = 0;
#endif

	public:
		Pipeline(){
			context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();
		}
		virtual ~Pipeline(){
		}

		virtual void Release(void){
			sPipelineManager.Remove( this );
			delete this;
		}

		virtual void SetConstantBuffer(const void *ptr, unsigned int size, int type){
#if defined(_WINDOWS)
			D3D11_MAPPED_SUBRESOURCE subres;
			GF_DX_RESULT(context->Map(pcb[type],
									  0,
									  D3D11_MAP_WRITE_DISCARD,
									  0,
									  &subres), "Map");
			void* pData = subres.pData;
			if(size == 0){
				memcpy(pData, ptr, mInfo.constant_size[type]);
			}else{
				memcpy(pData, ptr, size);
			}
			context->Unmap(pcb[type], 0);
#endif
		}

		virtual void SetShaderResource(ISRV **srvs, unsigned int srvNum){
#if defined(_WINDOWS)
			ID3D11ShaderResourceView* view[0x10] = { 0 };
			ID3D11SamplerState* sampler[0x10] = { 0 };
			for(unsigned int n=0;n<srvNum;n++){
				view[n] = (ID3D11ShaderResourceView*)srvs[n];
				sampler[n] = ss[n];
			}
			context->PSSetShaderResources(0, srvNum, view);
			context->PSSetSamplers(0, srvNum, sampler);
#endif
		}

		virtual void Set(void){
#if defined(_WINDOWS)

			context->IASetInputLayout(layout);
			context->VSSetShader(vs, NULL, 0);
			context->GSSetShader(NULL, NULL, 0);

			context->PSSetShader(ps, NULL, 0);
			context->RSSetState(raster);

			context->VSSetConstantBuffers(0, 1, &pcb[0]);
			if (pcb[1] != NULL) {
				context->PSSetConstantBuffers(0, 1, &pcb[1]);
			}
			else {
				context->PSSetConstantBuffers(0, 1, &pcb[0]);
			}

			context->OMSetDepthStencilState(depth, 0);
			{
				float factor[4] = {
					D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
				context->OMSetBlendState(blend, factor, 0xffffffff);
			}
			switch(mInfo.topology){
			case IPipeline::TOPOLOGY_POINTLIST:
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
				break;
			case IPipeline::TOPOLOGY_LINELIST:
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
				break;
			case IPipeline::TOPOLOGY_LINESTRIP:
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);
				break;
			case IPipeline::TOPOLOGY_TRIANGLESTRIP:
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
				break;
			default:
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				break;
			}

#endif
		}
		virtual void Dispatch(ISRV **views, int viewNum, IUAV **uavs, int uavNum,
							  int x, int y, int z){
#if defined(_WINDOWS)
			context->CSSetShader(cs, NULL, 0 );
			context->CSSetShaderResources( 0, viewNum,
										   (ID3D11ShaderResourceView**)views);
			context->CSSetUnorderedAccessViews( 0, uavNum, 
												(ID3D11UnorderedAccessView**)uavs, nullptr);
			if(ss != NULL){
				ID3D11SamplerState* sampler[0x10] = { 0 };
				for(int i=0;i<viewNum;i++)sampler[i] = (ID3D11SamplerState*)ss[i];
				context->CSSetSamplers(0, viewNum, sampler);
			}

			context->CSSetConstantBuffers( 0, 1, &pcb[0]);

			context->Dispatch( x, y, z );

			ID3D11UnorderedAccessView*  pNullUAVs[ 1 ] = { nullptr };
			ID3D11ShaderResourceView*   pNullSRVs[ 2 ] = { nullptr, nullptr };
			ID3D11Buffer*               pNullCBs [ 1 ] = { nullptr };
			context->CSSetShader( nullptr, nullptr, 0 );
			context->CSSetUnorderedAccessViews( 0, 1, pNullUAVs, nullptr );
			context->CSSetShaderResources( 0, 2, pNullSRVs );
			context->CSSetConstantBuffers( 0, 1, pNullCBs );			
#endif
		}

		void Create(const Info &info){
			mInfo = info;

			if(info.cs != 0){
				sPipelineManager.createConstantBuffer(info.cs, (void**)&pcb[0], info.constant_size[0]);
#if defined(_WINDOWS)
				cs = (ID3D11ComputeShader*)IShader::GetShader(info.cs);
				sPipelineManager.createSampler((unsigned char*)info.ss, (void**)ss, info.ss_num);
#endif
			}else{
				sPipelineManager.createBlend(info.bs, (void**)&blend);
				sPipelineManager.createDepth(info.ds, (void**)&depth);
				sPipelineManager.createRasterizer(info.rs, (void**)&raster);
				sPipelineManager.createSampler((unsigned char*)info.ss, (void**)&ss, info.ss_num);

				sPipelineManager.createLayout(info.layout, (void**)&layout, IShader::GetIShader(info.vs)->GetCode(), &base_stride);
				sPipelineManager.createConstantBuffer(info.vs, (void**)&pcb[0], info.constant_size[0]);
				if (info.constant_size[1] != 0){
					sPipelineManager.createConstantBuffer(info.ps, (void**)&pcb[1], info.constant_size[1]);
				}
			
#if defined(_WINDOWS)
				vs = (ID3D11VertexShader*)IShader::GetShader(info.vs);
				ps = (ID3D11PixelShader*)IShader::GetShader(info.ps);
#endif
			}
		}

		virtual void Reload(void){
			if(mInfo.cs != 0){
				cs = (ID3D11ComputeShader*)IShader::GetShader(mInfo.cs);
			}else{
				vs = (ID3D11VertexShader*)IShader::GetShader(mInfo.vs);
				ps = (ID3D11PixelShader*)IShader::GetShader(mInfo.ps);
			}
		}
	};

	IPipeline *IPipeline::GetPipeline(const Info &info){
		IPipeline *p = sPipelineManager.GetPipeline(info);
		if(p != NULL)return p;

		Pipeline *pipeline = new Pipeline();
		sPipelineManager.AddPipeline(info, pipeline);
		pipeline->Create(info);
		return pipeline;
	}

}
