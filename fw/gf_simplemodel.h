﻿//-----------------------------------------------------------------------------
// File: gf_simplemodel.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_resource.h"
#include "gf_singleton.h"

#include <vector>

namespace GF
{

	class ISimpleModel {
	public:
		virtual ~ISimpleModel(){}
		virtual void Release(void) = 0;

		virtual void SetAnim(unsigned int idx) = 0;
		virtual void SetAnimLoop(bool bLoop) = 0;
		virtual void SetAnimSpeed(float spd) = 0;
		virtual void UpdateAnim(void) = 0;

		virtual void Update(const Matrix4 &transform) = 0;

		virtual bool GetBoundingBox(Point3 *bb, bool bAll = false) = 0;
		virtual void CalcBoundingBox(const Matrix4 &transform,
									 const Point3 *src, Point3 *dst, bool bInit = true) = 0;


		virtual IBuffer *GetVertexBuffer(void) = 0;
		virtual void *GetVertexBufferRaw(void) = 0;

		virtual unsigned int UpdateBoneMatrix(void) = 0;
		virtual Matrix4 *GetBoneMatrix(void) = 0;

		virtual void SetMaterial(IMaterial *mat, unsigned int id = 0xffffffff) = 0;
		virtual void SetMaterialParam(const void *vsParam, const void *psParam) = 0;
		virtual void Draw(const Matrix4 &transform) = 0;
		

		static ISimpleModel *Create(const void *model, std::vector<const void*> &anims);
	};

	class ISimpleModelManager : public Singleton<ISimpleModelManager> {
	public:
		virtual ~ISimpleModelManager(){}
		virtual void Release(void){}
	};


}


