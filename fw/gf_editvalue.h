﻿//-----------------------------------------------------------------------------
// File: gf_editvalue.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_singleton.h"
#include "gf_color.h"

namespace GF
{
	typedef void* EDIT_HANDLE;
	static const char *EDIT_CMD_TAG   = "cmds";
	static const char *EDIT_CMD_TYPE  = "type";
	static const char *EDIT_CMD_VALUE = "value";

	class EditHandle {
	public:
		EDIT_HANDLE handle;
	public:
		EditHandle();
		~EditHandle();

		virtual void SetName(const std::string &name);
		virtual void SetValue(const std::string &name, int &value);
		virtual void SetValue(const std::string &name, unsigned int &value);
		virtual void SetValue(const std::string &name, bool &value);
		virtual void SetValue(const std::string &name, PColor &value);
		virtual void SetValue(const std::string &name, float &value);
		virtual void SetValue(const std::string &name, std::string &value);
	};
	class EditRegister {
	private:
		EditHandle *p_handle;
	public:
		EditRegister(EditHandle &handle, const std::string &name);
		~EditRegister();
	};

	class IEditValue : public Singleton<IEditValue> {
	public:
		virtual ~IEditValue(){}
		virtual void Release(void) = 0;

		virtual EDIT_HANDLE Create(void) = 0;
		virtual void Unregist(EDIT_HANDLE h) = 0;
		virtual void Update(EDIT_HANDLE h) = 0;

		virtual void GetCommand(std::string &command) = 0;
		virtual void EditedCommand(std::string &command) = 0;
		virtual void SelectedCommand(std::string &command) = 0;
		virtual void CustomCommand(std::string &sendCommand, const std::string &tag, const std::string &command) = 0;
		virtual void UpdateSelectedCommand(void) = 0;

		static IEditValue *CreateInstance(void);
	};

#if defined(EDIT_PARAM_NO_USE)
    #define EDIT_PARAMS
    #define EDIT_REGIST(_ee_, _name_)
    #define EDIT_VALUE(_ee_, _value_)
#else
    #define EDIT_PARAMS               GF::EditHandle
    #define EDIT_REGIST(_eh_, _name_) GF::EditRegister _edit_register_(_eh_, _name_)
    #define EDIT_VALUE(_eh_, _value_) _eh_.SetValue(#_value_, _value_)
#endif
}








