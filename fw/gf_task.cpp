﻿
#include <vector>

#include "gf_task.h"

namespace GF
{
	BootTaskFunc BootTask::bootFunc = 0; 
	std::map<std::string, BootTaskFunc> BootTask::select_Funcs;


	class TaskManager : public ITaskManager {
	public:
		static TaskManager *s_Instance;
		static unsigned long long uid;

		std::vector<ITask*> task_lists;
		bool m_bActive = true;
	public:
		TaskManager(){
			s_Instance = this;
		}
		virtual ~TaskManager(){
			s_Instance = NULL;
		}

		virtual void Release(void){
			m_bActive = false;
			Clear();
			delete this;
		}

		virtual void Clear(void) {
			for (std::vector<ITask*>::iterator itr = task_lists.begin();
				itr != task_lists.end(); ++itr) {
				(*itr)->Release();
			}
			task_lists.clear();
		}

		virtual void Update(void){
			if(!m_bActive)return;
			for(std::vector<ITask*>::iterator itr = task_lists.begin();
				itr != task_lists.end();++itr){
				(*itr)->Update();
			}
		}
		virtual void PreDraw(void){
			if(!m_bActive)return;
			for(std::vector<ITask*>::iterator itr = task_lists.begin();
				itr != task_lists.end();++itr){
				(*itr)->PreDraw();
			}
		}
		virtual void Draw(void){
			if(!m_bActive)return;
			for(std::vector<ITask*>::iterator itr = task_lists.begin();
				itr != task_lists.end();++itr){
				(*itr)->Draw();
			}
		}

		virtual void Start(const char *bootTaskName = NULL){
			m_bActive = true;
			if(bootTaskName == NULL){
				if(BootTask::bootFunc != NULL){
					Regist( BootTask::bootFunc() );
				}
			}else{
				std::map<std::string, BootTaskFunc>::iterator itr =
					BootTask::select_Funcs.find(bootTaskName);
				if(itr != BootTask::select_Funcs.end()){
					if(itr->second != NULL){
						Regist( (*itr->second)() );
					}
				}
			}
		}

		virtual void GetModuleNames(std::vector<std::string> &moduleNames) {
			for (std::map<std::string, BootTaskFunc>::iterator itr =
				BootTask::select_Funcs.begin(); itr != BootTask::select_Funcs.end(); ++itr) {
				moduleNames.push_back(itr->first);
			}
		}


		virtual void Regist(ITask *task){
			if(!m_bActive)return;
			task_lists.push_back( task );
		}

		virtual void Unregist(ITask *task){
			if(!m_bActive)return;
			std::vector<ITask*>::iterator itr = find(
				task_lists.begin(), task_lists.end(), task);
			if(itr != task_lists.end()){
				task_lists.erase( itr );
			}
		}

	};
	TaskManager *TaskManager::s_Instance = NULL;
	unsigned long long TaskManager::uid = 0;


	ITaskManager *ITaskManager::CreateInstance(void){ return new TaskManager();}
	ITaskManager *ITaskManager::GetInstance(void){ return TaskManager::s_Instance;}

}
