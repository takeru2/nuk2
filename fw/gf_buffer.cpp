﻿
#include "gf_system.h"
#include "gf_device.h"

#include "gf_buffer.h"

namespace GF
{

	class Buffer : public IBuffer {
	public:

#if defined(_WINDOWS)
		ID3D11UnorderedAccessView *m_pUAV = NULL;
		ID3D11ShaderResourceView *m_pSRV = NULL;
		ID3D11Buffer *m_pBuffer = NULL;
		ID3D11Buffer *m_pBufferCopy = NULL;
#endif

		char *m_pMemoryBuffer = NULL;
		unsigned int mMemoryBufferPos;
		unsigned int mSize = 0;

		Buffer(){
			IBufferResource::GetInstance()->Add( this );
		}

		virtual ~Buffer(){
		}

		virtual void Release(void){
			IBufferResource::GetInstance()->Remove( this );

			SAFE_RELEASE(m_pBuffer);
			SAFE_RELEASE(m_pBufferCopy);

			SAFE_RELEASE(m_pSRV);
			SAFE_RELEASE(m_pUAV);

			delete this;
		}

		virtual unsigned int GetSize(void){
			return mSize;
		}

		virtual void Map(void){
			ID3D11DeviceContext *context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

			D3D11_MAPPED_SUBRESOURCE subres;
			GF_DX_RESULT(context->Map(m_pBuffer,
									 0,
									 D3D11_MAP_WRITE_DISCARD,
									 0,
									 &subres), "Map");
			m_pMemoryBuffer = (char*)subres.pData;
			mMemoryBufferPos = 0;
		}

		virtual void Unmap(void){
			ID3D11DeviceContext *context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

			context->Unmap(m_pBuffer, 0);
			m_pMemoryBuffer = NULL;
		}

		void createCopyBuffer(void) {
			if (m_pBufferCopy == NULL) {
				ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();
				D3D11_BUFFER_DESC desc;
				memset(&desc, 0, sizeof(desc));
				desc.ByteWidth = mSize;
				desc.Usage = D3D11_USAGE_STAGING;
				desc.BindFlags = 0;
				desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
				desc.MiscFlags = 0;

				GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBufferCopy),
					"CreateBuffer");
			}
		}

		virtual void ReadData(void *ptr, unsigned int offset, unsigned int size) {
			ID3D11DeviceContext *context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

			createCopyBuffer();

			context->CopyResource(m_pBufferCopy, m_pBuffer);

			D3D11_MAPPED_SUBRESOURCE subres;
			GF_DX_RESULT(context->Map(m_pBufferCopy,
				0,
				D3D11_MAP_WRITE,
				0,
				&subres), "Map");
			if(subres.pData != NULL){
				memcpy(ptr, (char*)subres.pData + offset, size);
			}
			memcpy(subres.pData, ptr, size);
			context->Unmap(m_pBufferCopy, 0);

		}

		virtual void CopyData(const void *ptr, unsigned int size){
			ID3D11DeviceContext *context = (ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

			createCopyBuffer();

			if(m_pBufferCopy != NULL){
				D3D11_MAPPED_SUBRESOURCE subres;
				GF_DX_RESULT(context->Map(m_pBufferCopy,
										  0,
										  D3D11_MAP_WRITE,
										  0,
										  &subres), "Map");
				memcpy(subres.pData, ptr, size);
				context->Unmap(m_pBufferCopy, 0);

				context->CopyResource(m_pBuffer, m_pBufferCopy);
			}
		}

		virtual unsigned int GetMapMemoryOffset(void){
			return mMemoryBufferPos;
		}

		virtual char *GetMapMemory(unsigned int size){
			if((m_pMemoryBuffer == NULL) || ((mMemoryBufferPos + size) > mSize) ){
				GF_EXCEPTION("MapMemoryOver");
			}
			char *ptr = &m_pMemoryBuffer[mMemoryBufferPos];
			mMemoryBufferPos += size;
			return ptr;
		}

		virtual void *GetBuffer(void){
			return m_pBuffer;
		}

		virtual ISRV *GetSRV(void){
			return (ISRV*)m_pSRV;
		}
		virtual IUAV *GetUAV(void){
			return (IUAV*)m_pUAV;
		}

		virtual bool IsMapped(void){
			return (m_pMemoryBuffer != NULL)?true:false;
		}

		void createVertexBuffer(unsigned int size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset(&desc, 0, sizeof(desc));
			desc.Usage             = D3D11_USAGE_DYNAMIC;
			desc.ByteWidth = mSize = size;
			desc.BindFlags         = D3D11_BIND_VERTEX_BUFFER;
			desc.CPUAccessFlags    = D3D11_CPU_ACCESS_WRITE;
			desc.MiscFlags = 0;
			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBuffer),
						 "CreateBuffer");
		}

		void createIndexBuffer(unsigned int size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset(&desc, 0, sizeof(desc));
			desc.Usage             = D3D11_USAGE_DYNAMIC;
			desc.ByteWidth = mSize = size;
			desc.BindFlags         = D3D11_BIND_INDEX_BUFFER;
			desc.CPUAccessFlags    = D3D11_CPU_ACCESS_WRITE;
			desc.MiscFlags = 0;
			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBuffer),
						 "CreateBuffer");
		}

		void createVertexBufferUAV(unsigned int elementSize, unsigned int num){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset(&desc, 0, sizeof(desc));
			desc.ByteWidth = elementSize * num;
			desc.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;;
			desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
			desc.StructureByteStride = elementSize;

			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBuffer),
						 "CreateBuffer");


			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			memset( &uavDesc, 0, sizeof(uavDesc ) );
			uavDesc.Format              = DXGI_FORMAT_R32_TYPELESS;
			uavDesc.ViewDimension       = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Buffer.Flags        = D3D11_BUFFER_UAV_FLAG_RAW;
			uavDesc.Buffer.NumElements  = desc.ByteWidth / 4;
			GF_DX_RESULT(device->CreateUnorderedAccessView(
							 m_pBuffer, &uavDesc, &m_pUAV), "CreateUnorderedAccessView");

			mSize = desc.ByteWidth;
		}

		void createStructuredBuffer(unsigned int elementSize, unsigned int num){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset( &desc, 0, sizeof(desc) );
		    desc.BindFlags           = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
			desc.ByteWidth           = elementSize * num;
			desc.MiscFlags           = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			desc.StructureByteStride = elementSize;
			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBuffer),
						 "CreateBuffer");

			//Copy
			desc.Usage          = D3D11_USAGE_STAGING;
			desc.BindFlags      = 0;
			desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
			desc.MiscFlags		= 0;
			GF_DX_RESULT(device->CreateBuffer( &desc, NULL, &m_pBufferCopy ),
						 "CreateBuffer");
			
		}

		void createBuffer(unsigned int size) {
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset(&desc, 0, sizeof(desc));
			desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
			desc.ByteWidth = size;
			desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBuffer),
				"CreateBuffer");

			//Copy
			desc.Usage = D3D11_USAGE_STAGING;
			desc.BindFlags = 0;
			desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
			desc.MiscFlags = 0;
			GF_DX_RESULT(device->CreateBuffer(&desc, NULL, &m_pBufferCopy),
				"CreateBuffer");

		}

		void CreateSRV(void){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset( &desc, 0, sizeof(desc) );
			m_pBuffer->GetDesc( &desc );

			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
			memset( &srvDesc, 0, sizeof(srvDesc) );
			
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
			srvDesc.BufferEx.FirstElement = 0;

			if ( desc.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS ) {
				srvDesc.Format               = DXGI_FORMAT_R32_TYPELESS;
				srvDesc.BufferEx.Flags       = D3D11_BUFFEREX_SRV_FLAG_RAW;
				srvDesc.BufferEx.NumElements = desc.ByteWidth / 4;
			} else if ( desc.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_STRUCTURED ) {
				srvDesc.Format               = DXGI_FORMAT_UNKNOWN;
				srvDesc.BufferEx.NumElements = desc.ByteWidth / desc.StructureByteStride;
			}else {
				return;
			}

			GF_DX_RESULT(device->CreateShaderResourceView( m_pBuffer, &srvDesc, &m_pSRV),
						 "CreateShaderResourceView");
		}

		void CreateUAV(void){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3D11_BUFFER_DESC desc;
			memset( &desc, 0, sizeof(desc) );
			m_pBuffer->GetDesc( &desc );

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			memset( &uavDesc, 0, sizeof(uavDesc ) );
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Buffer.FirstElement = 0;

			if ( desc.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS ) {
				uavDesc.Format              = DXGI_FORMAT_R32_TYPELESS;
				uavDesc.ViewDimension       = D3D11_UAV_DIMENSION_BUFFER;
				uavDesc.Buffer.Flags        = D3D11_BUFFER_UAV_FLAG_RAW;
				uavDesc.Buffer.NumElements  = desc.ByteWidth / 4;
			}else if ( desc.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_STRUCTURED ){
				uavDesc.Format              = DXGI_FORMAT_UNKNOWN;
				uavDesc.Buffer.NumElements  = desc.ByteWidth / desc.StructureByteStride;
			}else {
				return;
			}

			GF_DX_RESULT(device->CreateUnorderedAccessView( m_pBuffer, &uavDesc, &m_pUAV),
						 "CreateUnorderedAccessView");
		}
	};

	IBuffer *IBuffer::CreateVertexBuffer(unsigned int size){
		Buffer *pBuffer = new Buffer();
		pBuffer->createVertexBuffer(size);
		return pBuffer;
	}
	IBuffer *IBuffer::CreateIndexBuffer(unsigned int size){
		Buffer *pBuffer = new Buffer();
		pBuffer->createIndexBuffer(size);
		return pBuffer;
	}

	IBuffer *IBuffer::CreateVertexBufferUAV(unsigned int elementSize, unsigned int num){
		Buffer *pBuffer = new Buffer();
		pBuffer->createVertexBufferUAV(elementSize, num);
		return pBuffer;
	}

	IBuffer *IBuffer::CreateStructuredBuffer(unsigned int elementSize, unsigned int num){
		Buffer *pBuffer = new Buffer();
		pBuffer->createStructuredBuffer(elementSize, num);
		pBuffer->CreateSRV();
		return pBuffer;
	}

	IBuffer *IBuffer::CreateBuffer(unsigned int size){
		Buffer *pBuffer = new Buffer();
		pBuffer->createBuffer(size);
		pBuffer->CreateSRV();
		return pBuffer;
	}

}
