﻿//-----------------------------------------------------------------------------
// File: gf_rendertexture.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_resource.h"
#include "gf_texture.h"

namespace GF
{

class IRenderTexture : public ITexture {
public:
	virtual ~IRenderTexture(){}
	virtual void Release(void) = 0;

	virtual IRTV *GetRTV(void) = 0;
	virtual ISRV *GetDepthSRV(void) = 0;

	virtual void Begin(void) = 0;
	virtual void End(void) = 0;
	virtual void Clear(const float *clearColor, float depth) = 0;
	
	static IRenderTexture *Create(unsigned int format, unsigned short w, unsigned short h);
};

class IRenderTextureResource : public IResource<IRenderTextureResource, IRenderTexture> {virtual ~IRenderTextureResource(){} };


}


