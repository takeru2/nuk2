﻿//-----------------------------------------------------------------------------
// File: gf_file.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_resource.h"

namespace GF
{

	class File {
	private:
		std::string mName;
		std::pair<char*,unsigned int> mData;

	public:
		File();
		virtual ~File();

		std::pair<char*,unsigned int> &Load(const std::string &name);

		std::pair<char*,unsigned int> Copy(void);

		const std::string &GetName(void){return mName;}
		void *GetData(void){return mData.first;}
		unsigned int GetSize(void){return mData.second;}
	};

}
