﻿
#include "gf_system.h"
#include "gf_device.h"
#include "gf_primitive.h"

namespace GF
{

	void Primitive::SetMaterial(IMaterial *material, const void *constantBuffer, const void *constantBufferPS){
		material->Set(constantBuffer, constantBufferPS);
	}

	void Primitive::SetPipeline(IPipeline *pipeline, const void *constantBuffer, unsigned int size, void *srv){
		pipeline->SetConstantBuffer(constantBuffer, size);
		if(srv != NULL){
			void *srv_array[1] = {srv};
			pipeline->SetShaderResource(srv_array, 1);
		}
		pipeline->Set();
	}
	void Primitive::SetPipeline(IPipeline *pipeline, const void *constantBuffer, unsigned int size,
								ISRV **srvs, unsigned int srvNum){
		pipeline->SetConstantBuffer(constantBuffer, size);
		pipeline->SetShaderResource(srvs, srvNum);
		pipeline->Set();
	}

	void Primitive::Draw(IBuffer *buffer, unsigned int stride,
						 unsigned int vtxOffset, unsigned int vtxNum){
#if defined(_WINDOWS)
		ID3D11DeviceContext *context = 
			(ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

		ID3D11Buffer* vb = (ID3D11Buffer*)buffer->GetBuffer();
		unsigned int _stride = stride;
		unsigned int _offset = vtxOffset;
		context->IASetVertexBuffers(0, 1, &vb, &_stride, &_offset);

		context->Draw(vtxNum, 0);

		ID3D11Buffer*	pNull = NULL;
		context->IASetVertexBuffers(0, 1, &pNull, &_stride, &_offset);
#endif
	}

	void Primitive::DrawIndexed(IBuffer *vtxBuffer, unsigned int stride,
								unsigned int vtxOffset, unsigned int vtxNum,
								IBuffer *idxBuffer,
								unsigned int idxOffset, unsigned int idxCount, bool index32){
#if defined(_WINDOWS)
		ID3D11DeviceContext *context = 
			(ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

		ID3D11Buffer* vb = (ID3D11Buffer*)vtxBuffer->GetBuffer();
		unsigned int _stride = stride;
		unsigned int _offset = vtxOffset;
		context->IASetVertexBuffers(0, 1, &vb, &_stride, &_offset);

		ID3D11Buffer* ib = (ID3D11Buffer*)idxBuffer->GetBuffer();
		_offset = 0;
		context->IASetIndexBuffer(ib, index32?DXGI_FORMAT_R32_UINT:DXGI_FORMAT_R16_UINT, _offset);
		unsigned int _startIndex = idxOffset;
		unsigned int _baseIndex = 0;
		context->DrawIndexed(idxCount, _startIndex, _baseIndex);

		ID3D11Buffer*	pNull = NULL;
		context->IASetVertexBuffers(0, 1, &pNull, &_stride, &_offset);
		context->IASetIndexBuffer(0, DXGI_FORMAT_R32_UINT, 0);

#endif
	}

	void Primitive::DrawIndexedInstanced(
		IBuffer *vtxBuffer, unsigned int stride, unsigned int vtxNum,
		IBuffer *idxBuffer, unsigned int indexCount,
		IBuffer *instancedBuffer, unsigned int instancedStride, unsigned int instanceCount){
#if defined(_WINDOWS)
		ID3D11DeviceContext *context = 
			(ID3D11DeviceContext*)GF::IDevice::GetInstance()->GetContext();

		ID3D11Buffer* vbs[] = {
			(ID3D11Buffer*)vtxBuffer->GetBuffer(),
			(ID3D11Buffer*)instancedBuffer->GetBuffer()};
		unsigned int _stride[] = {stride, instancedStride};
		unsigned int _offset[] = {0,0};
		context->IASetVertexBuffers(0, 2, vbs, _stride, _offset);

		ID3D11Buffer* ib = (ID3D11Buffer*)idxBuffer->GetBuffer();
		_offset[0] = 0;
		context->IASetIndexBuffer(ib, DXGI_FORMAT_R32_UINT, _offset[0]);
		unsigned int _startIndex = 0;
		unsigned int _baseIndex = 0;
		unsigned int _startInst = 0;

		context->DrawIndexedInstanced(indexCount, instanceCount, _startIndex, _baseIndex, _startInst);

		ID3D11Buffer*	pNulls[] = {NULL, NULL};
		context->IASetVertexBuffers(0, 2, pNulls, _stride, _offset);
		context->IASetIndexBuffer(0, DXGI_FORMAT_R32_UINT, 0);

#endif
	}

}

