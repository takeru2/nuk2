﻿//-----------------------------------------------------------------------------
// File: gf_system.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#define _USE_MATH_DEFINES
#include<math.h>

namespace GF
{

    #ifndef M_PI
    #define M_PI 3.14159265358979323846f
    #endif
    #define DEG2RAD(_deg_) (M_PI * (_deg_) / 180.f)
    #define RAD2DEG(_rad_) (360.f * (_rad_) / M_2PI)

    #define Max(_a_,_b_)            (((_a_) > (_b_)) ? (_a_) : (_b_))
    #define Min(_a_,_b_)            (((_a_) < (_b_)) ? (_a_) : (_b_))
    #define Clamp(_a_, _low_, _high_) (Max(_low_, Min(_a_, _high_)))
    #define Abs(_a_)                (((_a_) < 0) ? -(_a_) : (_a_))
    #define Sign(_a_)               (((_a_) < 0) ? -1 : 1)

    template<typename T> inline void Swap(T &a, T &b){T tmp = a;a = b;b = tmp;}
    #define Lerp(_a_, _b_, _s_) ((_a_) + ((_b_) - (_a_)) * (_s_))


	struct RandContext {unsigned int x, y, z, w;};
	float Rand(RandContext *pCtx = NULL);
	unsigned int Rand(RandContext *pCtx, unsigned int range);
	void RandSeed(RandContext *pCtx, unsigned int seed);
}
