﻿
#pragma once

#include "gf_buffer.h"
#include "gf_pipeline.h"
#include "gf_material.h"

namespace GF
{
	class Primitive {
	public:
		enum {
			Vertex_P3_C8_Stride       = 16,
			Vertex_P3_C8_UV_Stride    = 16 + 8,
			Vertex_P3_N3_C8_UV_Stride = 12 + 12 + 4 + 8,
		};
		static inline void SetVertex_P3_C8(float *&f_ptr,
										   float x, float y, float z, unsigned int color){
			*f_ptr++ = x;*f_ptr++ = y;*f_ptr++ = z;
			*reinterpret_cast<unsigned int*>( f_ptr++ ) = color;
		}
		static inline void SetVertex_P3_C8_UV(float *&f_ptr,
											  float x, float y, float z, unsigned int color, float u, float v){
			*f_ptr++ = x;*f_ptr++ = y;*f_ptr++ = z;
			*reinterpret_cast<unsigned int*>( f_ptr++ ) = color;
			*f_ptr++ = u;*f_ptr++ = v;
		}
		static inline void SetVertex_P3_N3_C8_UV(float *&f_ptr,
												 float x, float y, float z,
												 float nx, float ny, float nz,
												 unsigned int color, float u, float v){
			*f_ptr++ = x;*f_ptr++ = y;*f_ptr++ = z;
			*f_ptr++ = nx;*f_ptr++ = ny;*f_ptr++ = nz;
			*reinterpret_cast<unsigned int*>( f_ptr++ ) = color;
			*f_ptr++ = u;*f_ptr++ = v;
		}

		static void SetMaterial(IMaterial *material, const void *constantBuffer, const void *constantBufferPS = NULL);
		static void SetPipeline(IPipeline *pipeline, const void *constantBuffer, unsigned int size, void *srv);
		static void SetPipeline(IPipeline *pipeline, const void *constantBuffer, unsigned int size,
								ISRV **srvs, unsigned int srvNum);

		static void Draw(IBuffer *buffer, unsigned int stride,
						 unsigned int vtxOffset, unsigned int vtxNum);

		static void Draw(IBuffer *buffer, unsigned int stride, unsigned int vtxNum){
			Draw(buffer, stride, 0, vtxNum);
		}

		static void DrawIndexed(IBuffer *vtxBuffer, unsigned int stride,
								unsigned int vtxOffset, unsigned int vtxNum,
								IBuffer *idxBuffer,
								unsigned int idxOffset, unsigned int idxCount, bool index32);
		static void DrawIndexed(IBuffer *vtxBuffer, unsigned int stride, unsigned int vtxNum,
								IBuffer *idxBuffer, unsigned int idxCount){
			DrawIndexed(vtxBuffer, stride, 0, vtxNum, idxBuffer, 0, idxCount, true);
		}

		static void DrawIndexedInstanced(
			IBuffer *vtxBuffer, unsigned int stride, unsigned int vtxNum,
			IBuffer *idxBuffer, unsigned int idxCount,
			IBuffer *instBuffer, unsigned int instStride, unsigned int instCount);
								
	};

}
