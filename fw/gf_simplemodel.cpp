﻿
#include "gf.h"

#include "gf_simplemodel.h"

namespace GF {

	namespace Resource {
		enum {INVALID_ID = 0xffffffff};
		struct setParamInfo {
			unsigned int type;
			unsigned int nameOffset;
			union{
				float f4[4];
				int   i4[4];
				bool  b4[4];
				int stringOffset;
			};
			int index;
			int padding[1];
		};

		struct imageInfo {
			int stringOffset;
			int type;
			unsigned int size;
			int padding[1];
		};
		struct materialInfo {
			unsigned int shaderID;
			unsigned int setParamIndex;
			unsigned int setParamNum;
			unsigned int nameOffset;
		};
		struct geomInfo {
			unsigned int vertexOffset;
			unsigned int vertexNum;
			unsigned int indexOffset;
			unsigned int indexNum;

			unsigned int primitiveType;
			unsigned int stride;
			unsigned int materialIndex;
			int nameOffset;
		};
		struct nodeInfo {
			float bindMatrix[16];
			float localMatrix[16];
			float pivotMatrix[16];

			float boundingBox[6];
			unsigned int geomNum;
			unsigned int geomIndex;
			int parentIndex;
			int childIndex;
			int siblingIndex;
			int nameOffset;
		};
		struct objInfo {
			unsigned int tag;
			unsigned int version;
			unsigned int nodeNum;
			unsigned int geomNum;
			unsigned int materialNum;
			unsigned int imageNum;

			unsigned int setParamNum;
			unsigned int vertexSize;
			unsigned int indexSize;
			unsigned int stringSize;

			unsigned int nameOffset;
			unsigned char lodNum;
			unsigned char lodOffset;
			unsigned char padding[2];
		};

		struct animNodeInfo {
			unsigned int nameOffset;
			unsigned int startFrame;
			unsigned int endFrame;
			unsigned int transformOffset;
		};

		struct animInfo {
			unsigned int tag;
			unsigned int version;
			unsigned int nodeNum;
			unsigned int startFrame;

			unsigned int endFrame;
			unsigned int transformSize;
			unsigned int stringSize;
			unsigned int nameOffset;

			unsigned int ex_flag;
			unsigned int ex_offset;
			unsigned int ex_size;
			unsigned int padding;
		};

	} // Resource

	class SimpleModel : public ISimpleModel {
	private:
		class ModelMaterial {
		public:
			std::string name;
			IMaterial *shaderMaterial;
			ModelMaterial() {
				shaderMaterial = NULL;
			}
			~ModelMaterial() {
			}
		};

		class ModelGeom {
		public:
			unsigned int vertexOffset;
			unsigned int vertexNum;
			unsigned int indexOffset;
			unsigned int indexNum;
			enum {
				PRIM_NORMAL  = (1<<0),
				PRIM_TANGENT = (1<<1),
				PRIM_UV      = (1<<2),
				PRIM_COLOR   = (1<<3),
				PRIM_SKIN    = (1<<4),

				PRIM_INDEX32 = (1 << 16),
			};
			unsigned int primitiveType;
			unsigned int stride;
			ModelMaterial *material;
			std::string name;
		};

		class ModelNode {
		public:
			Matrix4 pivotMatrix;
			Matrix4 bindMatrix;
			Matrix4 localBaseTransform;
			Matrix4 localTransform;
			Matrix4 worldTransform;

			std::string name;
			std::vector<ModelGeom*> vGeom;

			ModelNode *parent;
			ModelNode *child;
			ModelNode *sibling;

			Point3 boundingBox[2];
		};

		char *m_pVertexData;
		unsigned int mVertexDataSize;
		IBuffer *m_pVertex;

		char *m_pIndexData;
		unsigned int mIndexDataSize;
		IBuffer *m_pIndex;

		std::vector<ModelNode*> m_vNode;
		std::vector<ModelGeom*> m_vGeom;
		std::vector<ModelMaterial*> m_vMat;

		std::unordered_map<std::string, ModelNode*> m_mNode;

		std::string mName;

		struct AnimNode {
			std::string name;
			ModelNode *node;
			Matrix4 *transform;
			int startFrame;
			int endFrame;
		};

		class Anim {
		public:
			float mSpeed;
			float mCurrentFrame;
			int mStartFrame;
			int mEndFrame;
			char *m_pTransform = NULL;
			char *m_pBB = NULL;
			bool m_bLoop;
			int mLoopCount;
			std::vector<AnimNode> m_vAnimNode;
			~Anim(){
				if(m_pTransform != NULL)delete [] m_pTransform;
				if(m_pBB != NULL)delete [] m_pBB;
			}
		};
		std::vector<Anim*> m_vAnim;
		unsigned int mAnimIndex = 0;

		enum {MATERIAL_MATRIX_MAX = 0x100};
		Matrix4 shaderMatrix[MATERIAL_MATRIX_MAX];

		const void *shaderParamVS = NULL;
		const void *shaderParamPS = NULL;

	public:
		SimpleModel() {
			m_pVertexData = NULL;
			mVertexDataSize = 0;
			m_pVertex = NULL;

			m_pIndexData = NULL;
			mIndexDataSize = 0;
			m_pIndex = NULL;

		}

		virtual ~SimpleModel() {
			if (m_pVertexData != NULL)delete[] m_pVertexData;
			if (m_pIndexData != NULL)delete[] m_pIndexData;

			SAFE_RELEASE(m_pVertex);
			SAFE_RELEASE(m_pIndex);

			for (std::vector<ModelNode*>::iterator itr = m_vNode.begin(); itr != m_vNode.end(); ++itr) {
				SAFE_DELETE(*itr);
			}
			for (std::vector<ModelGeom*>::iterator itr = m_vGeom.begin(); itr != m_vGeom.end(); ++itr) {
				SAFE_DELETE(*itr);
			}
			for (std::vector<ModelMaterial*>::iterator itr = m_vMat.begin(); itr != m_vMat.end(); ++itr) {
				SAFE_DELETE(*itr);
			}
			for (std::vector<Anim*>::iterator itr = m_vAnim.begin(); itr != m_vAnim.end(); ++itr) {
				SAFE_DELETE(*itr);
			}
		}

		virtual void Release(void) {
			delete this;
		}

		bool LoadModel(const char *buffer) {
			const Resource::objInfo *info = reinterpret_cast<const Resource::objInfo*> (buffer);
			const Resource::nodeInfo *node = reinterpret_cast<const Resource::nodeInfo*>(
				buffer + sizeof(Resource::objInfo));
			const Resource::geomInfo *geom = reinterpret_cast <const Resource::geomInfo*>(
				reinterpret_cast<const char*>(node) + sizeof(Resource::nodeInfo) * info->nodeNum);
			const Resource::materialInfo *mat = reinterpret_cast<const Resource::materialInfo*>(
				reinterpret_cast<const char*>(geom) + sizeof(Resource::geomInfo) * info->geomNum);
			const Resource::setParamInfo *sp = reinterpret_cast<const Resource::setParamInfo*>(
				reinterpret_cast<const char*>(mat) + sizeof(Resource::materialInfo) * info->materialNum);
			const char *vertex = reinterpret_cast<const char*>(
				reinterpret_cast<const char*>(sp) + sizeof(Resource::setParamInfo) * info->setParamNum);
			const char *index = reinterpret_cast<const char*>(
				reinterpret_cast<const char*>(vertex) + info->vertexSize);
			const char *string = reinterpret_cast<const char*>(
				reinterpret_cast<const char*>(index) + info->indexSize);

			mName = std::string(&string[info->nameOffset]);

			//copy vertex buffer
			if (info->vertexSize > 0) {
				mVertexDataSize = info->vertexSize;
				m_pVertexData = new char[mVertexDataSize];
				memcpy(m_pVertexData, vertex, mVertexDataSize);

				m_pVertex = GF::IBuffer::CreateVertexBuffer(mVertexDataSize);
				m_pVertex->Map();
				memcpy(m_pVertex->GetMapMemory(), m_pVertexData, mVertexDataSize);
				m_pVertex->Unmap();
			}
			if (info->indexSize > 0) {
				mIndexDataSize = info->indexSize;
				m_pIndexData = new char[mIndexDataSize];
				memcpy(m_pIndexData, index, mIndexDataSize);

				m_pIndex = GF::IBuffer::CreateIndexBuffer(mIndexDataSize);
				m_pIndex->Map();
				memcpy(m_pIndex->GetMapMemory(), m_pIndexData, mIndexDataSize);
				m_pIndex->Unmap();
			}

			//material
			if (info->materialNum > 0) {
				m_vMat.resize(info->materialNum);
				for (unsigned int n = 0; n < info->materialNum; n++) {
					m_vMat[n] = new ModelMaterial();
					m_vMat[n]->name = std::string(&string[mat[n].nameOffset]);
				}
			}
			else {
				m_vMat.resize(1);
				m_vMat[0] = new ModelMaterial();
			}

			//geom
			if (info->geomNum > 0) {
				m_vGeom.resize(info->geomNum);
				for (unsigned int n = 0; n < info->geomNum; n++) {
					if (info->geomNum > 0) {
						m_vGeom[n] = new ModelGeom();
						m_vGeom[n]->name = std::string(&string[geom[n].nameOffset]);
						m_vGeom[n]->vertexOffset = geom[n].vertexOffset;
						m_vGeom[n]->vertexNum = geom[n].vertexNum;
						m_vGeom[n]->indexOffset = geom[n].indexOffset;
						m_vGeom[n]->indexNum = geom[n].indexNum;
						m_vGeom[n]->primitiveType = geom[n].primitiveType;
						m_vGeom[n]->stride = geom[n].stride;
						m_vGeom[n]->material = (geom[n].materialIndex != Resource::INVALID_ID) ? m_vMat[geom[n].materialIndex] : m_vMat[0];
					}
				}
			}

			//node
			if (info->nodeNum > 0) {
				m_vNode.resize(info->nodeNum);
				for (unsigned int n = 0; n < info->nodeNum; n++) {
					m_vNode[n] = new ModelNode();
					m_vNode[n]->name = std::string(&string[node[n].nameOffset]);

					m_mNode.insert(std::pair<std::string, ModelNode*>(m_vNode[n]->name, m_vNode[n]));

					memcpy(&m_vNode[n]->bindMatrix, node[n].bindMatrix, sizeof(Matrix4));
					memcpy(&m_vNode[n]->localTransform, node[n].localMatrix, sizeof(Matrix4));
					memcpy(&m_vNode[n]->pivotMatrix, node[n].pivotMatrix, sizeof(Matrix4));

					m_vNode[n]->localBaseTransform = m_vNode[n]->localTransform;
					m_vNode[n]->worldTransform = m_vNode[n]->localTransform * m_vNode[n]->bindMatrix;

					m_vNode[n]->boundingBox[0] = Point3(node[n].boundingBox[0],
														node[n].boundingBox[1],
														node[n].boundingBox[2]);
					m_vNode[n]->boundingBox[1] = Point3(node[n].boundingBox[3],
														node[n].boundingBox[4],
														node[n].boundingBox[5]);
					if (node[n].geomNum > 0) {
						m_vNode[n]->vGeom.resize(node[n].geomNum);
						for (unsigned int g = 0; g < node[n].geomNum; g++) {
							m_vNode[n]->vGeom[g] = m_vGeom[node[n].geomIndex + g];
						}
					}
				}

				//Link
				for (unsigned int n = 0; n < info->nodeNum; n++) {
					m_vNode[n]->parent = (node[n].parentIndex != Resource::INVALID_ID) ?
						m_vNode[node[n].parentIndex] : NULL;
					m_vNode[n]->sibling = (node[n].siblingIndex != Resource::INVALID_ID) ?
						m_vNode[node[n].siblingIndex] : NULL;
					m_vNode[n]->child = (node[n].childIndex != Resource::INVALID_ID) ?
						m_vNode[node[n].childIndex] : NULL;
				}
			}
			return true;
		}

		bool LoadAnim(const char *buffer) {
			if (buffer == NULL)return false;

			const Resource::animInfo *info = reinterpret_cast<const Resource::animInfo*> (buffer);
			const Resource::animNodeInfo *node = reinterpret_cast<const Resource::animNodeInfo*>(
				buffer + sizeof(Resource::animInfo));
			const Matrix4 *transform = reinterpret_cast <const Matrix4*>(
				reinterpret_cast<const char*>(node) + sizeof(Resource::animNodeInfo) * info->nodeNum);
			const char *string = reinterpret_cast<const char*>(
				reinterpret_cast<const char*>(transform) +
				sizeof(Matrix4) * info->transformSize + info->ex_size);


			Anim *ma = new Anim();
			ma->mStartFrame = 0;// info->startFrame;
			ma->mEndFrame = info->endFrame - 1;
			ma->mCurrentFrame = (float)ma->mStartFrame;

			//copy transform
			if (info->transformSize > 0) {
				ma->m_pTransform = new char[sizeof(Matrix4) * info->transformSize];
				memcpy(ma->m_pTransform, transform, sizeof(Matrix4) * info->transformSize);
			}

			//BB
			if(info->ex_size != 0){
				ma->m_pBB = new char[info->ex_size];
				memcpy(ma->m_pBB,
					   reinterpret_cast<const char*>(
						   reinterpret_cast<const char*>(transform) +
						   sizeof(Matrix4) * info->transformSize),
					   info->ex_size);
			}
			
			//anim node
			if (info->nodeNum > 0) {
				ma->m_vAnimNode.resize(info->nodeNum);
				for (unsigned int n = 0; n < info->nodeNum; n++) {
					ma->m_vAnimNode[n].name = std::string(&string[node[n].nameOffset]);
					ma->m_vAnimNode[n].transform = reinterpret_cast<Matrix4*>(&ma->m_pTransform[node[n].transformOffset]);
					ma->m_vAnimNode[n].startFrame = node[n].startFrame;
					ma->m_vAnimNode[n].endFrame = node[n].endFrame;

					//Link
					std::unordered_map<std::string, ModelNode*>::iterator n_itr = m_mNode.find(ma->m_vAnimNode[n].name);
					if (n_itr != m_mNode.end()) {
						ma->m_vAnimNode[n].node = n_itr->second;
					}
					else {
						GF_EXCEPTION("anim error\n");
					}
				}
			}
			m_vAnim.push_back(ma);

			InitAnim();

			return true;
		}

		IBuffer *GetVertexBuffer(void){
			return m_pVertex;
		}

		void *GetVertexBufferRaw(void) {
			return m_pVertexData;
		}

		Matrix4 *GetBoneMatrix(void){
			return shaderMatrix;
		}

		void updateTransform(ModelNode *node, const Matrix4 &transform) {
			do {
				if(node->vGeom.empty()){
					node->worldTransform = transform * (node->localTransform * node->bindMatrix);
				}else{
					node->worldTransform = transform * node->localTransform;
				}
				if (node->child != NULL) {
					updateTransform(node->child, transform );
				}	
			} while ((node = node->sibling) != NULL);
		}

		void Update(const Matrix4 &transform) {
			updateTransform(m_vNode[0], transform);
		}

		bool GetBoundingBox(Point3 *bb, bool bAll){
			if (m_vNode.empty())return false;
			if (!m_vAnim.empty()) {
				Anim &anim = *m_vAnim[0];
				if (anim.m_pBB == NULL) {
					bb[0] = m_vNode[0]->boundingBox[0];
					bb[1] = m_vNode[0]->boundingBox[1];
					return true;
				}

				float *f_ptr = (float*)anim.m_pBB;
				if(bAll){
					bb[0] = Point3(f_ptr[0], f_ptr[1], f_ptr[2]);
					bb[1] = Point3(f_ptr[4], f_ptr[5], f_ptr[6]);
				}else{
					float *f_ptr = (float*)anim.m_pBB + (int)anim.mCurrentFrame * 8;
					bb[0] = Point3(f_ptr[0], f_ptr[1], f_ptr[2]);
					bb[1] = Point3(f_ptr[4], f_ptr[5], f_ptr[6]);
				}
			}
			else{
				bb[0] = m_vNode[0]->boundingBox[0];
				bb[1] = m_vNode[0]->boundingBox[1];
			}
			return true;
		}

		void CalcBoundingBox(const Matrix4 &transform,
							 const Point3 *src, Point3 *dst, bool bInit){
			if(bInit)
			{
				dst[0] = Point3(FLT_MAX, FLT_MAX, FLT_MAX);
				dst[1] = Point3(FLT_MIN, FLT_MIN, FLT_MIN);
			}
	
			{
				Point3 tp[8];
				Transform3 tr(transform.getCol0().getXYZ(),
							  transform.getCol1().getXYZ(),
							  transform.getCol2().getXYZ(),
							  transform.getCol3().getXYZ());
				tp[0] = tr * Point3(src[0].getX(),
									src[0].getY(),
									src[0].getZ());
				tp[1] = tr * Point3(src[0].getX(),
									src[1].getY(),
									src[0].getZ());
				tp[2] = tr * Point3(src[0].getX(),
									src[0].getY(),
									src[1].getZ());
				tp[3] = tr * Point3(src[0].getX(),
									src[1].getY(),
									src[1].getZ());
				tp[4] = tr * Point3(src[1].getX(),
									src[0].getY(),
									src[0].getZ());
				tp[5] = tr * Point3(src[1].getX(),
									src[1].getY(),
									src[0].getZ());
				tp[6] = tr * Point3(src[1].getX(),
									src[0].getY(),
									src[1].getZ());
				tp[7] = tr * Point3(src[1].getX(),
									src[1].getY(),
									src[1].getZ());
				for(int i=0;i<8;i++){
					dst[0].setX(Min(dst[0].getX(), tp[i].getX()));
					dst[0].setY(Min(dst[0].getY(), tp[i].getY()));
					dst[0].setZ(Min(dst[0].getZ(), tp[i].getZ()));

					dst[1].setX(Max(dst[1].getX(), tp[i].getX()));
					dst[1].setY(Max(dst[1].getY(), tp[i].getY()));
					dst[1].setZ(Max(dst[1].getZ(), tp[i].getZ()));
				}
			}
		}

		void InitAnim(void){
			mAnimIndex = 0;
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				anim.mCurrentFrame = (float)anim.mStartFrame;
				anim.mSpeed = 1;
				anim.m_bLoop = true;
				anim.mLoopCount = 0;
			}
		}

		void SetAnim(unsigned int idx) {
			if ((unsigned int)m_vAnim.size() > idx) {
				mAnimIndex = idx;
			}
		}

		void SetAnimLoop(bool bLoop) {
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				anim.m_bLoop = bLoop;
			}
		}

		void SetAnimSpeed(float spd) {
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				anim.mSpeed = spd;
			}
		}

		bool IsAnimEnd(void) {
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				return (anim.mLoopCount > 0) ? true : false;
			}
			return true;
		}

		void SetAnimEndFrame(void) {
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				anim.mCurrentFrame = (float)anim.mEndFrame - 1;
				anim.mLoopCount = 1;
			}
		}

		void UpdateAnim(void) {
			if ((unsigned int)m_vAnim.size() > mAnimIndex) {
				Anim &anim = *m_vAnim[mAnimIndex];
				anim.mCurrentFrame += anim.mSpeed;
				if (anim.mCurrentFrame >= (float)anim.mEndFrame) {
					if (anim.m_bLoop) {
						anim.mCurrentFrame = (float)anim.mStartFrame;
						anim.mLoopCount++;
					}
					else {
						anim.mCurrentFrame = (float)anim.mEndFrame - 1;
						if (anim.mLoopCount == 0)anim.mLoopCount = 1;
					}
				}

				//node anim
				for (std::vector<AnimNode>::iterator itr = anim.m_vAnimNode.begin();
					itr != anim.m_vAnimNode.end(); ++itr) {
					AnimNode &an = *itr;
					int idx = (int)anim.mCurrentFrame;
					if (an.endFrame > 0)idx = idx % an.endFrame;
					an.node->localTransform = an.transform[idx];
				}
			}
		}

		void SetMaterial(IMaterial *mat, unsigned int id) {
			if (id == 0xffffffff) {
				for (std::vector<ModelMaterial*>::iterator itr = m_vMat.begin();
					itr != m_vMat.end(); ++itr) {
					(*itr)->shaderMaterial = mat;
				}
			}
			else {
				if (id < (unsigned int)m_vMat.size()) {
					m_vMat[id]->shaderMaterial = mat;
				}
			}
		}
		virtual void SetMaterialParam(const void *vsParam, const void *psParam) {
			shaderParamVS = vsParam;
			shaderParamPS = psParam;
		}

		void draw_node(ModelNode *node, const Matrix4 &transform) {
			do {
				for (std::vector<ModelGeom*>::iterator itr = node->vGeom.begin();
					itr != node->vGeom.end(); ++itr) {
					ModelGeom *geom = *itr;
					if(geom->primitiveType & ModelGeom::PRIM_SKIN){
						shaderMatrix[0] = transform;
						shaderMatrix[1] = Matrix4::identity();
					}else{
						shaderMatrix[0] = transform * node->worldTransform;
						shaderMatrix[1] = node->worldTransform;
					}

					if (geom->material->shaderMaterial != NULL) {
						Primitive::SetMaterial(geom->material->shaderMaterial, shaderMatrix, shaderParamPS);
					}
					Primitive::DrawIndexed(m_pVertex,
						geom->stride,
						geom->vertexOffset, geom->vertexNum,
						m_pIndex,
						geom->indexOffset, geom->indexNum,
						(geom->primitiveType & ModelGeom::PRIM_INDEX32)?true:false);
				}
				if (node->child != NULL) {
					draw_node(node->child, transform);
				}
			} while ((node = node->sibling) != NULL);
		}

		unsigned int UpdateBoneMatrix(void){
			int idx = 2; //0:WVP 1:World
			for (std::vector<ModelNode*>::iterator itr = m_vNode.begin();
				 itr != m_vNode.end() && idx < MATERIAL_MATRIX_MAX; ++itr, ++idx) {
				shaderMatrix[idx] = (*itr)->worldTransform;
			}
			return (unsigned int)m_vNode.size();
		}

		void Draw(const Matrix4 &transform) {
			if (!m_vNode.empty()) {
				UpdateBoneMatrix();
				draw_node(m_vNode[0], transform);
			}
		}
	};

	ISimpleModel *ISimpleModel::Create(const void *model, std::vector<const void*> &anims){
		SimpleModel *pModel = new SimpleModel();
		pModel->LoadModel((const char*)model);
		for(std::vector<const void*>::iterator itr = anims.begin();
			itr != anims.end();++itr){
			pModel->LoadAnim((const char*)*itr);
		}
		return pModel;
	}

}
