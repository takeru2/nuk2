﻿//-----------------------------------------------------------------------------
// File: gf_task.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <vector>
#include "gf_singleton.h"

namespace GF
{

	class ITask {
	public:
		virtual ~ITask(){}
		virtual void Release(void){};
		virtual void Update(void){}

		virtual void PreDraw(void){}
		virtual void Draw(void){}
	};

	class ITaskManager {
	public:
		virtual ~ITaskManager(){}
		virtual void Release(void) = 0;
		virtual void Update(void) = 0;
		virtual void PreDraw(void) = 0;
		virtual void Draw(void) = 0;

		virtual void Clear(void) = 0;
		virtual void Start(const char *bootTaskName = NULL) = 0;
		virtual void Regist(ITask *task) = 0;
		virtual void Unregist(ITask *task) = 0;
		virtual void GetModuleNames(std::vector<std::string> &moduleNames) = 0;

		static ITaskManager *CreateInstance(void);
		static ITaskManager *GetInstance(void);
	};

	typedef ITask *(*BootTaskFunc)(void);
	class BootTask { 
	public:
		static BootTaskFunc bootFunc;
		static std::map<std::string, BootTaskFunc> select_Funcs;
		static int Regist(BootTaskFunc func){bootFunc = func;return 0;}
		static int RegistSelect(const char *name, BootTaskFunc func){select_Funcs.insert(
				std::pair<std::string,BootTaskFunc>(name, func));return 0;}
	};

    #define BOOT_TASK(_task_) \
	extern "C" {			  \
		static GF::ITask *_create_##_task_(void){ return new _task_(); } \
		static int _id_create_##_task_ = GF::BootTask::Regist((GF::BootTaskFunc)_create_##_task_); \
	}
    #define SELECT_TASK(_task_) \
	extern "C" {			  \
		static GF::ITask *_create_##_task_(void){ return new _task_(); } \
		static int _id_create_##_task_ = GF::BootTask::RegistSelect(#_task_, (GF::BootTaskFunc)_create_##_task_); \
	}
	
}




