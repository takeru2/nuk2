﻿//-----------------------------------------------------------------------------
// File: gf_material.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_singleton.h"

namespace GF
{

	class IMaterial {
	public:
		virtual ~IMaterial(){}
		virtual void Release(void) = 0;
		virtual void UpdateSRV(int srv_num, const ISRV **srvs) = 0;
		virtual void Set(const void *constantBuffer, const void *constantBufferPS = NULL) = 0;

		static IMaterial *GetMaterial(const std::string &name);
	};


	class IMaterialManager : public Singleton<IMaterialManager> {
	public:
		virtual ~IMaterialManager(){}
		virtual void Release(void){}
	};

}


