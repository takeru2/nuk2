﻿
#include <list>

#include "gf_system.h"
#include "gf_device.h"

#include "gf_texture.h"

#if defined(_WINDOWS)
#include <D3DX11tex.h>
#endif

namespace GF
{
	class Texture : public ITexture {
	public:
		ID3D11Texture2D *m_pTexture = NULL;
		ID3D11ShaderResourceView *m_pSRV = NULL;
		unsigned short mWidth  = 0;
		unsigned short mHeight = 0;
		unsigned short mDepth  = 0;
	public:
		Texture(){
			m_pTexture = NULL;
			m_pSRV = NULL;

			ITextureResource::GetInstance()->Add( this );
		}

		virtual ~Texture(){
		}

		virtual void Release(void){
			ITextureResource::GetInstance()->Remove( this );

			SAFE_RELEASE(m_pTexture);
			SAFE_RELEASE(m_pSRV);
			
			delete this;
		}

		void createFromMemory(const void *buffer, unsigned int size){
			ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

			D3DX11_IMAGE_LOAD_INFO loadInfo;
			memset(&loadInfo, 0, sizeof(loadInfo));
			loadInfo.Width  = D3DX11_DEFAULT;
			loadInfo.Height = D3DX11_DEFAULT;
			loadInfo.Depth = D3DX11_DEFAULT;
			loadInfo.FirstMipLevel = D3DX11_FROM_FILE;
			loadInfo.MipLevels = D3DX11_FROM_FILE;
			loadInfo.Usage = (D3D11_USAGE)D3DX11_DEFAULT;
			loadInfo.BindFlags = D3DX11_DEFAULT;
			loadInfo.CpuAccessFlags = D3DX11_DEFAULT;
			loadInfo.MiscFlags = D3DX11_DEFAULT;
			loadInfo.Format = DXGI_FORMAT_FROM_FILE;
			loadInfo.Filter = D3DX11_DEFAULT;
			loadInfo.MipFilter = D3DX11_DEFAULT;

			GF_DX_RESULT(D3DX11CreateTextureFromMemory(device,
													   (const uint8_t*)buffer,
													   (size_t)size,
													   &loadInfo, NULL,
													   (ID3D11Resource**)&m_pTexture,
													   NULL), 
						 "D3DX11CreateTextureFromMemory");

			GF_DX_RESULT(device->CreateShaderResourceView(m_pTexture, NULL, &m_pSRV),
						 "CreateShaderResourceView");

			D3D11_TEXTURE2D_DESC desc;
			m_pTexture->GetDesc(&desc);

			mWidth  = desc.Width;
			mHeight = desc.Height;
		}

		virtual unsigned short GetWidth(void){return mWidth;}
		virtual unsigned short GetHeight(void){return mHeight;}
		virtual unsigned short GetDepth(void){return mDepth;}
		virtual ISRV *GetSRV(void){return (ISRV*)m_pSRV;}


	};


	ITexture *ITexture::Create(unsigned int format, unsigned short w, unsigned short h, unsigned short depth){
		ID3D11Device *device = (ID3D11Device*)GF::IDevice::GetInstance()->GetDevice();

		Texture *pTex = new Texture();
		pTex->mWidth = w;
		pTex->mHeight = h;
		pTex->mDepth  = depth;

		D3D11_TEXTURE2D_DESC desc;
		desc.Width  = w;
		desc.Height = h;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		desc.Format = (DXGI_FORMAT)format;
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.MiscFlags = 0;
	
		GF_DX_RESULT(device->CreateTexture2D(
						 &desc, NULL, &pTex->m_pTexture),
					 "CreateTexture2D");

		D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
		memset(&srv_desc, 0, sizeof(srv_desc));
		srv_desc.Format = desc.Format;
		srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv_desc.Texture1D.MostDetailedMip = 0;
		srv_desc.Texture1D.MipLevels = 1;
		GF_DX_RESULT(device->CreateShaderResourceView(
						 pTex->m_pTexture, &srv_desc, &pTex->m_pSRV),
					 "CreateShaderResourceView");

		return pTex;
	}

	ITexture *ITexture::CreateFromMemory(const void *buffer, unsigned int size){
		Texture *pTex = new Texture();
		if (buffer != NULL) {
			pTex->createFromMemory(buffer, size);
		}
		return pTex;
	}

}

