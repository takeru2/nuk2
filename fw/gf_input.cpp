﻿
#include "gf_system.h"
#include "gf_input.h"

#if defined(_WINDOWS)
#include <windows.h>
#endif

namespace GF
{

	class Input : public IInput {
	private:
		enum {
			D_NOW,
			D_PREV,
			D_REQ,
			D_NUM,
		};
		static unsigned char sKeyState[D_NUM][0x100];

		int mousePos[D_NUM][2];
		int mouseVel[2];

		//0xC1 - 0xDA
		enum {
			CUSTOM_01 = 0xC1,
			CUSTOM_02,
			CUSTOM_03,
			CUSTOM_04,

			CUSTOM_START = CUSTOM_01,
			CUSTOM_END   = CUSTOM_04,
		};

	public:
		Input(){
			memset(sKeyState, 0, sizeof(sKeyState));
			memset(mousePos, 0, sizeof(mousePos));
			memset(mouseVel, 0, sizeof(mouseVel));
		}
		virtual ~Input(){
		}

		virtual void SetEvent(int stat, int p0, int p1){
			switch(stat){
			case 4: //TODO
				sKeyState[D_REQ][CUSTOM_01] |= 0x80;
				break;
			case 8: //TODO
				sKeyState[D_REQ][CUSTOM_02] |= 0x80;
				break;
			case 0:
				mousePos[D_NOW][0] = p0;
				mousePos[D_NOW][1] = p1;
				break;
			}
		}
		virtual void Update(void){
#if defined(_WINDOWS)
			memcpy(sKeyState[D_PREV], sKeyState[D_NOW], sizeof(sKeyState[D_NOW]));
			::GetKeyboardState(sKeyState[D_NOW]);

			for(int i=CUSTOM_START;i<=CUSTOM_END;i++){
				sKeyState[D_NOW][i] |= sKeyState[D_REQ][i];
				sKeyState[D_REQ][i] = 0;
			}

			sKeyState[0][VK_LBUTTON] = (GetKeyState(VK_LBUTTON) < 0)?0x80:0;
			sKeyState[0][VK_RBUTTON] = (GetKeyState(VK_RBUTTON) < 0)?0x80:0;
			sKeyState[0][VK_MBUTTON] = (GetKeyState(VK_MBUTTON) < 0)?0x80:0;
			sKeyState[0][VK_XBUTTON1] = (GetKeyState(VK_XBUTTON1) < 0)?0x80:0;
			sKeyState[0][VK_XBUTTON2] = (GetKeyState(VK_XBUTTON2) < 0)?0x80:0;
#endif
			mouseVel[0] = mousePos[D_NOW][0] - mousePos[D_PREV][0];
			mouseVel[1] = mousePos[D_NOW][1] - mousePos[D_PREV][1];
			mousePos[D_PREV][0] = mousePos[D_NOW][0];
			mousePos[D_PREV][1] = mousePos[D_NOW][1];

		}
		virtual bool On(unsigned char id){
			return (sKeyState[D_NOW][id] & 0x80)?true:false;
		}
		virtual bool Press(unsigned char id){
			return ( (sKeyState[D_NOW][id] & 0x80) && !(sKeyState[D_PREV][id] & 0x80) )?true:false;
		}
		virtual void GetPositionInfo(float *px, float *py, float *vx, float *vy){
			if(px != NULL)*px = (float)mousePos[D_NOW][0];
			if(py != NULL)*py = (float)mousePos[D_NOW][1];
			if(vx != NULL)*vx = (float)mouseVel[0];
			if(vy != NULL)*vy = (float)mouseVel[1];
		}

	};
	unsigned char Input::sKeyState[D_NUM][0x100];

	Input sInput;
}
