﻿//-----------------------------------------------------------------------------
// File: gf_system.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#if defined(_WINDOWS)
#include <d3d11.h>
#include <d3dcompiler.h>
#include <iostream> 
#include <stdexcept>
#endif

namespace GF
{

class ISystem {
public:
	virtual ~ISystem(){}
	virtual void Release(void) = 0;

	virtual void Init(const char *commands = NULL) = 0;
	virtual void Update(void) = 0;

	virtual void Pause(bool bPause = true) = 0;
	virtual bool IsPause(void) = 0;

#if defined(_WINDOWS)
	static bool CreateDevice_DX11(void *param, unsigned short w, unsigned short h, unsigned char msaa);
#endif
	
	static bool CreateInstance(void);
	static ISystem *GetInstance(void);
};

typedef void ISRV;
typedef void IRTV;
typedef void IUAV;
	
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)	  { if( p ) { (p)->Release(); (p) = NULL; } }
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(p)	  { if( p ) { delete (p); (p) = NULL; } }
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p)	  { if( p ) { delete [] (p); (p) = NULL; } }
#endif

#if defined(_WINDOWS)
class Exception : public std::domain_error {
public:
    Exception(const std::string& cause)
		: std::domain_error(cause) {}
};

#define GF_EXCEPTION(_cause_) {throw Exception(_cause_);}
#define GF_DX_RESULT(_res_, _cause_) {if(FAILED(_res_))throw Exception(_cause_);}

#else
#define GF_EXCEPTION(_cause_) ;
#define GF_DX_RESULT(_res_, _cause_) ;
#endif

extern void(*CustomLog)(const char*);
inline void LOG(const char *format, ...){
	char buf[1024];
	va_list va;
	va_start(va, format);
#if defined(__GOT_SECURE_LIB__) && __GOT_SECURE_LIB__ >= 200402L
	_vsnprintf_s((char *)buf, 1024 - 1, _TRUNCATE, format, va);
#else
	vsnprintf((char *)buf, 1024, format, va);
#endif
	va_end(va);

	if (CustomLog != NULL) {
		CustomLog(buf);
	}
}

}


