﻿//-----------------------------------------------------------------------------
// File: gf_color.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

namespace GF
{

class PColor {
public:
    union{
        unsigned int dwColor;
        unsigned char  rgba[4];
    };
    inline PColor() {};
    inline PColor( unsigned int color );
    inline PColor( unsigned char r, unsigned char g, unsigned char b, unsigned char a);

    inline PColor & operator =( const PColor & color );

    inline PColor operator * ( float scalar ) const;
    inline PColor & operator *=( float scalar );
    inline PColor operator * ( const PColor &color ) const;
    inline PColor & operator *= ( const PColor &color );
    inline const PColor operator +( const PColor & color ) const;
    inline const PColor operator -( const PColor & color ) const;

    inline PColor & setR( unsigned char r );
    inline PColor & setG( unsigned char g );
    inline PColor & setB( unsigned char b );
    inline PColor & setA( unsigned char a );
	inline PColor & setRGBA(unsigned int argb);
	inline PColor & setARGB(unsigned int argb);
	inline PColor & setRGBA(float r, float g, float b, float a);
	inline PColor & setHSVA(float h, float s, float v, float a);

	inline PColor & store(float *rgba);
	inline PColor & load(const float *rgba);

    inline unsigned char getR( ) const;
    inline unsigned char getG( ) const;
    inline unsigned char getB( ) const;
    inline unsigned char getA( ) const;

    inline operator PColor *();
    inline operator const PColor *() const;
    inline operator unsigned char *();
    inline operator const unsigned char *() const ;
    inline operator unsigned int () const;

};

//-----------------------------------------------------------------------------
// Name : PColor
// Desc : 
//-----------------------------------------------------------------------------

#define PCOLOR_IA    3
#define PCOLOR_IR    0
#define PCOLOR_IG    1
#define PCOLOR_IB    2

#define COLOR_Max(_a_,_b_)            (((_a_) > (_b_)) ? (_a_) : (_b_))
#define COLOR_Min(_a_,_b_)            (((_a_) < (_b_)) ? (_a_) : (_b_))
#define COLOR_Clamp(_a_, _low_, _high_) (COLOR_Max(_low_, COLOR_Min(_a_, _high_)))

inline PColor::PColor( unsigned int color ) {
    dwColor = color;
}

inline PColor::PColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a){
    rgba[ PCOLOR_IR ] = r;
    rgba[ PCOLOR_IG ] = g;
    rgba[ PCOLOR_IB ] = b;
    rgba[ PCOLOR_IA ] = a;
}

inline PColor & PColor::operator =( const PColor & color ) {
    dwColor = color.dwColor;
    return *this;
}

inline PColor PColor::operator * ( float scalar ) const{
    return PColor(static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IR]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IG]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IB]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IA]) * scalar, 0, 255.f)));
}
inline PColor & PColor::operator *=( float scalar ){
    *this = *this * scalar;
    return *this;
}
inline PColor PColor::operator * ( const PColor &color ) const{
    return PColor(COLOR_Clamp(rgba[PCOLOR_IR] * color.rgba[PCOLOR_IR] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IG] * color.rgba[PCOLOR_IG] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IB] * color.rgba[PCOLOR_IB] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IA] * color.rgba[PCOLOR_IA] / 255, 0, 255));
}
inline PColor & PColor::operator *= ( const PColor &color ) {
    *this = *this * color;
    return *this;
}
inline const PColor PColor::operator +( const PColor & color ) const {
    return PColor(
        COLOR_Clamp(rgba[PCOLOR_IR] + color.rgba[PCOLOR_IR], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IG] + color.rgba[PCOLOR_IG], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IB] + color.rgba[PCOLOR_IB], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IA] + color.rgba[PCOLOR_IA], 0, 255));
}
inline const PColor PColor::operator -( const PColor & color ) const {
    return PColor(
        COLOR_Clamp(rgba[PCOLOR_IR] - color.rgba[PCOLOR_IR], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IG] - color.rgba[PCOLOR_IG], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IB] - color.rgba[PCOLOR_IB], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IA] - color.rgba[PCOLOR_IA], 0, 255));
}


inline PColor & PColor::setR( unsigned char r ){
    rgba[ PCOLOR_IR ] = r;
    return *this;
}

inline PColor & PColor::setG( unsigned char g ){
    rgba[ PCOLOR_IG ] = g;
    return *this;
}

inline PColor & PColor::setB( unsigned char b ){
    rgba[ PCOLOR_IB ] = b;
    return *this;
}

inline PColor & PColor::setA( unsigned char a ){
    rgba[ PCOLOR_IA ] = a;
    return *this;
}

inline PColor & PColor::setRGBA(float r, float g, float b, float a){
    rgba[ PCOLOR_IR ] = COLOR_Clamp((int)(r * 255.f), 0, 255);
    rgba[ PCOLOR_IG ] = COLOR_Clamp((int)(g * 255.f), 0, 255);
    rgba[ PCOLOR_IB ] = COLOR_Clamp((int)(b * 255.f), 0, 255);
    rgba[ PCOLOR_IA ] = COLOR_Clamp((int)(a * 255.f), 0, 255);
	return *this;
}

inline PColor & PColor::setRGBA(unsigned int rgba) {
	PColor _rgba(rgba);
	dwColor = _rgba.dwColor;
	return *this;
}

inline PColor & PColor::setARGB(unsigned int argb) {
	PColor _argb(argb);
	PColor _rgba(_argb.getB(), _argb.getG(), _argb.getR(), _argb.getA());
	return setRGBA(_rgba);
}

inline unsigned char PColor::getR( ) const {
    return rgba[ PCOLOR_IR ];
}

inline unsigned char PColor::getG( ) const {
    return rgba[ PCOLOR_IG ];
}

inline unsigned char PColor::getB( ) const {
    return rgba[ PCOLOR_IB ];
}

inline unsigned char PColor::getA( ) const {
    return rgba[ PCOLOR_IA ];
}

inline PColor & PColor::store(float *rgba) {
	rgba[0] = (float)getR() / 255.f;
	rgba[1] = (float)getG() / 255.f;
	rgba[2] = (float)getB() / 255.f;
	rgba[3] = (float)getA() / 255.f;
	return *this;
}

inline PColor & PColor::load(const float *rgba) {
	setRGBA(rgba[0], rgba[1], rgba[2], rgba[3]);
	return *this;
}

inline PColor::operator PColor *() {
    return (PColor*)&dwColor;
}

inline PColor::operator const PColor *() const {
    return (const PColor*)&dwColor;
}

inline PColor::operator unsigned char *() {
    return (unsigned char*)&dwColor;
}

inline PColor::operator const unsigned char *() const {
    return (const unsigned char*)&dwColor;
}

inline PColor::operator unsigned int () const {
    return (unsigned int)dwColor;
}

inline PColor & PColor::setHSVA(float h, float s, float v, float a){
    float cx,cy,cz;
    if( s == 0 ){
        cx = cy = cz = v;
    }else{
        if( 1.0 <= h ) h -= 1.0;
          h *= 6.0;
        float i = floor (h);
        float f = h - i;
        float aa = v * (1 - s);
        float bb = v * (1 - (s * f));
        float cc = v * (1 - (s * (1 - f)));
        if( i < 1 ){
            cx = v;  cy = cc; cz = aa;
        }else if( i < 2 ){
            cx = bb; cy = v;  cz = aa;
        }else if( i < 3 ){
            cx = aa; cy = v;  cz = cc;
        }else if( i < 4 ){
            cx = aa; cy = bb; cz = v;
        }else if( i < 5 ){
            cx = cc; cy = aa; cz = v;
        }else{
            cx = v;  cy = aa; cz = bb;
        }
    }
    setRGBA(cx, cy, cz, a);

	return *this;
}

}
