﻿//-----------------------------------------------------------------------------
// File: gf_input.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

#include "gf_singleton.h"

namespace GF
{

	class IInput : public Singleton<IInput> {
	public:
		virtual ~IInput(){}

		virtual void SetEvent(int stat, int p0, int p1) = 0;
		virtual void Update(void) = 0;

		virtual bool On(unsigned char id) = 0;
		virtual bool Press(unsigned char id) = 0;

		virtual void GetPositionInfo(float *px, float *py, float *vx, float *vy) = 0;

	};
	
}






