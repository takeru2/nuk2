﻿//-----------------------------------------------------------------------------
// File: gf_singleton.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once

namespace GF
{

template< typename T_ > class Singleton {
    typedef T_ object_type;
private:
    static object_type *static_pInstance;
    Singleton(const Singleton&);
    Singleton &operator=(const Singleton&);
public:
    static object_type *GetInstance(void){return static_pInstance;}
    Singleton(){static_pInstance = (object_type*)this;}
    virtual ~Singleton(){static_pInstance = 0;}
};
template< typename T_ > typename Singleton< T_ >::object_type* Singleton< T_ >::static_pInstance = 0;

}
