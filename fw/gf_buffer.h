﻿//-----------------------------------------------------------------------------
// File: gf_buffer.h
//
// Desc: 
//
// Copyright (C) 2017 monstars.inc All Rights Reserved.
//-----------------------------------------------------------------------------

#pragma once


#include "gf_resource.h"

namespace GF
{

class IBuffer {
public:
	virtual ~IBuffer(){}
	virtual void Release(void) = 0;

	virtual unsigned int GetSize(void) = 0;
	virtual void Map(void) = 0;
	virtual void Unmap(void) = 0;
	virtual void CopyData(const void *ptr, unsigned int size) = 0;
	virtual void ReadData(void *ptr, unsigned int offset, unsigned int size) = 0;

	virtual bool IsMapped(void) = 0;
	virtual unsigned int GetMapMemoryOffset(void) = 0;
	virtual char *GetMapMemory(unsigned int size = 0) = 0;

	virtual void *GetBuffer(void) = 0;
	virtual ISRV *GetSRV(void) = 0;
	virtual IUAV *GetUAV(void) = 0;

	virtual void CreateSRV(void) = 0;
	virtual void CreateUAV(void) = 0;

	static IBuffer *CreateVertexBuffer(unsigned int size);
	static IBuffer *CreateIndexBuffer(unsigned int size);

	static IBuffer *CreateVertexBufferUAV(unsigned int elementSize, unsigned int num);
	static IBuffer *CreateStructuredBuffer(unsigned int elementSize, unsigned int num);
	static IBuffer *CreateBuffer(unsigned int size);

};

class IBufferResource : public IResource<IBufferResource, IBuffer> {virtual ~IBufferResource(){} };

}
