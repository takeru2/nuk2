﻿
#include <windows.h>
#include <vector>

#include "gf_buffer.h"
#include "gf_system.h"
#include "gf_texture.h"
#include "gf_device.h"
#include "gf_shader.h"
#include "gf_pipeline.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#if _DEBUG
#pragma comment(lib, "d3dx11d.lib")
#else
#pragma comment(lib, "d3dx11.lib")
#endif

namespace GF
{
	class DeviceDX11 : public IDevice {

	private:
		HWND hWnd;

		ID3D11Device*			m_pD3DDevice;
		ID3D11DeviceContext*    m_pImmediateContext;
		IDXGISwapChain* 		m_pSwapChain;

		ID3D11Texture2D*       	m_pBackBuffer;
		ID3D11Texture2D*		m_pDepthBuffer;

		IDXGIAdapter*           m_pAdapter;

		ID3D11RenderTargetView*	m_pRenderTargetView;
		ID3D11DepthStencilView* m_pDepthStencilView;

		DXGI_SAMPLE_DESC		MSAA;
		std::vector<DXGI_SAMPLE_DESC> vMSAA;

		unsigned short mWidth;
		unsigned short mHeight;
	public:
		DeviceDX11() {
			m_pD3DDevice = NULL;
			m_pImmediateContext = NULL;
			m_pSwapChain = NULL;
			m_pAdapter = NULL;

			m_pBackBuffer  = NULL;
			m_pDepthBuffer = NULL;
			m_pRenderTargetView = NULL;
			m_pDepthStencilView = NULL;

			hWnd = NULL;
		}

		virtual void Release(void){
			IPipelineManager::GetInstance()->Release();
			IShaderManager::GetInstance()->Release();

			ITextureResource::GetInstance()->Release();
			IBufferResource::GetInstance()->Release();

			if (m_pSwapChain != NULL){
				m_pSwapChain->SetFullscreenState(false, NULL);
			}

			SAFE_RELEASE(m_pRenderTargetView);
			SAFE_RELEASE(m_pDepthStencilView);
			SAFE_RELEASE(m_pDepthBuffer);
			SAFE_RELEASE(m_pBackBuffer);

			SAFE_RELEASE(m_pSwapChain);
			SAFE_RELEASE(m_pImmediateContext);
			SAFE_RELEASE(m_pD3DDevice);
		}

		bool Init(void *handle, unsigned short w, unsigned short h, unsigned char msaa) {
			hWnd = (HWND)handle;

			mWidth = w;
			mHeight = h;

			UINT i = 0;
			IDXGIAdapter *pAdapter;
			std::vector<IDXGIAdapter*> vAdapters;
			std::vector<DXGI_ADAPTER_DESC> vDescs;

			IDXGIFactory * pFactory;
			HRESULT hr = CreateDXGIFactory1(__uuidof(IDXGIFactory), (void**)(&pFactory));
			if (hr != S_OK)return false;

			while (pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND) {
				DXGI_ADAPTER_DESC desc;
				pAdapter->GetDesc(&desc);
				vAdapters.push_back(pAdapter);
				vDescs.push_back(desc);
				++i;
			}
#if 1       //複数ある場合、チップセットは除外する？
			if (vAdapters.size() > 2) {
				for (unsigned int i = 0; i < vAdapters.size(); i++) {
					if (wcsncmp(vDescs[i].Description, L"Intel", 5) == 0) {
						vDescs.erase(vDescs.begin() + i);
						vAdapters.erase(vAdapters.begin() + i);
						break;
					}
				}
			}
#endif

			if (vAdapters.empty())return false;

#if defined(DEBUG) || defined(_DEBUG)
			D3D11_CREATE_DEVICE_FLAG createDeviceFlag = (D3D11_CREATE_DEVICE_FLAG)0;// D3D11_CREATE_DEVICE_DEBUG;
#else
			D3D11_CREATE_DEVICE_FLAG createDeviceFlag = (D3D11_CREATE_DEVICE_FLAG)0;
#endif

			D3D_FEATURE_LEVEL dfl;
			for (unsigned int i = 0; i < vAdapters.size(); i++) {
				HRESULT hr = D3D11CreateDevice(vAdapters[i],
					D3D_DRIVER_TYPE_UNKNOWN,
					NULL,
					createDeviceFlag,
					NULL,
					0,
					D3D11_SDK_VERSION,
					&m_pD3DDevice,
					&dfl,
					&m_pImmediateContext);
				if (hr == S_OK) {
					break;
				}
			}
			if (i == (vAdapters.size() - 1))return false;

			DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;
			MSAA.Count = 1;
			MSAA.Quality = 0;
			{
				int smpl = 1;
				while (smpl > 0) {
					smpl <<= 1;
					if (smpl > D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT)break;
					unsigned int quality_level;
					hr = m_pD3DDevice->CheckMultisampleQualityLevels(format, smpl, &quality_level);
					if (hr == S_OK) {
						if (0 < quality_level) {
							DXGI_SAMPLE_DESC sd;
							sd.Count = smpl;
							sd.Quality = quality_level-1;
							vMSAA.push_back(sd);
							if (msaa == smpl) {
								MSAA = sd;
							}
						}
					}
				}
			}

			DXGI_SWAP_CHAIN_DESC desc;
			memset(&desc, 0, sizeof(desc));

			desc.BufferDesc.Width = w;
			desc.BufferDesc.Height = h;

			desc.BufferDesc.RefreshRate.Numerator = 0;
			desc.BufferDesc.RefreshRate.Denominator = 1;

			desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
			desc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
			desc.SampleDesc = MSAA;
			desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
			desc.BufferCount = 2;
			desc.OutputWindow = hWnd;
			desc.Windowed = TRUE;
			desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			GF_DX_RESULT(pFactory->CreateSwapChain(m_pD3DDevice,
				&desc,
				&m_pSwapChain),
				"CreateSwapChain");

			//フルスクリーン解除は
			//GF_DX_RESULT(pFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER),
			//	"pFactory->MakeWindowAssociation");

			GF_DX_RESULT(m_pSwapChain->GetBuffer(0, __uuidof(*m_pBackBuffer), (LPVOID*)&m_pBackBuffer),
				"CreateSwapChain->GetBuffer");
			GF_DX_RESULT(m_pD3DDevice->CreateRenderTargetView( m_pBackBuffer, NULL, &m_pRenderTargetView),
				"CreateRenderTargetView");

			//DEPTH
			D3D11_TEXTURE2D_DESC		depthDesc;
			memset(&depthDesc, 0, sizeof(depthDesc));
			depthDesc.Width  = w;
			depthDesc.Height = h;
			depthDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			depthDesc.MipLevels = 1;
			depthDesc.ArraySize = 1;
			depthDesc.MiscFlags = 0;
			depthDesc.SampleDesc = desc.SampleDesc;

			depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			depthDesc.CPUAccessFlags = 0;
			depthDesc.Usage = D3D11_USAGE_DEFAULT;
			GF_DX_RESULT(m_pD3DDevice->CreateTexture2D(&depthDesc, NULL, &m_pDepthBuffer), "CreateTexture2D");
						 
			D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
			memset(&dsv_desc, 0, sizeof(dsv_desc));
			dsv_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			dsv_desc.ViewDimension = (depthDesc.SampleDesc.Count > 1)? D3D11_DSV_DIMENSION_TEXTURE2DMS:D3D11_DSV_DIMENSION_TEXTURE2D;
			GF_DX_RESULT(m_pD3DDevice->CreateDepthStencilView(m_pDepthBuffer,
															  &dsv_desc,
															  &m_pDepthStencilView), "CreateDepthStencilView");


			m_pImmediateContext->OMSetRenderTargets( 1, &m_pRenderTargetView, m_pDepthStencilView );


			D3D11_VIEWPORT vp;
			vp.Width = (float)w;
			vp.Height = (float)h;
			vp.MinDepth = 0;
			vp.MaxDepth = 1;
			vp.TopLeftX = 0;
			vp.TopLeftY = 0;
			m_pImmediateContext->RSSetViewports(1, &vp);

			return true;
		}

		virtual void Update(void){
		}

		virtual void Begin(void) {

		}

		virtual void Submit(void) {
			m_pSwapChain->Present(0, 0);
		}

		virtual void *GetDevice(void){
			return m_pD3DDevice;
		}

		virtual void *GetContext(void){
			return m_pImmediateContext;
		}

		virtual void GetScreenSize(unsigned short *w, unsigned short *h){
			*w = mWidth;
			*h = mHeight;
		}

		virtual void SetScreenSize(unsigned short w, unsigned short h) {
			mWidth = w;
			mHeight = h;
		}

		virtual void SetViewport(unsigned short w, unsigned short h){
			D3D11_VIEWPORT vp;
			vp.Width = (float)w;
			vp.Height = (float)h;
			vp.MinDepth = 0;
			vp.MaxDepth = 1;
			vp.TopLeftX = 0;
			vp.TopLeftY = 0;
			m_pImmediateContext->RSSetViewports(1, &vp);
		}

		virtual void Clear(float *clearColor){
			m_pImmediateContext->ClearRenderTargetView(m_pRenderTargetView,
													   clearColor);
			m_pImmediateContext->ClearDepthStencilView(m_pDepthStencilView,
													   D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 0);
		}

	};

	bool ISystem::CreateDevice_DX11(void *param, unsigned short w, unsigned short h, unsigned char msaa){
		DeviceDX11 *p = new DeviceDX11();
		p->Init(param, w, h, msaa);
		IDevice::SetInstance(p);
		return true;
	}


}
