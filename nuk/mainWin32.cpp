﻿//-----------------------------------------------------------------------------
// File: mainWin32.cpp
//
// Desc: 
//
// Copyright (C) 2017 Monstars Rights Reserved.
//-----------------------------------------------------------------------------
#define    __mainWin32_cpp__

#include <windows.h>
#include <windowsx.h>
#include <winuser.h>
#include <assert.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <codecvt> 

#include <d3d11.h>

#include <iostream> 
#include <stdexcept>

#include "resource.h"

#include "gf_system.h"
#include "gf_device.h"
#include "gf_input.h"
#include "gf.h"

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
#define CLASSNAME   TEXT("TEST")
#define TITLENAME   TEXT("TEST")

#define SCREEN_WIDTH  1920
#define SCREEN_HEIGHT 1080
#define MSAA   (4)

HWND ghWnd;
HWND ghParentWnd = (HWND)-2;

#define PIPE_NAME   L"\\\\.\\pipe\\EditForms"
#define PIPE_NAMES   L"\\\\.\\pipe\\EditFormsS"
class ConnectModule{

private:
	HANDLE hPipe;
	HANDLE hPipeRecv;
	HANDLE mhThread;
	bool bActive;
	bool bConnected;

	int errorResult;
	int connectClientID;

	enum { bufferMAX = 0x4000 };
	char szBuff[bufferMAX];
	DWORD dwBytesRW;

	char szBuffSend[bufferMAX];
	DWORD dwBytesSend;

	HANDLE     hEvent;

	std::string errorCommand;

public:
	ConnectModule(){
		hPipe = NULL;
		mhThread = NULL;
		bActive = false;
		bConnected = false;
		errorResult = 0;
		connectClientID = 0;

		hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	}
	virtual ~ConnectModule(){
	}

	bool updateServer(void) {
		OVERLAPPED ov;
		ZeroMemory(&ov, sizeof(OVERLAPPED));
		ov.hEvent = hEvent;

		hPipeRecv = CreateNamedPipeW(
			PIPE_NAMES,
			PIPE_ACCESS_INBOUND,//PIPE_ACCESS_DUPLEX,
			PIPE_TYPE_BYTE,
			1,
			0,
			0,
			100,
			NULL);

		ConnectNamedPipe(hPipeRecv, &ov);
		WaitForSingleObject(hEvent, INFINITE);

		if (!ReadFile(hPipeRecv, szBuff, sizeof(szBuff), &dwBytesRW, NULL)) {
			DisconnectNamedPipe(hPipeRecv);
			return false;
		}
		DisconnectNamedPipe(hPipeRecv);
		CloseHandle(hPipeRecv);

		//文字列からコマンド処理に
		{
			szBuff[dwBytesRW] = 0; //cut data
			std::vector<std::string> result;
			std::string field;
			std::istringstream stream(szBuff);
			while (std::getline(stream, field, '\n'))result.push_back(field);
			for (std::vector<std::string>::iterator itr = result.begin();
				itr != result.end(); ++itr)
			{
				//OutputDebugString((*itr).c_str());
				//OutputDebugString("\n");

				std::string cmd;
				std::string rep;
				try {
					auto jpt = json::parse(*itr);
					cmd = jpt.at("type").get<std::string>();
					rep = jpt.at("value").get<std::string>();
				}
				catch (...) {
					break;
				}

				if (cmd == "wnd") { //Parent Window
					if (GetParent(ghWnd) != ghParentWnd) {
						void *wh = 0;
						if (sscanf(rep.c_str(), "%x", (unsigned int*)&wh) == 1) {
							HWND parent = (HWND)wh;
							SetParent(ghWnd, parent);
							ghParentWnd = parent;
							::SetWindowLong(ghWnd, GWL_STYLE, WS_VISIBLE | WS_POPUP);
							InvalidateRect(parent, NULL, TRUE);
						}
					}
				}
				else if (cmd == "sys") { //System Command
					if(rep == "play"){
						GF::ISystem::GetInstance()->Pause(false);
					}else if(rep == "stop"){
						GF::ISystem::GetInstance()->Pause(true);
					}
				}
				else if (cmd == "edit") { //Edit Window
					GF::IEditValue::GetInstance()->EditedCommand(rep);
				}
				else if (cmd == "list") { //Edit Window
					GF::IEditValue::GetInstance()->SelectedCommand(rep);
				}
				else if (cmd == "module") { //Boot Module
					GF::ITaskManager::GetInstance()->Clear();
					GF::ITaskManager::GetInstance()->Start(rep.c_str());
				}
				else
				{

				}
			}
		}
		return true;
	}

	static void *_updateServer(void *param){
		ConnectModule *p = (ConnectModule*)param;

		while(p->bActive){
			p->updateServer();
		}

		ExitThread(0);
		return NULL;
	}

	void CreateRecver(void){
		bConnected = false;
		bActive = true;
		mhThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)_updateServer,
								(void*)this, 0, NULL);
	}

	void SendMessage(const std::string &message){
		bool errorRetry = false;
		if (message.empty()) {
			if (errorCommand.empty())return;
			else errorRetry = true;
		}

		HANDLE hPipeSend = CreateFileW(PIPE_NAME,
									   GENERIC_WRITE,
									   0,
									   NULL,
									   OPEN_EXISTING,
									   0,
									   NULL);
		if (hPipeSend == INVALID_HANDLE_VALUE) {
			if (errorCommand.empty())errorCommand += message;
			else {
				errorCommand += "\n";
				errorCommand += message;
			}
			return;
		}
		if (errorRetry) {
			strncpy(szBuffSend, errorCommand.c_str(), bufferMAX);
			errorCommand.clear();
		}
		else {
			strncpy(szBuffSend, message.c_str(), bufferMAX);
		}
		dwBytesSend = (DWORD)strlen(szBuffSend);
		szBuff[dwBytesSend] = 0;
		if(!WriteFile(hPipeSend, szBuffSend, dwBytesSend, &dwBytesSend, NULL)){
			return;
		}
		CloseHandle(hPipeSend);
	}

};
ConnectModule sCm;

static void mouse_window_inside(HWND hWnd, bool b, bool bForce = false){
	static bool b_locked = false;
	if(b){
		if(!b_locked || bForce){
			RECT rc;
			HWND hParent = NULL;// ::GetParent(hWnd);
			if (hParent != NULL){
				::GetWindowRect(hParent, &rc);
			}
			else{
				::GetWindowRect(hWnd, &rc);
			}
			BOOL res = ::ClipCursor(&rc);
			b_locked = true;
		}
	}else{
		if(b_locked || bForce){
			BOOL res = ::ClipCursor(NULL);
			b_locked = false;
		}
	}
}

static void showCursor(HWND hWnd, bool bShow = false, bool bForceUpdate = false) {
	static bool bLast = false;

	if (bShow == bLast && !bForceUpdate) return;

	int curInfo = 0;
	CURSORINFO cur;
	cur.cbSize = sizeof(CURSORINFO);
	if(::GetCursorInfo(&cur)){
		curInfo = (cur.flags == CURSOR_SHOWING)?1:-1;
	}

	if (bShow && (curInfo != 1)) {
		while (::ShowCursor(true) < 0);
		bLast = true;
	}
	else if (!bShow && (curInfo != -1)) {
		while (::ShowCursor(false) >= 0);
		bLast = false;
	}
}


void TestMonitorHZ(void){
	DISPLAY_DEVICE device;
	DEVMODE devmode;
	int dev_id = 0;
	device.cb = sizeof(DISPLAY_DEVICE);
	devmode.dmSize = sizeof(DEVMODE);
	int idx = 0;

	if (EnumDisplayDevices(NULL,
						   idx,
						   &device,
						   NULL)){
		if (EnumDisplaySettings(device.DeviceName, ENUM_CURRENT_SETTINGS, &devmode) != 0){
			//gWindowMonitorHZ = (int)devmode.dmDisplayFrequency;
		}
	}
}

//-----------------------------------------------------------------------------
// Name : MsgProc
// Desc : 
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam){

	int xPos = GET_X_LPARAM(lParam);
	int yPos = GET_Y_LPARAM(lParam);
	static HWND s_hParent = NULL;

	switch (msg){
	case WM_COMMAND:
		switch(LOWORD(wParam)){
		case 4000:
		    {
			}
			break;
		default:
			break;
		}
		break;
	case WM_DISPLAYCHANGE:
		TestMonitorHZ();
		break;
	case WM_WINDOWPOSCHANGED:
	    {
		}
		break;
	case WM_DPICHANGED:
		break;
	case WM_SIZE:
		{
			switch (wParam){
			case SIZE_RESTORED:
				if (GF::IDevice::GetInstance() != NULL) {
					GF::IDevice::GetInstance()->SetScreenSize(lParam & 0xFFFF, (lParam >> 16) & 0xFFFF);
				}
				break;
			case SIZE_MINIMIZED:
				break;
			default:
				break;
			}
		}
		break;
	case WM_KEYDOWN:
		if (wParam == VK_F5) {
			GF::IShaderManager::GetInstance()->Reload();
			GF::IPipelineManager::GetInstance()->Reload();
		}
		break;
	case WM_KEYUP:
		break;
	case WM_NCMOUSELEAVE:
		//mouse_window_inside(hWnd, false, true);
		break;
	case WM_NCMOUSEMOVE:
		break;
	case WM_MOUSEMOVE:
		GF::IInput::GetInstance()->SetEvent(0, xPos, yPos);
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_LBUTTONUP:
		break;
	case WM_RBUTTONDOWN:
		break;
	case WM_RBUTTONUP:
		break;
	case WM_MBUTTONDOWN:
		break;
	case WM_XBUTTONDOWN:
		switch (GET_KEYSTATE_WPARAM(wParam)){
		case MK_XBUTTON1:
			break;
		case MK_XBUTTON2:
			break;
		default:
			break;
		}
		break;
	case WM_MOUSEWHEEL:
		if (GET_WHEEL_DELTA_WPARAM(wParam) > 0) {
			GF::IInput::GetInstance()->SetEvent(4, xPos, yPos);
		}else if (GET_WHEEL_DELTA_WPARAM(wParam) < 0){
			GF::IInput::GetInstance()->SetEvent(8, xPos, yPos);
		}
		break;
	case WM_SYSKEYDOWN:
		if (wParam == VK_RETURN){
			SendMessage(hWnd, WM_COMMAND, 4000, 0);
		}
		break;
    case WM_ERASEBKGND:
        return 0;
    case WM_CREATE:
		s_hParent = GetParent(hWnd);

		ShowCursor(FALSE); //

		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		return 0;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        return 0;
	}
    return DefWindowProc(hWnd, msg, wParam, lParam);
}

//-----------------------------------------------------------------------------
// Name : GetProcessTime
// Desc :
//-----------------------------------------------------------------------------
unsigned int GetProcessTime(void){
	LARGE_INTEGER	freq;
	LARGE_INTEGER	counter;
	QueryPerformanceCounter(&counter);
	QueryPerformanceFrequency(&freq);
	return (unsigned int)((double)counter.QuadPart * 1000000.f / (double)freq.QuadPart);
}
static unsigned int processTime = GetProcessTime();


void _Log(const char *log) {
	std::string editLog("log:");
	editLog += log;

	OutputDebugString(log);
	sCm.SendMessage(editLog.c_str()); //Editor 転送
}

//-----------------------------------------------------------------------------
// Name : WinMain
// Desc : 
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE hInstPrev,
                    LPSTR lpCmdLine, INT nCmdShow){

    CoInitializeEx(NULL, COINIT_MULTITHREADED);

	GF::ISystem::CreateInstance();
	GF::CustomLog = _Log; //ログ登録

	std::string bootModule;
	int nArgc = 0;
	LPWSTR *pArgv = ::CommandLineToArgvW(::GetCommandLineW(), &nArgc);
	for (int i = 0; i < nArgc; i++){
		std::wstring arg = std::wstring(pArgv[i]);
		if (arg.substr(0, 5) == L"boot:") {

			std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
			bootModule = cv.to_bytes(arg.substr(5, -1));
		}
	}

	{//DPI
#if 1
		SetProcessDPIAware();
#else
		HMODULE ShCoreDll = LoadLibrary("shcore.dll");
		if(ShCoreDll != NULL){
			typedef HRESULT(STDAPICALLTYPE *SetProcessDpiAwarenessProc)(int Value);
			SetProcessDpiAwarenessProc SetProcessDpiAwareness = 
				(SetProcessDpiAwarenessProc)::GetProcAddress(ShCoreDll, "SetProcessDpiAwareness");
			::FreeLibrary(ShCoreDll);
			if (SetProcessDpiAwareness){
				SetProcessDpiAwareness(2);
			}
		}else{
			SetProcessDPIAware();
		}
#endif
	}

    WNDCLASSEX wc = {sizeof(WNDCLASSEX), CS_DBLCLKS, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), 
                     NULL,
					 LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR1)),
                     ::GetSysColorBrush(COLOR_3DDKSHADOW),
                     NULL,
                     CLASSNAME, NULL};
    RegisterClassEx(&wc);

    HWND hWnd = CreateWindow(CLASSNAME, TITLENAME,
							 WS_OVERLAPPEDWINDOW, 
							 CW_USEDEFAULT, CW_USEDEFAULT,
							 SCREEN_WIDTH/2 + 
							 15,
							 SCREEN_HEIGHT/2 + 
							 38,
							 GetDesktopWindow(),
							 NULL, wc.hInstance, NULL);
	ghWnd = hWnd;

	RECT rc;
	GetClientRect(hWnd, &rc);
	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);

	//EDITOR Check
	ConnectModule cm;
	sCm.CreateRecver();
	{
		json j;
		j[GF::EDIT_CMD_TYPE] = "wnd";
		j[GF::EDIT_CMD_VALUE] = TITLENAME;
		sCm.SendMessage(j.dump());
	}

	bool bReady = true;
	try {  
		//初期化
		GF::ISystem::GetInstance()->CreateDevice_DX11((void*)hWnd, SCREEN_WIDTH, SCREEN_HEIGHT, MSAA);
		if (bootModule.empty()) {
			GF::ISystem::GetInstance()->Init();
		}
		else {
			GF::ISystem::GetInstance()->Init(bootModule.c_str());
		}
	}
	catch(const GF::Exception &cause) {  
		//DX ERROR
		(void)cause;
#if _DEBUG
		OutputDebugString(cause.what());
#endif
		//再起動処理
		bReady = false;
	}


	{
		std::vector<std::string> moduleNames;
		GF::ITaskManager::GetInstance()->GetModuleNames(moduleNames);
		json m;
		std::string str;
		for (std::vector<std::string>::iterator itr = moduleNames.begin();
			itr != moduleNames.end(); ++itr) {
			m["name"] += *itr;
		}
		GF::IEditValue::GetInstance()->CustomCommand(str, "module", m.dump());
		sCm.SendMessage(str);
	}



	HACCEL hAccel = NULL;
	{
		ACCEL  wAccel[1];
		wAccel[0].cmd = 4000;
		wAccel[0].key = VK_RETURN;
		wAccel[0].fVirt = FALT | FVIRTKEY;
		hAccel = CreateAcceleratorTable(wAccel, 1);
	}
	showCursor(hWnd, true);

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while( msg.message!=WM_QUIT ){
		//TranslateAccelerator(hWnd, hAccel, &msg);
		if(PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE )){
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}else{
			
			showCursor(hWnd, true);

			//通常処理
			try {

				bool bUpdate = true;
				{
					//タイミング調整
					unsigned int nowTime = GetProcessTime();
					unsigned int fps60 = 1000000 / 60;
					if((nowTime - processTime) >= fps60){ //FPS60
						processTime = nowTime;
					}else{
						bUpdate = false;
					}
				}
				if(bUpdate){
					GF::ISystem::GetInstance()->Update();
					{
						static int _update_frame = 0;
						if ( (++_update_frame) >= 60){
							GF::IEditValue::GetInstance()->UpdateSelectedCommand();
							_update_frame = 0;
						}
					}
					std::string cmds;
					GF::IEditValue::GetInstance()->GetCommand(cmds);
					sCm.SendMessage(cmds); //Editor 転送
				}
			}
			catch (const GF::Exception &cause) {
				//DX ERROR
				OutputDebugString(cause.what());
				//再起動処理
				::PostMessage(hWnd, WM_CLOSE, 0, 0);
			}
		}
	}

	DestroyAcceleratorTable(hAccel);


	GF::ISystem::GetInstance()->Release();

    CoUninitialize();

	return 0;
}

