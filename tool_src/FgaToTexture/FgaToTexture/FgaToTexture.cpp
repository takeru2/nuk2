// FgaToTexture.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include <windows.h>
#include <string>
#include <vector>

#include "png/lodepng.h";

//-----------------------------------------------------------------------------
// Name : file
// Desc : 
//-----------------------------------------------------------------------------
bool writeFile(const char *filename, void *buffer, size_t fsize)
{
	FILE *fp;
	size_t wsize;
	fp = fopen(filename, "wb");
	if (!fp)return false;
	wsize = fwrite(buffer, sizeof(char), fsize, fp);
	fclose(fp);
	if (wsize == fsize)return true;
	return false;
}

bool readFile(const char *filename, char **buffer, size_t *size)
{
	HANDLE hFile;
	DWORD file_size, read;
	*buffer = NULL;
	*size = 0;
	hFile = CreateFile(filename,
		GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		file_size = GetFileSize(hFile, NULL);
		*size = (size_t)file_size;
		*buffer = new char[*size + 1]; //0 end
		(*buffer)[*size] = 0;
		ReadFile(hFile, (LPVOID)*buffer, file_size, &read, NULL);
		CloseHandle(hFile);
		if (*size != read)
		{
			delete[] * buffer;
			return false;
		}
		return true;
	}
	return false;
}

void args_split(std::vector<float> &sp, const std::string &str, const std::string &key, int maxCount = 0)
{
	std::string::size_type si_st, si_fin;
	si_st = si_fin = 0;
	while ((si_fin = str.find(key, si_st)) != std::string::npos)
	{
		std::string sub = str.substr(si_st, si_fin - si_st);
		sp.push_back((float)atof(sub.c_str()));
		si_st = si_fin + 1;
		if (maxCount != 0)
		{
			if ((--maxCount) <= 0)return;
		}
	}
	{
		std::string sub = str.substr(si_st, -1);
		if (!sub.empty())sp.push_back((float)atof(sub.c_str()));
	}
}

struct value3
{
	float x, y, z;
};

inline void get_value3(value3 &val,
	const std::vector<float> &elems, unsigned int valueOffst,
	int ix, int iy, int iz,
	int gx, int gy, int gz)
{
	int index = iz * (gx * gy) + iy * gx + ix;
	if (index >= (gx * gy * gz))
	{
		val.x = val.y = val.z = 0;
	}
	else
	{
		val.x = elems[index * 3 + 0 + valueOffst];
		val.y = elems[index * 3 + 1 + valueOffst];
		val.z = elems[index * 3 + 2 + valueOffst];
	}
}

inline float clamp(float value, float min_value, float max_value)
{
	if (min_value > value)value = min_value;
	if (max_value < value)value = max_value;
	return value;
}

int main(int argc, char *argv[])
{
	int fileIndex = 1;
	int index = 1;

	int gridXYZ[3] = { -1,-1,0 };

	while (index < argc)
	{
		if (argv[index] != NULL)
		{
			if (argv[index][0] == '-')
			{ //オプション
				std::string opt = std::string(argv[index]);
				if (opt == "-x")
				{
					gridXYZ[0] = (int)atoi(argv[index + 1]);
					gridXYZ[1] = gridXYZ[2] = -1;
				}
				else if (opt == "-y")
				{
					gridXYZ[1] = (int)atoi(argv[index + 1]);
					gridXYZ[0] = gridXYZ[2] = -1;
				}
				else if (opt == "-z")
				{
					gridXYZ[2] = (int)atoi(argv[index + 1]);
					gridXYZ[0] = gridXYZ[1] = -1;
				}
				index++;
			}
			else
			{
				//ファイル名
				fileIndex = index;
				index++;
			}
		}
		else
		{
			index++;
		}
	}

	std::string fname(argv[fileIndex]);

	char *buf;
	size_t size;
	if (readFile(fname.c_str(), &buf, &size))
	{
		std::string fileText(buf);
		delete[] buf;

		std::vector<float> elems;
		args_split(elems, fileText, ",", 10);

		int gx = (int)elems[0];
		int gy = (int)elems[1];
		int gz = (int)elems[2];

		float nx = elems[3];
		float ny = elems[4];
		float nz = elems[5];

		float px = elems[6];
		float py = elems[7];
		float pz = elems[8];

		printf("grid: %d,%d,%d n:%f,%f,%f p:%f,%f,%f\n",
			gx, gy, gz,
			nx, ny, nz,
			px, py, pz);

		elems.clear();
		elems.reserve(gx * gy * gz * 3 + 10);
		args_split(elems, fileText, ",");

		std::vector<value3> values;
		int width = 0;
		int height = 0;

		value3 v3;

		if (gridXYZ[0] != -1)
		{
			width = gy;
			height = gz;
			values.reserve(width * height);
			for (int y = 0; y < gy; y++)
			{
				for (int z = 0; z < gz; z++)
				{
					get_value3(v3, elems, 9, gridXYZ[0], y, z, gx, gy, gz);
					values.push_back(v3);
				}
			}
		}
		else if (gridXYZ[1] != -1)
		{
			width = gx;
			height = gz;
			values.reserve(width * height);
			for (int x = 0; x < gx; x++)
			{
				for (int z = 0; z < gz; z++)
				{
					get_value3(v3, elems, 9, x, gridXYZ[1], z, gx, gy, gz);
					values.push_back(v3);
				}
			}
		}
		else if (gridXYZ[2] != -1)
		{
			width = gx;
			height = gy;
			values.reserve(width * height);
			for (int x = 0; x < gx; x++)
			{
				for (int y = 0; y < gy; y++)
				{
					get_value3(v3, elems, 9, x, y, gridXYZ[2], gx, gy, gz);
					values.push_back(v3);
				}
			}
		}

		float fmin = FLT_MAX;
		float fmax = FLT_MIN;
		for (std::vector<value3>::iterator itr = values.begin();
		itr != values.end(); ++itr)
		{

			//printf("%f,%f\n", itr->x, itr->y);

			if (itr->x < fmin)fmin = itr->x;
			else if (itr->x > fmax)fmax = itr->x;
			if (itr->y < fmin)fmin = itr->y;
			else if (itr->y > fmax)fmax = itr->y;
			if (itr->z < fmin)fmin = itr->z;
			else if (itr->z > fmax)fmax = itr->z;
		}

		for (std::vector<value3>::iterator itr = values.begin();
		itr != values.end(); ++itr)
		{
			itr->x = (itr->x - fmin) / (fmax - fmin);
			itr->y = (itr->y - fmin) / (fmax - fmin);
			itr->z = (itr->z - fmin) / (fmax - fmin);

			itr->x = clamp(itr->x, 0, 1);
			itr->y = clamp(itr->y, 0, 1);
			itr->z = clamp(itr->z, 0, 1);
		}

		//Png書き込み
		{
			std::vector<unsigned char> img;
			img.reserve(width * height * 4);
			LodePNG::Encoder encoder;
			encoder.getSettings().zlibsettings.useLZ77 = 0;
			img.clear();

			unsigned char *p_color = new unsigned char[width * height * 4];
			{
				unsigned int idx = 0;
				for (std::vector<value3>::iterator itr = values.begin();
				itr != values.end(); ++itr)
				{
					p_color[idx++] = (unsigned char)(itr->x * 255.f);
					p_color[idx++] = (unsigned char)(itr->y * 255.f);
					p_color[idx++] = (unsigned char)(itr->z * 255.f);
					p_color[idx++] = 255;
				}
			}
			unsigned char *pNewImage = (unsigned char*)p_color;
			encoder.encode(img, pNewImage, width, height);

			unsigned int size = (unsigned int)img.size();
			unsigned char *buffer = new unsigned char[size];
			for (unsigned int i = 0; i < size; i++)buffer[i] = img[i];

			{
				std::string outname = fname;
				size_t si;
				si = outname.find_last_of(".");
				if (si != std::string::npos)
				{
					outname.erase(si, -1);
					outname += ".png";
				}
				writeFile(outname.c_str(), buffer, size);
			}

			delete[] buffer;
			delete[] p_color;
		}

	}

	return 0;
}

