﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.IO;
using System.IO.Pipes;
using Newtonsoft.Json;

namespace EditForms
{

    class PipeServer
    {
        public class EditCommand
        {
            public string type { get; set; }
            public string value { get; set; }
        }
        public class EditCommandParam
        {
            public int id { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string value { get; set; }
        }
        public class EditCommandParamList
        {
            public List<EditCommandParam> list = null;
        }

        public class CommandNames
        {
            public List<string> name = null;
        }
 
        private static int numThreads = 2;
        private static bool b_active = true;
        public Thread[] servers;
        public static ManualResetEvent waitHandle = new ManualResetEvent(false);

        public static String windowPID = null;
        public static string sendCommands = null;
        public static PipeServer Instance;
        public int currentPropertyID_Tag = -1;

        private ReaderWriterLock rwLock = new ReaderWriterLock();

        public PipeServer(String pipeName)
        {
            Instance = this;
            servers = new Thread[numThreads];
            for (int i = 0; i < numThreads; i++)
            {
                if (i == 0)
                {
                    servers[i] = new Thread(ServerThreadRecv);
                    servers[i].Start(pipeName);
                    servers[i].IsBackground = true;
                } else
                {
                    servers[i] = new Thread(ServerThreadSend);
                    servers[i].Start(pipeName + "S");
                    servers[i].IsBackground = true;
                }
            }

        }
        public void Stop()
        {
            for (int i = 0; i < numThreads; i++)
            {
                servers[i].Suspend();
            }
        }

        private void jsonToString(ref string value)
        {
            value = value.Replace('{', '<');
            value = value.Replace('}', '>');
            value = value.Replace('[', '(');
            value = value.Replace(']', '(');
        }

        private void stringToJson(ref string value)
        {
            value = value.Replace('<', '{');
            value = value.Replace('>', '}');
            value = value.Replace('(', '[');
            value = value.Replace(')', ']');
        }

        private void editCommand(string value)
        {

            bool redraw = false;
            stringToJson(ref value);
            EditCommandParamList cmds = JsonConvert.DeserializeObject<EditCommandParamList>(value);
            foreach (EditCommandParam cmd in cmds.list)
            {
                if (cmd.type == "float")
                {
                    redraw |= FormBase.Instance.formProperty.Add(cmd.id, cmd.name, float.Parse(cmd.value));
                }
                else if(cmd.type == "int")
                {
                    redraw |= FormBase.Instance.formProperty.Add(cmd.id, cmd.name, int.Parse(cmd.value));
                }
                else if (cmd.type == "bool")
                {
                    redraw |= FormBase.Instance.formProperty.Add(cmd.id, cmd.name, bool.Parse(cmd.value));
                }
                else if (cmd.type == "color")
                {
                    redraw |= FormBase.Instance.formProperty.Add(cmd.id, cmd.name, System.Drawing.Color.FromArgb(int.Parse(cmd.value)));
                }
                else if (cmd.type == "string")
                {
                    redraw |= FormBase.Instance.formProperty.Add(cmd.id, cmd.name, cmd.value);
                }
            }
            if (redraw)
            {
                FormBase.Instance.formProperty.PropertyUpdate();
            }
        }

        private void uneditCommand(string value)
        {
        }

        private void cheditCommand(string value)
        {
        }

        private void setList(string value)
        {
            stringToJson(ref value);
            EditCommandParam cmd = JsonConvert.DeserializeObject<EditCommandParam>(value);

            System.Windows.Forms.ListViewItem listItem = new System.Windows.Forms.ListViewItem();
            listItem.Name = cmd.name;
            listItem.Text = cmd.name;
            listItem.Tag = cmd.id;

            FormBase.Instance.formList.listView.BeginUpdate();
            { //連続追加
                FormBase.Instance.formList.listView.Items.Add(listItem);
            }
            FormBase.Instance.formList.listView.EndUpdate();
        }

        private void setUnList(string value)
        {
            stringToJson(ref value);
            EditCommandParam cmd = JsonConvert.DeserializeObject<EditCommandParam>(value);

            if(currentPropertyID_Tag == cmd.id)
            {
                //現在のpropery消す
                FormBase.Instance.formProperty.PropertyClear();
                FormBase.Instance.formProperty.PropertyUpdate();
            }

            for (int i = 0; i < FormBase.Instance.formList.listView.Items.Count; i++) {
                if((int)FormBase.Instance.formList.listView.Items[i].Tag == cmd.id)
                {
                    FormBase.Instance.formList.listView.Items.RemoveAt(i);
                    return;
                }
            }

        }

        private void setModule(string value)
        {

            setSysCommand("clear");

            stringToJson(ref value);
            CommandNames cmd = JsonConvert.DeserializeObject<CommandNames>(value);
            foreach(string name in cmd.name)
            {
                FormBase.Instance.AddMenu(name, name);
            }
        }


        private void setWnd(string value)
        {
            IntPtr prev = FormBase.Instance.childWindow;
            FormBase.Instance.childWindow = FormBase.FindWindow(value, value);
            if(prev != FormBase.Instance.childWindow)
            {
                FormBase.Instance.formDX.Text = value;

                setSysCommand("clear");
                FormBase.Instance.formDX.Init();
            }
        }

        private void setSysCommand(string value)
        {
            if(value == "clear")
            {
                FormBase.Instance.formList.listView.Items.Clear();
                FormBase.Instance.formProperty.editProperties.ClearAll();
                FormBase.Instance.formProperty.PropertyUpdate();

                FormBase.Instance.ClearMenu();

               currentPropertyID_Tag = -1;
            }
        }

        private void parse_command_thread(EditCommand cmd)
        {
            if (!FormBase.bActive) return;

            //Command Exec
            if (cmd.type == "list")
            {
                SetTextCallback d = new SetTextCallback(setList);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "unlist")
            {
                SetTextCallback d = new SetTextCallback(setUnList);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "module")
            {
                SetTextCallback d = new SetTextCallback(setModule);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "edit")
            {
                SetTextCallback d = new SetTextCallback(editCommand);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "unedit")
            {
                SetTextCallback d = new SetTextCallback(uneditCommand);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "chedit")
            {
                SetTextCallback d = new SetTextCallback(cheditCommand);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "wnd")
            {
                SetTextCallback d = new SetTextCallback(setWnd);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }
            else if (cmd.type == "sys")
            {
                SetTextCallback d = new SetTextCallback(setSysCommand);
                FormBase.Instance.Invoke(d, new object[] { cmd.value });
            }

        }

        delegate void SetTextCallback(string text);

        private void ServerThreadRecv(object data)
        {
            do
            {
                var server = new NamedPipeServerStream(data.ToString());
                server.WaitForConnection();

                // メッセージを受信（読み込み）
                using (var reader = new StreamReader(server))
                {
                    //Console.WriteLine("Receive");
                    String message = "";
                    while(reader.Peek() >= 0) {

                        if (!FormBase.bActive)
                        {
                            return;
                        }

                        message += reader.ReadLine();

                        string head = "";
                        if(message.Length > 4) {
                            head = message.Substring(0, 4);
                        }

                        if (head == "log:")
                        {
                            LogThread(message.Substring(4));
                            message = "";
                        }
                        else
                        {

                            EditCommand cmd;
                            try
                            {
                                cmd = JsonConvert.DeserializeObject<EditCommand>(message);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                continue;
                            }
                            message = "";
                            if (cmd == null) break;

                            parse_command_thread(cmd);
                        }
                    }

                }
                server.Dispose();

                waitHandle.Set();

            } while (b_active);
        }

        private void ServerThreadSend(object data)
        {
            do
            {
                try
                {
                    waitHandle.WaitOne(Timeout.Infinite);
                    waitHandle.Reset();
                }
                catch (Exception e){
                    Console.WriteLine(e);
                    break;
                }

                //Console.WriteLine("Send");
                var client = new NamedPipeClientStream(".", data.ToString(),
                    PipeDirection.Out, PipeOptions.None);
                client.Connect();

                using (var writer = new StreamWriter(client))
                {

                    EditCommand cmd = new EditCommand();
                    cmd.type = "wnd";
                    cmd.value = windowPID;
                    var sends = JsonConvert.SerializeObject(cmd);

                    if (sendCommands != null)
                    {
                        sends += "\n";

                        {
                            rwLock.AcquireReaderLock(Timeout.Infinite);
                            {
                                sends += sendCommands;
                            }
                            sendCommands = null;
                            rwLock.ReleaseReaderLock();
                        }
                    }
                    writer.WriteLine(sends);
                    //Console.WriteLine(sends);
                    //LogThread(sends);
                }
                client.Dispose();
                //Console.WriteLine("Sended");

            } while (b_active);
        }
        public void AddCommand(string cmd)
        {
            {
                rwLock.AcquireReaderLock(Timeout.Infinite);
                {
                    if (sendCommands != null)
                    {
                        sendCommands += "\n" + cmd;
                    }
                    else
                    {
                        sendCommands = cmd;
                    }
                }
                rwLock.ReleaseReaderLock();
            }
            waitHandle.Set();
        }

        public void SendEditValue(string name, string value)
        {
            EditCommand ec = new EditCommand();
            ec.type = name;
            ec.value = value;
            string ec_string = JsonConvert.SerializeObject(ec);
            jsonToString(ref ec_string);

            EditCommand cmd = new EditCommand();
            cmd.type = "edit";
            cmd.value = ec_string;
            var sends = JsonConvert.SerializeObject(cmd);
            AddCommand(sends);
        }

        public void SendSysCommand(string value)
        {
            EditCommand cmd = new EditCommand();
            cmd.type = "sys";
            cmd.value = value;
            var sends = JsonConvert.SerializeObject(cmd);
            AddCommand(sends);
        }

        public void SetSelectModuleID(object id)
        {
            EditCommand cmd = new EditCommand();
            cmd.type = "module";
            cmd.value = id.ToString();
            var sends = JsonConvert.SerializeObject(cmd);
            AddCommand(sends);
        }

        public void SetSelectListID(object id)
        {
            if (currentPropertyID_Tag != (int)id)
            {
                //現在のpropery消す
                FormBase.Instance.formProperty.PropertyClear();
                FormBase.Instance.formProperty.PropertyUpdate();
            }

            currentPropertyID_Tag = (int)id;

            FormBase.Instance.formProperty.editProperties.ClearAll();

            EditCommandParam ec = new EditCommandParam();
            ec.id = (int)id;
            string ec_string = JsonConvert.SerializeObject(ec);
            jsonToString(ref ec_string);

            EditCommand cmd = new EditCommand();
            cmd.type = "list";
            cmd.value = ec_string;
            var sends = JsonConvert.SerializeObject(cmd);
            AddCommand(sends);
        }

        public void Log(String str)
        {
            String log = str.Replace('\\', '/');
            FormBase.Instance.formConsole.richTextBox.AppendText(log + "\n");
        }

        public void LogThread(String str)
        {
            SetTextCallback d = new SetTextCallback(Log);
            FormBase.Instance.Invoke(d, new object[] { str });
        }

    }
}
