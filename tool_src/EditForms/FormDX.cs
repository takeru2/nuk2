﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

namespace EditForms
{
    public partial class FormDX : DockContent
    {
        public FormDX()
        {
            InitializeComponent();

            PipeServer.windowPID = pictureBox.Handle.ToString("X");
        }

        public void Init()
        {
            playStop.Text = "stop";
            playStop.Checked = true;
        }

        private void sizeChanged(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if(FormBase.Instance.childWindow != (IntPtr)null)
            {
                if( (c.Width != 0) && (c.Height != 0))
                {
                    FormBase.MoveWindow(FormBase.Instance.childWindow, 0, 0, c.Width, c.Height, 1);
                }
            }
        }

        private void paint(object sender, PaintEventArgs e)
        {
            if (FormBase.bActive)
            {
                sizeChanged(sender, e);
            }
        }

        private void styleChanged(object sender, EventArgs e)
        {
            if (FormBase.bActive)
            { 
                PipeServer.windowPID = pictureBox.Handle.ToString("X");
            }
        }

        private void clickPlayStop(object sender, EventArgs e)
        {
            if (playStop.Checked)
            {
                playStop.Text = "stop";
                PipeServer.Instance.SendSysCommand("play");
            }
            else
            {
                playStop.Text = "play";
                PipeServer.Instance.SendSysCommand("stop");
            }
        }
    }
}
