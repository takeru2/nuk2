﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

namespace EditForms
{
    public partial class FormProperty : DockContent
    {
        public CustomClass editProperties = new CustomClass();
        public Dictionary<string, CustomProperty> editMap = new Dictionary<string, CustomProperty>();
        public FormProperty()
        {
            InitializeComponent();

            propertyGrid.SelectedObject = editProperties;

        }
        public void PropertyUpdate()
        {
            propertyGrid.Refresh();
        }
        public void PropertyClear()
        {
            editMap.Clear();
            editProperties.ClearAll();
        }
        public bool Add(int tag, string name, object value)
        {
            if (editMap.ContainsKey(name))
            {
                CustomProperty myProp = editMap[name];
                myProp.Value = value;
                return false;
            }
            else
            {
                CustomProperty myProp = new CustomProperty(tag, name, value, false, true);
                editProperties.Add(myProp);
                editMap.Add(name, myProp);
                return true;
            }
        }
        public void Remove(string tag)
        {
            editProperties.Remove(tag);
        }

        private void propertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // CustomProperty myProp = (CustomProperty)e.ChangedItem;
            Type t = e.ChangedItem.Value.GetType();
            if (t.Equals(typeof(System.Drawing.Color)))
            {
                System.Drawing.Color argb = (System.Drawing.Color)e.ChangedItem.Value;
                PipeServer.Instance.SendEditValue(e.ChangedItem.Label, argb.ToArgb().ToString());
            }
            else
            {
                PipeServer.Instance.SendEditValue(e.ChangedItem.Label, e.ChangedItem.Value.ToString());
            }
            CustomProperty prop = (CustomProperty)e.ChangedItem.Tag;

            Console.WriteLine("ChangeValue: {0}:{1}:{2}",
                e.ChangedItem.Label,
                e.OldValue.ToString(),
                e.ChangedItem.Value);
        }
    }
}
