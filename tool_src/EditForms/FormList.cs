﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

namespace EditForms
{
    public partial class FormList : DockContent
    {
        public FormList()
        {
            InitializeComponent();
        }

        private void selectedIndexChanged(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {
                int idx = listView.SelectedItems[0].Index;
                System.Windows.Forms.ListViewItem listItem = listView.Items[idx];
                PipeServer.Instance.SetSelectListID(listItem.Tag);
            }
        }
    }
}
