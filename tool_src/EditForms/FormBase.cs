﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using WeifenLuo.WinFormsUI.Docking;

using System.Runtime.InteropServices;

namespace EditForms
{
    public partial class FormBase : Form
    {
        public static FormBase Instance;

        static readonly String pipeName = "EditForms";
        static readonly String configFileName = "cfg/EditForms.xml";
        static bool firstBootFlag = false; //初回起動、Windowセーブデータ作成用フラグ

        PipeServer pipeServer;

        [DllImport("user32")]
        public extern static IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32")]
        public extern static int MoveWindow(IntPtr hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

        //Child Form
        public FormDX formDX = null;
        public FormProperty formProperty = null;
        public FormConsole formConsole = null;
        public FormList formList = null;
        public IntPtr childWindow = (IntPtr)null; //DX module
        public static bool bActive = false;

        public FormBase()
        {
            Instance = this;
            IsMdiContainer = true;

            InitializeComponent();

            if (firstBootFlag)
            {
                GetDockContentFromPersistString(typeof(FormDX).ToString());
                GetDockContentFromPersistString(typeof(FormProperty).ToString());
                GetDockContentFromPersistString(typeof(FormConsole).ToString());
                GetDockContentFromPersistString(typeof(FormList).ToString());
                formDX.Show(dockPanel, DockState.Float);
                formProperty.Show(dockPanel, DockState.Float);
                formConsole.Show(dockPanel, DockState.Float);
                formList.Show(dockPanel, DockState.Float);
            }

            bActive = true;
            pipeServer = new EditForms.PipeServer(pipeName);

        }

        private void formClosing(object sender, FormClosingEventArgs e)
        {
            dockPanel.SaveAsXml(configFileName);

            pipeServer.Stop();
            pipeServer = null;
            bActive = false;
        }

        private void formLoad(object sender, EventArgs e)
        {
            if (firstBootFlag) return;

            if (System.IO.File.Exists(configFileName))
            {
                DeserializeDockContent deserializeDockContent
                = new DeserializeDockContent(GetDockContentFromPersistString);
                dockPanel.LoadFromXml(configFileName, deserializeDockContent);
            }

        }

        IDockContent GetDockContentFromPersistString(string persistString)
  　　　{
            if (persistString.Equals(typeof(FormDX).ToString()))
            {
                formDX = new FormDX();
        　　　　return formDX;
            }
            if (persistString.Equals(typeof(FormProperty).ToString()))
            {
                formProperty = new FormProperty();
                return formProperty;
            }
            if (persistString.Equals(typeof(FormConsole).ToString()))
            {
                formConsole = new FormConsole();
                return formConsole;
            }
            if (persistString.Equals(typeof(FormList).ToString()))
            {
                formList = new FormList();
                return formList;
            }
            return null;
        }

        private void clickClose(object sender, EventArgs e)
        {
            this.Close();
        }

        private void contextMenuStrip_SubMenuClick(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            PipeServer.Instance.SetSelectModuleID(mi.Text);
        }

        public void AddMenu(string name, string tips)
        {
            ToolStripMenuItem tsi = new ToolStripMenuItem();
            tsi.Text = name;
            tsi.ToolTipText = tips;
            tsi.Click += contextMenuStrip_SubMenuClick;
            moduleMToolStripMenuItem.DropDownItems.Add(tsi);
        }
        public void ClearMenu()
        {
            moduleMToolStripMenuItem.DropDownItems.Clear();
        }
    }
}
