﻿//-----------------------------------------------------------------------------
// File: main.cpp
//
// Desc: 
//
// Copyright (C) 2010 Kode All Rights Reserved.
//-----------------------------------------------------------------------------
#define    __main_cpp__

#include <windows.h>
#include "convert.h"

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment( lib, "../x64/Debug/convertFBXd.lib" )
#else
#pragma comment( lib, "../x64/Release/convertFBX.lib" )
#endif

//-----------------------------------------------------------------------------
// Name : main
// Desc : 
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("----------------------------------------------\n");
		printf(" Model Convert Ver. 1.0   (c) Monstars.inc    \n");
		printf("----------------------------------------------\n");
		printf(" -scale <0.1f>  スケール変換  スケール  \n");
		printf(" -frame <start> <end> 出力フレーム指定\n");
		printf("----------------------------------------------\n");
		return -1;
	}
	convert(argc, argv);


	//出力画像の解像度を設定
	//ポリゴンを三角分割
	//ポリゴンの辺を重複なく拾得
	//各辺を出力画像(予備)のピクセルに割り当てる
	//出力画像のピクセルを設定

	int width = 256;
	int height = 256;










	return 0;
}