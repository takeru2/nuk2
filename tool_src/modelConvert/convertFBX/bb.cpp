

#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "bb.h"

class BB {
public:

	class BB_Node {
	public:
		bool bSkin;
		Matrix4 pivot;
		Matrix4 bind;
		Matrix4 local;
		Matrix4 transform;
		std::vector<Vector3> pos;
		std::vector<Vector4> bw;
		std::vector<unsigned int> bi;
	};
	class BB_Anim {
	public:
		BB_Node *node;
		std::vector<Matrix4> tr;
	};
	std::vector<BB_Node*> node_array;
	std::map<std::string, BB_Node*> nodes;
	std::map<std::string, BB_Anim*> anims;
	Matrix4 matrixArray[0x100];

	struct bb_MM{
		Vector4 min;
		Vector4 max;
	};
	std::vector<bb_MM> bb_mm;

	char *bb_array;
	unsigned int bb_array_size = 0;
	
	BB_Node *currentNode = NULL;
	BB_Anim *currentAnim = NULL;
public:
	BB(){
	}
	void test(Point3 *bb, const Point3 &wp){
		bb[0].setX(std::min(bb[0].getX(), wp.getX()));
		bb[0].setY(std::min(bb[0].getY(), wp.getY()));
		bb[0].setZ(std::min(bb[0].getZ(), wp.getZ()));

		bb[1].setX(std::max(bb[1].getX(), wp.getX()));
		bb[1].setY(std::max(bb[1].getY(), wp.getY()));
		bb[1].setZ(std::max(bb[1].getZ(), wp.getZ()));
	}

	void calcBB(Point3 *bb_all, Point3 *bb, BB_Node *node){
		if(node->bSkin){
			unsigned int num = (unsigned int)node->pos.size();
			for(unsigned int n=0;n<num;n++){
				Vector3 pos = node->pos[n];
				Vector4 bw  = node->bw[n];
				unsigned int bi4  = node->bi[n];
				unsigned int bi[4] = {
					(bi4 >> 0) & 0xff,
					(bi4 >> 8) & 0xff,
					(bi4 >> 16) & 0xff,
					(bi4 >> 24) & 0xff
				};
				Vector3 wp(0);
				wp += (matrixArray[bi[0]] * Vector4(pos, 1)).getXYZ() * bw.getX();
				wp += (matrixArray[bi[1]] * Vector4(pos, 1)).getXYZ() * bw.getY();
				wp += (matrixArray[bi[2]] * Vector4(pos, 1)).getXYZ() * bw.getZ();
				wp += (matrixArray[bi[3]] * Vector4(pos, 1)).getXYZ() * bw.getW();
				test(bb, Point3(wp));
				test(bb_all, Point3(wp));
			}
		}else{
			for(std::vector<Vector3>::iterator itr = node->pos.begin();
				itr != node->pos.end();++itr){
				Vector3 pos = *itr;
				Point3 wp = Point3((node->transform * Vector4(pos, 1)).getXYZ());
				test(bb, wp);
				test(bb_all, wp);
			}
		}
	}

	void GetBB(void){
		//AnimTransformSize
		if(anims.empty())return;

		Point3 bb_all[2];
		bb_all[0] = Point3(FLT_MAX, FLT_MAX, FLT_MAX);
		bb_all[1] = Point3(FLT_MIN, FLT_MIN, FLT_MIN);

		unsigned int animFrame = (unsigned int)anims.begin()->second->tr.size();
		for(unsigned int n=0;n<animFrame;n++){
			Point3 bb[2];

			bb[0] = Point3(FLT_MAX, FLT_MAX, FLT_MAX);
			bb[1] = Point3(FLT_MIN, FLT_MIN, FLT_MIN);

			for(std::map<std::string, BB_Anim*>::iterator itr = anims.begin();
				itr != anims.end();++itr){
				BB_Anim *anim = itr->second;
				anim->node->local = anim->tr[n];
				if(anim->node->pos.empty()){
					anim->node->transform = anim->node->local * anim->node->bind;
				}else{
					anim->node->transform = anim->node->local;
				}
			}
			unsigned int idx = 0;
			for(std::vector<BB_Node*>::iterator itr = node_array.begin();
				itr != node_array.end();++itr){
				if(idx >= sizeof(matrixArray)/sizeof(matrixArray[0]))break;
				matrixArray[idx++] = (*itr)->transform;
			}
			for(std::vector<BB_Node*>::iterator itr = node_array.begin();
				itr != node_array.end();++itr){
				if(!(*itr)->pos.empty()){
					calcBB(bb_all, bb, *itr);
				}
			}
			bb_MM bm;
			bm.min = Vector4(Vector3(bb[0]), 0);
			bm.max = Vector4(Vector3(bb[1]), 0);
			bb_mm.push_back(bm);

			printf("BB frame %d\n", n);
			printf("%f,%f,%f: %f,%f,%f\n",
				bm.min.getX(), bm.min.getY(), bm.min.getZ(),
				bm.max.getX(), bm.max.getY(), bm.max.getZ());
		}
		bb_array_size = (unsigned int)( (bb_mm.size() + 1) * sizeof(bb_MM));
		bb_array = new char[bb_array_size];
		unsigned int write_index = 0;

		{
			bb_MM bm;
			bm.min = Vector4(Vector3(bb_all[0]), 0);
			bm.max = Vector4(Vector3(bb_all[1]), 0);
			memcpy(&bb_array[write_index], &bm, sizeof(bb_MM));
			write_index += sizeof(bb_MM);

			printf("BB All\n");
			printf("%f,%f,%f: %f,%f,%f\n",
				bm.min.getX(), bm.min.getY(), bm.min.getZ(),
				bm.max.getX(), bm.max.getY(), bm.max.getZ());

		}

		for(std::vector<bb_MM>::iterator itr = bb_mm.begin();
			itr != bb_mm.end();++itr){
			bb_MM &bm = *itr;
			memcpy(&bb_array[write_index], &bm, sizeof(bb_MM));
			write_index += sizeof(bb_MM);
		}
	}
};
BB sBB;
static bool s_bEnable = true;

CalcBB::CalcBB(){
}

CalcBB::~CalcBB(){
}

void CalcBB::Enable(bool bEnable) {
	s_bEnable = bEnable;
}

void CalcBB::GetBB(void){
	if(!s_bEnable)return;
	
	sBB.GetBB();
}

void CalcBB::Node(const char *name) {
	if(!s_bEnable)return;
	std::map<std::string, BB::BB_Node*>::iterator itr = sBB.nodes.find(name);
	if(itr != sBB.nodes.end()){
		sBB.currentNode = itr->second;
	}else{
		sBB.currentNode = new BB::BB_Node();
		sBB.nodes.insert(std::pair<std::string, BB::BB_Node*>(name, sBB.currentNode));
		sBB.node_array.push_back( sBB.currentNode );
	}
}
void CalcBB::Pivot(const void *pivot) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	memcpy(&sBB.currentNode->pivot, pivot, sizeof(Matrix4));

}
void CalcBB::Skin(bool bSkin) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	sBB.currentNode->bSkin = bSkin;

}
void CalcBB::SetPos(const void *pos) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	Vector3 p;
	memcpy(&p, pos, sizeof(Vector3));
	sBB.currentNode->pos.push_back( p );

}
void CalcBB::SetBI(const void *bi) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	unsigned int _bi;
	memcpy(&_bi, bi, sizeof(unsigned int));
	sBB.currentNode->bi.push_back( _bi );

}
void CalcBB::SetBW(const void *bw) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	Vector4 _bw;
	memcpy(&_bw, bw, sizeof(Vector4));
	sBB.currentNode->bw.push_back( _bw );
}
void CalcBB::Bind(const char *name, const void *transform) {
	if(!s_bEnable || (sBB.currentNode == NULL))return;
	if (name == NULL) {
		memcpy(&sBB.currentNode->bind, transform, sizeof(Matrix4));
	}
	else {
		std::map<std::string, BB::BB_Node*>::iterator n_itr = sBB.nodes.find(name);
		if (n_itr != sBB.nodes.end()) {
			memcpy(&n_itr->second->bind, transform, sizeof(Matrix4));
		}
	}
}
void CalcBB::AnimNode(const char *name) {
	if(!s_bEnable)return;
	std::map<std::string, BB::BB_Anim*>::iterator itr = sBB.anims.find(name);
	if(itr != sBB.anims.end()){
		sBB.currentAnim = itr->second;
	}else{
		sBB.currentAnim = new BB::BB_Anim();
		std::map<std::string, BB::BB_Node*>::iterator n_itr = sBB.nodes.find(name);
		if(n_itr != sBB.nodes.end()){
			sBB.currentAnim->node = n_itr->second;
		}else{
			sBB.currentAnim->node = NULL;
		}
		sBB.anims.insert(std::pair<std::string, BB::BB_Anim*>(name, sBB.currentAnim));
	}
}
void CalcBB::AnimNodeTransform(int idx, const void *transform) {
	if(!s_bEnable || (sBB.currentAnim == NULL) )return;
	Matrix4 m;
	memcpy(&m, transform, sizeof(Matrix4));
	sBB.currentAnim->tr.push_back( m );

}
unsigned int CalcBB::GetBB_Size(void) {
	if(!s_bEnable)return 0;
	return 0;
}
unsigned int CalcBB::GetDataSize(void) {
	if(!s_bEnable)return 0;
	return sBB.bb_array_size;
}
const void *CalcBB::GetData(void) {
	if(!s_bEnable)return NULL;
	return sBB.bb_array;
}

