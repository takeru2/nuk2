﻿//-----------------------------------------------------------------------------
// File: triSplit.h
//
// Desc: 
//
// Copyright (C) 2012 Kode All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __triSplit_h__
#define    __triSplit_h__

#include <vector>

struct convTri
{
	char *vtx;
	unsigned int nbVtx;
};

void ConvertTriSplit(std::vector<convTri> &vTri, const char *vertex, unsigned int nbVertex,
	unsigned int stride, int splitX, int splitZ);

#endif // __triSplit_h__
