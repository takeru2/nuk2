﻿//-----------------------------------------------------------------------------
// File: convert.cpp
//
// Desc: 
//
// Copyright (C) 2013 Kode All Rights Reserved.
//-----------------------------------------------------------------------------
#define    __convert_cpp__

#include <windows.h>

#include <map>
#include <string>
#include <algorithm>

#include "math_utils.h"

#include "convert.h"


//-----------------------------------------------------------------------------
// Name : ExecCommand
// Desc :
//-----------------------------------------------------------------------------
DWORD ExecCommand(std::string &str, const char *lpCommandLine){

    OutputDebugString(lpCommandLine);

    HANDLE hReadPipe = NULL, hWritePipe = NULL;
    HANDLE hErrReadPipe = NULL, hErrWritePipe = NULL;

    SECURITY_ATTRIBUTES sa;
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.lpSecurityDescriptor = NULL;
    sa.bInheritHandle = TRUE;

    ::CreatePipe(&hReadPipe, &hWritePipe, &sa, 8192);

    ::CreatePipe(&hErrReadPipe, &hErrWritePipe, &sa, 8192);

    STARTUPINFO StartupInfo;
    PROCESS_INFORMATION ProcessInfo;

    ::ZeroMemory(&StartupInfo, sizeof(STARTUPINFO));
    StartupInfo.cb = sizeof(STARTUPINFO);
    StartupInfo.dwFlags = STARTF_USESTDHANDLES;
    StartupInfo.wShowWindow = SW_HIDE;
    StartupInfo.hStdOutput = hWritePipe;
    StartupInfo.hStdError  = hErrWritePipe;

    if (::CreateProcess(
            NULL,
            (LPSTR)lpCommandLine,
            &sa,
            NULL,
            TRUE,    // ハンドルの継承
            DETACHED_PROCESS, // DOS窓を表示しないための指定
            NULL,
            NULL,
            &StartupInfo, &ProcessInfo)){

        char bufStdOut[8192], bufErrOut[8192];
        DWORD dwStdOut = 0, dwErrOut = 0;
        DWORD dwRet;

        while ( (dwRet = ::WaitForSingleObject(ProcessInfo.hProcess, 0)) !=
                WAIT_ABANDONED){
            memset(bufStdOut, 0, sizeof(bufStdOut));
            memset(bufErrOut, 0, sizeof(bufErrOut));

            ::PeekNamedPipe(hReadPipe, NULL, 0, NULL, &dwStdOut, NULL);
            if (dwStdOut > 0){
                ::ReadFile(hReadPipe, bufStdOut, sizeof(bufStdOut) - 1, &dwStdOut,
                           NULL);

                OutputDebugString(bufStdOut);

                str += std::string(bufStdOut);

            }

            ::PeekNamedPipe(hErrReadPipe, NULL, 0, NULL, &dwErrOut, NULL);
            if (dwErrOut > 0){
                ::ReadFile(hErrReadPipe, bufErrOut, sizeof(bufErrOut) - 1, &dwErrOut,
                           NULL);
            }

            MSG msg;
            if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
                ::TranslateMessage(&msg);
                ::DispatchMessage(&msg);
            }
            if (dwRet == WAIT_OBJECT_0)
                break;
        }
    }else{
        char str[0x20];
        sprintf(str, "0x%x\n", GetLastError());
        OutputDebugString(str);
    }

    DWORD res;
    ::GetExitCodeProcess(ProcessInfo.hProcess, &res);
    CloseHandle(ProcessInfo.hProcess);
    CloseHandle(ProcessInfo.hThread);

    ::CloseHandle(hWritePipe);
    ::CloseHandle(hReadPipe);
    ::CloseHandle(hErrWritePipe);
    ::CloseHandle(hErrReadPipe);

    return res;
}

