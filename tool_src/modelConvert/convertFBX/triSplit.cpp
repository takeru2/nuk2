﻿//-----------------------------------------------------------------------------
// File: triSplit.cpp
//
// Desc: 
//
// Copyright (C) 2012 Kode All Rights Reserved.
//-----------------------------------------------------------------------------
#define    __triSplit_cpp__

#include "math_utils.h"

#include "triSplit.h"

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function-prototypes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Name : ConvertTriSplit
// Desc : 
//-----------------------------------------------------------------------------
void ConvertTriSplit(std::vector<convTri> &vTri, const char *vertex, unsigned int nbVertex,
	unsigned int stride, int splitX, int splitZ)
{
	//全体のサイズ
	float x_min = FLT_MAX;
	float x_max = -FLT_MAX;
	float z_min = FLT_MAX;
	float z_max = -FLT_MAX;

	struct vtxInfo
	{
		std::vector<const char*> vtxPtr;
	};
	std::vector<vtxInfo> vSplit;
	vSplit.resize(splitX * splitZ);

	const char *p = vertex;
	const float *fp;
	for (unsigned int n = 0; n < nbVertex; n++)
	{
		fp = reinterpret_cast<const float*>(p);
		if (fp[0] < x_min)x_min = fp[0];
		if (fp[0] > x_max)x_max = fp[0];

		if (fp[2] < z_min)z_min = fp[2];
		if (fp[2] > z_max)z_max = fp[2];

		p += stride;
	}
	float wx = x_max - x_min;
	float wz = z_max - z_min;

#if 1
	int idx = (int)(floor(wx / (float)splitX + 0.5f));
	int idz = (int)(floor(wz / (float)splitZ + 0.5f));
	idx += (idx & 1) ? 1 : 0; //偶数に
	idz += (idx & 1) ? 1 : 0;

	float dx = (float)idx;
	float dz = (float)idz;

	int ix_min = -idx * (splitX / 2);
	int iz_min = -idz * (splitZ / 2);

	//そもそも割り切れる？
	if (((int)wx % splitX) != 0)
	{ //割れない
		ix_min -= 1; //奇数から
	}

	if (((int)wz % splitZ) != 0)
	{ //割れない
		iz_min -= 1; //奇数から
	}

	float sx = (float)ix_min;
	float sz = (float)iz_min;
#endif

#if 0
	float sx = x_min;
	float sz = z_min;

	float dx = wx / (float)splitX;
	float dz = wz / (float)splitZ;
#endif

	//Triangle 分割
	unsigned int nbFace = nbVertex / 3;
	p = vertex;

	float x, z;
	const char *bp;
	for (unsigned int n = 0; n < nbFace; n++)
	{
		bp = p;
		fp = reinterpret_cast<const float*>(p);
		x = fp[0]; z = fp[2];
		p += stride;

		fp = reinterpret_cast<const float*>(p);
		//x += fp[0]; z += fp[2];
		x = Min(fp[0], x);
		z = Min(fp[2], z);

		p += stride;

		fp = reinterpret_cast<const float*>(p);
		//x += fp[0]; z += fp[2];
		x = Min(fp[0], x);
		z = Min(fp[2], z);

		p += stride;

		//重心だとまずい
		//x /= 3.f;
		//z /= 3.f;

		//
		int gx = Clamp((int)((x - sx) / dx), 0, splitX - 1);
		int gz = Clamp((int)((z - sz) / dz), 0, splitZ - 1);

		//今回の３頂点基点保存
		vSplit[gx + gz * splitX].vtxPtr.push_back(bp);
	}

	//    for(std::vector<vtxInfo>::iterator itr = vSplit.begin();
	//        itr != vSplit.end();++itr){
	for (std::vector<vtxInfo>::reverse_iterator itr = vSplit.rbegin();
		itr != vSplit.rend(); ++itr)
	{
		if (!itr->vtxPtr.empty())
		{
			convTri ct;
			ct.nbVtx = (unsigned int)itr->vtxPtr.size() * 3;
			ct.vtx = new char[ct.nbVtx * stride];
			char *wp = ct.vtx;
			for (std::vector<const char*>::iterator v_itr = itr->vtxPtr.begin();
				v_itr != itr->vtxPtr.end(); ++v_itr)
			{
				memcpy(wp, *v_itr, 3 * stride);
				wp += (3 * stride);
			}
			vTri.push_back(ct);
		}
	}
}

