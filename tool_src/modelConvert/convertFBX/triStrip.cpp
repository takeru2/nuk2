﻿//-----------------------------------------------------------------------------
// File: triStrip.cpp
//
// Desc: 
//
// Copyright (c) 2007 Kode. All rights reserved.
//-----------------------------------------------------------------------------
#define    __triStrip_cpp

#include <algorithm>
#include <set>
#include <map>
#include <functional>

#define _USE_MATH_DEFINES
#include <math.h>

#include <immintrin.h>
#include <emmintrin.h>

#include "math_utils.h"

#include "NvTriStrip.h"

#include "triStrip.h"

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
static unsigned int g_elem_num = 0;

//-----------------------------------------------------------------------------
// Function-prototypes
//-----------------------------------------------------------------------------




//-----------------------------------------------------------------------------
__declspec(noinline) void float2half(const float* floats, short* halfs, int n)
{
	float flt[4] = { 0 };
	for (int i = 0; i < n; i++)flt[i] = floats[i];
	__m256 float_vector = _mm256_load_ps(flt);
	__m128i half_vector = _mm256_cvtps_ph(float_vector, 0);
	short *ptr = (short*)&half_vector;
	for (int i = 0; i < n; i++)halfs[i] = ptr[i];
}

//-----------------------------------------------------------------------------
// Name : CalcStride
// Desc : 
//-----------------------------------------------------------------------------
unsigned int CalcStride(std::vector<int> &offsetElemArray)
{
	unsigned int nStride = 0;
	for (std::vector<int>::iterator itr = offsetElemArray.begin();
		itr != offsetElemArray.end(); ++itr)
	{
		int value = *itr;
		if (value < 0)
		{
			value = -value;
			nStride += sizeof(short) * value;
		}
		else
		{
			nStride += sizeof(float) * value;
		}
	}
	nStride = (nStride + (4 - 1))  & ~(4 - 1);
	return nStride;
}

//-----------------------------------------------------------------------------
// Name : ConvertFloat2Half
// Desc : offsetElemArray  float num array 
//-----------------------------------------------------------------------------
void ConvertFloat2Half(char **newPtr,
	unsigned int *newStride,
	const void *ptr,
	unsigned int stride, unsigned int vtxNum,
	std::vector<int> &offsetElemArray)
{

	unsigned int nStride = 0;
	for (std::vector<int>::iterator itr = offsetElemArray.begin();
		itr != offsetElemArray.end(); ++itr)
	{
		int value = *itr;
		if (value < 0)
		{
			value = -value;
			nStride += sizeof(short) * value;
		}
		else
		{
			nStride += sizeof(float) * value;
		}
	}
	nStride = (nStride + (4 - 1))  & ~(4 - 1);
	char *nPtr = new char[nStride * vtxNum];
	memset(nPtr, 0, nStride * vtxNum);

	float value4[4] = { 0 };
	short svalue4[4];
	const char *p = (const char*)ptr;
	for (unsigned int n = 0; n < vtxNum; n++)
	{
		unsigned int ofs = 0;
		for (std::vector<int>::iterator itr = offsetElemArray.begin();
			itr != offsetElemArray.end(); ++itr)
		{
			int value = *itr;
			if (value < 0)
			{
				value = -value;
				memcpy(value4, &p[n * stride + ofs], sizeof(float) * value);
				float2half(value4, svalue4, 4);
				memcpy(&nPtr[n * nStride + ofs], svalue4,
					sizeof(short) * value);
				ofs += sizeof(short) * value;
			}
			else
			{
				memcpy(&nPtr[n * nStride + ofs],
					&p[n * stride + ofs], sizeof(float) * value);
				ofs += sizeof(float) * value;
			}
		}
	}

	*newPtr = nPtr;
	*newStride = nStride;
}




//-----------------------------------------------------------------------------
#define maxStride 0x40
struct DefStream
{
	int val[maxStride];
	int id;
};
inline bool operator == (const DefStream &a1, const DefStream &a2)
{
	if (memcmp(a1.val, a2.val, g_elem_num * sizeof(int)) == 0)return true;
	return false;
}

//-----------------------------------------------------------------------------
// Name : ConvertSmoothNormal_SamePos
// Desc : 
//-----------------------------------------------------------------------------
bool ConvertSmoothNormal_SamePos(void *ptrPN,
	unsigned int stride, unsigned int vtxNum, unsigned int normalOffset)
{

	g_elem_num = 3;

	std::map<unsigned int, std::vector<unsigned int>> sameIndexs;
	std::vector<DefStream>m_vDefStream;
	char *ptr = (char*)ptrPN;
	for (unsigned int n = 0; n < vtxNum; n++)
	{
		DefStream df;
		memset(&df, 0, sizeof(df));
		memcpy(df.val, ptr, normalOffset + sizeof(float) * 3);

		std::vector<DefStream>::iterator itr = find(m_vDefStream.begin(),
			m_vDefStream.end(), df);
		if (itr == m_vDefStream.end())
		{
			df.id = (int)m_vDefStream.size();
			m_vDefStream.push_back(df);
			std::vector<unsigned int> array;
			array.push_back(n);
			sameIndexs.insert(std::pair<unsigned int, std::vector<unsigned int>>(
				df.id, array));
		}
		else
		{
			//same position
			std::map<unsigned int, std::vector<unsigned int>>::iterator s_itr =
				sameIndexs.find((*itr).id);
			if (s_itr != sameIndexs.end())
			{
				s_itr->second.push_back(n);
			}
		}
		ptr += stride;
	}

	//same pos ... smooth normal
	for (std::map<unsigned int, std::vector<unsigned int>>::iterator itr = sameIndexs.begin(); itr != sameIndexs.end(); ++itr)
	{
		std::vector<unsigned int> &idxs = itr->second;

		char *n_ptr = (char*)ptrPN;
		Vector3 normal(0, 0, 0);
		for (std::vector<unsigned int>::iterator n_itr = idxs.begin();
			n_itr != idxs.end(); ++n_itr)
		{
			float *f_ptr = (float*)&n_ptr[stride * (*n_itr) + normalOffset];
			normal += Vector3(f_ptr[0], f_ptr[1], f_ptr[2]);
		}
		normal = normalize(normal);
		for (std::vector<unsigned int>::iterator n_itr = idxs.begin();
			n_itr != idxs.end(); ++n_itr)
		{
			memcpy(&n_ptr[stride * (*n_itr) + normalOffset], &normal, sizeof(float) * 3);
		}
	}
	return true;
}

//-----------------------------------------------------------------------------
// Name : CreateTangent
// Desc : 
//-----------------------------------------------------------------------------
bool CreateTangent(float *tangent, float *binormal,
	const float *pos0, const float *pos1, const float *pos2,
	const float *uv0, const float *uv1, const float *uv2)
{

	Vector3 cp[3][3];
	float _u[3];
	float _v[3];

	cp[0][0] = Vector3(pos0[0], uv0[0], uv0[1]);
	cp[0][1] = Vector3(pos0[1], uv0[0], uv0[1]);
	cp[0][2] = Vector3(pos0[2], uv0[0], uv0[1]);

	cp[1][0] = Vector3(pos1[0], uv1[0], uv1[1]);
	cp[1][1] = Vector3(pos1[1], uv1[0], uv1[1]);
	cp[1][2] = Vector3(pos1[2], uv1[0], uv1[1]);

	cp[2][0] = Vector3(pos2[0], uv2[0], uv2[1]);
	cp[2][1] = Vector3(pos2[1], uv2[0], uv2[1]);
	cp[2][2] = Vector3(pos2[2], uv2[0], uv2[1]);

	for (int i = 0; i < 3; i++)
	{
		Vector3 v1 = cp[1][i] - cp[0][i];
		Vector3 v2 = cp[2][i] - cp[0][i];
		Vector3 cr = cross(v1, v2);
		_u[i] = -cr.getY() / cr.getX();
		_v[i] = -cr.getZ() / cr.getX();
	}
	Vector3 vtangent = normalize(Vector3(_u[0], _u[1], _u[2]));
	Vector3 vbinormal = normalize(Vector3(_v[0], _v[1], _v[2]));
	memcpy(tangent, &vtangent, sizeof(float) * 3);
	memcpy(binormal, &vbinormal, sizeof(float) * 3);
	return true;
}


//-----------------------------------------------------------------------------
// Name : ConvertTriIndex
// Desc : 
//-----------------------------------------------------------------------------
bool ConvertTriIndex(std::vector<int> &nv_stream,
	std::vector<unsigned short> &ni_stream,
	const std::vector<int> &v_stream, unsigned int stride, int prim)
{

	if (stride > (maxStride * sizeof(int)))return false;
	if (prim != 3)return false; //triangles only

	std::vector<DefStream>m_vDefStream;
	unsigned int index_max = (unsigned int)v_stream.size();
	g_elem_num = stride / sizeof(int);

	unsigned int index = 0;
	while (index < index_max)
	{
		for (int n = 0; n < prim; n++)
		{
			DefStream df;
			for (unsigned int s = 0; s < stride / sizeof(int); s++)
			{
				df.val[s] = v_stream[index++];
			}
			std::vector<DefStream>::iterator itr = find(m_vDefStream.begin(),
				m_vDefStream.end(), df);
			if (itr == m_vDefStream.end())
			{
				df.id = (int)m_vDefStream.size();
				ni_stream.push_back((unsigned short)df.id);
				m_vDefStream.push_back(df);

				for (unsigned int s = 0; s < stride / sizeof(int); s++)
				{
					nv_stream.push_back(df.val[s]);
				}

			}
			else
			{
				ni_stream.push_back((unsigned short)itr->id);
			}
		}
	}

	if (m_vDefStream.size() >= 0x10000)
	{
		//index over
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
// Name : ConvertTriIndexStrip
// Desc : 
//-----------------------------------------------------------------------------
bool ConvertTriIndexStrip(std::vector<int> &nv_stream,
	std::vector<unsigned short> &ni_stream,
	const std::vector<int> &v_stream, unsigned int stride, int prim)
{

	if (stride > (maxStride * sizeof(int)))return false;
	if (prim != 3)return false; //triangles only

	std::vector<DefStream>m_vDefStream;
	std::vector<unsigned short> index_stream;
	unsigned int index_max = (unsigned int)v_stream.size();
	g_elem_num = stride / sizeof(int);

	unsigned int index = 0;
	while (index < index_max)
	{
		for (int n = 0; n < prim; n++)
		{
			DefStream df;
			for (unsigned int s = 0; s < stride / sizeof(int); s++)
			{
				df.val[s] = v_stream[index++];
			}
			std::vector<DefStream>::iterator itr = find(m_vDefStream.begin(),
				m_vDefStream.end(), df);
			if (itr == m_vDefStream.end())
			{
				df.id = (int)m_vDefStream.size();
				index_stream.push_back((unsigned short)df.id);
				m_vDefStream.push_back(df);

				for (unsigned int s = 0; s < stride / sizeof(int); s++)
				{
					nv_stream.push_back(df.val[s]);
				}

			}
			else
			{
				index_stream.push_back((unsigned short)itr->id);
			}
		}
	}
	if (m_vDefStream.size() >= 0x10000)
	{
		//index over
		return false;
	}

	unsigned int in_numIndices = (unsigned int)index_stream.size();
	unsigned short *in_indices = new unsigned short[in_numIndices];
	for (unsigned int i = 0; i < in_numIndices; i++)in_indices[i] = index_stream[i];

	NVTRI::PrimitiveGroup *pGroup = NULL;
	unsigned short numGroup = 0;
	NVTRI::GenerateStrips(in_indices, in_numIndices, &pGroup, &numGroup);

	if (numGroup > 0)
	{
		unsigned int stripNum = 0;
		unsigned short lastIndex = 0;
		for (int i = 0; i < (int)numGroup; i++)
		{
			stripNum += pGroup[i].numIndices;
			if (i > 0)
			{ //connect strip
				ni_stream.push_back(lastIndex);
				ni_stream.push_back(pGroup[i].indices[0]);
			}
			for (int n = 0; n < (int)pGroup[i].numIndices; n++)
			{
				ni_stream.push_back(pGroup[i].indices[n]);
			}
			lastIndex = pGroup[i].indices[pGroup[i].numIndices - 1];
		}
#if 0 //check index size
		unsigned int vnum = nv_stream.size() / (stride / sizeof(int));
		for (unsigned int n = 0; n < ni_stream.size(); n++)
		{
			if (vnum <= ni_stream[n])
			{
				printf("index over !!! %d <= %d\n", vnum, ni_stream[n]);
			}
		}

#endif

		delete[] pGroup;
		delete[] in_indices;
		return true;
	}

	delete[] in_indices;

	return false;
}

//-----------------------------------------------------------------------------
// Name : ConvertTriStrip
// Desc : 
//-----------------------------------------------------------------------------
bool ConvertTriStrip(std::vector<int> &nv_stream,
	const std::vector<int> &v_stream, unsigned int stride, int prim)
{

	if (prim != 3)return false; //triangles only

	std::vector<DefStream>m_vDefStream;
	std::vector<unsigned short> index_stream;
	unsigned int index_max = (unsigned int)v_stream.size();
	g_elem_num = stride / sizeof(int);
	unsigned int index = 0;
	while (index < index_max)
	{
		for (int n = 0; n < prim; n++)
		{
			DefStream df;
			for (unsigned int s = 0; s < g_elem_num; s++)
			{
				df.val[s] = v_stream[index++];
			}
			std::vector<DefStream>::iterator itr = find(
				m_vDefStream.begin(),
				m_vDefStream.end(), df);

			if (itr == m_vDefStream.end())
			{
				df.id = (int)m_vDefStream.size();
				index_stream.push_back((unsigned short)df.id);
				m_vDefStream.push_back(df);
			}
			else
			{
				index_stream.push_back((unsigned short)itr->id);
			}
		}
	}

	unsigned int in_numIndices = (unsigned int)index_stream.size();
	if (in_numIndices >= 0xffff)return false;

	unsigned short *in_indices = new unsigned short[in_numIndices];
	for (unsigned int i = 0; i < in_numIndices; i++)in_indices[i] = index_stream[i];

	NVTRI::PrimitiveGroup *pGroup = NULL;
	unsigned short numGroup = 0;
	NVTRI::GenerateStrips(in_indices, in_numIndices, &pGroup, &numGroup);

	if (numGroup > 0)
	{
		unsigned int stripNum = 0;
		unsigned short lastIndex = 0;
		for (int i = 0; i < (int)numGroup; i++)
		{
			stripNum += pGroup[i].numIndices;
			if (i > 0)
			{
				{
					DefStream &df = m_vDefStream[lastIndex];
					for (unsigned int n = 0; n < g_elem_num; n++)
					{
						nv_stream.push_back(df.val[n]);
					}
				}
				{
					DefStream &df = m_vDefStream[pGroup[i].indices[0]];
					for (unsigned int n = 0; n < g_elem_num; n++)
					{
						nv_stream.push_back(df.val[n]);
					}
				}
			}
			for (int n = 0; n < (int)pGroup[i].numIndices; n++)
			{
				DefStream &df = m_vDefStream[pGroup[i].indices[n]];
				for (unsigned int n = 0; n < g_elem_num; n++)
				{
					nv_stream.push_back(df.val[n]);
				}
			}
			lastIndex = pGroup[i].indices[pGroup[i].numIndices - 1];
		}

		delete[] pGroup;
		delete[] in_indices;

		return true;
	}

	delete[] in_indices;

	return false;
}



bool ConvertTriIndexStrip(char **new_vertex, unsigned int *new_vertexNum,
	char **new_index, unsigned int *new_indexNum,
	const char *v_stream, unsigned int v_stream_num, unsigned int stride)
{

	std::vector<DefStream>m_vDefStream;
	std::vector<unsigned short> index_stream;
	unsigned int index_max = v_stream_num;
	g_elem_num = stride / sizeof(int);

	std::vector<int> nv_stream;
	std::vector<unsigned short> ni_stream;

	unsigned int index = 0;
	while (index < index_max)
	{
		for (int n = 0; n < 3; n++)
		{
			DefStream df;
			memcpy(df.val, v_stream, stride);
			v_stream += stride;
			std::vector<DefStream>::iterator itr = find(m_vDefStream.begin(),
				m_vDefStream.end(), df);
			if (itr == m_vDefStream.end())
			{
				df.id = (int)m_vDefStream.size();
				index_stream.push_back((unsigned short)df.id);
				m_vDefStream.push_back(df);

				for (unsigned int s = 0; s < stride / sizeof(int); s++)
				{
					nv_stream.push_back(df.val[s]);
				}

			}
			else
			{
				index_stream.push_back((unsigned short)itr->id);
			}
			index++;
		}
	}
	if (m_vDefStream.size() >= 0x10000)
	{
		//index over
		return false;
	}

	unsigned int in_numIndices = (unsigned int)index_stream.size();
	unsigned short *in_indices = new unsigned short[in_numIndices];
	for (unsigned int i = 0; i < in_numIndices; i++)in_indices[i] = index_stream[i];

	NVTRI::PrimitiveGroup *pGroup = NULL;
	unsigned short numGroup = 0;
	NVTRI::GenerateStrips(in_indices, in_numIndices, &pGroup, &numGroup);

	if (numGroup > 0)
	{
		unsigned int stripNum = 0;
		unsigned short lastIndex = 0;
		for (int i = 0; i < (int)numGroup; i++)
		{
			stripNum += pGroup[i].numIndices;
			if (i > 0)
			{ //connect strip
				ni_stream.push_back(lastIndex);
				ni_stream.push_back(pGroup[i].indices[0]);
			}
			for (int n = 0; n < (int)pGroup[i].numIndices; n++)
			{
				ni_stream.push_back(pGroup[i].indices[n]);
			}
			lastIndex = pGroup[i].indices[pGroup[i].numIndices - 1];
		}

		//
		char *p;
		*new_vertex = p = new char[nv_stream.size() * 4];
		*new_vertexNum = ((unsigned int)nv_stream.size() * 4) / stride;
		for (unsigned int n = 0; n < nv_stream.size(); n++)
		{
			memcpy(p, &nv_stream[n], 4);
			p += 4;
		}
		*new_index = p = new char[ni_stream.size() * 2];
		*new_indexNum = (unsigned int)ni_stream.size();
		for (unsigned int n = 0; n < ni_stream.size(); n++)
		{
			memcpy(p, &ni_stream[n], 2);
			p += 2;
		}

		delete[] pGroup;
		delete[] in_indices;
		return true;
	}

	delete[] in_indices;

	return false;
}

//線分リスト作成
class LineEdge
{
public:
	int ref;
	unsigned short idx[2];
	LineEdge(unsigned short idx0, unsigned short idx1)
	{
		ref = 1;
		if (idx0 < idx1)
		{
			idx[0] = idx0;
			idx[1] = idx1;
		}
		else
		{
			idx[1] = idx0;
			idx[0] = idx1;
		}
	}
};
inline bool operator == (const LineEdge &a1, const LineEdge &a2)
{
	if ((a1.idx[0] == a2.idx[0]) &&
		(a1.idx[1] == a2.idx[1]))
	{
		return true;
	}
	return false;
}

bool ConvertEdgeIndexLines(int faceType,
	char **new_vertex, unsigned int *new_vertexNum,
	char **new_index, unsigned int *new_indexNum,
	const char *v_stream, unsigned int v_stream_num, unsigned int stride)
{

	std::vector<DefStream>m_vDefStream;
	std::vector<unsigned short> index_stream;
	unsigned int index_max = v_stream_num;
	g_elem_num = stride / sizeof(int);

	std::vector<int> nv_stream;
	std::vector<unsigned short> ni_stream;

	unsigned int index = 0;
	while (index < index_max)
	{
		for (int n = 0; n < faceType; n++)
		{
			DefStream df;
			memcpy(df.val, v_stream, stride);
			v_stream += stride;
			std::vector<DefStream>::iterator itr = find(m_vDefStream.begin(),
				m_vDefStream.end(), df);
			if (itr == m_vDefStream.end())
			{
				df.id = (int)m_vDefStream.size();
				index_stream.push_back((unsigned short)df.id);
				m_vDefStream.push_back(df);

				for (unsigned int s = 0; s < stride / sizeof(int); s++)
				{
					nv_stream.push_back(df.val[s]);
				}

			}
			else
			{
				index_stream.push_back((unsigned short)itr->id);
			}
			index++;
		}
	}
	if (m_vDefStream.size() >= 0x10000)
	{
		//index over
		return false;
	}

	//線分登録
	std::vector<LineEdge> lineEdge;
	for (unsigned int n = 0; n < index_stream.size() / faceType; n++)
	{
		int ce = (faceType == 2) ? 1 : 3; //Triangleなら閉じる必要あり
		for (int e = 0; e < ce; e++)
		{
			LineEdge le(index_stream[n * faceType + e], index_stream[n * faceType + (e + 1) % faceType]);
			std::vector<LineEdge>::iterator itr = find(lineEdge.begin(), lineEdge.end(), le);
			if (itr != lineEdge.end())
			{
				itr->ref++;
			}
			else
			{
				lineEdge.push_back(le);
			}
		}
	}
	//２箇所以上使用しているものを削除
	{
		std::vector<LineEdge>::iterator itr = lineEdge.begin();
		while (itr != lineEdge.end())
		{
			if (itr->ref > 1)
			{
				itr = lineEdge.erase(itr);
			}
			else
			{
				++itr;
			}
		}
	}
	//使用している頂点検索
	std::set<unsigned short> vIdx;
	for (std::vector<LineEdge>::iterator itr = lineEdge.begin();
		itr != lineEdge.end(); ++itr)
	{
		vIdx.insert(itr->idx[0]);
		vIdx.insert(itr->idx[1]);
	}
	//逆引き
	unsigned short rn = 0;
	std::map<unsigned short, unsigned short> mIdx;
	for (std::set<unsigned short>::iterator itr = vIdx.begin();
		itr != vIdx.end(); ++itr)
	{
		mIdx.insert(std::pair<unsigned short, unsigned short>(*itr, rn));
		rn++;
	}
	//インデックス再構成
	for (std::vector<LineEdge>::iterator itr = lineEdge.begin();
		itr != lineEdge.end(); ++itr)
	{
		itr->idx[0] = mIdx[itr->idx[0]];
		itr->idx[1] = mIdx[itr->idx[1]];
	}

	char *p;
	*new_vertex = p = new char[vIdx.size() * stride];
	*new_vertexNum = (unsigned int)vIdx.size();
	for (std::set<unsigned short>::iterator itr = vIdx.begin();
		itr != vIdx.end(); ++itr)
	{
		for (unsigned int i = 0; i < (stride / 4); i++)
		{
			memcpy(p, &nv_stream[(stride / 4) * (*itr) + i], 4);
			p += 4;
		}
	}
	*new_index = p = new char[lineEdge.size() * 2 * 2];
	*new_indexNum = (unsigned int)lineEdge.size() * 2;
	for (unsigned int n = 0; n < lineEdge.size(); n++)
	{
		memcpy(p, &lineEdge[n].idx[0], 2);
		p += 2;
		memcpy(p, &lineEdge[n].idx[1], 2);
		p += 2;
	}
	return true;

}
