﻿//-----------------------------------------------------------------------------
// File: vec.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __vec_h__
#define    __vec_h__

#include <float.h>
#define _VECTORMATH_SLERP_TOL 0.999f


//-----------------------------------------------------------------------------
// Name : Vector2I
// Desc : 
//-----------------------------------------------------------------------------
inline Vector2I::Vector2I( int _x, int _y ) {
    x = _x;
    y = _y;
}

inline int Vector2I::getX( ) const {
    return x;
}

inline int Vector2I::getY( ) const {
    return y;
}

inline const Vector2I Vector2I::operator +( const Vector2I & vec ) const {
    return Vector2I(
        ( x + vec.x ),
        ( y + vec.y )
    );
}

inline const Vector2I Vector2I::operator -( const Vector2I & vec ) const {
    return Vector2I(
        ( x - vec.x ),
        ( y - vec.y )
    );
}

inline Vector2I & Vector2I::operator +=( const Vector2I & vec ) {
    *this = *this + vec;
    return *this;
}

inline Vector2I & Vector2I::operator -=( const Vector2I & vec ) {
    *this = *this - vec;
    return *this;
}

inline int cross( const Vector2I & vec0, const Vector2I & vec1 ) {
    return  ( vec0.getX() * vec1.getY() ) - ( vec0.getY() * vec1.getX() );
}

//-----------------------------------------------------------------------------
// Name : Point2
// Desc : 
//-----------------------------------------------------------------------------
inline Point2::Point2( const Vector2 & vec ) {
    x = vec.x;
    y = vec.y;
}

inline Point2::Point2( float _x, float _y ) {
    x = _x;
    y = _y;
}
inline Point2 & Point2::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Point2::getX( ) const {
    return x;
}

inline Point2 & Point2::setY( float _y ) {
    y = _y;
    return *this;
}

inline Point2 & Point2::setXY( float _x, float _y ) {
    x = _x;
    y = _y;
    return *this;
}

inline float Point2::getY( ) const {
    return y;
}

//-----------------------------------------------------------------------------
// Name : Vector2
// Desc : 
//-----------------------------------------------------------------------------
inline Vector2::Vector2( const Vector2 & vec ) {
    x = vec.x;
    y = vec.y;
}

inline Vector2::Vector2( const Point2 & pnt ) {
    x = pnt.x;
    y = pnt.y;
}

inline Vector2::Vector2( float _x, float _y ) {
    x = _x;
    y = _y;
}

inline Vector2::Vector2( float scalar ) {
    x = scalar;
    y = scalar;
}

inline Vector2 & Vector2::operator =( const Vector2 & vec ) {
    x = vec.x;
    y = vec.y;
    return *this;
}

inline Vector2 & Vector2::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Vector2::getX( ) const {
    return x;
}

inline Vector2 & Vector2::setY( float _y ) {
    y = _y;
    return *this;
}

inline Vector2 & Vector2::setXY( float _x, float _y ) {
    x = _x;
    y = _y;
    return *this;
}

inline float Vector2::getY( ) const {
    return y;
}

inline Vector2 & Vector2::setElem( int idx, float value ) {
    *(&x + idx) = value;
    return *this;
}

inline float Vector2::getElem( int idx ) const {
    return *(&x + idx);
}

inline float & Vector2::operator []( int idx ) {
    return *(&x + idx);
}

inline float Vector2::operator []( int idx ) const {
    return *(&x + idx);
}

inline Vector2::operator Vector2 *() {
    return (Vector2*)&x;
}

inline Vector2::operator const Vector2 *() const {
    return (const Vector2*)&x;
}

inline Vector2::operator const float *() const {
    return (const float*)&x;
}

inline const Vector2 Vector2::operator +( const Vector2 & vec ) const {
    return Vector2(
        ( x + vec.x ),
        ( y + vec.y )
    );
}

inline const Vector2 Vector2::operator -( const Vector2 & vec ) const {
    return Vector2(
        ( x - vec.x ),
        ( y - vec.y )
    );
}

inline const Vector2 Vector2::operator *( float scalar ) const {
    return Vector2(
        ( x * scalar ),
        ( y * scalar )
    );
}

inline Vector2 & Vector2::operator +=( const Vector2 & vec ) {
    *this = *this + vec;
    return *this;
}

inline Vector2 & Vector2::operator -=( const Vector2 & vec ) {
    *this = *this - vec;
    return *this;
}

inline Vector2 & Vector2::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

inline const Vector2 Vector2::operator /( float scalar ) const {
    return Vector2(
        ( x / scalar ),
        ( y / scalar )
    );
}

inline Vector2 & Vector2::operator /=( float scalar ) {
    *this = *this / scalar;
    return *this;
}

inline const Vector2 Vector2::operator -( ) const {
    return Vector2(
        -x,
        -y
    );
}

inline const Vector2 Vector2::xAxis( ) {
    return Vector2( 1.0f, 0.0f );
}

inline const Vector2 Vector2::yAxis( ) {
    return Vector2( 0.0f, 1.0f );
}

inline const Vector2 operator *( float scalar, const Vector2 & vec ) {
    return vec * scalar;
}

inline const Vector2 mulPerElem( const Vector2 & vec0, const Vector2 & vec1 ) {
    return Vector2(
        ( vec0.getX() * vec1.getX() ),
        ( vec0.getY() * vec1.getY() )
    );
}

inline const Vector2 divPerElem( const Vector2 & vec0, const Vector2 & vec1 ) {
    return Vector2(
        ( vec0.getX() / vec1.getX() ),
        ( vec0.getY() / vec1.getY() )
    );
}

inline const Vector2 absPerElem( const Vector2 & vec ) {
    return Vector2(
        fabsf( vec.getX() ),
        fabsf( vec.getY() )
    );
}
inline float dot( const Vector2 & vec0, const Vector2 & vec1 ) {
    float result;
    result = ( vec0.getX() * vec1.getX() );
    result = ( result + ( vec0.getY() * vec1.getY() ) );
    return result;
}

inline float cross( const Vector2 & vec0, const Vector2 & vec1 ) {
    return  ( vec0.getX() * vec1.getY() ) - ( vec0.getY() * vec1.getX() );
}

inline const Vector2 cross(const Vector2 & vec, float scalar){
	return Vector2( scalar * vec.getY(), -scalar * vec.getX() );
}

inline const Vector2 cross(float scalar, const Vector2 & vec){
	return Vector2( -scalar * vec.getY(), scalar * vec.getX() );
}

inline float lengthSqr( const Vector2 & vec ) {
    float result;
    result = ( vec.getX() * vec.getX() );
    result = ( result + ( vec.getY() * vec.getY() ) );
    return result;
}

inline float length( const Vector2 & vec ) {
    return sqrtf( lengthSqr( vec ) );
}

inline float lengthSafety( const Vector2 & vec ) {
    float result = lengthSqr( vec );
    if(result > FLT_EPSILON){
        return sqrtf( lengthSqr( vec ) );
    }else{
        return 0;
    }
}

inline const Vector2 normalize( const Vector2 & vec ){
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    lenInv = ( 1.0f / sqrtf( lenSqr ) );
    return Vector2(
        ( vec.getX() * lenInv ),
        ( vec.getY() * lenInv )
    );
}

inline const Vector2 normalizeSafety( const Vector2 & vec ){
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    if(lenSqr > FLT_EPSILON){
        lenInv = ( 1.0f / sqrtf( lenSqr ) );
        return Vector2(
            ( vec.getX() * lenInv ),
            ( vec.getY() * lenInv )
            );
    }else{
        return Vector2(0, 0);
    }
}

inline const Vector2 & Vector2::rotation( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _x = x*c - y*s; 
    float _y = x*s + y*c;
    x = _x;
    y = _y;
    return *this;
}

inline const Vector2 lerp( float t, const Vector2 & vec0, const Vector2 & vec1 ) {
    return ( vec0 + ( ( vec1 - vec0 ) * t ) );
}

//-----------------------------------------------------------------------------
// Name : Vector3
// Desc : 
//-----------------------------------------------------------------------------
inline Vector3::Vector3( const Vector3 & vec ) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
}

inline Vector3::Vector3( float _x, float _y, float _z ) {
    x = _x;
    y = _y;
    z = _z;
}

inline Vector3::Vector3( const Point3 & pnt ) {
    x = pnt.getX();
    y = pnt.getY();
    z = pnt.getZ();
}

inline Vector3::Vector3( float scalar ) {
    x = scalar;
    y = scalar;
    z = scalar;
}

inline const Vector3 Vector3::xAxis( ) {
    return Vector3( 1.0f, 0.0f, 0.0f );
}

inline const Vector3 Vector3::yAxis( ) {
    return Vector3( 0.0f, 1.0f, 0.0f );
}

inline const Vector3 Vector3::zAxis( ) {
    return Vector3( 0.0f, 0.0f, 1.0f );
}

inline const Vector3 lerp( float t, const Vector3 & vec0, const Vector3 & vec1 ) {
    return ( vec0 + ( ( vec1 - vec0 ) * t ) );
}

inline const Vector3 slerp( float t, const Vector3 & unitVec0, const Vector3 & unitVec1 ) {
    float recipSinAngle, scale0, scale1, cosAngle, angle;
    cosAngle = dot( unitVec0, unitVec1 );
    if ( cosAngle < _VECTORMATH_SLERP_TOL ) {
        angle = acosf( cosAngle );
        recipSinAngle = ( 1.0f / sinf( angle ) );
        scale0 = ( sinf( ( ( 1.0f - t ) * angle ) ) * recipSinAngle );
        scale1 = ( sinf( ( t * angle ) ) * recipSinAngle );
    } else {
        scale0 = ( 1.0f - t );
        scale1 = t;
    }
    return ( ( unitVec0 * scale0 ) + ( unitVec1 * scale1 ) );
}

inline Vector3 & Vector3::operator =( const Vector3 & vec ) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
    return *this;
}

inline Vector3 & Vector3::operator =( const Point3 & pnt ) {
    x = pnt.x;
    y = pnt.y;
    z = pnt.z;
    return *this;
}

inline Vector3 & Vector3::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Vector3::getX( ) const {
    return x;
}

inline Vector3 & Vector3::setY( float _y ) {
    y = _y;
    return *this;
}

inline float Vector3::getY( ) const {
    return y;
}

inline Vector3 & Vector3::setZ( float _z ) {
    z = _z;
    return *this;
}

inline float Vector3::getZ( ) const {
    return z;
}

inline Vector3 & Vector3::setXYZ( float _x, float _y, float _z ) {
    x = _x;
    y = _y;
    z = _z;
    return *this;
}

inline Vector3 & Vector3::setElem( int idx, float value ) {
    *(&x + idx) = value;
    return *this;
}

inline float Vector3::getElem( int idx ) const {
    return *(&x + idx);
}

inline float & Vector3::operator []( int idx ) {
    return *(&x + idx);
}

inline float Vector3::operator []( int idx ) const {
    return *(&x + idx);
}

inline Vector3::operator Vector3 *() {
    return (Vector3*)&x;
}

inline Vector3::operator const Vector3 *() const {
    return (const Vector3*)&x;
}

inline Vector3::operator const float *() const {
    return (const float*)&x;
}

inline const Vector3 Vector3::operator +( const Vector3 & vec ) const {
    return Vector3(
        ( x + vec.x ),
        ( y + vec.y ),
        ( z + vec.z )
    );
}

inline const Vector3 Vector3::operator -( const Vector3 & vec ) const {
    return Vector3(
        ( x - vec.x ),
        ( y - vec.y ),
        ( z - vec.z )
    );
}

inline const Point3 Vector3::operator +( const Point3 & pnt ) const {
    return Point3(
        ( x + pnt.getX() ),
        ( y + pnt.getY() ),
        ( z + pnt.getZ() )
    );
}

inline const Vector3 Vector3::operator *( float scalar ) const {
    return Vector3(
        ( x * scalar ),
        ( y * scalar ),
        ( z * scalar )
    );
}

inline Vector3 & Vector3::operator +=( const Vector3 & vec ) {
    *this = *this + vec;
    return *this;
}

inline Vector3 & Vector3::operator -=( const Vector3 & vec ) {
    *this = *this - vec;
    return *this;
}

inline Vector3 & Vector3::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

inline const Vector3 Vector3::operator /( float scalar ) const {
    return Vector3(
        ( x / scalar ),
        ( y / scalar ),
        ( z / scalar )
    );
}

inline Vector3 & Vector3::operator /=( float scalar ) {
    *this = *this / scalar;
    return *this;
}

inline const Vector3 Vector3::operator -( ) const {
    return Vector3(
        -x,
        -y,
        -z
    );
}

inline const Vector3 & Vector3::rotationX( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _y = y*c - z*s;
    float _z = y*s + z*c;
    y = _y;
    z = _z;
    return *this;
}

inline const Vector3 & Vector3::rotationY( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _x = x*c + z*s;
    float _z = -x*s + z*c;
    x = _x;
    z = _z;
    return *this;
}

inline const Vector3 & Vector3::rotationZ( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _x = x*c - y*s; 
    float _y = x*s + y*c;
    x = _x;
    y = _y;
    return *this;
}


inline const Vector3 operator *( float scalar, const Vector3 & vec ) {
    return vec * scalar;
}

inline const Vector3 mulPerElem( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        ( vec0.getX() * vec1.getX() ),
        ( vec0.getY() * vec1.getY() ),
        ( vec0.getZ() * vec1.getZ() )
    );
}

inline const Vector3 divPerElem( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        ( vec0.getX() / vec1.getX() ),
        ( vec0.getY() / vec1.getY() ),
        ( vec0.getZ() / vec1.getZ() )
    );
}

inline const Vector3 recipPerElem( const Vector3 & vec ) {
    return Vector3(
        ( 1.0f / vec.getX() ),
        ( 1.0f / vec.getY() ),
        ( 1.0f / vec.getZ() )
    );
}

inline const Vector3 absPerElem( const Vector3 & vec ) {
    return Vector3(
        fabsf( vec.getX() ),
        fabsf( vec.getY() ),
        fabsf( vec.getZ() )
    );
}

inline const Vector3 copySignPerElem( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        ( vec1.getX() < 0.0f )? -fabsf( vec0.getX() ) : fabsf( vec0.getX() ),
        ( vec1.getY() < 0.0f )? -fabsf( vec0.getY() ) : fabsf( vec0.getY() ),
        ( vec1.getZ() < 0.0f )? -fabsf( vec0.getZ() ) : fabsf( vec0.getZ() )
    );
}

inline const Vector3 maxPerElem( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        (vec0.getX() > vec1.getX())? vec0.getX() : vec1.getX(),
        (vec0.getY() > vec1.getY())? vec0.getY() : vec1.getY(),
        (vec0.getZ() > vec1.getZ())? vec0.getZ() : vec1.getZ()
    );
}

inline float maxElem( const Vector3 & vec ) {
    float result;
    result = (vec.getX() > vec.getY())? vec.getX() : vec.getY();
    result = (vec.getZ() > result)? vec.getZ() : result;
    return result;
}

inline const Vector3 minPerElem( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        (vec0.getX() < vec1.getX())? vec0.getX() : vec1.getX(),
        (vec0.getY() < vec1.getY())? vec0.getY() : vec1.getY(),
        (vec0.getZ() < vec1.getZ())? vec0.getZ() : vec1.getZ()
    );
}

inline float minElem( const Vector3 & vec ) {
    float result;
    result = (vec.getX() < vec.getY())? vec.getX() : vec.getY();
    result = (vec.getZ() < result)? vec.getZ() : result;
    return result;
}

inline float sum( const Vector3 & vec ) {
    float result;
    result = ( vec.getX() + vec.getY() );
    result = ( result + vec.getZ() );
    return result;
}

inline float dot( const Vector3 & vec0, const Vector3 & vec1 ) {
    float result;
    result = ( vec0.getX() * vec1.getX() );
    result = ( result + ( vec0.getY() * vec1.getY() ) );
    result = ( result + ( vec0.getZ() * vec1.getZ() ) );
    return result;
}

inline float lengthSqr( const Vector3 & vec ) {
    float result;
    result = ( vec.getX() * vec.getX() );
    result = ( result + ( vec.getY() * vec.getY() ) );
    result = ( result + ( vec.getZ() * vec.getZ() ) );
    return result;
}

inline float length( const Vector3 & vec ) {
    return sqrtf( lengthSqr( vec ) );
}

inline float lengthSafety( const Vector3 & vec ) {
    float result = lengthSqr( vec );
    if(result > FLT_EPSILON){
        return sqrtf( result );
    }else{
        return 0;
    }
}

inline const Vector3 normalize( const Vector3 & vec ) {
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    lenInv = ( 1.0f / sqrtf( lenSqr ) );
    return Vector3(
        ( vec.getX() * lenInv ),
        ( vec.getY() * lenInv ),
        ( vec.getZ() * lenInv )
    );
}

inline const Vector3 normalizeSafety( const Vector3 & vec ) {
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    if(lenSqr > FLT_EPSILON){
        lenInv = ( 1.0f / sqrtf( lenSqr ) );
        return Vector3(
            ( vec.getX() * lenInv ),
            ( vec.getY() * lenInv ),
            ( vec.getZ() * lenInv )
            );
    }else{
        return Vector3(0,0,0);
    }
}

inline const Vector3 cross( const Vector3 & vec0, const Vector3 & vec1 ) {
    return Vector3(
        ( ( vec0.getY() * vec1.getZ() ) - ( vec0.getZ() * vec1.getY() ) ),
        ( ( vec0.getZ() * vec1.getX() ) - ( vec0.getX() * vec1.getZ() ) ),
        ( ( vec0.getX() * vec1.getY() ) - ( vec0.getY() * vec1.getX() ) )
    );
}

inline const Vector3 select( const Vector3 & vec0, const Vector3 & vec1, bool select1 ){
    return Vector3(
        ( select1 )? vec1.getX() : vec0.getX(),
        ( select1 )? vec1.getY() : vec0.getY(),
        ( select1 )? vec1.getZ() : vec0.getZ()
    );
}

//-----------------------------------------------------------------------------
// Name : Vector4
// Desc : 
//-----------------------------------------------------------------------------
inline Vector4::Vector4( const Vector4 & vec ) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
    w = vec.w;
}

inline Vector4::Vector4( float _x, float _y, float _z, float _w ) {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
}

inline Vector4::Vector4( const Vector3 & xyz, float _w ) {
    this->setXYZ( xyz );
    this->setW( _w );
}

inline Vector4::Vector4( const Vector3 & vec ) {
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
    w = 0.0f;
}

inline Vector4::Vector4( const Point3 & pnt ) {
    x = pnt.getX();
    y = pnt.getY();
    z = pnt.getZ();
    w = 1.0f;
}

inline Vector4::Vector4( const Quat & quat ) {
    x = quat.getX();
    y = quat.getY();
    z = quat.getZ();
    w = quat.getW();
}

inline Vector4::Vector4( float scalar ) {
    x = scalar;
    y = scalar;
    z = scalar;
    w = scalar;
}

inline Vector4::Vector4( const float * vec ){
    x = vec[0];
    y = vec[1];
    z = vec[2];
    w = vec[3];
}

inline const Vector4 Vector4::xAxis( ) {
    return Vector4( 1.0f, 0.0f, 0.0f, 0.0f );
}

inline const Vector4 Vector4::yAxis( ) {
    return Vector4( 0.0f, 1.0f, 0.0f, 0.0f );
}

inline const Vector4 Vector4::zAxis( ) {
    return Vector4( 0.0f, 0.0f, 1.0f, 0.0f );
}

inline const Vector4 Vector4::wAxis( ) {
    return Vector4( 0.0f, 0.0f, 0.0f, 1.0f );
}

inline const Vector4 lerp( float t, const Vector4 & vec0, const Vector4 & vec1 ) {
    return ( vec0 + ( ( vec1 - vec0 ) * t ) );
}

inline const Vector4 slerp( float t, const Vector4 & unitVec0, const Vector4 & unitVec1 ) {
    float recipSinAngle, scale0, scale1, cosAngle, angle;
    cosAngle = dot( unitVec0, unitVec1 );
    if ( cosAngle < _VECTORMATH_SLERP_TOL ) {
        angle = acosf( cosAngle );
        recipSinAngle = ( 1.0f / sinf( angle ) );
        scale0 = ( sinf( ( ( 1.0f - t ) * angle ) ) * recipSinAngle );
        scale1 = ( sinf( ( t * angle ) ) * recipSinAngle );
    } else {
        scale0 = ( 1.0f - t );
        scale1 = t;
    }
    return ( ( unitVec0 * scale0 ) + ( unitVec1 * scale1 ) );
}

inline Vector4 & Vector4::operator =( const Vector4 & vec ) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
    w = vec.w;
    return *this;
}

inline Vector4 & Vector4::setXYZ( const Vector3 & vec ) {
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
    return *this;
}

inline const Vector3 Vector4::getXYZ( ) const {
    return Vector3( x, y, z );
}

inline Vector4 & Vector4::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Vector4::getX( ) const {
    return x;
}

inline Vector4 & Vector4::setY( float _y ) {
    y = _y;
    return *this;
}

inline float Vector4::getY( ) const {
    return y;
}

inline Vector4 & Vector4::setZ( float _z ) {
    z = _z;
    return *this;
}

inline float Vector4::getZ( ) const {
    return z;
}

inline Vector4 & Vector4::setW( float _w ) {
    w = _w;
    return *this;
}

inline float Vector4::getW( ) const {
    return w;
}

inline Vector4 & Vector4::setXYZW( float _x, float _y, float _z, float _w ) {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
    return *this;
}

inline Vector4 & Vector4::setElem( int idx, float value ) {
    *(&x + idx) = value;
    return *this;
}

inline float Vector4::getElem( int idx ) const {
    return *(&x + idx);
}

inline float & Vector4::operator []( int idx ) {
    return *(&x + idx);
}

inline float Vector4::operator []( int idx ) const {
    return *(&x + idx);
}

inline Vector4::operator Vector4 *() {
    return (Vector4*)&x;
}

inline Vector4::operator const Vector4 *() const {
    return (const Vector4*)&x;
}

inline Vector4::operator const float *() const {
    return (const float*)&x;
}

inline const Vector4 Vector4::operator +( const Vector4 & vec ) const {
    return Vector4(
        ( x + vec.x ),
        ( y + vec.y ),
        ( z + vec.z ),
        ( w + vec.w )
    );
}

inline const Vector4 Vector4::operator -( const Vector4 & vec ) const {
    return Vector4(
        ( x - vec.x ),
        ( y - vec.y ),
        ( z - vec.z ),
        ( w - vec.w )
    );
}

inline const Vector4 Vector4::operator *( float scalar ) const {
    return Vector4(
        ( x * scalar ),
        ( y * scalar ),
        ( z * scalar ),
        ( w * scalar )
    );
}

inline Vector4 & Vector4::operator +=( const Vector4 & vec ) {
    *this = *this + vec;
    return *this;
}

inline Vector4 & Vector4::operator -=( const Vector4 & vec ) {
    *this = *this - vec;
    return *this;
}

inline Vector4 & Vector4::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

inline const Vector4 Vector4::operator /( float scalar ) const {
    return Vector4(
        ( x / scalar ),
        ( y / scalar ),
        ( z / scalar ),
        ( w / scalar )
    );
}

inline Vector4 & Vector4::operator /=( float scalar ) {
    *this = *this / scalar;
    return *this;
}

inline const Vector4 Vector4::operator -( ) const {
    return Vector4(
        -x,
        -y,
        -z,
        -w
    );
}

inline const Vector4 & Vector4::rotationX( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _y = y*c - z*s;
    float _z = y*s + z*c;
    y = _y;
    z = _z;
    return *this;
}

inline const Vector4 & Vector4::rotationY( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _x = x*c + z*s;
    float _z = -x*s + z*c;
    x = _x;
    z = _z;
    return *this;
}

inline const Vector4 & Vector4::rotationZ( float radians ) {
    float s, c;
    s = sinf( radians );
    c = cosf( radians );
    float _x = x*c - y*s; 
    float _y = x*s + y*c;
    x = _x;
    y = _y;
    return *this;
}

inline const Vector4 operator *( float scalar, const Vector4 & vec ) {
    return vec * scalar;
}

inline const Vector4 mulPerElem( const Vector4 & vec0, const Vector4 & vec1 ) {
    return Vector4(
        ( vec0.getX() * vec1.getX() ),
        ( vec0.getY() * vec1.getY() ),
        ( vec0.getZ() * vec1.getZ() ),
        ( vec0.getW() * vec1.getW() )
    );
}

inline const Vector4 divPerElem( const Vector4 & vec0, const Vector4 & vec1 ) {
    return Vector4(
        ( vec0.getX() / vec1.getX() ),
        ( vec0.getY() / vec1.getY() ),
        ( vec0.getZ() / vec1.getZ() ),
        ( vec0.getW() / vec1.getW() )
    );
}

inline const Vector4 recipPerElem( const Vector4 & vec ) {
    return Vector4(
        ( 1.0f / vec.getX() ),
        ( 1.0f / vec.getY() ),
        ( 1.0f / vec.getZ() ),
        ( 1.0f / vec.getW() )
    );
}

inline const Vector4 absPerElem( const Vector4 & vec ) {
    return Vector4(
        fabsf( vec.getX() ),
        fabsf( vec.getY() ),
        fabsf( vec.getZ() ),
        fabsf( vec.getW() )
    );
}

inline const Vector4 copySignPerElem( const Vector4 & vec0, const Vector4 & vec1 ) {
    return Vector4(
        ( vec1.getX() < 0.0f )? -fabsf( vec0.getX() ) : fabsf( vec0.getX() ),
        ( vec1.getY() < 0.0f )? -fabsf( vec0.getY() ) : fabsf( vec0.getY() ),
        ( vec1.getZ() < 0.0f )? -fabsf( vec0.getZ() ) : fabsf( vec0.getZ() ),
        ( vec1.getW() < 0.0f )? -fabsf( vec0.getW() ) : fabsf( vec0.getW() )
    );
}

inline const Vector4 maxPerElem( const Vector4 & vec0, const Vector4 & vec1 ) {
    return Vector4(
        (vec0.getX() > vec1.getX())? vec0.getX() : vec1.getX(),
        (vec0.getY() > vec1.getY())? vec0.getY() : vec1.getY(),
        (vec0.getZ() > vec1.getZ())? vec0.getZ() : vec1.getZ(),
        (vec0.getW() > vec1.getW())? vec0.getW() : vec1.getW()
    );
}

inline float maxElem( const Vector4 & vec ) {
    float result;
    result = (vec.getX() > vec.getY())? vec.getX() : vec.getY();
    result = (vec.getZ() > result)? vec.getZ() : result;
    result = (vec.getW() > result)? vec.getW() : result;
    return result;
}

inline const Vector4 minPerElem( const Vector4 & vec0, const Vector4 & vec1 ) {
    return Vector4(
        (vec0.getX() < vec1.getX())? vec0.getX() : vec1.getX(),
        (vec0.getY() < vec1.getY())? vec0.getY() : vec1.getY(),
        (vec0.getZ() < vec1.getZ())? vec0.getZ() : vec1.getZ(),
        (vec0.getW() < vec1.getW())? vec0.getW() : vec1.getW()
    );
}

inline float minElem( const Vector4 & vec ) {
    float result;
    result = (vec.getX() < vec.getY())? vec.getX() : vec.getY();
    result = (vec.getZ() < result)? vec.getZ() : result;
    result = (vec.getW() < result)? vec.getW() : result;
    return result;
}

inline float sum( const Vector4 & vec ) {
    float result;
    result = ( vec.getX() + vec.getY() );
    result = ( result + vec.getZ() );
    result = ( result + vec.getW() );
    return result;
}

inline float dot( const Vector4 & vec0, const Vector4 & vec1 ) {
    float result;
    result = ( vec0.getX() * vec1.getX() );
    result = ( result + ( vec0.getY() * vec1.getY() ) );
    result = ( result + ( vec0.getZ() * vec1.getZ() ) );
    result = ( result + ( vec0.getW() * vec1.getW() ) );
    return result;
}

inline float lengthSqr( const Vector4 & vec ) {
    float result;
    result = ( vec.getX() * vec.getX() );
    result = ( result + ( vec.getY() * vec.getY() ) );
    result = ( result + ( vec.getZ() * vec.getZ() ) );
    result = ( result + ( vec.getW() * vec.getW() ) );
    return result;
}

inline float length( const Vector4 & vec ) {
    return sqrtf( lengthSqr( vec ) );
}

inline float lengthSafety( const Vector4 & vec ) {
    float result = lengthSqr( vec ); 
    if(result > FLT_EPSILON){
        return sqrtf( result );
    }else{
        return 0;
    }
}

inline const Vector4 normalize( const Vector4 & vec ) {
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    lenInv = ( 1.0f / sqrtf( lenSqr ) );
    return Vector4(
        ( vec.getX() * lenInv ),
        ( vec.getY() * lenInv ),
        ( vec.getZ() * lenInv ),
        ( vec.getW() * lenInv )
    );
}

inline const Vector4 normalizeSafety( const Vector4 & vec ) {
    float lenSqr, lenInv;
    lenSqr = lengthSqr( vec );
    if(lenSqr > FLT_EPSILON){
        lenInv = ( 1.0f / sqrtf( lenSqr ) );
        return Vector4(
            ( vec.getX() * lenInv ),
            ( vec.getY() * lenInv ),
            ( vec.getZ() * lenInv ),
            ( vec.getW() * lenInv )
            );
    }else{
        return Vector4(0,0,0,1);
    }
}

inline const Vector4 select( const Vector4 & vec0, const Vector4 & vec1, bool select1 ) {
    return Vector4(
        ( select1 )? vec1.getX() : vec0.getX(),
        ( select1 )? vec1.getY() : vec0.getY(),
        ( select1 )? vec1.getZ() : vec0.getZ(),
        ( select1 )? vec1.getW() : vec0.getW()
    );
}

//-----------------------------------------------------------------------------
// Name : Point3
// Desc : 
//-----------------------------------------------------------------------------
inline Point3::Point3( const Point3 & pnt ) {
    x = pnt.x;
    y = pnt.y;
    z = pnt.z;
}

inline Point3::Point3( float _x, float _y, float _z ) {
    x = _x;
    y = _y;
    z = _z;
}

inline Point3::Point3( const Vector3 & vec ) {
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
}

inline Point3::Point3( float scalar ) {
    x = scalar;
    y = scalar;
    z = scalar;
}

inline const Point3 lerp( float t, const Point3 & pnt0, const Point3 & pnt1 ) {
    return ( pnt0 + ( ( pnt1 - pnt0 ) * t ) );
}

inline Point3 & Point3::operator =( const Point3 & pnt ) {
    x = pnt.x;
    y = pnt.y;
    z = pnt.z;
    return *this;
}

inline Point3 & Point3::operator =( const Vector3 & vec ) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
    return *this;
}

inline Point3 & Point3::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Point3::getX( ) const {
    return x;
}

inline Point3 & Point3::setY( float _y ) {
    y = _y;
    return *this;
}

inline float Point3::getY( ) const {
    return y;
}

inline Point3 & Point3::setZ( float _z ) {
    z = _z;
    return *this;
}

inline float Point3::getZ( ) const {
    return z;
}

inline Point3 & Point3::setXYZ( float _x, float _y, float _z ) {
    x = _x;
    y = _y;
    z = _z;
    return *this;
}

inline Point3 & Point3::setElem( int idx, float value ) {
    *(&x + idx) = value;
    return *this;
}

inline float Point3::getElem( int idx ) const {
    return *(&x + idx);
}

inline float & Point3::operator []( int idx ) {
    return *(&x + idx);
}

inline float Point3::operator []( int idx ) const {
    return *(&x + idx);
}

inline Point3::operator Point3 *() {
    return (Point3*)&x;
}

inline Point3::operator const Point3 *() const {
    return (const Point3*)&x;
}

inline const Vector3 Point3::operator -( const Point3 & pnt ) const {
    return Vector3(
        ( x - pnt.x ),
        ( y - pnt.y ),
        ( z - pnt.z )
    );
}

inline const Point3 Point3::operator +( const Vector3 & vec ) const {
    return Point3(
        ( x + vec.getX() ),
        ( y + vec.getY() ),
        ( z + vec.getZ() )
    );
}

inline const Point3 Point3::operator -( const Vector3 & vec ) const {
    return Point3(
        ( x - vec.getX() ),
        ( y - vec.getY() ),
        ( z - vec.getZ() )
    );
}

inline Point3 & Point3::operator +=( const Vector3 & vec ) {
    *this = *this + vec;
    return *this;
}

inline Point3 & Point3::operator -=( const Vector3 & vec ) {
    *this = *this - vec;
    return *this;
}

inline const Point3 Point3::operator *( float scalar ) const {
    return Point3(
        ( x * scalar ),
        ( y * scalar ),
        ( z * scalar )
    );
}

inline Point3 & Point3::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

inline const Point3 Point3::operator /( float scalar ) const {
    return Point3(
        ( x / scalar ),
        ( y / scalar ),
        ( z / scalar )
    );
}

inline Point3 & Point3::operator /=( float scalar ) {
    *this = *this / scalar;
    return *this;
}

inline const Point3 mulPerElem( const Point3 & pnt0, const Point3 & pnt1 ) {
    return Point3(
        ( pnt0.getX() * pnt1.getX() ),
        ( pnt0.getY() * pnt1.getY() ),
        ( pnt0.getZ() * pnt1.getZ() )
    );
}

inline const Point3 divPerElem( const Point3 & pnt0, const Point3 & pnt1 ) {
    return Point3(
        ( pnt0.getX() / pnt1.getX() ),
        ( pnt0.getY() / pnt1.getY() ),
        ( pnt0.getZ() / pnt1.getZ() )
    );
}

inline const Point3 recipPerElem( const Point3 & pnt ) {
    return Point3(
        ( 1.0f / pnt.getX() ),
        ( 1.0f / pnt.getY() ),
        ( 1.0f / pnt.getZ() )
    );
}

inline const Point3 absPerElem( const Point3 & pnt ) {
    return Point3(
        fabsf( pnt.getX() ),
        fabsf( pnt.getY() ),
        fabsf( pnt.getZ() )
    );
}

inline const Point3 copySignPerElem( const Point3 & pnt0, const Point3 & pnt1 ) {
    return Point3(
        ( pnt1.getX() < 0.0f )? -fabsf( pnt0.getX() ) : fabsf( pnt0.getX() ),
        ( pnt1.getY() < 0.0f )? -fabsf( pnt0.getY() ) : fabsf( pnt0.getY() ),
        ( pnt1.getZ() < 0.0f )? -fabsf( pnt0.getZ() ) : fabsf( pnt0.getZ() )
    );
}

inline const Point3 maxPerElem( const Point3 & pnt0, const Point3 & pnt1 ) {
    return Point3(
        (pnt0.getX() > pnt1.getX())? pnt0.getX() : pnt1.getX(),
        (pnt0.getY() > pnt1.getY())? pnt0.getY() : pnt1.getY(),
        (pnt0.getZ() > pnt1.getZ())? pnt0.getZ() : pnt1.getZ()
    );
}

inline float maxElem( const Point3 & pnt ) {
    float result;
    result = (pnt.getX() > pnt.getY())? pnt.getX() : pnt.getY();
    result = (pnt.getZ() > result)? pnt.getZ() : result;
    return result;
}

inline const Point3 minPerElem( const Point3 & pnt0, const Point3 & pnt1 ) {
    return Point3(
        (pnt0.getX() < pnt1.getX())? pnt0.getX() : pnt1.getX(),
        (pnt0.getY() < pnt1.getY())? pnt0.getY() : pnt1.getY(),
        (pnt0.getZ() < pnt1.getZ())? pnt0.getZ() : pnt1.getZ()
    );
}

inline float minElem( const Point3 & pnt ) {
    float result;
    result = (pnt.getX() < pnt.getY())? pnt.getX() : pnt.getY();
    result = (pnt.getZ() < result)? pnt.getZ() : result;
    return result;
}

inline float sum( const Point3 & pnt ) {
    float result;
    result = ( pnt.getX() + pnt.getY() );
    result = ( result + pnt.getZ() );
    return result;
}

inline const Point3 scale( const Point3 & pnt, float scaleVal ) {
    return mulPerElem( pnt, Point3( scaleVal ) );
}

inline const Point3 scale( const Point3 & pnt, const Vector3 & scaleVec ) {
    return mulPerElem( pnt, Point3( scaleVec ) );
}

inline float projection( const Point3 & pnt, const Vector3 & unitVec ) {
    float result;
    result = ( pnt.getX() * unitVec.getX() );
    result = ( result + ( pnt.getY() * unitVec.getY() ) );
    result = ( result + ( pnt.getZ() * unitVec.getZ() ) );
    return result;
}

inline float distSqrFromOrigin( const Point3 & pnt ) {
    return lengthSqr( Vector3( pnt ) );
}

inline float distFromOrigin( const Point3 & pnt ) {
    return length( Vector3( pnt ) );
}

inline float distSqr( const Point3 & pnt0, const Point3 & pnt1 ) {
    return lengthSqr( ( pnt1 - pnt0 ) );
}

inline float dist( const Point3 & pnt0, const Point3 & pnt1 ) {
    return length( ( pnt1 - pnt0 ) );
}

inline const Point3 select( const Point3 & pnt0, const Point3 & pnt1, bool select1 ) {
    return Point3(
        ( select1 )? pnt1.getX() : pnt0.getX(),
        ( select1 )? pnt1.getY() : pnt0.getY(),
        ( select1 )? pnt1.getZ() : pnt0.getZ()
    );
}

#endif // __vec_h__
