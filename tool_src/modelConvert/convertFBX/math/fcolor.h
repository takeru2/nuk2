//-----------------------------------------------------------------------------
// File: fcolor.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __fcolor_h__
#define    __fcolor_h__

#include "math_utils.h"

//-----------------------------------------------------------------------------
// Name : FColor
// Desc : 
//-----------------------------------------------------------------------------
inline FColor::FColor( const float * color ){
    rgba[ 0 ] = color[ 0 ];
    rgba[ 1 ] = color[ 1 ];
    rgba[ 2 ] = color[ 2 ];
    rgba[ 3 ] = color[ 3 ];
}

inline FColor::FColor( float r, float g, float b, float a){
    rgba[ 0 ] = r;
    rgba[ 1 ] = g;
    rgba[ 2 ] = b;
    rgba[ 3 ] = a;
}

inline FColor::FColor( const PColor & color ) {
    rgba[ 0 ] = (float)color.getR() / 255.f;
    rgba[ 1 ] = (float)color.getG() / 255.f;
    rgba[ 2 ] = (float)color.getB() / 255.f;
    rgba[ 3 ] = (float)color.getA() / 255.f;
}

inline FColor::FColor( unsigned int color ) {
    *this = FColor(PColor(color));
}

inline FColor & FColor::operator =( const FColor & color ) {
    rgba[ 0 ] = color.rgba[ 0 ];
    rgba[ 1 ] = color.rgba[ 1 ];
    rgba[ 2 ] = color.rgba[ 2 ];
    rgba[ 3 ] = color.rgba[ 3 ];
    return *this;
}

inline FColor & FColor::setR( float r ){
    rgba[ 0 ] = r;
    return *this;
}

inline FColor & FColor::setG( float g ){
    rgba[ 1 ] = g;
    return *this;
}

inline FColor & FColor::setB( float b ){
    rgba[ 2 ] = b;
    return *this;
}

inline FColor & FColor::setA( float a ){
    rgba[ 3 ] = a;
    return *this;
}

inline float FColor::getR( ) const {
    return rgba[ 0 ];
}

inline float FColor::getG( ) const {
    return rgba[ 1 ];
}

inline float FColor::getB( ) const {
    return rgba[ 2 ];
}

inline float FColor::getA( ) const {
    return rgba[ 3 ];
}

inline FColor::operator FColor *() {
    return (FColor*)&rgba[ 0 ];
}

inline FColor::operator const FColor *() const {
    return (const FColor*)&rgba[ 0 ];
}

inline FColor::operator float *() {
    return (float*)&rgba[ 0 ];
}

inline FColor::operator const float *() const {
    return (const float*)&rgba[ 0 ];
}

inline const FColor FColor::operator *( const FColor & color ) const {
    return FColor(
        getR() * color.getR(),
        getG() * color.getG(),
        getB() * color.getB(),
        getA() * color.getA()
    );
}

inline FColor & FColor::operator *=( const FColor & color ) {
    *this = *this * color;
    return *this;
}

inline const FColor FColor::operator +( const FColor & color ) const {
    return FColor(
        getR() + color.getR(),
        getG() + color.getG(),
        getB() + color.getB(),
        getA() + color.getA()
    );
}

inline FColor & FColor::operator +=( const FColor & color ) {
    *this = *this + color;
    return *this;
}

inline const FColor FColor::operator *( float scalar ) const {
    return FColor(
        getR() * scalar,
        getG() * scalar,
        getB() * scalar,
        getA() * scalar
    );
}

inline FColor & FColor::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

//-----------------------------------------------------------------------------
// Name : HSVtoRGB
// Desc : 
//-----------------------------------------------------------------------------
inline const FColor HSVtoRGB( const FColor & hsvColor ){
    float rgba[4];
    float h,s,v;
    float i, f, p, q, t; 

    h = hsvColor.getR();
    s = hsvColor.getG();
    v = hsvColor.getB();

    if(s == 0.f){
        rgba[0] = rgba[1] = rgba[2] = v; 
    }else{
        if(h >= 1.f)h -= 1.f;
        h = h * 6.f;
        i = (float)floor(h);
        f = h - i;
        p = v * (1.f - s);
        q = v * (1.f - (s * f));
        t = v * (1.f - (s * (1.f - f)));
        if(i < 1){
            rgba[0] = v;rgba[1] = t;rgba[2] = p;
        }else if(i < 2){
            rgba[0] = q;rgba[1] = v;rgba[2] = p;
        }else if(i < 3){
            rgba[0] = p;rgba[1] = v;rgba[2] = t;
        }else if(i < 4){
            rgba[0] = p;rgba[1] = q;rgba[2] = v;
        }else if(i < 5){
            rgba[0] = t;rgba[1] = p;rgba[2] = v;
        }else{
            rgba[0] = v;rgba[1] = p;rgba[2] = q;
        }
    }
    return FColor(rgba[0], rgba[1], rgba[2], hsvColor.getA());
} 

//-----------------------------------------------------------------------------
// Name : RGBtoHSV
// Desc : 
//-----------------------------------------------------------------------------
inline const FColor RGBtoHSV( const FColor & rgbColor ){
    float r,g,b;
    float hsv[4];

    r = rgbColor.getR();
    g = rgbColor.getG();
    b = rgbColor.getB();

	float cmax = Max(r, Max(g, b));
	float cmin = Min(r, Min(g, b));
	float delta = cmax - cmin;
	hsv[2] = cmax;
	if(cmax != 0.0f){
		hsv[1] = delta / cmax;
	}else{
		hsv[1] = 0.0;
	}
	if(r == cmax ){
		hsv[0] =     (g - b) / delta;
	}else if (g == cmax){
		hsv[0] = 2 + (b - r) / delta;
	}else{
		hsv[0] = 4 + (r - g) / delta;
	}
	hsv[0] /= 6.0;
	if (hsv[0] < 0) hsv[0] += 1.0;

    return FColor(hsv[0], hsv[1], hsv[2], rgbColor.getA());
}


//-----------------------------------------------------------------------------
// Name : YUVtoRGB
// Desc : 
//-----------------------------------------------------------------------------
inline const FColor YUVtoRGB( const FColor & yuvColor ){
    float y = yuvColor.getR();
    float u = yuvColor.getG();
    float v = yuvColor.getB();

    float r = 1.164f * ( y - 0.0625f ) + 1.596f * ( y - 0.5f );
    float g = 1.164f * ( y - 0.0625f ) - 0.391f * ( u - 0.5f ) - 0.813f * ( v - 0.5f); 
    float b = 1.164f * ( y - 0.0625f ) + 2.018f * ( u - 0.5f );
    return FColor(r, g, b, yuvColor.getA());
}

//-----------------------------------------------------------------------------
// Name : RGBtoYUV
// Desc : 
//-----------------------------------------------------------------------------
inline const FColor RGBtoYUV( const FColor & rgbColor ){
    float r = rgbColor.getR();
    float g = rgbColor.getG();
    float b = rgbColor.getB();

    float y =  0.257f * r + 0.504f * g + 0.098f * b + 0.0625f;
    float u = -0.148f * r - 0.291f * g + 0.439f * b + 0.5f;
    float v =  0.439f * r - 0.368f * g - 0.071f * b + 0.5f;

    return FColor(y, u, v, 1.f);
}

#endif // __fcolor_h__
