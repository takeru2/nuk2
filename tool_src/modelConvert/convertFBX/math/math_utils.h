﻿//-----------------------------------------------------------------------------
// File: math_utils.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __math_utils_h__
#define    __math_utils_h__

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <vector>

#include "vectormath.h"

#ifndef M_PI
#define M_PI 3.141592654f
#endif

#define M_2PI (2.f * M_PI)
#define M_hPI (0.5f * M_PI)
#define DEG2RAD(_deg_) (M_PI * (_deg_) / 180.f)
#define RAD2DEG(_rad_) (360.f * (_rad_) / M_2PI)

#define Max(_a_,_b_)            (((_a_) > (_b_)) ? (_a_) : (_b_))
#define Min(_a_,_b_)            (((_a_) < (_b_)) ? (_a_) : (_b_))
#define Clamp(_a_, _low_, _high_) (Max(_low_, Min(_a_, _high_)))
#define Abs(_a_)                (((_a_) < 0) ? -(_a_) : (_a_))
#define Sign(_a_)               (((_a_) < 0) ? -1 : 1)


template<typename T> inline void Swap(T &a, T &b){T tmp = a;a = b;b = tmp;}
//template<typename T> inline T Lerp(T &a, T &b, float s){return a + (b - a)*s;}
#define Lerp(_a_, _b_, _s_) ((_a_) + ((_b_) - (_a_)) * (_s_))

//#ifndef min
//#define min Min
//#endif
//#ifndef max
//#define max Max
//#endif
//#define clamp (max(_low_, min(_a_, _high_)))
//#define lerp(_a_, _b_, _c_)  (_a_ + (_b_ - _a_)*(_c_))


#include "color.h"

template<typename _T> class RectT {
public:
    _T left,top,right,bottom;
    inline RectT( ) { };
    inline RectT(_T _left,_T _top, _T _right, _T _bottom){
        left = _left;top = _top;right = _right;bottom = _bottom;
    }
    inline RectT & Offset(_T dx, _T dy){
        left += dx;right += dx;top += dy;bottom += dy;
        return *this;
    }
    inline bool Test(_T px, _T py){
        return ((left <= px)&&(right >= px)&&(top <= py)&&(bottom >= py))?true:false;
    }
};
typedef RectT<float> RectF;
typedef RectT<int> RectI;

struct RandContext {unsigned long x, y, z, w;};
float Rand(RandContext *pCtx = NULL);
unsigned int Rand(RandContext *pCtx, unsigned int range);
void RandSeed(RandContext *pCtx, unsigned int seed);

inline float coefficent(float t){
    float r,d;
    if(t < 0.f)t = -t;
    if(t < 1.f){
        r = (3.f * t * t * t -6.f * t * t + 4.f) / 6.f;
    }else if(t < 2.f){
        d = t - 2.f;
        r = -d * d * d / 6.f;
    }else r = 0.f;
    return r;
}

inline Vector2 spline(const std::vector<Vector2> &posArray, int n, float t){
    float cn;
    int j,k;
    Vector2 dp(0,0);
    for(j = -2;j <= n + 2;j++){
        k = j;
        if(j < 0)k = 0;
        if(j > n)k = n;
        cn = coefficent(t - j);
        dp += posArray[k] * cn;
    }
    return dp;
}

#endif // __math_utils_h__
