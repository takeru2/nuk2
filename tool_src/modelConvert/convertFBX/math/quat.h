﻿//-----------------------------------------------------------------------------
// File: quat.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __quat_h__
#define    __quat_h__


inline Quat::Quat( const Quat & quat ) {
    x = quat.x;
    y = quat.y;
    z = quat.z;
    w = quat.w;
}

inline Quat::Quat( float _x, float _y, float _z, float _w ) {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
}

inline Quat::Quat( const Vector3 & xyz, float _w ) {
    this->setXYZ( xyz );
    this->setW( _w );
}

inline Quat::Quat( const Vector4 & vec ) {
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
    w = vec.getW();
}

inline Quat::Quat( float scalar ) {
    x = scalar;
    y = scalar;
    z = scalar;
    w = scalar;
}

inline const Quat Quat::identity( ) {
    return Quat( 0.0f, 0.0f, 0.0f, 1.0f );
}

inline const Quat lerp( float t, const Quat & quat0, const Quat & quat1 ) {
    return ( quat0 + ( ( quat1 - quat0 ) * t ) );
}

inline const Quat slerp( float t, const Quat & unitQuat0, const Quat & unitQuat1 ) {
    Quat start;
    float recipSinAngle, scale0, scale1, cosAngle, angle;
    cosAngle = dot( unitQuat0, unitQuat1 );
    if ( cosAngle < 0.0f ) {
        cosAngle = -cosAngle;
        start = ( -unitQuat0 );
    } else {
        start = unitQuat0;
    }
    if ( cosAngle < _VECTORMATH_SLERP_TOL ) {
        angle = acosf( cosAngle );
        recipSinAngle = ( 1.0f / sinf( angle ) );
        scale0 = ( sinf( ( ( 1.0f - t ) * angle ) ) * recipSinAngle );
        scale1 = ( sinf( ( t * angle ) ) * recipSinAngle );
    } else {
        scale0 = ( 1.0f - t );
        scale1 = t;
    }
    return ( ( start * scale0 ) + ( unitQuat1 * scale1 ) );
}

inline const Quat squad( float t, const Quat & unitQuat0, const Quat & unitQuat1, const Quat & unitQuat2, const Quat & unitQuat3 ) {
    Quat tmp0, tmp1;
    tmp0 = slerp( t, unitQuat0, unitQuat3 );
    tmp1 = slerp( t, unitQuat1, unitQuat2 );
    return slerp( ( ( 2.0f * t ) * ( 1.0f - t ) ), tmp0, tmp1 );
}

inline Quat & Quat::operator =( const Quat & quat ) {
    x = quat.x;
    y = quat.y;
    z = quat.z;
    w = quat.w;
    return *this;
}

inline Quat & Quat::setXYZ( const Vector3 & vec ) {
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
    return *this;
}

inline const Vector3 Quat::getXYZ( ) const {
    return Vector3( x, y, z );
}

inline Quat & Quat::setX( float _x ) {
    x = _x;
    return *this;
}

inline float Quat::getX( ) const {
    return x;
}

inline Quat & Quat::setY( float _y ) {
    y = _y;
    return *this;
}

inline float Quat::getY( ) const {
    return y;
}

inline Quat & Quat::setZ( float _z ) {
    z = _z;
    return *this;
}

inline float Quat::getZ( ) const {
    return z;
}

inline Quat & Quat::setW( float _w ) {
    w = _w;
    return *this;
}

inline float Quat::getW( ) const {
    return w;
}

inline Quat & Quat::setElem( int idx, float value ) {
    *(&x + idx) = value;
    return *this;
}

inline float Quat::getElem( int idx ) const {
    return *(&x + idx);
}

inline float & Quat::operator []( int idx ) {
    return *(&x + idx);
}

inline float Quat::operator []( int idx ) const {
    return *(&x + idx);
}

inline const Quat Quat::operator +( const Quat & quat ) const {
    return Quat(
        ( x + quat.x ),
        ( y + quat.y ),
        ( z + quat.z ),
        ( w + quat.w )
    );
}

inline const Quat Quat::operator -( const Quat & quat ) const {
    return Quat(
        ( x - quat.x ),
        ( y - quat.y ),
        ( z - quat.z ),
        ( w - quat.w )
    );
}

inline const Quat Quat::operator *( float scalar ) const {
    return Quat(
        ( x * scalar ),
        ( y * scalar ),
        ( z * scalar ),
        ( w * scalar )
    );
}

inline Quat & Quat::operator +=( const Quat & quat ) {
    *this = *this + quat;
    return *this;
}

inline Quat & Quat::operator -=( const Quat & quat ) {
    *this = *this - quat;
    return *this;
}

inline Quat & Quat::operator *=( float scalar ) {
    *this = *this * scalar;
    return *this;
}

inline const Quat Quat::operator /( float scalar ) const {
    return Quat(
        ( x / scalar ),
        ( y / scalar ),
        ( z / scalar ),
        ( w / scalar )
    );
}

inline Quat & Quat::operator /=( float scalar ) {
    *this = *this / scalar;
    return *this;
}

inline const Quat Quat::operator -( ) const {
    return Quat(
        -x,
        -y,
        -z,
        -w
    );
}

inline const Quat operator *( float scalar, const Quat & quat ) {
    return quat * scalar;
}

inline float dot( const Quat & quat0, const Quat & quat1 ) {
    float result;
    result = ( quat0.getX() * quat1.getX() );
    result = ( result + ( quat0.getY() * quat1.getY() ) );
    result = ( result + ( quat0.getZ() * quat1.getZ() ) );
    result = ( result + ( quat0.getW() * quat1.getW() ) );
    return result;
}

inline float norm( const Quat & quat ) {
    float result;
    result = ( quat.getX() * quat.getX() );
    result = ( result + ( quat.getY() * quat.getY() ) );
    result = ( result + ( quat.getZ() * quat.getZ() ) );
    result = ( result + ( quat.getW() * quat.getW() ) );
    return result;
}

inline float length( const Quat & quat ) {
    return sqrtf( norm( quat ) );
}

inline const Quat normalize( const Quat & quat ) {
    float lenSqr, lenInv;
    lenSqr = norm( quat );
    lenInv = ( 1.0f / sqrtf( lenSqr ) );
    return Quat(
        ( quat.getX() * lenInv ),
        ( quat.getY() * lenInv ),
        ( quat.getZ() * lenInv ),
        ( quat.getW() * lenInv )
    );
}

inline const Quat Quat::rotation( const Vector3 & unitVec0, const Vector3 & unitVec1 ) {
    float cosHalfAngleX2, recipCosHalfAngleX2;
    cosHalfAngleX2 = sqrtf( ( 2.0f * ( 1.0f + dot( unitVec0, unitVec1 ) ) ) );
    recipCosHalfAngleX2 = ( 1.0f / cosHalfAngleX2 );
    return Quat( ( cross( unitVec0, unitVec1 ) * recipCosHalfAngleX2 ), ( cosHalfAngleX2 * 0.5f ) );
}

inline const Quat Quat::rotation( float radians, const Vector3 & unitVec ) {
    float s, c, angle;
    angle = ( radians * 0.5f );
    s = sinf( angle );
    c = cosf( angle );
    return Quat( ( unitVec * s ), c );
}

inline const Quat Quat::rotationX( float radians ) {
    float s, c, angle;
    angle = ( radians * 0.5f );
    s = sinf( angle );
    c = cosf( angle );
    return Quat( s, 0.0f, 0.0f, c );
}

inline const Quat Quat::rotationY( float radians ) {
    float s, c, angle;
    angle = ( radians * 0.5f );
    s = sinf( angle );
    c = cosf( angle );
    return Quat( 0.0f, s, 0.0f, c );
}

inline const Quat Quat::rotationZ( float radians ) {
    float s, c, angle;
    angle = ( radians * 0.5f );
    s = sinf( angle );
    c = cosf( angle );
    return Quat( 0.0f, 0.0f, s, c );
}

inline const Quat Quat::operator *( const Quat & quat ) const {
    return Quat(
        ( ( ( ( w * quat.x ) + ( x * quat.w ) ) + ( y * quat.z ) ) - ( z * quat.y ) ),
        ( ( ( ( w * quat.y ) + ( y * quat.w ) ) + ( z * quat.x ) ) - ( x * quat.z ) ),
        ( ( ( ( w * quat.z ) + ( z * quat.w ) ) + ( x * quat.y ) ) - ( y * quat.x ) ),
        ( ( ( ( w * quat.w ) - ( x * quat.x ) ) - ( y * quat.y ) ) - ( z * quat.z ) )
    );
}

inline Quat & Quat::operator *=( const Quat & quat ) {
    *this = *this * quat;
    return *this;
}

inline const Vector3 rotate( const Quat & quat, const Vector3 & vec ) {
    float tmpX, tmpY, tmpZ, tmpW;
    tmpX = ( ( ( quat.getW() * vec.getX() ) + ( quat.getY() * vec.getZ() ) ) - ( quat.getZ() * vec.getY() ) );
    tmpY = ( ( ( quat.getW() * vec.getY() ) + ( quat.getZ() * vec.getX() ) ) - ( quat.getX() * vec.getZ() ) );
    tmpZ = ( ( ( quat.getW() * vec.getZ() ) + ( quat.getX() * vec.getY() ) ) - ( quat.getY() * vec.getX() ) );
    tmpW = ( ( ( quat.getX() * vec.getX() ) + ( quat.getY() * vec.getY() ) ) + ( quat.getZ() * vec.getZ() ) );
    return Vector3(
        ( ( ( ( tmpW * quat.getX() ) + ( tmpX * quat.getW() ) ) - ( tmpY * quat.getZ() ) ) + ( tmpZ * quat.getY() ) ),
        ( ( ( ( tmpW * quat.getY() ) + ( tmpY * quat.getW() ) ) - ( tmpZ * quat.getX() ) ) + ( tmpX * quat.getZ() ) ),
        ( ( ( ( tmpW * quat.getZ() ) + ( tmpZ * quat.getW() ) ) - ( tmpX * quat.getY() ) ) + ( tmpY * quat.getX() ) )
    );
}

inline const Quat conj( const Quat & quat ) {
    return Quat( -quat.getX(), -quat.getY(), -quat.getZ(), quat.getW() );
}

inline const Quat select( const Quat & quat0, const Quat & quat1, bool select1 ) {
    return Quat(
        ( select1 )? quat1.getX() : quat0.getX(),
        ( select1 )? quat1.getY() : quat0.getY(),
        ( select1 )? quat1.getZ() : quat0.getZ(),
        ( select1 )? quat1.getW() : quat0.getW()
    );
}


#endif // __quat_h__
