//-----------------------------------------------------------------------------
// File: math_utils.cpp
//
// Desc: 
//
// Copyright (C) 2009 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------
#define    __math_utils_cpp__

#include "math_utils.h"

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
static const RandContext scRandCtx = {123456789,362436069,521288629,88675123};
static RandContext sRandCtx = scRandCtx;

//-----------------------------------------------------------------------------
// Function-prototypes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Name : XorShift
// Desc : 
//-----------------------------------------------------------------------------
static inline unsigned long XorShift(RandContext &ctx){
    unsigned long &x = ctx.x;
    unsigned long &y = ctx.y;
    unsigned long &z = ctx.z;
    unsigned long &w = ctx.w;
    unsigned long t;
    t=(x^(x<<11));x=y;y=z;z=w;
    return(w=(w^(w>>19))^(t^(t>>8)));
}

//-----------------------------------------------------------------------------
// Name : Rand
// Desc : 
//-----------------------------------------------------------------------------
float Rand(RandContext *pCtx){
    if(pCtx == NULL){
        return static_cast<float>(XorShift(sRandCtx)) / (0xffffffff - 1);
    }else{
        return static_cast<float>(XorShift(*pCtx)) / (0xffffffff - 1);
    }
}

unsigned int Rand(RandContext *pCtx, unsigned int range){
    if(range == 0)return 0;

    if(pCtx == NULL){
        return XorShift(sRandCtx) % range;
    }else{
        return XorShift(*pCtx) % range;
    }
}

//-----------------------------------------------------------------------------
// Name : RandSeed
// Desc : 
//-----------------------------------------------------------------------------
void RandSeed(RandContext *pCtx, unsigned int seed){
    if(pCtx == NULL)pCtx = &sRandCtx;
    if(seed == 0){
        *pCtx = scRandCtx;
    }else{
        unsigned long s = seed;
        for(int i=0;i<4;i++)reinterpret_cast<unsigned long*>(pCtx)[i] = 
                                s = 1812433253 * (s^(s >> 30)) + i;
    }
}

