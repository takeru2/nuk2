﻿//-----------------------------------------------------------------------------
// File: color.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __color_h__
#define    __color_h__

#include "vectormath.h"

class PColor;
class FColor;

class PColor {
public:
    union{
        unsigned int dwColor;
        unsigned char  rgba[4];
    };
    inline PColor() {};
    inline PColor( unsigned int color );
    inline PColor( unsigned char r, unsigned char g, unsigned char b, unsigned char a);
    inline PColor( const FColor & color );
    inline PColor( const Vector3 & vec );
    inline PColor( const Vector4 & vec );

    inline PColor & operator =( const PColor & color );

    inline PColor operator * ( float scalar ) const;
    inline PColor & operator *=( float scalar );
    inline PColor operator * ( const PColor &color ) const;
    inline PColor & operator *= ( const PColor &color );
    inline const PColor operator +( const PColor & color ) const;
    inline const PColor operator -( const PColor & color ) const;

    inline PColor & setR( unsigned char r );
    inline PColor & setG( unsigned char g );
    inline PColor & setB( unsigned char b );
    inline PColor & setA( unsigned char a );

    inline unsigned char getR( ) const;
    inline unsigned char getG( ) const;
    inline unsigned char getB( ) const;
    inline unsigned char getA( ) const;

    inline operator PColor *();
    inline operator const PColor *() const;
    inline operator unsigned char *();
    inline operator const unsigned char *() const ;
    inline operator unsigned int () const;

};


class FColor {
public:
    float  rgba[4];

    inline FColor() {};
    inline FColor( const float * color );
    inline FColor( float r, float g, float b, float a );
    inline FColor( const PColor & color );
    inline FColor( unsigned int color );

    inline FColor & operator =( const FColor & color );

    inline FColor & setR( float r );
    inline FColor & setG( float g );
    inline FColor & setB( float b );
    inline FColor & setA( float a );

    inline float getR( ) const;
    inline float getG( ) const;
    inline float getB( ) const;
    inline float getA( ) const;

    inline operator FColor *();
    inline operator const FColor *() const;
    inline operator float *();
    inline operator const float *() const ;

    inline const FColor operator *( const FColor & color ) const;
    inline FColor & operator *=( const FColor & color );
    inline const FColor operator +( const FColor & color ) const;
    inline FColor & operator +=( const FColor & color );
    inline const FColor operator *( float scalar ) const;
    inline FColor & operator *=( float scalar );

};

inline const FColor hsvFColor( float h, float s, float v){
    float cx,cy,cz;
    if( s == 0 ){
        cx = cy = cz = v;
    }else{
        if( 1.0 <= h ) h -= 1.0;
          h *= 6.0;
        float i = floor (h);
        float f = h - i;
        float aa = v * (1 - s);
        float bb = v * (1 - (s * f));
        float cc = v * (1 - (s * (1 - f)));
        if( i < 1 ){
            cx = v;  cy = cc; cz = aa;
        }else if( i < 2 ){
            cx = bb; cy = v;  cz = aa;
        }else if( i < 3 ){
            cx = aa; cy = v;  cz = cc;
        }else if( i < 4 ){
            cx = aa; cy = bb; cz = v;
        }else if( i < 5 ){
            cx = cc; cy = aa; cz = v;
        }else{
            cx = v;  cy = aa; cz = bb;
        }
    }
    return FColor(cx,cy,cz,1);
}

#include "pcolor.h"
#include "fcolor.h"

#endif // __color_h__
