﻿//-----------------------------------------------------------------------------
// File: vectormath.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __vectormath_h__
#define    __vectormath_h__

#include <math.h>

class Vector2I;

class Vector2;
class Vector3;
class Vector4;
class Point2;
class Point3;
class Quat;
class Matrix2;
class Transform2;
class Matrix3;
class Matrix4;
class Transform3;


class Vector2I {
public:
    int x,y;

public:
    inline Vector2I( ) { };
    inline Vector2I( int x, int y );
    inline const Vector2I operator +( const Vector2I & vec ) const;
    inline const Vector2I operator -( const Vector2I & vec ) const;
    inline Vector2I & operator +=( const Vector2I & vec );
    inline Vector2I & operator -=( const Vector2I & vec );
    inline int getX( ) const;
    inline int getY( ) const;

};
inline int cross( const Vector2I & vec0, const Vector2I & vec1 );

class Point2 {
public:
    float x;
    float y;
public:
    inline Point2( ) { };
    inline Point2( const Vector2 & vec );
    inline Point2( float x, float y );

    inline Point2 & setX( float x );
    inline Point2 & setY( float y );
    inline Point2 & setXY( float x, float y);
    inline float getX( ) const;
    inline float getY( ) const;
};

class Vector2 {
public:
    float x;
    float y;

public:
    inline Vector2( ) { };
    inline Vector2( const Vector2 & vec );
    inline Vector2( const Point2 & pnt );
    inline Vector2( float x, float y );
    explicit inline Vector2( float scalar );

    inline Vector2 & operator =( const Vector2 & vec );

    inline Vector2 & setX( float x );
    inline Vector2 & setY( float y );
    inline Vector2 & setXY( float x, float y);
    inline float getX( ) const;
    inline float getY( ) const;
    inline Vector2 & setElem( int idx, float value );
    inline float getElem( int idx ) const;
    inline float & operator []( int idx );
    inline float operator []( int idx ) const;

    inline operator Vector2 *();
    inline operator const Vector2 *() const;
    inline operator const float *() const;

    inline const Vector2 operator +( const Vector2 & vec ) const;
    inline const Vector2 operator -( const Vector2 & vec ) const;
    inline const Vector2 operator *( float scalar ) const;
    inline const Vector2 operator /( float scalar ) const;
    inline Vector2 & operator +=( const Vector2 & vec );
    inline Vector2 & operator -=( const Vector2 & vec );
    inline Vector2 & operator *=( float scalar );
    inline Vector2 & operator /=( float scalar );
    inline const Vector2 operator -( ) const;

    inline const Vector2 & rotation( float radians );

    static inline const Vector2 xAxis( );
    static inline const Vector2 yAxis( );

};

inline const Vector2 operator *( float scalar, const Vector2 & vec );
inline const Vector2 mulPerElem( const Vector2 & vec0, const Vector2 & vec1 );
inline const Vector2 divPerElem( const Vector2 & vec0, const Vector2 & vec1 );
inline const Vector2 absPerElem( const Vector2 & vec );
inline float lengthSqr( const Vector2 & vec );
inline float length( const Vector2 & vec );
inline float lengthSafety( const Vector2 & vec );
inline const Vector2 normalize( const Vector2 & vec );
inline const Vector2 normalizeSafety( const Vector2 & vec );
inline float dot( const Vector2 & vec0, const Vector2 & vec1 );
inline float cross( const Vector2 & vec0, const Vector2 & vec1 );
inline const Vector2 cross( const Vector2 & vec, float scalar);
inline const Vector2 cross( float scalar, const Vector2 & vec);
inline const Vector2 lerp( float t, const Vector2 & vec0, const Vector2 & vec1 );



class Vector3 {
public:
    float x;
    float y;
    float z;
    float w;

public:
    inline Vector3( ) { };
    inline Vector3( const Vector3 & vec );
    inline Vector3( float x, float y, float z );
    explicit inline Vector3( const Point3 & pnt );
    explicit inline Vector3( float scalar );

    inline Vector3 & operator =( const Vector3 & vec );
    inline Vector3 & operator =( const Point3 & pnt );

    inline Vector3 & setX( float x );
    inline Vector3 & setY( float y );
    inline Vector3 & setZ( float z );
    inline Vector3 & setXYZ( float x, float y, float z);

    inline float getX( ) const;
    inline float getY( ) const;
    inline float getZ( ) const;
    inline Vector3 & setElem( int idx, float value );
    inline float getElem( int idx ) const;
    inline float & operator []( int idx );
    inline float operator []( int idx ) const;

    inline operator Vector3 *();
    inline operator const Vector3 *() const;
    inline operator const float *() const;

    inline const Vector3 operator +( const Vector3 & vec ) const;
    inline const Vector3 operator -( const Vector3 & vec ) const;
    inline const Point3 operator +( const Point3 & pnt ) const;
    inline const Vector3 operator *( float scalar ) const;
    inline const Vector3 operator /( float scalar ) const;
    inline Vector3 & operator +=( const Vector3 & vec );
    inline Vector3 & operator -=( const Vector3 & vec );
    inline Vector3 & operator *=( float scalar );
    inline Vector3 & operator /=( float scalar );
    inline const Vector3 operator -( ) const;

    inline const Vector3 & rotationX( float radians );
    inline const Vector3 & rotationY( float radians );
    inline const Vector3 & rotationZ( float radians );

    static inline const Vector3 xAxis( );
    static inline const Vector3 yAxis( );
    static inline const Vector3 zAxis( );

};


inline const Vector3 operator *( float scalar, const Vector3 & vec );
inline const Vector3 mulPerElem( const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 divPerElem( const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 recipPerElem( const Vector3 & vec );
inline const Vector3 absPerElem( const Vector3 & vec );
inline const Vector3 copySignPerElem( const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 maxPerElem( const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 minPerElem( const Vector3 & vec0, const Vector3 & vec1 );
inline float maxElem( const Vector3 & vec );
inline float minElem( const Vector3 & vec );
inline float sum( const Vector3 & vec );
inline float dot( const Vector3 & vec0, const Vector3 & vec1 );
inline float lengthSqr( const Vector3 & vec );
inline float length( const Vector3 & vec );
inline float lengthSafety( const Vector3 & vec );
inline const Vector3 normalize( const Vector3 & vec );
inline const Vector3 normalizeSafety( const Vector3 & vec );
inline const Vector3 cross( const Vector3 & vec0, const Vector3 & vec1 );
inline const Matrix3 outer( const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 rowMul( const Vector3 & vec, const Matrix3 & mat );
inline const Matrix3 crossMatrix( const Vector3 & vec );
inline const Matrix3 crossMatrixMul( const Vector3 & vec, const Matrix3 & mat );
inline const Vector3 lerp( float t, const Vector3 & vec0, const Vector3 & vec1 );
inline const Vector3 slerp( float t, const Vector3 & unitVec0, const Vector3 & unitVec1 );
inline const Vector3 select( const Vector3 & vec0, const Vector3 & vec1, bool select1 );



class Vector4 {
public:
    float x;
    float y;
    float z;
    float w;

public:
    inline Vector4( ) { };
    inline Vector4( const Vector4 & vec );
    inline Vector4( float x, float y, float z, float w );
    inline Vector4( const Vector3 & xyz, float w );
    explicit inline Vector4( const Vector3 & vec );
    explicit inline Vector4( const Point3 & pnt );
    explicit inline Vector4( const Quat & quat );
    explicit inline Vector4( float scalar );
    explicit inline Vector4( const float * vec );

    inline Vector4 & operator =( const Vector4 & vec );

    inline Vector4 & setXYZ( const Vector3 & vec );
    inline const Vector3 getXYZ( ) const;
    inline Vector4 & setX( float x );
    inline Vector4 & setY( float y );
    inline Vector4 & setZ( float z );
    inline Vector4 & setW( float w );
    inline Vector4 & setXYZW( float x, float y, float z, float w );
    inline float getX( ) const;
    inline float getY( ) const;
    inline float getZ( ) const;
    inline float getW( ) const;
    inline Vector4 & setElem( int idx, float value );
    inline float getElem( int idx ) const;
    inline float & operator []( int idx );
    inline float operator []( int idx ) const;

    inline operator Vector4 *();
    inline operator const Vector4 *() const;
    inline operator const float *() const;

    inline const Vector4 operator +( const Vector4 & vec ) const;
    inline const Vector4 operator -( const Vector4 & vec ) const;
    inline const Vector4 operator *( float scalar ) const;
    inline const Vector4 operator /( float scalar ) const;
    inline Vector4 & operator +=( const Vector4 & vec );
    inline Vector4 & operator -=( const Vector4 & vec );
    inline Vector4 & operator *=( float scalar );
    inline Vector4 & operator /=( float scalar );
    inline const Vector4 operator -( ) const;

    inline const Vector4 & rotationX( float radians );
    inline const Vector4 & rotationY( float radians );
    inline const Vector4 & rotationZ( float radians );

    static inline const Vector4 xAxis( );
    static inline const Vector4 yAxis( );
    static inline const Vector4 zAxis( );
    static inline const Vector4 wAxis( );

};

inline const Vector4 operator *( float scalar, const Vector4 & vec );
inline const Vector4 mulPerElem( const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 divPerElem( const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 recipPerElem( const Vector4 & vec );
inline const Vector4 absPerElem( const Vector4 & vec );
inline const Vector4 copySignPerElem( const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 maxPerElem( const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 minPerElem( const Vector4 & vec0, const Vector4 & vec1 );
inline float maxElem( const Vector4 & vec );
inline float minElem( const Vector4 & vec );
inline float sum( const Vector4 & vec );
inline float dot( const Vector4 & vec0, const Vector4 & vec1 );
inline float lengthSqr( const Vector4 & vec );
inline float length( const Vector4 & vec );
inline float lengthSafety( const Vector4 & vec );
inline const Vector4 normalize( const Vector4 & vec );
inline const Vector4 normalizeSafety( const Vector4 & vec );
inline const Matrix4 outer( const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 lerp( float t, const Vector4 & vec0, const Vector4 & vec1 );
inline const Vector4 slerp( float t, const Vector4 & unitVec0, const Vector4 & unitVec1 );
inline const Vector4 select( const Vector4 & vec0, const Vector4 & vec1, bool select1 );


class Point3 {
public:
    float x;
    float y;
    float z;
    float w;

public:
    inline Point3( ) { };
    inline Point3( const Point3 & pnt );
    inline Point3( float x, float y, float z );
    explicit inline Point3( const Vector3 & vec );
    explicit inline Point3( float scalar );

    inline Point3 & operator =( const Point3 & pnt );
    inline Point3 & operator =( const Vector3 & vec );

    inline Point3 & setX( float x );
    inline Point3 & setY( float y );
    inline Point3 & setZ( float z );
    inline Point3 & setXYZ( float x, float y, float z);
    inline float getX( ) const;
    inline float getY( ) const;
    inline float getZ( ) const;
    inline Point3 & setElem( int idx, float value );
    inline float getElem( int idx ) const;
    inline float & operator []( int idx );
    inline float operator []( int idx ) const;

    inline operator Point3 *();
    inline operator const Point3 *() const;
    inline operator const float *() const;

    inline const Vector3 operator -( const Point3 & pnt ) const;
    inline const Point3 operator +( const Vector3 & vec ) const;
    inline const Point3 operator -( const Vector3 & vec ) const;
    inline const Point3 operator *( float scalar ) const;
    inline const Point3 operator /( float scalar ) const;
    inline Point3 & operator +=( const Vector3 & vec );
    inline Point3 & operator -=( const Vector3 & vec );
    inline Point3 & operator *=( float scalar );
    inline Point3 & operator /=( float scalar );

};

inline const Point3 mulPerElem( const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 divPerElem( const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 recipPerElem( const Point3 & pnt );
inline const Point3 absPerElem( const Point3 & pnt );
inline const Point3 copySignPerElem( const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 maxPerElem( const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 minPerElem( const Point3 & pnt0, const Point3 & pnt1 );
inline float maxElem( const Point3 & pnt );
inline float minElem( const Point3 & pnt );
inline float sum( const Point3 & pnt );
inline const Point3 scale( const Point3 & pnt, float scaleVal );
inline const Point3 scale( const Point3 & pnt, const Vector3 & scaleVec );
inline float projection( const Point3 & pnt, const Vector3 & unitVec );
inline float distSqrFromOrigin( const Point3 & pnt );
inline float distFromOrigin( const Point3 & pnt );
inline float distSqr( const Point3 & pnt0, const Point3 & pnt1 );
inline float dist( const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 lerp( float t, const Point3 & pnt0, const Point3 & pnt1 );
inline const Point3 select( const Point3 & pnt0, const Point3 & pnt1, bool select1 );


class Quat {
public:
    float x;
    float y;
    float z;
    float w;

public:
    inline Quat( ) { };
    inline Quat( const Quat & quat );
    inline Quat( float x, float y, float z, float w );
    inline Quat( const Vector3 & xyz, float w );
    explicit inline Quat( const Vector4 & vec );
    explicit inline Quat( const Matrix3 & rotMat );
    explicit inline Quat( float scalar );

    inline Quat & operator =( const Quat & quat );

    inline Quat & setXYZ( const Vector3 & vec );
    inline const Vector3 getXYZ( ) const;
    inline Quat & setX( float x );
    inline Quat & setY( float y );
    inline Quat & setZ( float z );
    inline Quat & setW( float w );
    inline float getX( ) const;
    inline float getY( ) const;
    inline float getZ( ) const;
    inline float getW( ) const;
    inline Quat & setElem( int idx, float value );
    inline float getElem( int idx ) const;
    inline float & operator []( int idx );
    inline float operator []( int idx ) const;

    inline const Quat operator +( const Quat & quat ) const;
    inline const Quat operator -( const Quat & quat ) const;
    inline const Quat operator *( const Quat & quat ) const;
    inline const Quat operator *( float scalar ) const;
    inline const Quat operator /( float scalar ) const;
    inline Quat & operator +=( const Quat & quat );
    inline Quat & operator -=( const Quat & quat );
    inline Quat & operator *=( const Quat & quat );
    inline Quat & operator *=( float scalar );
    inline Quat & operator /=( float scalar );
    inline const Quat operator -( ) const;

    static inline const Quat identity( );
    static inline const Quat rotation( const Vector3 & unitVec0, const Vector3 & unitVec1 );
    static inline const Quat rotation( float radians, const Vector3 & unitVec );
    static inline const Quat rotationX( float radians );
    static inline const Quat rotationY( float radians );
    static inline const Quat rotationZ( float radians );

};

inline const Quat operator *( float scalar, const Quat & quat );
inline const Quat conj( const Quat & quat );
inline const Vector3 rotate( const Quat & unitQuat, const Vector3 & vec );
inline float dot( const Quat & quat0, const Quat & quat1 );
inline float norm( const Quat & quat );
inline float length( const Quat & quat );
inline const Quat normalize( const Quat & quat );
inline const Quat lerp( float t, const Quat & quat0, const Quat & quat1 );
inline const Quat slerp( float t, const Quat & unitQuat0, const Quat & unitQuat1 );
inline const Quat squad( float t, const Quat & unitQuat0, const Quat & unitQuat1, const Quat & unitQuat2, const Quat & unitQuat3 );
inline const Quat select( const Quat & quat0, const Quat & quat1, bool select1 );


class Matrix2 {
    Vector2 mCol0;
    Vector2 mCol1;

public:
    inline Matrix2( ) { };
    inline Matrix2( const Matrix2 & mat );
    inline Matrix2( const Vector2 & col0, const Vector2 & col1 );
    explicit inline Matrix2( float radians );

    inline Matrix2 & operator =( const Matrix2 & mat );

    inline Matrix2 & setCol0( const Vector2 & col0 );
    inline Matrix2 & setCol1( const Vector2 & col1 );
    inline const Vector2 getCol0( ) const;
    inline const Vector2 getCol1( ) const;
    inline Matrix2 & setCol( int col, const Vector2 & vec );
    inline Matrix2 & setRow( int row, const Vector2 & vec );
    inline const Vector2 getCol( int col ) const;
    inline const Vector2 getRow( int row ) const;
    inline Vector2 & operator []( int col );
    inline const Vector2 operator []( int col ) const;
    inline Matrix2 & setElem( int col, int row, float val );
    inline float getElem( int col, int row ) const;

    inline const Matrix2 operator +( const Matrix2 & mat ) const;
    inline const Matrix2 operator -( const Matrix2 & mat ) const;
    inline const Matrix2 operator -( ) const;
    inline const Matrix2 operator *( float scalar ) const;
    inline const Vector2 operator *( const Vector2 & vec ) const;
    inline const Matrix2 operator *( const Matrix2 & mat ) const;
    inline Matrix2 & operator +=( const Matrix2 & mat );
    inline Matrix2 & operator -=( const Matrix2 & mat );
    inline Matrix2 & operator *=( float scalar );
    inline Matrix2 & operator *=( const Matrix2 & mat );

    static inline const Matrix2 identity( );
};


inline const Matrix2 absPerElem( const Matrix2 & mat );
inline const Matrix2 transpose( const Matrix2 & mat );
inline const Matrix2 inverse( const Matrix2 & mat );
inline float determinant( const Matrix2 & mat );
inline const Matrix2 normalize( const Matrix2 & mat );

class Transform2 {
    Vector2 mCol0;
    Vector2 mCol1;
    Vector2 mCol2;

public:
    inline Transform2( ) { };
    inline Transform2( const Transform2 & tfrm );
    inline Transform2( const Vector2 & col0, const Vector2 & col1, const Vector2 & col2 );
    inline Transform2( const Matrix2 & tfrm, const Vector2 & translateVec );

    inline Transform2 & operator =( const Transform2 & tfrm );
    inline Transform2 & setUpper2x2( const Matrix2 & mat3 );
    inline const Matrix2 getUpper2x2( ) const;
    inline Transform2 & setTranslation( const Vector2 & translateVec );
    inline const Vector2 getTranslation( ) const;
    inline Transform2 & setCol0( const Vector2 & col0 );
    inline Transform2 & setCol1( const Vector2 & col1 );
    inline Transform2 & setCol2( const Vector2 & col2 );
    inline const Vector2 getCol0( ) const;
    inline const Vector2 getCol1( ) const;
    inline const Vector2 getCol2( ) const;
    inline Transform2 & setCol( int col, const Vector2 & vec );
    inline Transform2 & setRow( int row, const Vector3 & vec );
    inline const Vector2 getCol( int col ) const;
    inline const Vector3 getRow( int row ) const;
    inline Vector2 & operator []( int col );
    inline const Vector2 operator []( int col ) const;
    inline Transform2 & setElem( int col, int row, float val );
    inline float getElem( int col, int row ) const;
    inline const Vector2 operator *( const Vector2 & vec ) const;
    inline const Point2 operator *( const Point2 & pnt ) const;
    inline const Transform2 operator *( const Transform2 & tfrm ) const;
    inline Transform2 & operator *=( const Transform2 & tfrm );
    inline const Transform2 operator *( float scalar ) const;
    inline Transform2 & operator *=( float scalar );
    inline const Transform2 operator +( const Transform2 & mat ) const;
    inline Transform2 & operator +=( const Transform2 & mat );
    inline const Transform2 operator -( const Transform2 & mat ) const;
    inline Transform2 & operator -=( const Transform2 & mat );

    static inline const Transform2 identity( );
    static inline const Transform2 rotation( float radians );
    static inline const Transform2 scale( const Vector2 & scaleVec );
    static inline const Transform2 translation( const Vector2 & translateVec );

};

inline const Transform2 mulPerElem( const Transform2 & tfrm0, const Transform2 & tfrm1 );
inline const Transform2 absPerElem( const Transform2 & tfrm );
inline const Transform2 inverse( const Transform2 & tfrm );

class Matrix3 {
    Vector3 mCol0;
    Vector3 mCol1;
    Vector3 mCol2;

public:
    inline Matrix3( ) { };
    inline Matrix3( const Matrix3 & mat );
    inline Matrix3( const Vector3 & col0, const Vector3 & col1, const Vector3 & col2 );
    inline Matrix3( const Matrix4 & mat );
    explicit inline Matrix3( const Quat & unitQuat );
    explicit inline Matrix3( float scalar );

    inline Matrix3 & operator =( const Matrix3 & mat );

    inline Matrix3 & setCol0( const Vector3 & col0 );
    inline Matrix3 & setCol1( const Vector3 & col1 );
    inline Matrix3 & setCol2( const Vector3 & col2 );
    inline const Vector3 getCol0( ) const;
    inline const Vector3 getCol1( ) const;
    inline const Vector3 getCol2( ) const;
    inline Matrix3 & setCol( int col, const Vector3 & vec );
    inline Matrix3 & setRow( int row, const Vector3 & vec );
    inline const Vector3 getCol( int col ) const;
    inline const Vector3 getRow( int row ) const;
    inline Vector3 & operator []( int col );
    inline const Vector3 operator []( int col ) const;
    inline Matrix3 & setElem( int col, int row, float val );
    inline float getElem( int col, int row ) const;

    inline const Matrix3 operator +( const Matrix3 & mat ) const;
    inline const Matrix3 operator -( const Matrix3 & mat ) const;
    inline const Matrix3 operator -( ) const;
    inline const Matrix3 operator *( float scalar ) const;
    inline const Vector3 operator *( const Vector3 & vec ) const;
    inline const Matrix3 operator *( const Matrix3 & mat ) const;
    inline Matrix3 & operator +=( const Matrix3 & mat );
    inline Matrix3 & operator -=( const Matrix3 & mat );
    inline Matrix3 & operator *=( float scalar );
    inline Matrix3 & operator *=( const Matrix3 & mat );
    static inline const Matrix3 identity( );
    static inline const Matrix3 rotationX( float radians );
    static inline const Matrix3 rotationY( float radians );
    static inline const Matrix3 rotationZ( float radians );
    static inline const Matrix3 rotationZYX( const Vector3 & radiansXYZ );
    static inline const Matrix3 rotation( float radians, const Vector3 & unitVec );
    static inline const Matrix3 rotation( const Quat & unitQuat );
    static inline const Matrix3 scale( const Vector3 & scaleVec );

};

inline const Matrix3 operator *( float scalar, const Matrix3 & mat );
inline const Matrix3 appendScale( const Matrix3 & mat, const Vector3 & scaleVec );
inline const Matrix3 prependScale( const Vector3 & scaleVec, const Matrix3 & mat );
inline const Matrix3 mulPerElem( const Matrix3 & mat0, const Matrix3 & mat1 );
inline const Matrix3 normalize( const Matrix3 & mat );
inline const Matrix3 absPerElem( const Matrix3 & mat );
inline const Matrix3 transpose( const Matrix3 & mat );
inline const Matrix3 inverse( const Matrix3 & mat );
inline float determinant( const Matrix3 & mat );
inline const Matrix3 select( const Matrix3 & mat0, const Matrix3 & mat1, bool select1 );

class Matrix4 {
    Vector4 mCol0;
    Vector4 mCol1;
    Vector4 mCol2;
    Vector4 mCol3;

public:
    inline Matrix4( ) { };
    inline Matrix4( const Matrix4 & mat );
    inline Matrix4( const Vector4 & col0, const Vector4 & col1, const Vector4 & col2, const Vector4 & col3 );
    explicit inline Matrix4( const Transform3 & mat );
    inline Matrix4( const Matrix3 & mat, const Vector3 & translateVec );
    inline Matrix4( const Quat & unitQuat, const Vector3 & translateVec );
    explicit inline Matrix4( float scalar );

    inline Matrix4 & operator =( const Matrix4 & mat );

    inline Matrix4 & setUpper3x3( const Matrix3 & mat3 );
    inline const Matrix3 getUpper3x3( ) const;
    inline Matrix4 & setTranslation( const Vector3 & translateVec );
    inline const Vector3 getTranslation( ) const;
    inline Matrix4 & setCol0( const Vector4 & col0 );
    inline Matrix4 & setCol1( const Vector4 & col1 );
    inline Matrix4 & setCol2( const Vector4 & col2 );
    inline Matrix4 & setCol3( const Vector4 & col3 );
    inline const Vector4 getCol0( ) const;
    inline const Vector4 getCol1( ) const;
    inline const Vector4 getCol2( ) const;
    inline const Vector4 getCol3( ) const;
    inline Matrix4 & setCol( int col, const Vector4 & vec );
    inline Matrix4 & setRow( int row, const Vector4 & vec );
    inline const Vector4 getCol( int col ) const;
    inline const Vector4 getRow( int row ) const;
    inline Vector4 & operator []( int col );
    inline const Vector4 operator []( int col ) const;
    inline Matrix4 & setElem( int col, int row, float val );
    inline float getElem( int col, int row ) const;

    inline const Matrix4 operator +( const Matrix4 & mat ) const;
    inline const Matrix4 operator -( const Matrix4 & mat ) const;
    inline const Matrix4 operator -( ) const;
    inline const Matrix4 operator *( float scalar ) const;
    inline const Vector4 operator *( const Vector4 & vec ) const;
    inline const Vector4 operator *( const Vector3 & vec ) const;
    inline const Vector4 operator *( const Point3 & pnt ) const;
    inline const Matrix4 operator *( const Matrix4 & mat ) const;
    inline const Matrix4 operator *( const Transform3 & tfrm ) const;
    inline Matrix4 & operator +=( const Matrix4 & mat );
    inline Matrix4 & operator -=( const Matrix4 & mat );
    inline Matrix4 & operator *=( float scalar );
    inline Matrix4 & operator *=( const Matrix4 & mat );
    inline Matrix4 & operator *=( const Transform3 & tfrm );

    static inline const Matrix4 identity( );
    static inline const Matrix4 rotationX( float radians );
    static inline const Matrix4 rotationY( float radians );
    static inline const Matrix4 rotationZ( float radians );
    static inline const Matrix4 rotationZYX( const Vector3 & radiansXYZ );
    static inline const Matrix4 rotation( float radians, const Vector3 & unitVec );
    static inline const Matrix4 rotation( const Quat & unitQuat );
    static inline const Matrix4 scale( const Vector3 & scaleVec );
    static inline const Matrix4 translation( const Vector3 & translateVec );
    static inline const Matrix4 lookAt( const Point3 & eyePos, const Point3 & lookAtPos, const Vector3 & upVec );
    static inline const Matrix4 lookAtLH( const Point3 & eyePos, const Point3 & lookAtPos, const Vector3 & upVec );
    static inline const Matrix4 perspective( float fovyRadians, float aspect, float zNear, float zFar );
    static inline const Matrix4 perspectiveLH( float fovyRadians, float aspect, float zNear, float zFar );
    static inline const Matrix4 frustum( float left, float right, float bottom, float top, float zNear, float zFar );
    static inline const Matrix4 orthographic( float left, float right, float bottom, float top, float zNear, float zFar );

#if defined(_IPHONE_)
} __attribute__((aligned(16)));
#else
};
#endif

inline const Matrix4 operator *( float scalar, const Matrix4 & mat );
inline const Matrix4 appendScale( const Matrix4 & mat, const Vector3 & scaleVec );
inline const Matrix4 prependScale( const Vector3 & scaleVec, const Matrix4 & mat );
inline const Matrix4 mulPerElem( const Matrix4 & mat0, const Matrix4 & mat1 );
inline const Matrix4 absPerElem( const Matrix4 & mat );
inline const Matrix4 transpose( const Matrix4 & mat );
inline const Matrix4 inverse( const Matrix4 & mat );
inline const Matrix4 affineInverse( const Matrix4 & mat );
inline const Matrix4 orthoInverse( const Matrix4 & mat );
inline float determinant( const Matrix4 & mat );
inline const Matrix4 select( const Matrix4 & mat0, const Matrix4 & mat1, bool select1 );

class Transform3 {
    Vector3 mCol0;
    Vector3 mCol1;
    Vector3 mCol2;
    Vector3 mCol3;

public:
    inline Transform3( ) { };
    inline Transform3( const Transform3 & tfrm );
    inline Transform3( const Vector3 & col0, const Vector3 & col1, const Vector3 & col2, const Vector3 & col3 );
    inline Transform3( const Matrix3 & tfrm, const Vector3 & translateVec );
    inline Transform3( const Quat & unitQuat, const Vector3 & translateVec );
    inline Transform3( const Matrix4 & mat );
    explicit inline Transform3( float scalar );

    inline Transform3 & operator =( const Transform3 & tfrm );
    inline Transform3 & setUpper3x3( const Matrix3 & mat3 );
    inline const Matrix3 getUpper3x3( ) const;
    inline Transform3 & setTranslation( const Vector3 & translateVec );
    inline const Vector3 getTranslation( ) const;
    inline Transform3 & setCol0( const Vector3 & col0 );
    inline Transform3 & setCol1( const Vector3 & col1 );
    inline Transform3 & setCol2( const Vector3 & col2 );
    inline Transform3 & setCol3( const Vector3 & col3 );
    inline const Vector3 getCol0( ) const;
    inline const Vector3 getCol1( ) const;
    inline const Vector3 getCol2( ) const;
    inline const Vector3 getCol3( ) const;
    inline Transform3 & setCol( int col, const Vector3 & vec );
    inline Transform3 & setRow( int row, const Vector4 & vec );
    inline const Vector3 getCol( int col ) const;
    inline const Vector4 getRow( int row ) const;
    inline Vector3 & operator []( int col );
    inline const Vector3 operator []( int col ) const;
    inline Transform3 & setElem( int col, int row, float val );
    inline float getElem( int col, int row ) const;
    inline const Vector3 operator *( const Vector3 & vec ) const;
    inline const Point3 operator *( const Point3 & pnt ) const;
    inline const Transform3 operator *( const Transform3 & tfrm ) const;
    inline Transform3 & operator *=( const Transform3 & tfrm );
    inline const Transform3 operator *( float scalar ) const;
    inline Transform3 & operator *=( float scalar );
    inline const Transform3 operator +( const Transform3 & mat ) const;
    inline Transform3 & operator +=( const Transform3 & mat );

    inline const Transform3 operator -( const Transform3 & mat ) const;
    inline const Transform3 operator -( ) const;
    inline Transform3 & operator -=( const Transform3 & mat );

    static inline const Transform3 identity( );
    static inline const Transform3 rotationX( float radians );
    static inline const Transform3 rotationY( float radians );
    static inline const Transform3 rotationZ( float radians );
    static inline const Transform3 rotationZYX( const Vector3 & radiansXYZ );
    static inline const Transform3 rotation( float radians, const Vector3 & unitVec );
    static inline const Transform3 rotation( const Quat & unitQuat );
    static inline const Transform3 scale( const Vector3 & scaleVec );
    static inline const Transform3 translation( const Vector3 & translateVec );

};

inline const Transform3 appendScale( const Transform3 & tfrm, const Vector3 & scaleVec );
inline const Transform3 prependScale( const Vector3 & scaleVec, const Transform3 & tfrm );
inline const Transform3 mulPerElem( const Transform3 & tfrm0, const Transform3 & tfrm1 );
inline const Transform3 absPerElem( const Transform3 & tfrm );
inline const Transform3 inverse( const Transform3 & tfrm );
inline const Transform3 orthoInverse( const Transform3 & tfrm );
inline const Transform3 select( const Transform3 & tfrm0, const Transform3 & tfrm1, bool select1 );

#include "vec.h"
#include "quat.h"
#include "mat.h"

#endif // __vectormath_h__
