//-----------------------------------------------------------------------------
// File: pcolor.h
//
// Desc: 
//
// Copyright (C) 2007 Kode. All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __pcolor_h__
#define    __pcolor_h__

#define PCOLOR_IA    3
#define PCOLOR_IR    0
#define PCOLOR_IG    1
#define PCOLOR_IB    2

#define COLOR_Max(_a_,_b_)            (((_a_) > (_b_)) ? (_a_) : (_b_))
#define COLOR_Min(_a_,_b_)            (((_a_) < (_b_)) ? (_a_) : (_b_))
#define COLOR_Clamp(_a_, _low_, _high_) (COLOR_Max(_low_, COLOR_Min(_a_, _high_)))

//-----------------------------------------------------------------------------
// Name : PColor
// Desc : 
//-----------------------------------------------------------------------------
inline PColor::PColor( unsigned int color ) {
    dwColor = color;
}

inline PColor::PColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a){
    rgba[ PCOLOR_IR ] = r;
    rgba[ PCOLOR_IG ] = g;
    rgba[ PCOLOR_IB ] = b;
    rgba[ PCOLOR_IA ] = a;
}

inline PColor::PColor( const FColor & color ){
    rgba[ PCOLOR_IR ] = (unsigned char)(color.getR() * 255.f);
    rgba[ PCOLOR_IG ] = (unsigned char)(color.getG() * 255.f);
    rgba[ PCOLOR_IB ] = (unsigned char)(color.getB() * 255.f);
    rgba[ PCOLOR_IA ] = (unsigned char)(color.getA() * 255.f);
}

inline PColor::PColor( const Vector3 & vec ){
    rgba[ PCOLOR_IR ] = (unsigned char)(( vec.x + 1.f ) * 127.5f);
    rgba[ PCOLOR_IG ] = (unsigned char)(( vec.y + 1.f ) * 127.5f);
    rgba[ PCOLOR_IB ] = (unsigned char)(( vec.z + 1.f ) * 127.5f);
    rgba[ PCOLOR_IA ] = 0xff;
}

inline PColor::PColor( const Vector4 & vec ){
    rgba[ PCOLOR_IR ] = (unsigned char)(( vec.x + 1.f ) * 127.5f);
    rgba[ PCOLOR_IG ] = (unsigned char)(( vec.y + 1.f ) * 127.5f);
    rgba[ PCOLOR_IB ] = (unsigned char)(( vec.z + 1.f ) * 127.5f);
    rgba[ PCOLOR_IA ] = (unsigned char)(255.f * vec.w);
}

inline PColor & PColor::operator =( const PColor & color ) {
    dwColor = color.dwColor;
    return *this;
}

inline PColor PColor::operator * ( float scalar ) const{
    return PColor(static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IR]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IG]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IB]) * scalar, 0, 255.f)),
                  static_cast<unsigned char>(COLOR_Clamp(static_cast<float>(rgba[PCOLOR_IA]) * scalar, 0, 255.f)));
}
inline PColor & PColor::operator *=( float scalar ){
    *this = *this * scalar;
    return *this;
}
inline PColor PColor::operator * ( const PColor &color ) const{
    return PColor(COLOR_Clamp(rgba[PCOLOR_IR] * color.rgba[PCOLOR_IR] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IG] * color.rgba[PCOLOR_IG] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IB] * color.rgba[PCOLOR_IB] / 255, 0, 255),
                  COLOR_Clamp(rgba[PCOLOR_IA] * color.rgba[PCOLOR_IA] / 255, 0, 255));
}
inline PColor & PColor::operator *= ( const PColor &color ) {
    *this = *this * color;
    return *this;
}
inline const PColor PColor::operator +( const PColor & color ) const {
    return PColor(
        COLOR_Clamp(rgba[PCOLOR_IR] + color.rgba[PCOLOR_IR], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IG] + color.rgba[PCOLOR_IG], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IB] + color.rgba[PCOLOR_IB], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IA] + color.rgba[PCOLOR_IA], 0, 255));
}
inline const PColor PColor::operator -( const PColor & color ) const {
    return PColor(
        COLOR_Clamp(rgba[PCOLOR_IR] - color.rgba[PCOLOR_IR], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IG] - color.rgba[PCOLOR_IG], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IB] - color.rgba[PCOLOR_IB], 0, 255),
        COLOR_Clamp(rgba[PCOLOR_IA] - color.rgba[PCOLOR_IA], 0, 255));
}


inline PColor & PColor::setR( unsigned char r ){
    rgba[ PCOLOR_IR ] = r;
    return *this;
}

inline PColor & PColor::setG( unsigned char g ){
    rgba[ PCOLOR_IG ] = g;
    return *this;
}

inline PColor & PColor::setB( unsigned char b ){
    rgba[ PCOLOR_IB ] = b;
    return *this;
}

inline PColor & PColor::setA( unsigned char a ){
    rgba[ PCOLOR_IA ] = a;
    return *this;
}

inline unsigned char PColor::getR( ) const {
    return rgba[ PCOLOR_IR ];
}

inline unsigned char PColor::getG( ) const {
    return rgba[ PCOLOR_IG ];
}

inline unsigned char PColor::getB( ) const {
    return rgba[ PCOLOR_IB ];
}

inline unsigned char PColor::getA( ) const {
    return rgba[ PCOLOR_IA ];
}

inline PColor::operator PColor *() {
    return (PColor*)&dwColor;
}

inline PColor::operator const PColor *() const {
    return (const PColor*)&dwColor;
}

inline PColor::operator unsigned char *() {
    return (unsigned char*)&dwColor;
}

inline PColor::operator const unsigned char *() const {
    return (const unsigned char*)&dwColor;
}

inline PColor::operator unsigned int () const {
    return (unsigned int)dwColor;
}

#endif // __pcolor_h__

