﻿//-----------------------------------------------------------------------------
// File: bb.h
//
// Desc: 
//
// Copyright (C) 2012 Kode All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __bb_h__
#define    __bb_h__

#include <vector>
#include "vectormath.h"

class CalcBB {
public:
	CalcBB();
	~CalcBB();

	void Enable(bool enable);

	void GetBB(void);

	void Node(const char *name);
	void Pivot(const void *pivot);
	void Skin(bool bSkin);

	void SetPos(const void *pos);
	void SetBI(const void *bi);
	void SetBW(const void *bw);

	void Bind(const char *name, const void *transform);
	void AnimNode(const char *name);
	void AnimNodeTransform(int idx, const void *transform);

	unsigned int GetBB_Size(void);
	unsigned int GetDataSize(void);
	const void *GetData(void);
};

#endif
