﻿//-----------------------------------------------------------------------------
// File: main.cpp
//
// Desc: 
//
// Copyright (C) 2010 Kode All Rights Reserved.
//-----------------------------------------------------------------------------
#define    __main_cpp__

#include <windows.h>

#pragma warning(disable: 4100)
#pragma warning(disable: 4512)

#include <tchar.h>
#include <fbxsdk.h>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#define _USE_MATH_DEFINES
#include <math.h>

#include <immintrin.h>
#include <emmintrin.h>

#include "math_utils.h"
#include "cmd.h"
#include "bb.h"

#include "convert.h"


#pragma comment( lib, "wininet.lib" )
#pragma comment( lib, "advapi32.lib" )
#pragma comment( lib, "opengl32.lib" )

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment( lib, "vs2015/x64/lib_debug/libfbxsdk-md.lib" )
#else
#pragma comment( lib, "vs2015/x64/lib_release/libfbxsdk-md.lib" )
#endif

#ifndef MAKEDWORD
#	define MAKEDWORD(ch0, ch1, ch2, ch3) ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) | ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))
#endif

#include "triStrip.h"
#include "triSplit.h"
#include "convert.h"

//--------------------------------------
// POS UV NORMAL TANGENT SKIN COLOR
//--------------------------------------
enum
{
	PRIM_UV = 1 << 0,
	PRIM_NORMAL = 1 << 1,
	PRIM_TANGENT = 1 << 2,
	PRIM_SKIN = 1 << 3,
	PRIM_COLOR = 1 << 4,

	PRIM_IDX_32 = 1 << 16,

	PRIM_HALF_UNT = 1 << 17,
};

class ConvertInfo
{
public:
	std::string exportName;
	std::string filename;

	std::string shaderName;
	std::map<std::string, std::string> map_shaderName;

	std::string texExt;
	std::string texCmd;

	float scale = 1.f;
	bool bAnim = true;
	bool bModel = true;
	bool bExpMov = false;
	bool bFrame = false;
	bool bStrip = true;
	int startFrame = 0;
	int endFrame = INT_MAX;

	bool bImageConv = false;
	bool bObj = false;
	bool bHalf = false;

	unsigned int streamFlag = 0;
	unsigned int streamStride = 0;

	std::string outputName;

public:
	ConvertInfo()
	{

	}
};
ConvertInfo gConvertInfo;

CalcBB sBB;

//-----------------------------------------------------------------------------
// Name : BufferObject
// Desc : 
//-----------------------------------------------------------------------------
class BufferObject
{
protected:
	char *m_pBuffer;
	unsigned int mBufferSize;
	unsigned int mCurrentIndex;
public:
	BufferObject()
	{
		mBufferSize = 0;
		mCurrentIndex = 0;
		m_pBuffer = NULL;
	}
	~BufferObject()
	{
		if (m_pBuffer != NULL)delete[] m_pBuffer;
	}

	void Init(void)
	{
		mBufferSize = 1024;
		mCurrentIndex = 0;
		m_pBuffer = new char[mBufferSize];
		memset(m_pBuffer, 0, mBufferSize);
	}

	unsigned int GetBufferOffset(void) { return mCurrentIndex; }

	char *GetBuffer(void) { return m_pBuffer; }

	char *GetBuffer(unsigned int size)
	{
		while ((mCurrentIndex + size) >= mBufferSize)ResizeBuffer();
		char *ret = &m_pBuffer[mCurrentIndex];
		mCurrentIndex += size;
		return ret;
	}

	void ResizeBuffer(void)
	{ // x2
		if (mBufferSize == 0)Init();

		unsigned int newBufferSize = mBufferSize * 2;
		char *newBuffer = new char[newBufferSize];
		memset(newBuffer, 0, newBufferSize);
		memcpy(newBuffer, m_pBuffer, mBufferSize);
		mBufferSize = newBufferSize;
		delete[] m_pBuffer;
		m_pBuffer = newBuffer;
	}
};

class StringTable : public BufferObject
{
public:
	StringTable() {}
	~StringTable() {}

	unsigned int SetStringTable(const char *string)
	{
		unsigned int len = (unsigned int)strlen(string);
		while ((mCurrentIndex + len + 1) >= mBufferSize)ResizeBuffer();

		memcpy(&m_pBuffer[mCurrentIndex], string, len);
		unsigned int ret = mCurrentIndex;
		mCurrentIndex += len + 1;
		return ret;
	}
};


//-----------------------------------------------------------------------------
// Name : name ctrl
// Desc : 
//-----------------------------------------------------------------------------
void changePath(std::string &name, bool bWin = true)
{
	size_t si;
	if (bWin)
	{
		while ((si = name.find("/")) != std::string::npos)name.replace(si, 1, "\\");
	}
	else
	{
		while ((si = name.find("\\")) != std::string::npos)name.replace(si, 1, "/");
	}
}
void getName(std::string &name, const std::string &filename)
{
	name = filename;
	size_t si;
	while ((si = name.find("\\")) != std::string::npos)name.replace(si, 1, "/");
	if ((si = name.find_last_of("/")) != std::string::npos)name.erase(0, si + 1);
}
void getPath(std::string &name, const std::string &filename)
{
	name = filename;
	size_t si;
	while ((si = name.find("\\")) != std::string::npos)name.replace(si, 1, "/");
	if ((si = name.find_last_of("/")) != std::string::npos)name.erase(si + 1, -1);
}

void getExportName_Model(std::string &name, const std::string &filename)
{
	name = filename;
	if (!gConvertInfo.exportName.empty())
	{
		name = gConvertInfo.exportName;
	}
	size_t si;
	if ((si = name.find_last_of(".")) != std::string::npos)name.erase(si);
	name += std::string(".mdl");
}
void getExportName_Anim(std::string &name, const std::string &filename)
{
	name = filename;
	if (!gConvertInfo.exportName.empty())
	{
		name = gConvertInfo.exportName;
	}
	size_t si;
	if ((si = name.find_last_of(".")) != std::string::npos)name.erase(si);
	name += std::string(".anim");
}
void getExportName_Obj(std::string &name, const std::string &filename)
{
	name = filename;
	size_t si;
	if ((si = name.find_last_of(".")) != std::string::npos)name.erase(si);
	name += std::string(".obj");
}

bool checkName(std::string &keyName, std::map<std::string, std::string> &map_name, const std::string &name)
{
	for (std::map<std::string, std::string>::iterator itr = map_name.begin();
		itr != map_name.end(); ++itr)
	{
		if (itr->first == name)
		{
			keyName = itr->second;
			return true;
		}
		else
		{
			size_t si = itr->first.find("*");
			if (si != std::string::npos)
			{
				std::string nk = itr->first;
				nk.erase(si, 1);
				if (name.find(nk) != std::string::npos)
				{
					keyName = itr->second;
					return true;
				}
			}
		}
	}
	return false;
}

//-----------------------------------------------------------------------------
// Name : file
// Desc : 
//-----------------------------------------------------------------------------
bool writeFile(const char *filename, void *buffer, size_t fsize)
{
	FILE *fp;
	size_t wsize;
	fp = fopen(filename, "wb");
	if (!fp)return false;
	wsize = fwrite(buffer, sizeof(char), fsize, fp);
	fclose(fp);
	if (wsize == fsize)return true;
	return false;
}

bool readFile(const char *filename, char **buffer, size_t *size)
{
	HANDLE hFile;
	DWORD file_size, read;
	*buffer = NULL;
	*size = 0;
	hFile = CreateFile(filename,
		GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		file_size = GetFileSize(hFile, NULL);
		*size = (size_t)file_size;
		*buffer = new char[*size + 1]; //0 end
		(*buffer)[*size] = 0;
		ReadFile(hFile, (LPVOID)*buffer, file_size, &read, NULL);
		CloseHandle(hFile);
		if (*size != read)
		{
			delete[] * buffer;
			return false;
		}
		return true;
	}
	return false;
}

bool checkFile(std::string &filename)
{
	HANDLE hFile = CreateFile(filename.c_str(),
		GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
		return true;
	}
	return false;
}
void changeFileName(std::string &filename, const std::string &mdl_filename)
{
	if (!checkFile(filename))
	{
		std::string path;
		std::string name;
		getPath(path, mdl_filename);
		getName(name, filename);
		filename = path + name;
	}
}

bool readFile2(std::string &filename, char **buffer, size_t *size)
{
	bool ret = readFile(filename.c_str(), buffer, size);
	if (!ret)
	{
		//カレント検索
		std::string f_name(filename);
		std::string name;
		getName(name, f_name);
		ret = readFile(name.c_str(), buffer, size);
		if (ret)
		{
			filename = name;
		}
	}
	return ret;
}

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------

enum { INVALID_ID = 0xffffffff };

struct packed8888
{
	unsigned char b[4];
};
struct float4x4
{
	float f[16];
};
struct float4
{
	float f[4];
};
struct float3
{
	float f[3];
};
struct float2
{
	float f[2];
};
struct vertexPNUVBiBw
{
	float3 pos;
	float3 normal;
	float3 tangent;
	float2 uv;
	packed8888 bi;
	float4     bw;
};
struct setParamInfo
{
	unsigned int type;  // texture shader param とか
	unsigned int nameOffset;
	union
	{
		float4 f4;
		int   i4;
		bool  b1;
		int stringOffset;
	};
	int index;
	int padding[1];
};

struct imageInfo
{
	int stringOffset;
	int type;
	unsigned int size;
	int padding[1];
};

struct materialInfo
{
	unsigned int shaderID;
	unsigned int setParamIndex;
	unsigned int setParamNum;
	unsigned int nameOffset;
};

struct geomInfo
{
	unsigned int vertexOffset;
	unsigned int vertexNum;
	unsigned int indexOffset;
	unsigned int indexNum;

	unsigned int primitiveType;
	unsigned int stride;
	unsigned int materialIndex;
	int nameOffset;
};

struct nodeInfo
{
	float4x4 bindMatrix;
	float4x4 localMatrix;
	float4x4 pivotMatrix;
	float boundingBox[6];
	unsigned int geomNum;
	unsigned int geomIndex;
	int parentIndex;
	int childIndex;
	int siblingIndex;
	int nameOffset;
};

struct objInfo
{
	unsigned int tag;
	unsigned int version;
	unsigned int nodeNum;
	unsigned int geomNum;
	unsigned int materialNum;
	unsigned int imageNum;

	unsigned int setParamNum;
	unsigned int vertexSize;
	unsigned int indexSize;
	unsigned int stringSize;

	unsigned int nameOffset;
	unsigned char lodNum;
	unsigned char lodOffset;
	unsigned char padding[2];
};


//-----------------------------------------------------------------------------
// Name : Anim
// Desc : 
//-----------------------------------------------------------------------------
struct animNodeInfo
{
	unsigned int nameOffset;
	unsigned int startFrame;
	unsigned int endFrame;
	unsigned int transformOffset;
};

struct animInfo
{
	unsigned int tag;
	unsigned int version;
	unsigned int nodeNum;
	unsigned int startFrame;

	unsigned int endFrame;
	unsigned int transformSize;
	unsigned int stringSize;
	unsigned int nameOffset;

	unsigned int ex_flag;
	unsigned int ex_offset;
	unsigned int ex_size;
	unsigned int padding;

};

//-----------------------------------------------------------------------------
//[obj]    header
//[node0] 
//[node1]
//[geom0]
//[geom1]
//[material0]
//[material1]
//[setparam0]
//[setparam1]
//[vertex]
//[index]
//[string table]
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//[anim]    animheader
//[animNode0] 
//[animNode1]
//[transform]
//[string table]
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function-prototypes
//-----------------------------------------------------------------------------
inline void printMatrix(const float4x4 &m)
{
	printf("float4x4---------------\n");
	for (int i = 0; i < 4; i++)
	{
		printf("%f,%f,%f,%f\n", m.f[4 * i + 0], m.f[4 * i + 1], m.f[4 * i + 2], m.f[4 * i + 3]);
	}
	printf("-----------------------\n");
}

inline void printMatrix(const FbxAMatrix *fm)
{
	double *m = (double*)fm;
	printf("float4x4---------------\n");
	for (int i = 0; i < 4; i++)
	{
		printf("%f,%f,%f,%f\n", (float)m[0], (float)m[1], (float)m[2], (float)m[3]);
		m += 4;
	}
	printf("-----------------------\n");
}

inline void getFileNameToName(std::string &name)
{
	size_t si = name.find_last_of("/");
	if (si != std::string::npos)
	{
		name = name.substr(si + 1, -1);
		return;
	}
	si = name.find_last_of("\\");
	if (si != std::string::npos)
	{
		name = name.substr(si + 1, -1);
		return;
	}
}

inline void scaleMatrix(FbxAMatrix *fm, float scale, bool bTranspose = false)
{
	double *m = (double*)fm;
	if (bTranspose)
	{
		m[3] *= scale;
		m[7] *= scale;
		m[11] *= scale;
	}
	else
	{
		m[12] *= scale;
		m[13] *= scale;
		m[14] *= scale;
	}
}
inline void getMatrix(float4x4 &m, const FbxMatrix &fm, float scale = 1.f)
{
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			m.f[x + y * 4] = (float)fm.Get(y, x);
		}
	}

	m.f[12] *= scale;
	m.f[13] *= scale;
	m.f[14] *= scale;
}

inline void initMatrix(float4x4 &m)
{
	m.f[0] = 1; m.f[1] = 0; m.f[2] = 0; m.f[3] = 0;
	m.f[4] = 0; m.f[5] = 1; m.f[6] = 0; m.f[7] = 0;
	m.f[8] = 0; m.f[9] = 0; m.f[10] = 1; m.f[11] = 0;
	m.f[12] = 0; m.f[13] = 0; m.f[14] = 0; m.f[15] = 1;
}

FbxManager* m_pFBXManager = NULL;
FbxScene*      m_pFBXScene = NULL;

int gStartFrame = 0;
int gStopFrame = 0;


std::map<std::string, float4x4> m_mLocalMatrix;

//-----------------------------------------------------------------------------
// Name : InitFBX
// Desc : 
//-----------------------------------------------------------------------------
bool InitFbx()
{
	m_pFBXManager = FbxManager::Create();
	if (m_pFBXManager == NULL)
	{
		printf("fbx create error \n");
		return false;
	}
	{
		FbxIOSettings* ios = NULL;
		ios = FbxIOSettings::Create(m_pFBXManager, IOSROOT);
		m_pFBXManager->SetIOSettings(ios);
	}

	// 実行ファイルが置いてあるディレクトリ
	{
		FbxString lPath = FbxGetApplicationDirectory();
		FbxString lExtension = "dll";
		m_pFBXManager->LoadPluginsDirectory(lPath.Buffer(), lExtension.Buffer());
	}


	// シーンの作成
	m_pFBXScene = FbxScene::Create(m_pFBXManager, "");

	return true;

}

//-----------------------------------------------------------------------------
// Name : ExitFbx
// Desc : 
//-----------------------------------------------------------------------------
void ExitFbx()
{
	if (m_pFBXScene != NULL)
	{
		m_pFBXScene->Destroy();
		m_pFBXScene = NULL;
	}
	if (m_pFBXManager != NULL)
	{
		m_pFBXManager->Destroy();
		m_pFBXManager = NULL;
	}
}

//-----------------------------------------------------------------------------
// Name : LoadFbx
// Desc : 
//-----------------------------------------------------------------------------
bool LoadFbx(const char *filename)
{

	int lSDKMajor, lSDKMinor, lSDKRevision;
	int lFileMajor, lFileMinor, lFileRevision;

	// FBX SDK のバージョン取得
	FbxManager::GetFileFormatVersion(lSDKMajor, lSDKMinor, lSDKRevision);

	// FBX ファイルを読み込むためのインポーターを作成
	FbxImporter* lImporter = FbxImporter::Create(m_pFBXManager, "");

	TCHAR FullName[512];
	char FileName[512];
	{
		if (::GetFullPathName(filename, MAX_PATH, FullName, NULL) == 0) return false;

#ifdef _UNICODE
		::WideCharToMultiByte(CP_UTF8, 0, FullName, -1, FileName, (int)((wcslen(FullName) + 1) * 2), NULL, NULL);
#else
		WCHAR str[512];
		::MultiByteToWideChar(CP_ACP, 0, FullName, -1, str, (int)((strlen(FullName) + 1) * sizeof(WCHAR)));
		::WideCharToMultiByte(CP_UTF8, 0, str, -1, FileName, (int)((wcslen(str) + 1) * 2), NULL, NULL);
#endif
	}

	// インポーターを使用してファイルロード
	const bool lImportStatus = lImporter->Initialize(FileName,
		-1,
		m_pFBXManager->GetIOSettings());
	if (lImportStatus == false)
	{
		return false;
	}

	// 読み込んだFBXファイルのバージョンを取得
	lImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);
	if (lSDKMajor != lFileMajor || lSDKMinor != lFileMinor || lSDKRevision != lFileRevision)
	{
		//       return false;
	}

	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_MATERIAL, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_TEXTURE, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_LINK, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_SHAPE, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_GOBO, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_ANIMATION, true);
	m_pFBXManager->GetIOSettings()->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);

	// シーンオブジェクトにインポートする
	{
		bool lStatus = lImporter->Import(m_pFBXScene);
		if (lStatus == false)
		{
			return false;
		}
	}

	for (int i = 0; i < m_pFBXScene->GetSrcObjectCount<FbxAnimStack>(); i++)
	{
		FbxAnimStack* lAnimStack = m_pFBXScene->GetSrcObject<FbxAnimStack>(i);
		gStartFrame = (int)(lAnimStack->ReferenceStart.Get().GetMilliSeconds() * 60 / 1000);
		gStopFrame = (int)(lAnimStack->ReferenceStop.Get().GetMilliSeconds() * 60 / 1000);
	}



	// インポーターオブジェクトを開放
	if (lImporter)
	{
		lImporter->Destroy();
		lImporter = NULL;
	}

	return true;
}

//-----------------------------------------------------------------------------
// テンポラリー用バッファー
std::vector<setParamInfo> gSetParam;
std::vector<materialInfo> gMaterial;

std::vector<nodeInfo> gNodeInfo;
std::vector<geomInfo> gGeomInfo;
std::vector<std::string> gTextureName;
StringTable gStringTable;
BufferObject gVertexBuffer;
BufferObject gIndexBuffer;

std::vector<animNodeInfo> gAnimNodeInfo;
std::vector<float4x4> gAnimTransform;
StringTable gAnimStringTable;

//テクスチャー用
void changeTextureName(std::string &dst, const std::string &src)
{
	dst = src;
	if (!gConvertInfo.texCmd.empty() && !gConvertInfo.texExt.empty())
	{
		size_t si = dst.find_last_of(".");
		if (si != std::string::npos)
		{
			dst.erase(si, -1);
			dst += gConvertInfo.texExt;
		}
	}
}

//-----------------------------------------------------------------------------
//クラスター取得用
std::map<std::string, int> gNodeMap;
struct clusterInfo
{
	bool bUse;
	float4x4 link_matrix;
};
std::vector<clusterInfo> gClusterInfo;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//Weight
struct weightInfo
{
	int boneIndex;
	float weight;
};
struct weightPointInfo
{
	std::vector<weightInfo> weight;
};
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Name : recursiveCluster
// Desc : 
//-----------------------------------------------------------------------------
void recursiveCluster(FbxNode *node)
{
	int id = (unsigned int)gNodeMap.size();
	if (gNodeMap.find(std::string(node->GetName())) != gNodeMap.end())
	{
		printf("cluster error %s\n", node->GetName());
	}

	gNodeMap.insert(std::pair<std::string, int>(std::string(node->GetName()), id));
	clusterInfo ci;
	memset(&ci, 0, sizeof(ci));

	ci.link_matrix.f[0] = 1; ci.link_matrix.f[1] = 0; ci.link_matrix.f[2] = 0; ci.link_matrix.f[3] = 0;
	ci.link_matrix.f[4] = 0; ci.link_matrix.f[5] = 1; ci.link_matrix.f[6] = 0; ci.link_matrix.f[7] = 0;
	ci.link_matrix.f[8] = 0; ci.link_matrix.f[9] = 0; ci.link_matrix.f[10] = 1; ci.link_matrix.f[11] = 0;
	ci.link_matrix.f[12] = 0; ci.link_matrix.f[13] = 0; ci.link_matrix.f[14] = 0; ci.link_matrix.f[15] = 1;

	gClusterInfo.push_back(ci);

	int childNum = node->GetChildCount();
	for (int i = 0; i < childNum; i++)
	{
		recursiveCluster(node->GetChild(i));
	}
}

//-----------------------------------------------------------------------------
// Name : recursiveAnim
// Desc : 
//-----------------------------------------------------------------------------
void recursiveAnim(FbxNode *node, float scale)
{

	animNodeInfo info;
	memset(&info, 0, sizeof(info));
	info.startFrame = gStartFrame;
	info.endFrame = gStopFrame;
	info.transformOffset = (unsigned int)(gAnimTransform.size() * sizeof(float4x4));
	info.nameOffset = gAnimStringTable.SetStringTable(node->GetName());

	sBB.AnimNode(node->GetName());

	std::vector<float4x4> tmp_gAnimTransform;

	bool bBindPose = false;
	float4x4 m44_b;
	std::map<std::string, float4x4>::iterator itr = m_mLocalMatrix.find(node->GetName());
	if (itr != m_mLocalMatrix.end())
	{
		//		bBindPose = true; //バインドありか....
		m44_b = itr->second;
	}

	for (int frm = gStartFrame; frm <= gStopFrame; frm++)
	{
		float4x4 m44;
		if (!bBindPose)
		{
			getMatrix(m44, node->EvaluateGlobalTransform(FbxTime(FBXSDK_TC_SECOND / 60 * frm)));
		}
		else
		{
			m44 = m44_b;
		}

		m44.f[12 + 0] *= scale;
		m44.f[12 + 1] *= scale;
		m44.f[12 + 2] *= scale;

		if (gConvertInfo.bExpMov)
		{
			tmp_gAnimTransform.push_back(m44);
		}
		else
		{
			gAnimTransform.push_back(m44);
		}

		sBB.AnimNodeTransform(frm, &m44);
	}

	if (gConvertInfo.bExpMov)
	{
		//マトリックスチェック
		bool bOk = false;
		float4x4 mr = tmp_gAnimTransform[0];
		for (unsigned int n = 1; n < tmp_gAnimTransform.size(); n++)
		{
			float4x4 &m = tmp_gAnimTransform[n];
			if (memcmp(&mr, &m, sizeof(float4x4)) != 0)
			{
				bOk = true;
				break;
			}
		}
		if (bOk)
		{
			for (unsigned int n = 0; n < tmp_gAnimTransform.size(); n++)
			{
				gAnimTransform.push_back(tmp_gAnimTransform[n]);
			}
			gAnimNodeInfo.push_back(info);
			printf("export anim %s\n", node->GetName());
		}
	}
	else
	{
		printf("export anim %s\n", node->GetName());
		gAnimNodeInfo.push_back(info);
	}

	int childNum = node->GetChildCount();
	for (int i = 0; i < childNum; i++)
	{
		recursiveAnim(node->GetChild(i), scale);
	}
}


//-----------------------------------------------------------------------------
// Name : recursive
// Desc : 
//-----------------------------------------------------------------------------
void recursive(FbxNode *node, int parentID, int nodeIndex, int nodeNum)
{

	unsigned int prevGeomNum = (unsigned int)gGeomInfo.size();


	if (node != NULL)
	{

		printf("[[--------[%s]---------]]\n", node->GetName());

		sBB.Node(node->GetName());


		//--------------------
		// ノード
		//--------------------
		nodeInfo nodeInfo;
		memset(&nodeInfo, 0, sizeof(nodeInfo));

		//--------------------
		// 名前登録
		//--------------------
		nodeInfo.nameOffset = gStringTable.SetStringTable(node->GetName());

		//--------------------
		//デフォルト姿勢
		//--------------------
		FbxAMatrix m = node->EvaluateGlobalTransform();
		getMatrix(nodeInfo.localMatrix, m, gConvertInfo.scale);
		FbxAMatrix pivotM;
		{
			const FbxVector4 lT = node->GetGeometricTranslation(FbxNode::eSourcePivot);
			const FbxVector4 lR = node->GetGeometricRotation(FbxNode::eSourcePivot);
			const FbxVector4 lS = node->GetGeometricScaling(FbxNode::eSourcePivot);
			pivotM = FbxAMatrix(lT, lR, lS);
			getMatrix(nodeInfo.pivotMatrix, pivotM);

			sBB.Pivot(&pivotM);
		}

		{
			FbxPose * pPose = m_pFBXScene->GetPose(0);
			if (pPose != NULL)
			{
				int lNodeIndex = pPose->Find(node);
				if (lNodeIndex > -1)
				{
					if (pPose->IsBindPose() || !pPose->IsLocalMatrix(lNodeIndex))
					{
						FbxAMatrix lPoseMatrix;
						FbxMatrix lMatrix = pPose->GetMatrix(lNodeIndex);
						memcpy((double*)lPoseMatrix, (double*)lMatrix, sizeof(lMatrix.mData));
						getMatrix(nodeInfo.localMatrix, lPoseMatrix);

						printf("bind pose %s\n", node->GetName());
						//BindPose
						m_mLocalMatrix.insert(std::pair<std::string, float4x4>(node->GetName(), nodeInfo.localMatrix));
					}
					else
					{
						//
						printf("parent ...\n");
					}
				}
				else
				{
					printf("use ...\n");
				}
			}
			else
			{
				//use
				printf("use ...\n");
			}
		}

		//mesh
		FbxNodeAttribute* lNodeAttribute = node->GetNodeAttribute();
		if (lNodeAttribute != NULL)
		{
			switch (lNodeAttribute->GetAttributeType())
			{
			case FbxNodeAttribute::eNull:
				break;
			case FbxNodeAttribute::eSkeleton:
				break;
			case FbxNodeAttribute::eCamera:
				break;
			case FbxNodeAttribute::eMesh:
			{

				std::vector<weightPointInfo> wpointInfo;
				std::vector<packed8888> vWeightIndex;
				std::vector<float4> vWeight;
				bool bSkin = false;

				//skin
				{
					FbxMesh *mesh = (FbxMesh*)lNodeAttribute;
					int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);

					if (skinCount > 0)
					{
						wpointInfo.resize(mesh->GetControlPointsCount()); //Weightポイントリサイズ
						bSkin = true;
					}

					for (int n = 0; n < skinCount; n++)
					{
						FbxSkin* skin = (FbxSkin*)mesh->GetDeformer(n, FbxDeformer::eSkin);
						// クラスターの数を取得
						int clusterNum = skin->GetClusterCount();

						FbxSkin::EType lSkinningType = skin->GetSkinningType();


						for (int c = 0; c < clusterNum; c++)
						{
							FbxCluster* cluster = skin->GetCluster(c);
							FbxNode *linkNode = cluster->GetLink();
							printf("%s\n", linkNode->GetName());


							FbxCluster::ELinkMode lClusterMode = cluster->GetLinkMode();
							if (lClusterMode == FbxCluster::eAdditive && cluster->GetAssociateModel())
							{
								printf("not support\n");
							}


							weightInfo wi;
							std::map<std::string, int>::iterator itr =
								gNodeMap.find(std::string(linkNode->GetName()));
							if (itr != gNodeMap.end())
							{
								wi.boneIndex = itr->second;

								printf("find index %d\n", itr->second);

								FbxAMatrix refGlobalInitPos; //参照されるグローバル初期化位置
								FbxAMatrix clusGlobalInitPos;//クラスターのグローバル初期化位置
								cluster->GetTransformMatrix(refGlobalInitPos);

								refGlobalInitPos *= pivotM;

								cluster->GetTransformLinkMatrix(clusGlobalInitPos);

								scaleMatrix(&refGlobalInitPos, gConvertInfo.scale);
								scaleMatrix(&clusGlobalInitPos, gConvertInfo.scale);

								FbxAMatrix ibm = clusGlobalInitPos.Inverse() * refGlobalInitPos;

								sBB.Bind(NULL, &ibm);

								if (!gClusterInfo[itr->second].bUse)
								{
									getMatrix(gClusterInfo[itr->second].link_matrix, ibm);

									gClusterInfo[itr->second].bUse = true;
								}
								else
								{
									//チェックしてみる
									float4x4 m;
									getMatrix(m, ibm);
									if (memcmp(&m, &gClusterInfo[itr->second].link_matrix, sizeof(m)) != 0)
									{
										printf("not same cluster\n");
									}
									else
									{
										printf("cluster ok\n");
									}
								}
							}
							else
							{
								wi.boneIndex = -1;
								printf("error Find index\n");
							}

							int pointNum = cluster->GetControlPointIndicesCount();
							int* pointAry = cluster->GetControlPointIndices();
							double* weightAry = cluster->GetControlPointWeights();
							int dn = 0;
							for (int i = 0; i < pointNum; i++)
							{
								wi.weight = (float)weightAry[i];
								//wpointInfo[pointAry[i]].weight.insert(wi);
								wpointInfo[pointAry[i]].weight.push_back(wi);

								dn++;
#if _DEBUG
								if ((dn % 4) == 0)
								{
									printf("%d:[%d]%f\n", pointAry[i], wi.boneIndex, wi.weight);
								}
								else
								{
									printf("%d:[%d]%f,", pointAry[i], wi.boneIndex, wi.weight);
								}
#endif
							}
						}
					}
				}

				// Weight大きいものから４個取得
				if (bSkin)
				{
					vWeightIndex.resize(wpointInfo.size());
					vWeight.resize(wpointInfo.size());

					packed8888 wp8;
					float4     wf4;
					for (unsigned int i = 0; i < wpointInfo.size(); i++)
					{
						float totalWeight = 0;
						int nWriteIndex = 0;
						if (wpointInfo[i].weight.empty())
						{
							std::map<std::string, int>::iterator itr = gNodeMap.find(std::string(node->GetName()));
							wp8.b[0] = itr->second; //自分のＩＤ
							wf4.f[0] = 1.f;
							totalWeight = 1.f;
							//残り
							for (int n = 1; n < 4; n++)
							{
								wp8.b[n] = 0;
								wf4.f[n] = 0;
							}
						}
						else
						{
							for (std::vector<weightInfo>::iterator itr = wpointInfo[i].weight.begin();
								itr != wpointInfo[i].weight.end(); ++itr)
							{
								if (itr->weight > FLT_EPSILON)
								{
									totalWeight += itr->weight;
									wp8.b[nWriteIndex] = itr->boneIndex;
									wf4.f[nWriteIndex] = itr->weight;
									if ((++nWriteIndex) >= 4)break;
								}
							}
							if (totalWeight < 1.f)
							{
								printf("total weight error %d:%f \n", i, totalWeight);
								for (std::vector<weightInfo>::iterator itr = wpointInfo[i].weight.begin();
									itr != wpointInfo[i].weight.end(); ++itr)
								{
									printf("%d:%f\n", itr->boneIndex, itr->weight);
								}
							}

							//残り
							for (int n = nWriteIndex; n < 4; n++)
							{
								wp8.b[n] = 0;
								wf4.f[n] = 0;
							}
						}

						for (int n = 0; n < 4; n++)wf4.f[n] /= totalWeight;
						vWeightIndex[i] = wp8;
						vWeight[i] = wf4;

						if (wpointInfo[i].weight.size() >= 3)
						{
							if (vWeight[i].f[2] > 0.f)
							{
								printf("weight idx:%d: num:%d (%d,%d,%d,%d) : (%f,%f,%f,%f)\n",
									i,
									(unsigned int)wpointInfo[i].weight.size(),
									vWeightIndex[i].b[0], vWeightIndex[i].b[1], vWeightIndex[i].b[2], vWeightIndex[i].b[3],
									vWeight[i].f[0], vWeight[i].f[1], vWeight[i].f[2], vWeight[i].f[3]);
							}
						}
					}
				}

				//--------------------
				//メッシュ作成
				//--------------------
				FbxMesh *mesh = (FbxMesh*)lNodeAttribute;
				int vertexCount = mesh->GetPolygonVertexCount();
				int polyCount = mesh->GetPolygonCount();
				int *indexArray = mesh->GetPolygonVertices();

				std::vector<float3> vPos;
				std::vector<float2> vUV[4];
				std::vector<float3> vNormal;
				vPos.resize(vertexCount);
				vNormal.resize(vertexCount);
				for (int i = 0; i < 4; i++)vUV[i].resize(vertexCount);
				int uvIndex = 0;

				FbxLayerElement::EMappingMode mappingMode_Normal = FbxLayerElement::eByPolygonVertex;
				FbxLayerElement::EMappingMode mappingMode_UV = FbxLayerElement::eByPolygonVertex;
				FbxLayerElement::EMappingMode mappingMode_Color = FbxLayerElement::eByPolygonVertex;

				FbxVector4 *ctrlpos = mesh->GetControlPoints(); //座標並び
				unsigned int controlPointNum = mesh->GetControlPointsCount();

				bool bPos = true;
				bool bNormal = false;
				bool bUV = false;
				bool bColor = true;
				bool bTangent = false;

				//頂点
				if (controlPointNum > 0)
				{
					if (vPos.size() < controlPointNum)vPos.resize(controlPointNum);
					for (unsigned int v = 0; v < controlPointNum; v++)
					{
						vPos[v].f[0] = static_cast<float>(ctrlpos[v][0]) * gConvertInfo.scale;
						vPos[v].f[1] = static_cast<float>(ctrlpos[v][1]) * gConvertInfo.scale;
						vPos[v].f[2] = static_cast<float>(ctrlpos[v][2]) * gConvertInfo.scale;
					}
				}
				else
				{

				}

				//マテリアルインデックス保持
				materialInfo mi;
				memset(&mi, 0, sizeof(mi));
				mi.setParamIndex = (unsigned int)gSetParam.size();

				//layer
				int layerNum = mesh->GetLayerCount();
				for (int l = 0; l < layerNum; l++)
				{
					FbxLayer *layer = mesh->GetLayer(l);

					//マテリアルチェック
					FbxLayerElementMaterial *material = layer->GetMaterials();
					if (material != NULL)
					{
						switch (material->GetMappingMode())
						{
						default:
							printf("Unknown Material Mapping %d\n", material->GetMappingMode());
							break;
						case FbxLayerElement::eByPolygon:
						case FbxLayerElement::eAllSame:
						{
							int matID_Num = material->GetIndexArray().GetCount();
							//for(int m=0;m<matID_Num;m++){
							//    printf("%d,", material->GetIndexArray().GetAt(m));
							//}
							//printf("\n");
							int matID = material->GetIndexArray().GetAt(0); //すべて同じはず！
							FbxSurfaceMaterial *s_material = mesh->GetNode()->GetMaterial(matID);
							if (s_material != NULL)
							{
								std::string nodename(node->GetName());
								std::string rep_sh_name;
								if (checkName(rep_sh_name, gConvertInfo.map_shaderName, nodename))
								{
									//名前書き換え
									mi.nameOffset = gStringTable.SetStringTable(rep_sh_name.c_str());
								}
								else if (gConvertInfo.shaderName.empty())
								{
									mi.nameOffset = gStringTable.SetStringTable(s_material->GetName());
								}
								else
								{
									mi.nameOffset = gStringTable.SetStringTable(gConvertInfo.shaderName.c_str());
								}
								//マテリアル
								if (s_material->GetClassId().Is(FbxSurfaceLambert::ClassId))
								{
									/*
									FbxSurfaceLambert *lambert = (FbxSurfaceLambert*)s_material;
									 float4 ambi,diff;
									 for(int i=0;i<3;i++)ambi.f[i] = (float)lambert->GetAmbientColor().Get()[i];
									 ambi.f[3] = 1.f;
									 for(int i=0;i<3;i++)diff.f[i] = (float)lambert->GetDiffuseColor().Get()[i];
									 diff.f[3] = 1.f;

									 //--------------------
									 //SetParam追加
									 //--------------------
									 setParamInfo si;
									 memset(&si, 0, sizeof(si));

									 si.type = GF::Model::SETPARAM_AMBI;
									 si.f4 = ambi;
									 gSetParam.push_back(si);

									 si.type = GF::Model::SETPARAM_DIFF;
									 si.f4 = diff;
									 gSetParam.push_back(si);

									 //si.type = GF::Model::SETPARAM_COLOR_2;
									 //si.f4.f[0] = si.f4.f[1] = si.f4.f[2] = si.f4.f[3] = 1.f;
									 //gSetParam.push_back(si);
									 */

								}
								else if (s_material->GetClassId().Is(FbxSurfacePhong::ClassId))
								{
									FbxSurfacePhong *phong = (FbxSurfacePhong*)s_material;
									float4 ambi, diff;
									//for(int i=0;i<3;i++)ambi.f[i] = (float)phong->GetAmbientColor().Get()[i];
									ambi.f[3] = 1.f;
									//for(int i=0;i<3;i++)diff.f[i] = (float)phong->GetDiffuseColor().Get()[i];
									diff.f[3] = 1;//(float)phong->GetTransparencyFactor().Get();

									//--------------------
									//SetParam追加
									//--------------------
									setParamInfo si;
									memset(&si, 0, sizeof(si));

									/*
									si.type = GF::Model::SETPARAM_AMBI;
									si.f4 = ambi;
									gSetParam.push_back(si);

									si.type = GF::Model::SETPARAM_DIFF;
									si.f4 = diff;
									gSetParam.push_back(si);
									*/

								}

								FbxProperty lProperty = s_material->FindProperty(FbxSurfaceMaterial::sDiffuse);
								int layTexNum = lProperty.GetSrcObjectCount(FbxCriteria::ObjectType(FbxLayeredTexture::ClassId));
								if (layTexNum == 0)
								{ //通常テクスチャー
									int texNum = lProperty.GetSrcObjectCount(FbxCriteria::ObjectType(FbxTexture::ClassId));

									std::vector<std::string> textureNames;
									std::vector<std::string> textureNames2;
									for (int t = 0; t < texNum; t++)
									{
										FbxTexture* lTexture = FbxCast <FbxTexture>(
											lProperty.GetSrcObject(FbxCriteria::ObjectType(FbxTexture::ClassId), t));
										if (lTexture != NULL)
										{
											FbxFileTexture *fTexture = (FbxFileTexture*)lTexture;
											std::string tname(fTexture->GetRelativeFileName());
											getFileNameToName(tname);
											std::string tname2;
											changeTextureName(tname2, tname);

											textureNames.push_back(tname);
											textureNames2.push_back(tname2);
										}
									}
									if (texNum >= 2)
									{
										if (textureNames[0].find("enviroment") != std::string::npos)
										{
											std::swap(textureNames[0], textureNames[1]);
											std::swap(textureNames2[0], textureNames2[1]);
										}
									}

									for (int t = 0; t < texNum; t++)
									{
										std::string tname = textureNames[t];
										std::string tname2 = textureNames2[t];
										if (!tname2.empty())
										{
											setParamInfo si;
											memset(&si, 0, sizeof(si));
											si.type = MAKEDWORD('T', 'E', 'X', '2');// GF::Model::SETPARAM_TEXTURE_NAME;
											si.index = t;
											si.stringOffset = gStringTable.SetStringTable(tname2.c_str());
											//--------------------
											//SetParam追加
											//--------------------
											gSetParam.push_back(si);

											//テクスチャーネーム取得
											gTextureName.push_back(tname.c_str());
										}
									}

								}
								else
								{ //layer Texture ?
									for (int j = 0; j < layTexNum; j++)
									{
										FbxLayeredTexture *layeredTexture =
											(FbxLayeredTexture*)lProperty.GetSrcObject((const FbxCriteria&)FbxLayeredTexture::ClassId, j);

										std::vector<std::string> textureNames;
										std::vector<std::string> textureNames2;
										int texNum = layeredTexture->GetSrcObjectCount((const FbxCriteria&)FbxTexture::ClassId);
										for (int t = 0; t < texNum; t++)
										{
											FbxTexture *lTexture = (FbxTexture*)layeredTexture->GetSrcObject((const FbxCriteria&)FbxTexture::ClassId, t);
											if (lTexture != NULL)
											{
												FbxFileTexture *fTexture = (FbxFileTexture*)lTexture;
												FbxString uvSet = fTexture->UVSet.Get();
												std::string tname(fTexture->GetRelativeFileName());
												getFileNameToName(tname);
												std::string tname2;
												changeTextureName(tname2, tname);

												textureNames.push_back(tname);
												textureNames2.push_back(tname2);
											}
										}
										if (texNum >= 2)
										{
											if (textureNames[0].find("enviroment") != std::string::npos)
											{
												std::swap(textureNames[0], textureNames[1]);
												std::swap(textureNames2[0], textureNames2[1]);
											}
										}
									}
								}
							}
						}
						break;
						}
					}



					//法線チェック
					FbxLayerElementNormal* normalElem = layer->GetNormals();
					if (normalElem != NULL)
					{
						int normalNum = normalElem->GetDirectArray().GetCount();
						mappingMode_Normal = normalElem->GetMappingMode();
						FbxLayerElement::EReferenceMode refMode = normalElem->GetReferenceMode();
						switch (mappingMode_Normal)
						{
						case FbxLayerElement::eByPolygonVertex: //頂点単位
						case FbxLayerElement::eByControlPoint: //コントロールポイント単位
							bNormal = true;
							switch (refMode)
							{
							case FbxLayerElement::eDirect: //DEFAULT
								for (int n = 0; n < normalNum; n++)
								{
									vNormal[n].f[0] = (float)normalElem->GetDirectArray().GetAt(n)[0];
									vNormal[n].f[1] = (float)normalElem->GetDirectArray().GetAt(n)[1];
									vNormal[n].f[2] = (float)normalElem->GetDirectArray().GetAt(n)[2];
								}
								break;
							case FbxLayerElement::eIndexToDirect:
								for (int n = 0; n < normalNum; n++)
								{
									int idx = normalElem->GetIndexArray().GetAt(n);
									vNormal[n].f[0] = (float)normalElem->GetDirectArray().GetAt(idx)[0];
									vNormal[n].f[1] = (float)normalElem->GetDirectArray().GetAt(idx)[1];
									vNormal[n].f[2] = (float)normalElem->GetDirectArray().GetAt(idx)[2];
								}
								break;
							}
						}
					}
					//UV チェック
					FbxLayerElementUV *uvElem = layer->GetUVs();
					if (uvElem != NULL)
					{
						int uvNum = uvElem->GetDirectArray().GetCount();

						uvNum = Max(uvNum, uvElem->GetIndexArray().GetCount());
						if (uvNum > vertexCount)
						{ //リサイズ必要な場合がある
							for (int i = 0; i < 4; i++)
							{
								vUV[i].resize(uvNum);
							}
						}

						mappingMode_UV = uvElem->GetMappingMode();
						FbxLayerElement::EReferenceMode refMode = uvElem->GetReferenceMode();
						switch (mappingMode_UV)
						{
						case FbxLayerElement::eByPolygonVertex: //頂点単位
						case FbxLayerElement::eByControlPoint: //コントロールポイント単位
							bUV = true;
							switch (refMode)
							{
							case FbxLayerElement::eDirect:
								for (int n = 0; n < uvNum; n++)
								{
									vUV[uvIndex][n].f[0] = (float)uvElem->GetDirectArray().GetAt(n)[0];
									vUV[uvIndex][n].f[1] = (float)uvElem->GetDirectArray().GetAt(n)[1];
								}
								break;
							case FbxLayerElement::eIndexToDirect: //DEFAULT
								for (int n = 0; n < uvNum; n++)
								{
									int idx = uvElem->GetIndexArray().GetAt(n);
									vUV[uvIndex][n].f[0] = (float)uvElem->GetDirectArray().GetAt(idx)[0];
									vUV[uvIndex][n].f[1] = (float)uvElem->GetDirectArray().GetAt(idx)[1];
								}
								break;
							}
							break;
						}
						//複数はなし　uvIndex++;
					}
				}

				std::vector<vertexPNUVBiBw> vVertex_tmp;
				std::vector<vertexPNUVBiBw> vVertex;

				int useUV_Layer = 0; //(uvIndex > 1)?1:0;

				vertexPNUVBiBw vtx;
				int vcount = 0;
				for (int p = 0; p < polyCount; p++)
				{
					int indexNumInPoly = mesh->GetPolygonSize(p);
					for (int n = 0; n < indexNumInPoly; n++, vcount++)
					{
						int idx = mesh->GetPolygonVertex(p, n);
						vtx.pos = vPos[idx];
						if (bNormal)
						{
							if (mappingMode_Normal == FbxLayerElement::eByPolygonVertex)
								vtx.normal = vNormal[vcount];
							else 
								vtx.normal = vNormal[idx];
						}
						if (bUV)
						{
							if (mappingMode_UV == FbxLayerElement::eByPolygonVertex)
								vtx.uv = vUV[useUV_Layer][vcount];
							else
								vtx.uv = vUV[useUV_Layer][idx];
						}
						if (bSkin)
						{
							vtx.bi = vWeightIndex[idx];
							vtx.bw = vWeight[idx];
						}
						//VFlip
						vtx.uv.f[0] = vtx.uv.f[0];
						vtx.uv.f[1] = 1.f - vtx.uv.f[1];

						vVertex_tmp.push_back(vtx);
					}
				}
				//Quadをtriangleに
				vcount = 0;
				for (int p = 0; p < polyCount; p++)
				{
					int indexNumInPoly = mesh->GetPolygonSize(p);
					switch (indexNumInPoly)
					{
					case 3:

						//
						if (bNormal && !bTangent)
						{
							Vector3 tangent;
							Vector3 binormal;
							CreateTangent(
								(float*)&tangent, (float*)&binormal,
								(const float*)vVertex_tmp[vcount + 2].pos.f,
								(const float*)vVertex_tmp[vcount + 1].pos.f,
								(const float*)vVertex_tmp[vcount + 0].pos.f,
								(const float*)vVertex_tmp[vcount + 2].uv.f,
								(const float*)vVertex_tmp[vcount + 1].uv.f,
								(const float*)vVertex_tmp[vcount + 0].uv.f);
							for (int t = 0; t < 3; t++)
							{
								memcpy(&vVertex_tmp[vcount + t].tangent,
									&tangent, sizeof(float) * 3);
							}
						}

						vVertex.push_back(vVertex_tmp[vcount + 2]);
						vVertex.push_back(vVertex_tmp[vcount + 1]);
						vVertex.push_back(vVertex_tmp[vcount + 0]);

						vcount += 3;
						break;
					case 4:
						//
						if (bNormal && !bTangent)
						{
							Vector3 tangent;
							Vector3 binormal;
							CreateTangent(
								(float*)&tangent, (float*)&binormal,
								(const float*)vVertex_tmp[vcount + 2].pos.f,
								(const float*)vVertex_tmp[vcount + 1].pos.f,
								(const float*)vVertex_tmp[vcount + 0].pos.f,
								(const float*)vVertex_tmp[vcount + 2].uv.f,
								(const float*)vVertex_tmp[vcount + 1].uv.f,
								(const float*)vVertex_tmp[vcount + 0].uv.f);
							for (int t = 0; t < 3; t++)
							{
								memcpy(&vVertex_tmp[vcount + t].tangent,
									&tangent, sizeof(float) * 3);
							}
						}

						vVertex.push_back(vVertex_tmp[vcount + 2]);
						vVertex.push_back(vVertex_tmp[vcount + 1]);
						vVertex.push_back(vVertex_tmp[vcount + 0]);


						vertexPNUVBiBw quads[3];
						quads[0] = vVertex_tmp[vcount + 0];
						quads[1] = vVertex_tmp[vcount + 3];
						quads[2] = vVertex_tmp[vcount + 2];


						if (bNormal && !bTangent)
						{
							Vector3 tangent;
							Vector3 binormal;
							CreateTangent(
								(float*)&tangent, (float*)&binormal,
								(const float*)quads[0].pos.f,
								(const float*)quads[1].pos.f,
								(const float*)quads[2].pos.f,
								(const float*)quads[0].uv.f,
								(const float*)quads[1].uv.f,
								(const float*)quads[2].uv.f);
							for (int t = 0; t < 3; t++)
							{
								memcpy(&quads[t].tangent,
									&tangent, sizeof(float) * 3);
							}
						}

						vVertex.push_back(quads[0]);
						vVertex.push_back(quads[1]);
						vVertex.push_back(quads[2]);

						vcount += 4;
						break;
					default:
						printf("poly error %d\n", indexNumInPoly);
						break;
					}
				}

				if (bNormal && !bTangent)
				{
					bTangent = true;
				}

				//koko 頂点カラーは無視する
				bColor = false;

				geomInfo gi;
				memset(&gi, 0, sizeof(gi));

				//インターリーブしたデータにして保存
				gi.stride = 0;
				gi.stride += (bPos) ? sizeof(float3) : 0;
				gi.stride += (bUV) ? sizeof(float2) : 0;
				gi.stride += (bNormal) ? sizeof(float3) : 0;
				gi.stride += (bTangent) ? sizeof(float3) : 0;
				gi.stride += (bSkin) ? sizeof(packed8888) + sizeof(float4) : 0;
				gi.stride += (bColor) ? sizeof(unsigned int) : 0;

				gi.vertexNum = (unsigned int)vVertex.size();
				gi.vertexOffset = gVertexBuffer.GetBufferOffset();
				gi.indexOffset = gIndexBuffer.GetBufferOffset();

				//--------------------
				// 頂点バッファー追加
				//--------------------
				nodeInfo.boundingBox[0] = FLT_MAX;
				nodeInfo.boundingBox[1] = FLT_MAX;
				nodeInfo.boundingBox[2] = FLT_MAX;
				nodeInfo.boundingBox[3] = FLT_MIN;
				nodeInfo.boundingBox[4] = FLT_MIN;
				nodeInfo.boundingBox[5] = FLT_MIN;

				sBB.Skin(bSkin);

				{
					char *base, *vp;
					base = vp = new char[gi.stride * gi.vertexNum];
					for (unsigned int v = 0; v < gi.vertexNum; v++)
					{
						memcpy(vp, &vVertex[v].pos, sizeof(float3)); vp += sizeof(float3);

						//BB
						if (nodeInfo.boundingBox[0] > vVertex[v].pos.f[0])
							nodeInfo.boundingBox[0] = vVertex[v].pos.f[0];
						if (nodeInfo.boundingBox[1] > vVertex[v].pos.f[1])
							nodeInfo.boundingBox[1] = vVertex[v].pos.f[1];
						if (nodeInfo.boundingBox[2] > vVertex[v].pos.f[2])
							nodeInfo.boundingBox[2] = vVertex[v].pos.f[2];

						if (nodeInfo.boundingBox[3] < vVertex[v].pos.f[0])
							nodeInfo.boundingBox[3] = vVertex[v].pos.f[0];
						if (nodeInfo.boundingBox[4] < vVertex[v].pos.f[1])
							nodeInfo.boundingBox[4] = vVertex[v].pos.f[1];
						if (nodeInfo.boundingBox[5] < vVertex[v].pos.f[2])
							nodeInfo.boundingBox[5] = vVertex[v].pos.f[2];

						sBB.SetPos(vVertex[v].pos.f);

						if (bUV)
						{
							memcpy(vp, &vVertex[v].uv, sizeof(float2)); vp += sizeof(float2);
						}
						if (bNormal)
						{
							memcpy(vp, &vVertex[v].normal, sizeof(float3)); vp += sizeof(float3);
						}
						if (bTangent)
						{
							memcpy(vp, &vVertex[v].tangent, sizeof(float3)); vp += sizeof(float3);
						}
						if (bSkin)
						{
							memcpy(vp, &vVertex[v].bi, sizeof(packed8888)); vp += sizeof(packed8888);
							memcpy(vp, &vVertex[v].bw, sizeof(float4)); vp += sizeof(float4);

							sBB.SetBI(&vVertex[v].bi);
							sBB.SetBW(&vVertex[v].bw);
						}
						if (bColor)
						{
							unsigned int colorWhite = 0xffffffff;
							memcpy(vp, &colorWhite, sizeof(unsigned int)); vp += sizeof(unsigned int);
						}
					}

					char *new_vertex;
					unsigned int new_vertexNum;
					char *new_index;
					unsigned int new_indexNum;
					if (gConvertInfo.bStrip)
					{
						ConvertTriIndexStrip(&new_vertex, &new_vertexNum,
							&new_index, &new_indexNum,
							base, gi.vertexNum, gi.stride);
					}
					else
					{
						new_vertexNum = gi.vertexNum;
						new_vertex = new char[gi.stride * new_vertexNum];
						memcpy(new_vertex, base, gi.stride * new_vertexNum);

						new_indexNum = gi.vertexNum;
						new_index = new char[sizeof(short) * new_indexNum];
						{
							unsigned short *idx = (unsigned short*)new_index;
							for (unsigned short _idx = 0; _idx < new_indexNum; _idx++)
							{
								idx[_idx] = _idx;
							}
						}
					}

					delete[] base;

					vp = gVertexBuffer.GetBuffer(gi.stride * new_vertexNum);
					memcpy(vp, new_vertex, gi.stride * new_vertexNum);
					delete[] new_vertex;
					vp = gIndexBuffer.GetBuffer(new_indexNum * sizeof(short));
					memcpy(vp, new_index, new_indexNum * sizeof(short));
					delete[] new_index;

					gi.vertexNum = new_vertexNum; //変更
					gi.indexNum = new_indexNum;
				}


				//マテリアルパラメータ取得
				if (mi.setParamIndex < gSetParam.size())
				{
					//SetParam使用した
					gi.materialIndex = (unsigned int)gMaterial.size();
					mi.setParamNum = (unsigned int)gSetParam.size() - mi.setParamIndex;
					//--------------------
					//マテリアル追加
					//--------------------
					gMaterial.push_back(mi);
				}
				else
				{
					gi.materialIndex = INVALID_ID;
				}

				gi.primitiveType = 0;

				//vtx format
				gi.primitiveType |= (bUV) ? PRIM_UV : 0;
				gi.primitiveType |= (bNormal) ? PRIM_NORMAL : 0;
				gi.primitiveType |= (bTangent) ? PRIM_TANGENT : 0;
				gi.primitiveType |= (bSkin) ? PRIM_SKIN : 0;
				gi.primitiveType |= (bColor) ? PRIM_COLOR : 0;

				gi.nameOffset = gStringTable.SetStringTable("default");

				gConvertInfo.streamFlag |= gi.primitiveType;
				gConvertInfo.streamStride = gi.stride;

				//今のところジオメトリーは１個として保存
				nodeInfo.geomNum = 1;
				nodeInfo.geomIndex = (unsigned int)gGeomInfo.size();

				//--------------------
				// ジオメトリー追加
				//--------------------
				gGeomInfo.push_back(gi);

			}
			}
		}

		//--------------------
		// ノード追加
		//--------------------
		int thisNodeIndex = static_cast<int>(gNodeInfo.size());

		int childNum = node->GetChildCount();
		unsigned int geomNum = (unsigned int)gGeomInfo.size() - prevGeomNum;
		if (geomNum > 0)
		{ //ジオメトリが追加されたなら情報を保存
			nodeInfo.geomNum = geomNum;
			nodeInfo.geomIndex = prevGeomNum;
		}
		else
		{
			nodeInfo.geomNum = 0;
			nodeInfo.geomIndex = INVALID_ID;
		}


		nodeInfo.parentIndex = parentID;
		if (childNum > 0)
		{
			nodeInfo.childIndex = thisNodeIndex + 1; //次に入る
		}
		else
		{
			nodeInfo.childIndex = INVALID_ID;
		}
		gNodeInfo.push_back(nodeInfo);


		for (int i = 0; i < childNum; i++)
		{
			recursive(node->GetChild(i), thisNodeIndex, i, childNum);
		}

		if ((nodeNum > 1) && (nodeIndex != (nodeNum - 1)))
		{
			//その階層の最後じゃない場合、兄弟を作成
			gNodeInfo[thisNodeIndex].siblingIndex = (unsigned int)gNodeInfo.size();
		}
		else
		{
			gNodeInfo[thisNodeIndex].siblingIndex = INVALID_ID;
		}


	}
}

//-----------------------------------------------------------------------------
// Name : SaveModel
// Desc :
//-----------------------------------------------------------------------------
void SaveModel(const std::string &name)
{
	//ファイル名をオブジェクトの名前にする
	std::string mdlName;
	getName(mdlName, name);

	//最終的なデータを作成
	objInfo obj;
	memset(&obj, 0, sizeof(obj));

	//HALF------
	std::vector<int> streamSizeArray;
	if (gConvertInfo.bHalf)
	{
		streamSizeArray.push_back(3); //pos

		if (gConvertInfo.streamFlag & PRIM_UV)
		{
			streamSizeArray.push_back(-2);
		}
		if (gConvertInfo.streamFlag & PRIM_NORMAL)
		{
			streamSizeArray.push_back(-3);
		}
		if (gConvertInfo.streamFlag & PRIM_TANGENT)
		{
			streamSizeArray.push_back(-3);
		}
		if (gConvertInfo.streamFlag & PRIM_SKIN)
		{
			streamSizeArray.push_back(1);
			streamSizeArray.push_back(-4);
		}
		if (gConvertInfo.streamFlag & PRIM_COLOR)
		{
			streamSizeArray.push_back(1);
		}
	}
	unsigned int halfStreamStride = CalcStride(streamSizeArray);
	unsigned int halfStreamNum = gVertexBuffer.GetBufferOffset() / gConvertInfo.streamStride;
	//HALF------


	obj.tag = MAKEDWORD('M', 'M', 'O', 'N');
	obj.version = 0;
	obj.nameOffset = gStringTable.SetStringTable(mdlName.c_str());
	obj.nodeNum = (unsigned int)gNodeInfo.size();
	obj.geomNum = (unsigned int)gGeomInfo.size();
	obj.materialNum = (unsigned int)gMaterial.size();
	obj.setParamNum = (unsigned int)gSetParam.size();

	if (gConvertInfo.bHalf)
	{
		//HALF------
		obj.vertexSize = halfStreamStride * halfStreamNum;
		//HALF------
	}
	else
	{
		obj.vertexSize = gVertexBuffer.GetBufferOffset();
	}

	obj.indexSize = gIndexBuffer.GetBufferOffset();
	obj.stringSize = gStringTable.GetBufferOffset();

	//連結開始
	BufferObject buffer;

	//[objInfo]
	memcpy(buffer.GetBuffer(sizeof(objInfo)), &obj, sizeof(objInfo));

	//[nodeInfo]
	for (unsigned int n = 0; n < obj.nodeNum; n++)
	{
		memcpy(buffer.GetBuffer(sizeof(nodeInfo)), &gNodeInfo[n], sizeof(nodeInfo));
	}

	//[geomInfo]
	for (unsigned int n = 0; n < obj.geomNum; n++)
	{
		if (gConvertInfo.bHalf)
		{
			//HALF------
			{
				gGeomInfo[n].primitiveType |= PRIM_HALF_UNT;
				gGeomInfo[n].stride = halfStreamStride;
			}
			//HALF------
			memcpy(buffer.GetBuffer(sizeof(geomInfo)), &gGeomInfo[n], sizeof(geomInfo));
		}
		else
		{
			memcpy(buffer.GetBuffer(sizeof(geomInfo)), &gGeomInfo[n], sizeof(geomInfo));
		}
	}
	//[materialInfo]
	for (unsigned int n = 0; n < obj.materialNum; n++)
	{
		memcpy(buffer.GetBuffer(sizeof(materialInfo)), &gMaterial[n], sizeof(materialInfo));
	}
	//[setParam]
	for (unsigned int n = 0; n < obj.setParamNum; n++)
	{
		memcpy(buffer.GetBuffer(sizeof(setParamInfo)), &gSetParam[n], sizeof(setParamInfo));
	}

	//[vertex]
	if (gConvertInfo.bHalf)
	{
		//HALF------
		{
			char *ptr;
			unsigned int ns;
			ConvertFloat2Half(&ptr, &ns, gVertexBuffer.GetBuffer(),
				gConvertInfo.streamStride,
				halfStreamNum, streamSizeArray);

			memcpy(buffer.GetBuffer(obj.vertexSize),
				ptr, obj.vertexSize);
			delete[] ptr;
		}
		//HALF------
	}
	else
	{
		memcpy(buffer.GetBuffer(obj.vertexSize), gVertexBuffer.GetBuffer(), obj.vertexSize);
	}

	//[index]
	memcpy(buffer.GetBuffer(obj.indexSize), gIndexBuffer.GetBuffer(), obj.indexSize);

	//[string]
	memcpy(buffer.GetBuffer(obj.stringSize), gStringTable.GetBuffer(), obj.stringSize);

	//ファイルに出力
	std::string exportName;
	getExportName_Model(exportName, name);
	writeFile(exportName.c_str(), buffer.GetBuffer(), buffer.GetBufferOffset());
}

//-----------------------------------------------------------------------------
// Name : SaveAnim
// Desc :
//-----------------------------------------------------------------------------
void SaveAnim(const std::string &name)
{
	//ファイル名をオブジェクトの名前にする
	std::string animName;
	getName(animName, name);

	//最終的なデータを作成
	animInfo anim;
	memset(&anim, 0, sizeof(anim));

	anim.tag = MAKEDWORD('A', 'M', 'O', 'N');
	anim.version = 0;
	anim.nameOffset = gAnimStringTable.SetStringTable(animName.c_str());
	anim.nodeNum = (unsigned int)gAnimNodeInfo.size();
	if (gConvertInfo.bFrame)
	{
		anim.startFrame = 0;
		anim.endFrame = gStopFrame - gStartFrame;
	}
	else
	{
		anim.startFrame = gStartFrame;
		anim.endFrame = gStopFrame;
	}
	anim.transformSize = (unsigned int)gAnimTransform.size();
	anim.stringSize = gAnimStringTable.GetBufferOffset();

	sBB.GetBB();
	anim.ex_size = sBB.GetDataSize();

	//連結開始
	BufferObject anim_buffer;

	//[animInfo]
	memcpy(anim_buffer.GetBuffer(sizeof(animInfo)), &anim, sizeof(anim));
	//[animNodeInfo]
	for (unsigned int n = 0; n < anim.nodeNum; n++)
	{
		memcpy(anim_buffer.GetBuffer(sizeof(animNodeInfo)), &gAnimNodeInfo[n], sizeof(animNodeInfo));
	}
	//[transform]
	for (unsigned int n = 0; n < anim.transformSize; n++)
	{
		memcpy(anim_buffer.GetBuffer(sizeof(float4x4)), &gAnimTransform[n], sizeof(float4x4));
	}
	//EX  BB ...
	if (anim.ex_size > 0)
	{
		memcpy(anim_buffer.GetBuffer(anim.ex_size), sBB.GetData(), anim.ex_size);
	}

	//[string]
	memcpy(anim_buffer.GetBuffer(anim.stringSize), gAnimStringTable.GetBuffer(), anim.stringSize);

	//ファイルに出力
	std::string exportNameAnim;
	getExportName_Anim(exportNameAnim, name);
	writeFile(exportNameAnim.c_str(), anim_buffer.GetBuffer(), anim_buffer.GetBufferOffset());
}

void chomp(std::string &str)
{
	size_t si = 0;
	while ((si = str.find("\r")) != std::string::npos)
	{
		str.replace(si, si + 1, "");
	}
	while ((si = str.find("\n")) != std::string::npos)
	{
		str.replace(si, si + 1, "");
	}
}

void split(std::string &str, const std::string &key, std::vector<std::string> &elems)
{
	size_t pos = 0;
	size_t si = 0;
	while ((si = str.find(key, pos)) != std::string::npos)
	{
		elems.push_back(str.substr(pos, si - pos));
		pos = si + 1;
	}
	elems.push_back(str.substr(pos, -1));
	for (std::vector<std::string>::iterator itr = elems.begin();
		itr != elems.end(); ++itr)
	{
		chomp(*itr);
	}
}

void create_FromOBJ(const char *buffer, const std::string &fname)
{
	std::string lines(buffer);

	std::vector<float3> vPos;
	std::vector<float2> vUv;
	unsigned int idx = 0;

	nodeInfo ni;
	geomInfo gi;
	materialInfo mi;
	memset(&ni, 0, sizeof(ni));
	memset(&gi, 0, sizeof(gi));
	memset(&mi, 0, sizeof(mi));

	std::vector<std::string> elems;
	split(lines, "\n", elems);
	for (std::vector<std::string>::iterator itr = elems.begin();
		itr != elems.end(); ++itr)
	{
		std::string p = *itr;
		std::vector<std::string> params;
		split(p, " ", params);

		float3 vtx;
		float2 uv;

		if (params.size() > 1)
		{
			if (params[0] == "v")
			{
				if (params.size() == 4)
				{
					printf("pos:%s,%s,%s\n", params[1].c_str(), params[2].c_str(), params[3].c_str());
					for (int n = 0; n < 3; n++)vtx.f[n] = (float)atof(params[1 + n].c_str()) * gConvertInfo.scale;
					vPos.push_back(vtx);
				}
			}
			else if (params[0] == "vt")
			{
				if (params.size() == 3)
				{
					printf("uv:%s,%s\n", params[1].c_str(), params[2].c_str());
					for (int n = 0; n < 2; n++)uv.f[n] = (float)atof(params[1 + n].c_str());
					//vflip
					uv.f[1] = 1.f - uv.f[1];
					vUv.push_back(uv);
				}
			}
			else if (params[0] == "f")
			{
				if (params.size() >= 4)
				{
					if (params[1].find("/") != std::string::npos)
					{
						//UV
						if (params.size() == 5)
						{
							printf("quads\n");
						}
						std::vector<std::string> pos_uv[3];
						for (int i = 0; i < 3; i++)
						{
							split(params[1 + i], "/", pos_uv[i]);
						}
						printf("face:%s,%s,%s\n",
							pos_uv[0][0].c_str(), pos_uv[1][0].c_str(), pos_uv[2][0].c_str());
						{
							const bool swap = true;
							//swap
							if (swap)std::swap(pos_uv[0], pos_uv[2]);

							char *vp = gVertexBuffer.GetBuffer((12 + 12 + 8 + 12) * 3);
							char *ip = gIndexBuffer.GetBuffer(4 * 3);

							int vid[3];
							int uvid[3];
							for (int n = 0; n < 3; n++)vid[n] = atoi(pos_uv[n][0].c_str()) - 1; //1:start
							for (int n = 0; n < 3; n++)uvid[n] = atoi(pos_uv[n][1].c_str()) - 1;

							Vector3 pos3[3];
							Vector3 uv3[3];
							for (int n = 0; n < 3; n++)pos3[n] = Vector3(vPos[vid[n]].f[0],
								vPos[vid[n]].f[1],
								vPos[vid[n]].f[2]);
							for (int n = 0; n < 3; n++)uv3[n] = Vector3(vUv[uvid[n]].f[0],
								vUv[uvid[n]].f[1], 1);
							Vector3 normal;
							if (swap)
							{
								normal = normalize(cross(pos3[2] - pos3[0], pos3[1] - pos3[0]));
							}
							else
							{
								normal = normalize(cross(pos3[1] - pos3[0], pos3[2] - pos3[0]));
							}


							//CALC TANGENT
							Vector3 tangent;
							Vector3 binormal;
							CreateTangent(
								(float*)&tangent, (float*)&binormal,
								(const float*)&pos3[0],
								(const float*)&pos3[1],
								(const float*)&pos3[2],
								(const float*)&uv3[0],
								(const float*)&uv3[1],
								(const float*)&uv3[2]);


							for (int n = 0; n < 3; n++)
							{
								memcpy(vp, &pos3[n], sizeof(float) * 3);
								vp += sizeof(float) * 3;

								memcpy(vp, &uv3[n], sizeof(float) * 2);
								vp += sizeof(float) * 2;

								memcpy(vp, &normal, sizeof(float) * 3);
								vp += sizeof(float) * 3;

								memcpy(vp, &tangent, sizeof(float) * 3);
								vp += sizeof(float) * 3;


								memcpy(ip, &idx, sizeof(int));
								ip += sizeof(int);
								idx++;
							}
						}
					}
					else
					{
						printf("face:%s,%s,%s\n", params[1].c_str(), params[2].c_str(), params[3].c_str());
					}
				}
			}
		}
	}

	//Normal Smooth
#if 1
	{
		ConvertSmoothNormal_SamePos(
			gVertexBuffer.GetBuffer(),
			12 + 8 + 12 + 12, idx, 20); //pos + uv = 20

	}
#endif

	//create dummy data 
	ni.nameOffset = gStringTable.SetStringTable("OBJ");
	initMatrix(ni.bindMatrix);
	initMatrix(ni.localMatrix);
	initMatrix(ni.pivotMatrix);
	ni.geomNum = 1;
	ni.parentIndex = -1;
	ni.childIndex = -1;
	ni.siblingIndex = -1;
	gNodeInfo.push_back(ni);

	gi.vertexNum = idx;
	gi.stride = 12 + 12 + 8 + 12;
	gi.indexNum = idx;
	gi.materialIndex = 0;

	gi.primitiveType |= PRIM_UV;
	gi.primitiveType |= PRIM_NORMAL;
	gi.primitiveType |= PRIM_TANGENT;
	gi.primitiveType |= PRIM_IDX_32;

	gi.nameOffset = gStringTable.SetStringTable("GEOM");
	gGeomInfo.push_back(gi);

	mi.nameOffset = gStringTable.SetStringTable("MAT");
	gMaterial.push_back(mi);

	SaveModel(fname);
}


//-----------------------------------------------------------------------------
// Name : convert
// Desc : 
//-----------------------------------------------------------------------------
int convert(int argc, char *argv[])
{
	int fileIndex = 1;
	int index = 1;

	while (index < argc)
	{
		if (argv[index] != NULL)
		{
			if (argv[index][0] == '-')
			{ //オプション
				std::string opt = std::string(argv[index]);
				if (opt == "-texconv")
				{ //テクスチャー変換コマンド
//拡張子
					gConvertInfo.texExt = std::string(argv[index + 1]);
					gConvertInfo.texCmd = std::string(argv[index + 2]);
					index += 3;
				}
				else if (opt == "-op")
				{
					gConvertInfo.outputName = argv[index + 1];
					index += 2;
				}
				else if (opt == "-o")
				{
					gConvertInfo.exportName = argv[index + 1];
					index += 2;
				}
				else if (opt == "-obj")
				{
					gConvertInfo.bObj = true;
					index += 1;
				}
				else if (opt == "-scale")
				{
					gConvertInfo.scale = (float)atof(argv[index + 1]);
					index += 2;
				}
				else if (opt == "-anim")
				{
					gConvertInfo.bAnim = true;
					index += 1;
				}
				else if (opt == "-anim_bb")
				{
					sBB.Enable(true);
					index += 1;
				}
				else if (opt == "-triangles")
				{
					gConvertInfo.bStrip = false;
					index += 1;
				}
				else if (opt == "-trianglelist")
				{
					gConvertInfo.bStrip = false;
					index += 1;
				}
				else if (opt == "-half")
				{
					gConvertInfo.bHalf = true;
					index += 1;
				}
				else if (opt == "-frame")
				{
					gConvertInfo.bFrame = true;
					gConvertInfo.startFrame = atoi(argv[index + 1]);
					gConvertInfo.endFrame = atoi(argv[index + 2]);
					index += 3;
				}
				else
				{
					index++;
				}
			}
			else
			{ //ファイル名かな？
				fileIndex = index;
				index++;
			}
		}
		else
		{
			index++;
		}
	}


	std::string fname(argv[fileIndex]);
	gConvertInfo.filename = fname;

	if (!InitFbx())
	{
		printf("error \n");
	}

	if (gConvertInfo.bObj)
	{
		//Obj format
		char *buf;
		size_t size;
		if (readFile(fname.c_str(), &buf, &size))
		{
			create_FromOBJ(buf, fname);
			delete[] buf;
		}
		return 0;
	}

	LoadFbx(fname.c_str());

	{

		if (gConvertInfo.bFrame)
		{
			gStartFrame = max(gStartFrame, gConvertInfo.startFrame);
			gStopFrame = min(gStopFrame, gConvertInfo.endFrame);
		}

		printf("anim %d->%d\n", gStartFrame, gStopFrame);

		//ノード取得
		FbxNode *node = m_pFBXScene->GetRootNode();

		//名前だけ取得
		recursiveCluster(node);

		//ノード情報取得
		recursive(node, INVALID_ID, 0, 0);

		//BindPose全検索
		for (unsigned int n = 0; n < gNodeInfo.size(); n++)
		{
			char *name = gStringTable.GetBuffer() + gNodeInfo[n].nameOffset;
			std::map<std::string, int>::iterator itr = gNodeMap.find(name);
			if (itr != gNodeMap.end())
			{
				int cn = itr->second;
				if (cn != (int)n)
				{
					printf("error?\n");
					gNodeInfo[n].bindMatrix = gClusterInfo[cn].link_matrix;
				}
				else
				{
					gNodeInfo[n].bindMatrix = gClusterInfo[n].link_matrix;
				}
			}
			else
			{
				gNodeInfo[n].bindMatrix = gClusterInfo[n].link_matrix;
			}
			sBB.Bind(name, &gNodeInfo[n].bindMatrix);
		}

		//------------------------------------------------------------------
		//アニメーション
		if (gConvertInfo.bAnim)
		{
			recursiveAnim(node, gConvertInfo.scale);
			SaveAnim(fname);
		}


		//Root にBB入れる
		{
			memcpy(&gNodeInfo[0].boundingBox[0], sBB.GetData(), sizeof(float) * 3);
			memcpy(&gNodeInfo[0].boundingBox[3],
				(const char*)sBB.GetData() + sizeof(float) * 4, sizeof(float) * 3);
			SaveModel(fname);
		}

	}

	ExitFbx();

	return 0;
}

int convertFBXtoLoopTexture(int argc, char *argv[])
{
	int fileIndex = 1;
	int index = 1;

	while (index < argc)
	{
		if (argv[index] != NULL)
		{
			if (argv[index][0] == '-')
			{ //オプション
				std::string opt = std::string(argv[index]);
				if (opt == "-texconv")
				{ //テクスチャー変換コマンド
				  //拡張子
					gConvertInfo.texExt = std::string(argv[index + 1]);
					gConvertInfo.texCmd = std::string(argv[index + 2]);
					index += 3;
				}
				else if (opt == "-op")
				{
					gConvertInfo.outputName = argv[index + 1];
					index += 2;
				}
				else if (opt == "-o")
				{
					gConvertInfo.exportName = argv[index + 1];
					index += 2;
				}
				else if (opt == "-obj")
				{
					gConvertInfo.bObj = true;
					index += 1;
				}
				else if (opt == "-scale")
				{
					gConvertInfo.scale = (float)atof(argv[index + 1]);
					index += 2;
				}
				else if (opt == "-anim")
				{
					gConvertInfo.bAnim = true;
					index += 1;
				}
				else if (opt == "-anim_bb")
				{
					sBB.Enable(true);
					index += 1;
				}
				else if (opt == "-triangles")
				{
					gConvertInfo.bStrip = false;
					index += 1;
				}
				else if (opt == "-trianglelist")
				{
					gConvertInfo.bStrip = false;
					index += 1;
				}
				else if (opt == "-half")
				{
					gConvertInfo.bHalf = true;
					index += 1;
				}
				else if (opt == "-frame")
				{
					gConvertInfo.bFrame = true;
					gConvertInfo.startFrame = atoi(argv[index + 1]);
					gConvertInfo.endFrame = atoi(argv[index + 2]);
					index += 3;
				}
				else
				{
					index++;
				}
			}
			else
			{ //ファイル名かな？
				fileIndex = index;
				index++;
			}
		}
		else
		{
			index++;
		}
	}


	std::string fname(argv[fileIndex]);
	gConvertInfo.filename = fname;

	if (!InitFbx())
	{
		printf("error \n");
	}

	if (gConvertInfo.bObj)
	{
		//Obj format
		char *buf;
		size_t size;
		if (readFile(fname.c_str(), &buf, &size))
		{
			create_FromOBJ(buf, fname);
			delete[] buf;
		}
		return 0;
	}

	LoadFbx(fname.c_str());
}

TriangulateAllPolygons()
{

}

