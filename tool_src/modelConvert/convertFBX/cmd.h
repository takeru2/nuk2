﻿//-----------------------------------------------------------------------------
// File: convert.h
//
// Desc: 
//
// Copyright (C) 2012 Kode All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __convert_h__
#define    __convert_h__

#include <windows.h>
#include <string>
#include <vector>


DWORD ExecCommand(std::string &str, const char *lpCommandLine);

#endif // __convert_h__

