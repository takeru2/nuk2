﻿//-----------------------------------------------------------------------------
// File: triStrip.h
//
// Desc: 
//
// Copyright (c) 2012 Kode. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef    __triStrip_h__
#define    __triStrip_h__

#include <vector>

unsigned int CalcStride(std::vector<int> &offsetElemArray);

void ConvertFloat2Half(char **newPtr,
					   unsigned int *newStride,
					   const void *ptr,
					   unsigned int stride, unsigned int vtxNum,
					   std::vector<int> &offsetElemArray);

bool CreateTangent(float *tangent, float *binormal,
				   const float *pos0, const float *pos1, const float *pos2,
				   const float *uv0, const float *uv1, const float *uv2);

bool ConvertSmoothNormal_SamePos(void *ptrPN,
								 unsigned int stride, unsigned int vtxNum, unsigned int normalOffset);

bool ConvertTriIndex(std::vector<int> &nv_stream,
                     std::vector<unsigned short> &ni_stream,
                     const std::vector<int> &v_stream, unsigned int stride, int prim);

bool ConvertTriIndexStrip(std::vector<int> &nv_stream,
                          std::vector<unsigned short> &ni_stream,
                          const std::vector<int> &v_stream, unsigned int stride, int prim);

bool ConvertTriStrip(std::vector<int> &nv_stream,
                     const std::vector<int> &v_stream, unsigned int stride, int prim);


bool ConvertTriIndexStrip(char **new_vertex, unsigned int *new_vertexNum,
                          char **new_index, unsigned int *new_indexNum,
                          const char *v_stream, unsigned int v_stream_num, unsigned int stride);

bool ConvertEdgeIndexLines(int faceType,
                           char **new_vertex, unsigned int *new_vertexNum,
                           char **new_index, unsigned int *new_indexNum,
                           const char *v_stream, unsigned int v_stream_num, unsigned int stride);

#endif // __triStrip_h__
