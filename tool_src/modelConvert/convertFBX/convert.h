﻿//-----------------------------------------------------------------------------
// File: convert.h
//
// Desc: 
//
// Copyright (C) 2012 Kode All Rights Reserved.
//-----------------------------------------------------------------------------

#ifndef    __convert_h__
#define    __convert_h__

#include <vector>


int convert(int argc, char *argv[]);


#endif // __convert_h__

