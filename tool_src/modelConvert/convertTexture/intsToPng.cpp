#include <vector>
#include <cassert>
#include "fbxToLoopTexture.h"
#include "lodepng.h"
#include <string>
#include "intsToPng.h"

namespace
{
	bool WriteFile(const char *filename, void *buffer, size_t fsize)
	{
		FILE *fp;
		size_t wsize;
		fp = fopen(filename, "wb");
		if (!fp)
			return false;
		wsize = fwrite(buffer, sizeof(char), fsize, fp);
		fclose(fp);
		if
			(wsize == fsize)return true;
		return false;
	}
}

extern void IntsToPng(std::vector<int> colors_data, int width, int height,std::string file_name)
{
	assert(colors_data.size() == width * height);
	unsigned char *colors = new unsigned char[width * height * 4];
	int colors_index = 0;
	for (int i = 0; i < width * height; i++)
	{
		Color pixel = static_cast<Color>(colors_data[i]);
		switch (pixel)
		{
		case Color::white:
			colors[i * 4] = 255;
			colors[i * 4 + 1] = 255;
			colors[i * 4 + 2] = 255;
			colors[i * 4 + 3] = 255;
			break;
		case Color::black:
			colors[i * 4] = 0;
			colors[i * 4 + 1] = 0;
			colors[i * 4 + 2] = 0;
			colors[i * 4 + 3] = 255;
			break;
		case Color::red:
			colors[i * 4] = 255;
			colors[i * 4 + 1] = 0;
			colors[i * 4 + 2] = 0;
			colors[i * 4 + 3] = 255;
			break;
		case Color::green:
			colors[i * 4] = 0;
			colors[i * 4 + 1] = 255;
			colors[i * 4 + 2] = 0;
			colors[i * 4 + 3] = 255;
			break;
		case Color::blue:
			colors[i * 4] = 0;
			colors[i * 4 + 1] = 0;
			colors[i * 4 + 2] = 255;
			colors[i * 4 + 3] = 255;
			break;
		case Color::red_green:
			colors[i * 4] = 255;
			colors[i * 4 + 1] = 255;
			colors[i * 4 + 2] = 0;
			colors[i * 4 + 3] = 255;
			break;
		case Color::red_blue:
			colors[i * 4] = 255;
			colors[i * 4 + 1] = 0;
			colors[i * 4 + 2] = 255;
			colors[i * 4 + 3] = 255;
			break;
		case Color::green_blue:
			colors[i * 4] = 0;
			colors[i * 4 + 1] = 255;
			colors[i * 4 + 2] = 255;
			colors[i * 4 + 3] = 255;
			break;
		default:
			colors[i * 4] = 0;
			colors[i * 4 + 1] = 255;
			colors[i * 4 + 2] = 255;
			colors[i * 4 + 3] = 255;
			break;
			//assert(false);
			//break;
		}
	}
	//png�쐬
	LodePNG::Encoder encoder;
	encoder.getSettings().zlibsettings.useLZ77 = 0;
	std::vector<unsigned char> img;
	img.reserve(width * height * 4);
	encoder.encode(img, colors, width, height);
	unsigned int size = (unsigned int)img.size();
	unsigned char *buffer = new unsigned char[size];
	for (unsigned int i = 0; i < size; i++)
		buffer[i] = img[i];
	WriteFile(file_name.c_str(), buffer, size);
}