#include <cassert>
#include <cstdlib>
#include <fbxsdk.h>
#include <vector>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <set>
#include <iterator>
#include <string>
#include <complex>
#include <stack>
#include "fbxToLoopTexture.h"
#include "intsToPng.h"

namespace
{
	using int2 = std::array<int, 2>;

	int width;
	int height;
	const Uv uv_default = { -1,-1 };

	//Pixel図形
	struct Pixel
	{
		int x;
		int y;

		bool operator<(const Pixel& rhs) const
		{
			const Pixel &lhs = *this;
			return lhs.x < rhs.x ||
				(lhs.x == rhs.x && lhs.y < rhs.y);
		}

		bool operator==(const Pixel& rhs) const
		{
			const Pixel &lhs = *this;
			return lhs.x == rhs.x
				&& lhs.y == rhs.y;
		}

		bool operator!=(const Pixel& rhs) const
		{
			const Pixel &lhs = *this;
			return !(lhs == rhs);
		}
	};

	Uv PixelToUv(Pixel pixel)
	{
		double u = static_cast<double>(pixel.x) / static_cast<double>(width - 1);
		double v = static_cast<double>(pixel.y) / static_cast<double>(height - 1);
		return{ u,v };
	}

	Pixel UvToPixel(Uv uv)
	{
		double u = uv.u;
		double v = uv.v;
		int x = static_cast<int>(u * (width - 1));
		int y = static_cast<int>(v * (height - 1));
		return{ x,y };
	}

	//UV図形
	struct UvVertex
	{
		int control_point_id;
		Uv uv;

		bool operator<(const UvVertex &rhs) const
		{
			const UvVertex &lhs = *this;
			//return UvToPixel(lhs.uv) < UvToPixel(rhs.uv);
			if (lhs.control_point_id < rhs.control_point_id)
				return true;
			else if (lhs.control_point_id == rhs.control_point_id
				&& lhs.uv < rhs.uv)
				return true;
			else
				return false;
		}

		bool operator==(const UvVertex &rhs) const
		{
			const UvVertex &lhs = *this;
			//return UvToPixel(lhs.uv) == UvToPixel(rhs.uv);
			return lhs.control_point_id == rhs.control_point_id
				&& lhs.uv == rhs.uv;
		}

		bool operator!=(const UvVertex &rhs) const
		{
			return !(*this == rhs);
		}
	};

	using UvEdge = std::array<UvVertex, 2>;
	using UvTriangle = std::array<UvEdge, 3>;

	//px図形
	struct LoopId
	{
		//start_control_point_id < end_control_point_id
		int start_control_point_id;
		int end_control_point_id;
		int order;//startから何番目のピクセルか,startは0

		bool operator==(const LoopId &rhs) const
		{
			const LoopId &lhs = *this;
			return lhs.start_control_point_id == rhs.start_control_point_id
				&& lhs.end_control_point_id == rhs.end_control_point_id
				&& lhs.order == rhs.order;
		}
		bool operator!=(const LoopId &rhs) const
		{
			const LoopId &lhs = *this;
			return!(lhs == rhs);
		}
		/*bool operator<(const LoopId &rhs) const
		{
			const LoopId &lhs = *this;
			if (lhs.start_control_point_id < rhs.start_control_point_id)
				return true;
			else if (lhs.start_control_point_id == rhs.start_control_point_id
				&& lhs.end_control_point_id < rhs.end_control_point_id)
				return true;
			else if (lhs.start_control_point_id == rhs.start_control_point_id
				&& lhs.end_control_point_id == rhs.end_control_point_id
				&& lhs.order < rhs.order)
				return true;
			else
				return  false;
		}*/
	};

	struct Hash
	{
		std::size_t operator()(const LoopId& key) const
		{
			std::string s1 = std::to_string(key.start_control_point_id);
			std::string s2 = std::to_string(key.end_control_point_id);
			std::string s3 = std::to_string(key.order);
			return std::hash<std::string>()(s1 + s2 + s3);
		}
	};

	//pixel図形
	struct PixelVertex
	{
		Pixel pixel;
		LoopId loop_id;
	};
	using PixelEdge = std::vector<PixelVertex>;
	using PixelTriangle = std::array<PixelEdge, 3>;

	//線分を引くのに必要な情報
	struct PixelEdgeInfo
	{
		//start_control_id < end_control_id
		//一意的にするため
		Pixel start_pixel;
		Pixel end_pixel;
		int start_contro_point_id;
		int end_control_point_id;
	};

	enum Direction4
	{
		up,
		right,
		down,
		left,
	};

	//複数のポリゴンの頂点が1つのピクセルにある場合に必要
	struct MultiVertexPixel
	{
		//このピクセルにある頂点の内、各方向の1番大きいuv値
		std::array<double, 4> limit_values;
		std::array<LoopId, 4> loop_ids = { {-1} };
		std::array<Pixel, 4> pixels;
	};

	//FbxSdk
	FbxManager *fbx_manager;
	FbxScene *fbx_scene;
	FbxImporter *fbx_importer;
	FbxGeometryConverter *fbx_geometry_converter;
	FbxMesh *mesh = nullptr;
	FbxVector4 *control_point;

	//三角形
	std::vector<UvTriangle> uv_triangles;
	std::vector<PixelTriangle> pixel_triangles;

	//辺
	std::set<UvEdge> loop_uv_edges;
	std::set<UvEdge> to_be_deleted_uv_edges;
	std::vector<PixelEdge> loop_pixel_edges;

	//その他
	std::unordered_multimap<LoopId, Pixel, Hash> loop_id_map;
	std::string uv_set_name;

	//画像
	std::vector<int> edges_image;
	std::vector<int> loop_edges_count_image;
	std::vector<DstUvInfo4> loop_image;
	//std::vector<int> triangle_inner_image;
	std::vector<MultiVertexPixel> multi_vertex_pixels;//ピクセルポリゴンをUvTextureに貼り付けたもの


	PixelEdge GetPixelEdge(PixelEdgeInfo edge_info)
	{
		//ブレゼンハムのアルゴリズム
		//start_pixelは含むが、end_pixelは含まない辺を返す
		Pixel start_pixel = edge_info.start_pixel;
		Pixel end_pixel = edge_info.end_pixel;
		int start_control_point_id = edge_info.start_contro_point_id;
		int end_control_point_id = edge_info.end_control_point_id;

		int delta_x = abs(end_pixel.x - start_pixel.x);
		int delta_y = abs(end_pixel.y - start_pixel.y);
		int direction_x = (start_pixel.x < end_pixel.x) ? 1 : -1;
		int direction_y = (start_pixel.y < end_pixel.y) ? 1 : -1;
		Pixel now_pixel = start_pixel;
		int order = 0;
		PixelEdge pixel_edge;

		assert(start_control_point_id <= end_control_point_id);
		//傾きが1未満の場合
		if (delta_y < delta_x)
		{
			int error = -delta_x;
			for (int i = 0; i <= delta_x; i++)
			{
				LoopId loop_id = { start_control_point_id,end_control_point_id,order };
				PixelVertex pixel_vertex = { now_pixel, loop_id };
				loop_id_map.insert(std::make_pair(loop_id, now_pixel));
				pixel_edge.push_back(pixel_vertex);
				now_pixel.x += direction_x;
				order++;
				error += 2 * delta_y;
				if (0 <= error)
				{
					now_pixel.y += direction_y;
					error -= 2 * delta_x;
				}
			}
		}
		//傾きが1以上の場合
		else
		{
			int error = -delta_y;
			for (int i = 0; i <= delta_y; i++)
			{
				LoopId loop_id = { start_control_point_id,end_control_point_id,order };
				PixelVertex pixel_vertex = { now_pixel, loop_id };
				loop_id_map.insert(std::make_pair(loop_id, now_pixel));
				pixel_edge.push_back(pixel_vertex);
				now_pixel.y += direction_y;
				order++;
				error += 2 * delta_x;
				if (0 <= error)
				{
					now_pixel.x += direction_x;
					error -= 2 * delta_y;
				}
			}
		}
		/*	assert(1 <= pixel_edge.size());
			if (pixel_edge.size() != 1)
				pixel_edge.pop_back();*/

				//頂点を消す
				/*if (pixel_edge.size() <= 2)
				{
					return{};
				}
				else
				{
					pixel_edge.erase(pixel_edge.begin());
					pixel_edge.pop_back();
				}
				return pixel_edge;*/
		return pixel_edge;
	}

	PixelEdgeInfo UvEdgeToPixelEdgeInfo(UvEdge edge)
	{
		UvVertex uv_vertex_1 = edge[0];
		UvVertex uv_vertex_2 = edge[1];
		Pixel pixel_1 = UvToPixel(uv_vertex_1.uv);
		Pixel pixel_2 = UvToPixel(uv_vertex_2.uv);
		int control_point_1 = uv_vertex_1.control_point_id;
		int control_point_2 = uv_vertex_2.control_point_id;

		assert(control_point_1 != control_point_2);
		if (control_point_1 < control_point_2)
			return{ pixel_1, pixel_2, control_point_1, control_point_2 };
		else
			return{ pixel_2, pixel_1, control_point_2, control_point_1 };
	}

	void SetOuterPixelEdges()
	{
		for (UvEdge uv_edge : loop_uv_edges)
		{

			{
				PixelEdgeInfo pixel_edge_info = UvEdgeToPixelEdgeInfo(uv_edge);
				PixelEdge pixel_edge = GetPixelEdge(pixel_edge_info);
				loop_pixel_edges.push_back(pixel_edge);
			}
		}
	}

	void SetPixelTriangles()
	{
		for (UvTriangle uv_triangle : uv_triangles)
		{
			PixelTriangle pixel_triangle;
			for (int i = 0; i < 3; i++)
			{
				PixelEdgeInfo pixel_edge_info = UvEdgeToPixelEdgeInfo(uv_triangle[i]);
				PixelEdge pixel_edge = GetPixelEdge(pixel_edge_info);
				pixel_triangle[i] = pixel_edge;
			}
			pixel_triangles.push_back(pixel_triangle);
		}
	}

	Uv GetPolygonVertexUv(int pPolygonIndex, int pPositionInPolygon)//error
	{
		FbxVector2 uv;
		bool pUnmapped = false;
		if (!(mesh->GetPolygonVertexUV(pPolygonIndex, pPositionInPolygon, uv_set_name.c_str(), uv, pUnmapped)))
			assert(false);
		assert(pUnmapped == false);
		double u = uv.mData[0];
		double v = uv.mData[1];
		return{ u,v };
	}

	UvVertex GetUvVertex(int pPolygonIndex, int pPositionInPolygon)
	{
		Uv uv = GetPolygonVertexUv(pPolygonIndex, pPositionInPolygon);
		int control_point_id = mesh->GetPolygonVertex(pPolygonIndex, pPositionInPolygon);
		return{ control_point_id,uv };
	}

	UvEdge GetUvEdge(int nPolygonIndex, int pPositionInPolygon1, int pPositionInPolygon2)
	{
		UvVertex start_vertex = GetUvVertex(nPolygonIndex, pPositionInPolygon1);
		UvVertex end_vertex = GetUvVertex(nPolygonIndex, pPositionInPolygon2);
		if (end_vertex.control_point_id < start_vertex.control_point_id)
			std::swap(start_vertex, end_vertex);
		return{ start_vertex,end_vertex };
	}

	void SetUvSetName(int index)
	{
		assert(mesh != nullptr);
		FbxStringList uv_set_names;
		mesh->GetUVSetNames(uv_set_names);
		int uv_set_num = uv_set_names.GetCount();
		assert(index < uv_set_num);
		for (int i = 0; i < uv_set_num; i++)
		{
			printf("%s\n", uv_set_names[i].Buffer());
		}
		uv_set_name = uv_set_names[index].Buffer();
	}

	void SetMesh(FbxNode *node = nullptr)
	{
		assert(fbx_scene != nullptr);
		if (node == nullptr)
			node = fbx_scene->GetRootNode();
		FbxNodeAttribute *attribute = node->GetNodeAttribute();
		if (attribute != nullptr && attribute->GetAttributeType() == FbxNodeAttribute::eMesh)
		{
			//mesh = dynamic_cast<FbxMesh*>(attribute);//なぜかcastできない
			mesh = static_cast<FbxMesh*>(attribute);
			assert(mesh != nullptr);
			return;
		}
		else
		{
			int child_num = node->GetChildCount();
			for (int i = 0; i < child_num; i++)
			{
				FbxNode *child = node->GetChild(i);
				assert(child != nullptr);
				SetMesh(child);
			}
		}
	}

	void InsertUvEdge(UvEdge uv_edge)
	{
		auto pair = loop_uv_edges.insert(uv_edge);
		if (pair.second == false)
		{
			to_be_deleted_uv_edges.insert(uv_edge);
		}
	}

	void SetLoopUvEdge()
	{
		int polygon_num = mesh->GetPolygonCount();
		for (int i = 0; i < polygon_num; i++)
		{
			int polygon_id = i;
			int polygon_size = mesh->GetPolygonSize(i);
			assert(polygon_size == 3);

			//三角形の辺を拾得
			UvEdge uv_edge_1 = GetUvEdge(polygon_id, 0, 1);
			InsertUvEdge(uv_edge_1);
			UvEdge uv_edge_2 = GetUvEdge(polygon_id, 1, 2);
			InsertUvEdge(uv_edge_2);
			UvEdge uv_edge_3 = GetUvEdge(polygon_id, 2, 0);

			InsertUvEdge(uv_edge_3);
			uv_triangles.push_back({ uv_edge_1,uv_edge_2,uv_edge_3 });
		}
	}

	void SetUvTriangles()
	{
		int polygon_num = mesh->GetPolygonCount();
		for (int i = 0; i < polygon_num; i++)
		{
			int polygon_id = i;
			int polygon_size = mesh->GetPolygonSize(i);
			assert(polygon_size == 3);

			//三角形の辺を拾得
			UvVertex start_uv_vertex;
			UvVertex end_uv_vertex;
			UvTriangle triangle;

			start_uv_vertex = GetUvVertex(polygon_id, 0);
			end_uv_vertex = GetUvVertex(polygon_id, 1);
			triangle[0] = { start_uv_vertex,end_uv_vertex };

			start_uv_vertex = GetUvVertex(polygon_id, 1);
			end_uv_vertex = GetUvVertex(polygon_id, 2);
			triangle[1] = { start_uv_vertex,end_uv_vertex };

			start_uv_vertex = GetUvVertex(polygon_id, 2);
			end_uv_vertex = GetUvVertex(polygon_id, 0);
			triangle[2] = { start_uv_vertex,end_uv_vertex };

			//辺から三角形を作成
			uv_triangles.push_back(triangle);
		}
	}

	Pixel GetDstPixel(LoopId loop_id, Pixel src_pxiel)
	{
		//Hash hash;
		if (2 < loop_id_map.count(loop_id))
		{
			auto pair = loop_id_map.equal_range(loop_id);
			for (auto itr = pair.first; itr != pair.second; itr++)
			{
				Pixel dst_pixel = itr->second;
				printf("%d,%d,%d:%d,%d\n", loop_id.start_control_point_id, loop_id.end_control_point_id,
					loop_id.order, dst_pixel.x, dst_pixel.y);
			}
		}

		auto pair = loop_id_map.equal_range(loop_id);
		for (auto itr = pair.first; itr != pair.second; itr++)
		{
			Pixel dst_pixel = itr->second;
			if (src_pxiel != dst_pixel)
				return dst_pixel;
		}
		return src_pxiel;
	}

	Uv LoopIdToUv(LoopId loop_id, Pixel src_pxiel)
	{
		Pixel pixel = GetDstPixel(loop_id, src_pxiel);
		return PixelToUv(pixel);
	}

	bool IsEmpty(DstUvInfo dst_uv_info)
	{
		return dst_uv_info.distance == -1.f;
	}

	void WriteInnerPixel(Direction4 direction)
	{
		int first_move_line_start;
		int first_move_line_end;
		int first_move_line_add;

		int second_move_line_start;
		int second_move_line_end;
		int second_move_line_add;

		bool is_horizontal;

		switch (direction)
		{
		case Direction4::up:
			first_move_line_start = 0;
			first_move_line_end = height;
			first_move_line_add = 1;
			second_move_line_start = 0;
			second_move_line_end = width;
			second_move_line_add = 1;
			is_horizontal = false;
			break;
		case Direction4::right:
			first_move_line_start = 0;
			first_move_line_end = width;;
			first_move_line_add = 1;
			second_move_line_start = 0;
			second_move_line_end = height;
			second_move_line_add = 1;
			is_horizontal = true;
			break;
		case Direction4::down:
			first_move_line_start = height - 1;
			first_move_line_end = -1;
			first_move_line_add = -1;
			second_move_line_start = 0;
			second_move_line_end = width;
			second_move_line_add = 1;
			is_horizontal = false;
			break;
		case Direction4::left:
			first_move_line_start = width - 1;
			first_move_line_end = -1;;
			first_move_line_add = -1;
			second_move_line_start = 0;
			second_move_line_end = height;
			second_move_line_add = 1;
			is_horizontal = true;
			break;
		}

		for (int a = second_move_line_start; a != second_move_line_end; a += second_move_line_add)
		{
			std::stack<std::reference_wrapper<DstUvInfo>> stack;
			for (int b = first_move_line_start; b != first_move_line_end; b += first_move_line_add)
			{
				int index = is_horizontal ? (b + a * width) : (a + b * width);
				DstUvInfo &now_pixele = loop_image[index].d[direction];

				if (IsEmpty(now_pixele))
				{
					;
				}
				else
				{
					int distance = 0;
					DstUvInfo  &edge_pixel = now_pixele;
					while (!stack.empty())
					{
						DstUvInfo &inner_pixel = stack.top();
						inner_pixel = edge_pixel;
						inner_pixel.distance = static_cast<float>(distance) / width;
						distance++;
						stack.pop();
						printf("u:%f\nv:%f\ndistance%f\nangle%f\n", inner_pixel.u, inner_pixel.v, inner_pixel.distance, inner_pixel.angle);
					}

				}
				stack.push(std::ref(now_pixele));
			}
		}
	}

}

extern std::vector<DstUvInfo4> FbxToLoopTexture(int argc, char *argv[], int _width, int _height, int texture_index)
{
	//assert
	assert(0 <= texture_index);

	//init
	assert(argc == 2);
	fbx_manager = FbxManager::Create();
	fbx_scene = FbxScene::Create(fbx_manager, "");
	fbx_importer = FbxImporter::Create(fbx_manager, "");
	fbx_importer->Initialize(argv[1]);
	fbx_importer->Import(fbx_scene);
	width = _width;
	height = _height;
	//画像
	edges_image.resize(width * height, 0);
	//triangle_inner_image.resize(width * height, 0);
	loop_edges_count_image.resize(width * height, 0);
	loop_image.resize(width * height);
	multi_vertex_pixels.resize(width * height);

	//三角化
	fbx_geometry_converter = new FbxGeometryConverter(fbx_manager);
	fbx_geometry_converter->Triangulate(fbx_scene, false);

	//メッシュ拾得
	SetMesh();
	assert(mesh != nullptr);
	control_point = mesh->GetControlPoints();

	//UvSet名拾得
	SetUvSetName(texture_index);

	//UV三角形とループ辺と拾得
	SetLoopUvEdge();
	//重複削除
	for (UvEdge uv_edge : to_be_deleted_uv_edges)
	{
		int erase_num = loop_uv_edges.erase(uv_edge);
		assert(erase_num == 1);
		printf("erase:%d個\n", erase_num);
	}

	//ピクセル三角形とピクセルループ辺をget
	SetOuterPixelEdges();
	//SetPixelTriangles();

/*	//edges_imageを作成
	{
		for (PixelTriangle pixel_triangle : pixel_triangles)
		{
			for (PixelEdge pixel_edge : pixel_triangle)
			{
				for (PixelVertex pixel_vertex : pixel_edge)
				{
					int x = pixel_vertex.pixel.x;
					int y = pixel_vertex.pixel.y;
					int index = x + y * width;
					edges_image[index] = 1;
				}
			}
		}
		IntsToPng(edges_image, width, height, "edges_image.png");
	}*/

	//loop_edges_count_imageを作成
	assert(loop_uv_edges.size() == loop_pixel_edges.size());
	{
		int count = 0;
		for (UvEdge uv_edge : loop_uv_edges)
		{
			PixelEdge pixel_edge = loop_pixel_edges[count];
			int pixel_edge_num = pixel_edge.size();
			Uv polygon_uv_1 = uv_edge[0].uv;
			Uv polygon_uv_2 = uv_edge[1].uv;
			double max_u = std::max(polygon_uv_1.u, polygon_uv_2.u);
			double min_u = std::min(polygon_uv_1.u, polygon_uv_2.u);
			double max_v = std::max(polygon_uv_1.v, polygon_uv_2.v);
			double min_v = std::min(polygon_uv_1.v, polygon_uv_2.v);

			for (int i = 0; i < pixel_edge_num; i++)
			{
				PixelVertex pixel_vertex = pixel_edge[i];
				Pixel pixel = pixel_vertex.pixel;
				int x = pixel_vertex.pixel.x;
				int y = pixel_vertex.pixel.y;
				int index = x + y * width;
				Uv uv = PixelToUv(pixel);
				LoopId loop_id = pixel_vertex.loop_id;
				//ポリゴンの頂点とピクセルが一対一に対応しているとき
				if (loop_edges_count_image[index] == 0)
				{
					for (int j = 0; j < 4; j++)
					{
						multi_vertex_pixels[index].loop_ids[j] = loop_id;
						multi_vertex_pixels[index].pixels[j] = pixel;
					}
					multi_vertex_pixels[index].limit_values[0] = min_u;
					multi_vertex_pixels[index].limit_values[1] = max_v;
					multi_vertex_pixels[index].limit_values[2] = max_u;
					multi_vertex_pixels[index].limit_values[3] = min_v;
				}
				//ポリゴンの頂点が重複する場合
				else
				{
					MultiVertexPixel &original = multi_vertex_pixels[index];
					//up
					if (min_u < original.limit_values[0])
					{
						original.limit_values[0] = min_u;
						original.loop_ids[0] = loop_id;
						original.pixels[0] = pixel;
					}
					//right
					if (original.limit_values[1] < max_v)
					{
						original.limit_values[1] = max_v;
						original.loop_ids[1] = loop_id;
						original.pixels[1] = pixel;
					}
					//down
					if (original.limit_values[2] < max_u)
					{
						original.limit_values[2] = max_u;
						original.loop_ids[2] = loop_id;
						original.pixels[2] = pixel;
					}
					//left
					if (min_v < original.limit_values[3])
					{
						original.limit_values[3] = min_v;
						original.loop_ids[3] = loop_id;
						original.pixels[3] = pixel;
					}
				}
				loop_edges_count_image[index]++;
			}
			count++;
		}
		IntsToPng(loop_edges_count_image, width, height, "loop_edges_count_image.png");
		//std::abort();
	}

	//loop_imageの辺を作成
	{
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int index = x + y * width;
				MultiVertexPixel &multi_vertex_pixel = multi_vertex_pixels[index];
				if (multi_vertex_pixel.loop_ids[0].start_control_point_id != -1)
				{
					for (int i = 0; i < 4; i++)
					{
						LoopId loop_id = multi_vertex_pixel.loop_ids[i];
						Pixel pixel = multi_vertex_pixel.pixels[i];
						Uv src_uv = PixelToUv(pixel);
						Uv dst_uv = LoopIdToUv(loop_id, pixel);
						std::complex<float> src_complex(src_uv.u, src_uv.v);
						std::complex<float> dst_complex(dst_uv.u, dst_uv.v);
						float arg = std::arg(dst_complex - src_complex);
						loop_image[index].d[i].u = static_cast<float>(dst_uv.u);
						loop_image[index].d[i].v = static_cast<float>(dst_uv.v);
						loop_image[index].d[i].distance = 0.f;
						loop_image[index].d[i].angle = arg;
					}
				}
			}
		}
		//std::abort();
	}

	//loop_imageの内部領域を作成
	{
		for (int i = 0; i < 4; i++)
		{
			WriteInnerPixel(static_cast<Direction4>(i));
		}
	}
	return loop_image;

	/*//triangle_inner_imageを作成
	{
		const int2 none = { 0,0 };
		std::vector<int2> pixels;
		pixels.resize(width * height, none);
		for (PixelTriangle pixel_triangle : pixel_triangles)
		{
			//描写領域
			int max_x;
			int min_x;
			int max_y;
			int min_y;
			bool is_touched_edge = false;
			bool is_first = true;
			int2 first_edge_id;

			for (PixelEdge pixel_edge : pixel_triangle)
			{
				for (PixelVertex pixel_vertex : pixel_edge)
				{
					int x = pixel_vertex.pixel.x;
					int y = pixel_vertex.pixel.y;
					int2 edge_id = { pixel_vertex.loop_id.start_control_point_id,
						pixel_vertex.loop_id.end_control_point_id };
					if (pixels[x + y * width] == none)
						pixels[x + y * width] = edge_id;
					if (!is_first)
					{
						max_x = std::max(x, max_x);
						min_x = std::min(x, min_x);
						max_y = std::max(y, max_y);
						min_y = std::min(y, min_y);
					}
					else
					{
						max_x = min_x = x;
						max_y = min_y = y;
						is_first = false;
					}
				}
			}
			//三角形がつぶれているとき
			//if (max_x - min_x <= 1 || max_y - min_y <= 1)
				//continue;
			for (int i = min_y; i <= max_y; i++)
			{
				for (int j = min_x; j <= max_x; j++)
				{
					int index = j + i * width;
					if (is_touched_edge)
					{
						triangle_inner_image[index] = 1;
						int2 target_edge_id = pixels[index];
						if (first_edge_id == target_edge_id)
						{
							break;
						}
					}
					else
					{
						if (pixels[index] != none)
						{
							first_edge_id = pixels[index];
							triangle_inner_image[index] = 1;
							is_touched_edge = true;
						}
					}

				}
			}
		}
		IntsToPng(triangle_inner_image, width, height, "triangle_inner_image.png");
		std::abort();
	}*/
}