#pragma once

#include <vector>

struct Uv
{
	double u;
	double v;

	bool operator<(const Uv &rhs) const
	{
		const Uv &lhs = *this;
		if (lhs.u < rhs.u)
			return true;
		else if (lhs.u == rhs.u
			&& lhs.v < rhs.v)
			return true;
		else
			return false;
	}

	bool operator==(const Uv &rhs) const
	{
		const Uv &lhs = *this;
		return lhs.u == rhs.u
			&& lhs.v == rhs.v;
	}

	bool operator!=(const Uv &rhs) const
	{
		return !(*this == rhs);
	}
};
struct DstUvInfo
{
	float u;
	float v;
	float distance = -1.f;
	float angle = 1.f;
};
//using DstUvInfo4 = std::array<DstUvInfo, 4>;
struct DstUvInfo4
{
	DstUvInfo d[4];
};

extern std::vector<DstUvInfo4> FbxToLoopTexture(int argc, char *argv[], int width, int height,int texture_index = 0);