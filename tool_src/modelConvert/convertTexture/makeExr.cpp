#include "ImfHeader.h"
#include "ImfChannelList.h"
#include "ImfFrameBuffer.h"
#include "ImfOutputFile.h"
#include "fbxToLoopTexture.h"
#include <cassert>
#include <memory>
#include <vector>
#include <array>

namespace
{
	using float4 = std::array<float, 4>;
	using float4s = std::vector<float4>;
	size_t float_size = sizeof(float);

	float4 DstUvInfoToFloat4(DstUvInfo dst_uv_info)
	{
		float f1 = dst_uv_info.u;
		float f2 = dst_uv_info.v;
		float f3 = dst_uv_info.distance;
		float f4 = dst_uv_info.angle;
		return{ f1,f2,f3,f4 };
	}

	void MakeExr(float4s input, int width, int height, std::string file_name)
	{
		assert(input.size() == width * height);
		int max = width * height;
		float *float_data = new float[max * 4];//�㉺���E
		for (int i = 0; i < max; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				float_data[i + max * j] = input[i][j];
			}
		}
		char *char_data;
		char_data = reinterpret_cast<char*>(float_data);

		//header
		Imf::Header header(width,height);
		header.channels().insert("R", Imf::Channel(Imf::FLOAT));
		header.channels().insert("G", Imf::Channel(Imf::FLOAT));
		header.channels().insert("B", Imf::Channel(Imf::FLOAT));
		header.channels().insert("A", Imf::Channel(Imf::FLOAT));

		//buffer
		Imf::FrameBuffer buffer;
		buffer.insert("R", Imf::Slice(Imf::FLOAT, char_data, float_size, float_size * width));
		buffer.insert("G", Imf::Slice(Imf::FLOAT, (char_data + max * float_size), float_size, float_size * width));
		buffer.insert("B", Imf::Slice(Imf::FLOAT, (char_data + max * 2 * float_size), float_size, float_size * width));
		buffer.insert("A", Imf::Slice(Imf::FLOAT, (char_data + max * 3 * float_size), float_size, float_size * width));

		//output
		Imf::OutputFile output_file(file_name.c_str(), header);
		output_file.setFrameBuffer(buffer);
		output_file.writePixels(height);

		//delete
		delete char_data;
	}
}

extern void MakeExr(std::vector<DstUvInfo4> dst_uv_infos, int width, int height)
{
	assert(dst_uv_infos.size() == width * height);
	int index = width * height;

	float4s float4s_array[4] = { {},{},{},{} };
	for (int i = 0; i < 4; i++)
	{
		float4s_array[i].resize(index);
	}

	for (int i = 0; i < index; i++)
	{
		DstUvInfo4 dst_uv_info_4 = dst_uv_infos[i];
		for (int j = 0; j < 4; j++)
		{
			float4s_array[j][i] = DstUvInfoToFloat4(dst_uv_info_4.d[j]);
		}
	}

	for (int i = 0; i < 4; i++)
	{
		std::string file_name = "test" + std::to_string(i) + ".exr";
		MakeExr(float4s_array[i], width, height, file_name);
	}
}