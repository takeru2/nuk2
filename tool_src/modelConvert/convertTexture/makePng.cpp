#include "fbxToLoopTexture.h"
#include "lodepng.h"
#include "half.hpp"
#include <cassert>

namespace
{
/*	using half = half_float::half;

	struct HalfFloatPixel
	{
		half h[8];
	};

	std::vector<HalfFloatPixel> half_float_pixels;

	bool WriteFile(const char *filename, void *buffer, size_t fsize)
	{
		FILE *fp;
		size_t wsize;
		fp = fopen(filename, "wb");
		if (!fp)
			return false;
		wsize = fwrite(buffer, sizeof(char), fsize, fp);
		fclose(fp);
		if (wsize == fsize)
			return true;
		return false;
	}

	HalfFloatPixel DstUvInfoToHalfFloatPixel(OldDstINfo dst_uv_info)
	{
		HalfFloatPixel half_float_pixel;
		half_float_pixel.h[0] = static_cast<half>(dst_uv_info.up.u);
		half_float_pixel.h[1] = static_cast<half>(dst_uv_info.up.v);
		half_float_pixel.h[2] = static_cast<half>(dst_uv_info.right.u);
		half_float_pixel.h[3] = static_cast<half>(dst_uv_info.right.v);
		half_float_pixel.h[4] = static_cast<half>(dst_uv_info.down.u);
		half_float_pixel.h[5] = static_cast<half>(dst_uv_info.down.v);
		half_float_pixel.h[6] = static_cast<half>(dst_uv_info.left.u);
		half_float_pixel.h[7] = static_cast<half>(dst_uv_info.left.v);
		return half_float_pixel;
	}*/
}

/*extern void MakePing(std::vector<OldDstINfo> dst_uv_infos, int width, int height, std::string file_name)
{
	unsigned char *png_bytes_1 = new unsigned char[8 * width * height];
	unsigned char *png_bytes_2 = new unsigned char[8 * width * height];
	OldDstINfo dst_uv_info;
	HalfFloatPixel half_float_pixel;

	//*charに変換
	for (int i = 0; i < dst_uv_infos.size(); i++)
	{
		printf("%d\n", i);
		//確保
		half *half_array = new half[8];
		unsigned char *char_array = new unsigned char[16];

		dst_uv_info = dst_uv_infos[i];
		half_float_pixel = DstUvInfoToHalfFloatPixel(dst_uv_info);
		for (int j = 0; j < 8; j++)
		{
			half_array[j] = half_float_pixel.h[j];
		}
		char_array = reinterpret_cast<unsigned char*>(half_array);
		for (int k = 0; k < 8; k++)
			png_bytes_1[i * 8 + k] = char_array[k];
		for (int k = 8; k < 16; k++)
			png_bytes_2[i * 8 + (k - 8)] = char_array[k];
		//解放
		free(char_array);
	}
	//png作成
	std::string file_name_1 = file_name + "_1.png";
	std::string file_name_2 = file_name + "_2.png";
	LodePNG::encode(file_name_1, png_bytes_1, width, height, 6, 16);
	LodePNG::encode(file_name_2, png_bytes_2, width, height, 6, 16);
}*/