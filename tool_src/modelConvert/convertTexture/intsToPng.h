#pragma once

#include <vector>

enum Color
{
	white,
	black,
	red,
	green,
	blue,
	red_green,
	red_blue,
	green_blue,
};
void IntsToPng(std::vector<int> colors, int width, int height,std::string);