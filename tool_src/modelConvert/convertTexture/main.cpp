#include <windows.h>
#include <cassert>
//#include "makePng.h"
#include "makeExr.h"

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment( lib, "vs2015/x64/lib_debug/libfbxsdk-md.lib" )
#pragma comment( lib, "zlibstatic.lib")
#else
#pragma comment( lib, "vs2015/x64/lib_release/libfbxsdk-md.lib" )
#pragma comment( lib, "zlibstatic.lib")
#endif

int main(int argc, char *argv[])
{
	assert(argc == 2);
	std::string target_fbx = argv[1];
	//std::string output_file_name = "test";
	int width = 1024;
	int height = 1024;
	std::vector<DstUvInfo4> dst_uv_infos = FbxToLoopTexture(argc, argv, width, height,1);
	//MakePing(dst_uv_infos, width, height, output_file_name);
	MakeExr(dst_uv_infos, width, height);
	printf("終了しました\n");
	system("pause");
};