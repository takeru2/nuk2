﻿#!/usr/bin/perl

{
	$data_path = "../data/model";

	# Texture png -> dds
	$log = `/bin/find $data_path -type f -regex ".*png.*"`;
	@flist = split("\n", $log);

	for($i=0;$i<=$#flist;$i++){
		$full_name = @flist[$i];
		$out_name = $full_name;

#		$log = `./texconv -y -f BC5_UNORM $full_name`;
		$log = `./texconv -y $full_name`;
		print $log;
		
		$out_name =~ s/\.png/\.DDS/g;
		$new_out_name = $out_name;
		$new_out_name =~ s/\.DDS/\.dds/g;
		system("mv -f $out_name $new_out_name");
	}

	# Texture fbx -> model
	$log = `/bin/find $data_path -type f -regex ".*fbx.*"`;
	@flist = split("\n", $log);

	for($i=0;$i<=$#flist;$i++){
		$full_name = @flist[$i];
		$out_name = $full_name;

#		$log = `./modelConvert.exe -scale 0.001 $full_name`;
		$log = `./modelConvert.exe $full_name`;

		if($full_name =~ /test2/){
			$log = `./modelConvert.exe -anim_bb -trianglelist $full_name`;
			print "convert trianglelist $full_name";
		}
		print $log;
		
	}


}

