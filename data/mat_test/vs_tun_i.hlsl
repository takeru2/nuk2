
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj : packoffset( c0 );
	float4      lv : packoffset( c4 );
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
	float2	uv     : TEXCOORD0;
};

VS_OUTPUT VS(
	in float4 pos	 : POSITION,
	in float3 normal : NORMAL,
	in float4 color  : COLOR0,
	in float2 uv     : TEXCOORD0,
	in float4 inst_pos : MATRIX,
	uint InstanceId  : SV_InstanceID
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.pos    = mul(matWorldViewProj, pos + float4(inst_pos.xyz, 0));
	output.color  = color;
	output.uv     = uv;

	uint col = asuint(inst_pos.w);
	output.color.r = (float)((col & 0xff) / 255.0);
	output.color.g = (float)(((col >> 8) & 0xff) / 255.0);
	output.color.b = (float)(((col >> 16) & 0xff) / 255.0);
	output.color.a = (float)(((col >> 24) & 0xff) / 255.0);

	output.color.rgb *= clamp(dot(normal, lv.xyz), 0.2, 1.0);

	return output;
}
