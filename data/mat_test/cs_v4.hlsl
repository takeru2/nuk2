
struct InputBufferType
{
    float3 pos;
	uint col;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};


StructuredBuffer<InputBufferType>   input_buf   : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );

[numthreads(100, 1, 1)]

void CS( const CSInput input ) {
    int index = input.dispatch.z * 100 * 1 + input.dispatch.y * 100 + input.dispatch.x;

	uint vbIndex;
	uint vstride = 16;

	float3 cp = input_buf[ index ].pos;
	uint col  = input_buf[ index ].col;

	vbIndex = index * vstride;
	output_buf.Store3( vbIndex + 0, asuint( cp ) );
	output_buf.Store ( vbIndex + 12, col );
}


