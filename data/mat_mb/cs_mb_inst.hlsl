
struct InputBufferType
{
    float3 pos;
	uint col;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

StructuredBuffer<InputBufferType>   input_buf   : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );

Texture2D	textureF : register( t1 );
SamplerState textureFSampler : register( s0 );
Texture2D	textureFD : register( t2 );
SamplerState textureFDSampler : register( s1 );

Texture2D	textureB : register( t3 );
SamplerState textureBSampler : register( s2 );
Texture2D	textureBD : register( t4 );
SamplerState textureBDSampler : register( s3 );


[numthreads(100, 1, 1)]

void CS( const CSInput input ) {
    int index = input.dispatch.z * 100 * 1 + input.dispatch.y * 100 + input.dispatch.x;

	uint vbIndex;
	uint vstride = 16;

	float3 cp = input_buf[ index ].pos;
	uint col  = input_buf[ index ].col;

	float3 xyz_uvw = float3(
		(float)(col & 0xff) / 255.0,
		1.f - (float)((col >> 8) & 0xff) / 255.0,
		(float)((col >> 16) & 0xff) / 255.0
		);

	float4 tcol_fd = textureFD.SampleLevel(textureFDSampler, xyz_uvw.xy, 0);
	float4 tcol_bd = textureBD.SampleLevel(textureBDSampler, xyz_uvw.xy, 0);
	col = 0xffffffff;
	if( (tcol_fd.x > xyz_uvw.z) || (tcol_bd.x > (1 - xyz_uvw.z)) ){
		col = 0;
	}else{
		float4 tcol = 0;
		if(abs(tcol_fd.x - xyz_uvw.z) < abs(tcol_bd.x - (1 - xyz_uvw.z))){
			tcol = textureF.SampleLevel(textureFSampler, xyz_uvw.xy, 0);
		}else{
			tcol = textureB.SampleLevel(textureBSampler, xyz_uvw.xy, 0);
		}
		col = (uint)(tcol.x * 255.f) |
			((uint)(tcol.y * 255.f) << 8) |
			((uint)(tcol.z * 255.f) << 16) |
			((uint)(tcol.w * 255.f) << 24);
	}
	
	vbIndex = index * vstride;
	output_buf.Store3( vbIndex + 0, asuint( cp ) );
	output_buf.Store ( vbIndex + 12, col );
}


