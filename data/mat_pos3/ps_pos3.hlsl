
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj : packoffset( c0 );
};

struct PS_INPUT
{
    float4  position   : SV_POSITION;
	float4	color      : COLOR0;
};

//---- ピクセルシェーダー
float4 PS(PS_INPUT input) : SV_TARGET
{
	float4 output;
	output = input.color;

	return output;
}
