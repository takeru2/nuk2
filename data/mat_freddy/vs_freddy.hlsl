
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj;// : packoffset( c0 );
	matrix      bone[15];// : packoffset( c4 );
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
	float2	uv     : TEXCOORD0;
};

VS_OUTPUT VS(
	in float3 pos	 : POSITION,
	in float3 normal : NORMAL,
	in float2 uv     : TEXCOORD0,
	in int4 bi       : BLENDINDICES,
	in float4 bw     : BLENDWEIGHT
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	float3 wp = 0;
	wp += mul(bone[bi.x], float4(pos, 1)).xyz * bw.x;
	wp += mul(bone[bi.y], float4(pos, 1)).xyz * bw.y;
	wp += mul(bone[bi.z], float4(pos, 1)).xyz * bw.z;
	wp += mul(bone[bi.w], float4(pos, 1)).xyz * bw.w;

	output.pos    = mul(matWorldViewProj, float4(wp, 1));
	output.color  = float4(1,1,1,1);
	output.uv     = uv;

	return output;
}
