
cbuffer cbPerObject : register( b0 )
{
	uint4     bb_num;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

//-----------------------------
// In
// uint color
// uint voxBit
//-----------------------------

RWByteAddressBuffer output_buf : register( u0 );
[numthreads(32, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	uint vstride = 4 + 4; //color flag
	uint bb_num_xy = bb_num.x * bb_num.y;

	const uint exist_flag = (1<<0);
	const uint px_flag     = (2<<1);
	const uint mx_flag     = (1<<1);
	const uint pxy_flag    = (3<<1);
	
	const uint slice_flag = (1<<8);

	//index max y*z
	uint z = index / bb_num.y;
	uint y = index - z * bb_num.y;

	uint voxBitStart = 0;
	uint voxBitOffset = 4;
	uint idx = 0;

	if(y == bb_num.w){
		//断面の高さ
		uint left_idx = 0;
		bool flag = false;
		
		for(uint x=0;x<bb_num.x;++x){
			idx = (z * bb_num_xy + y * bb_num.x + x) * vstride;
			uint voxBit = output_buf.Load( idx + voxBitOffset);
			if(voxBit & exist_flag){

				if( (voxBit & pxy_flag) == px_flag){// +x
					voxBitStart = voxBit;
					left_idx = x + 1;
					flag = true;
				}else if( (voxBit & pxy_flag) == mx_flag){// -x
					if(flag){
						for(uint lx = left_idx;lx < x;++lx){
							idx = (z * bb_num_xy + y * bb_num.x + lx) * vstride;
							output_buf.Store( idx + voxBitOffset,
											  voxBitStart | slice_flag);
						}
					}
					flag = false;
				}else{
					flag = false;
				}
			}
		}
		//検索失敗している
		if(flag && (left_idx != 0) ){
			for(uint x=left_idx;x<bb_num.x;++x){
				idx = (z * bb_num_xy + y * bb_num.x + x) * vstride;
				output_buf.Store( idx + voxBitOffset, 0);
			}
		}

	}else if(y > bb_num.w){
		//断面より上
		for(uint x=0;x<bb_num.x;++x){
			idx = (z * bb_num_xy + y * bb_num.x + x) * vstride;
			output_buf.Store( idx + voxBitOffset, 0);
		}
	}

}


