
cbuffer cbPerObject : register( b0 )
{
	uint4 vox_info; //gridX gridY gridZ instNum
	uint4 vox_colors_param;
	float4 vox_scl;
	float4 vox_pos;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer   input_buf  : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );
RWByteAddressBuffer count_buf  : register( u1 );

//-----------------------------
// Out
// float3 pos
// float2 uv
// uint voxBit
//-----------------------------

[numthreads(256, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 256 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	uint vbIndex;
	uint strideSrc = 4 + 4; //color bit
	uint strideDst = 12 + 4;

	vbIndex = strideSrc * index;
	uint voxBit  = input_buf.Load( vbIndex + 4);
	uint col = 0xffffffff;
	if(voxBit & 1){
		uint write_index;
		count_buf.InterlockedAdd(0, 1, write_index);
		if(write_index >= vox_info.w)return;

		//ReCalc Pos
		uint3 _xyz;
		_xyz.z = index / (vox_info.x * vox_info.y);
		index -= _xyz.z * (vox_info.x * vox_info.y);
		_xyz.y = index / vox_info.x;
		_xyz.x = index - _xyz.y  * vox_info.x;
		
		float3 cp = float3((float)_xyz.x - (float)(vox_info.x / 2),
						   (float)_xyz.y - (float)(vox_info.y / 2),
						   (float)_xyz.z - (float)(vox_info.z / 2));
		cp = cp * vox_scl.xyz + vox_pos.xyz;

		if(voxBit & (1<<8)){ //Slice
			col = vox_colors_param.w;
		}else{
#if 1
			col = input_buf.Load( vbIndex );
#else
			//Face Test
			if( (voxBit & (3<<1)) == (3<<1) ){
				col = vox_colors_param.x;
			}else if(voxBit & (2<<1)){
				col = vox_colors_param.y;
			}else{
				col = vox_colors_param.z;
			}
#endif
		}
		
		vbIndex = strideDst * write_index;
		output_buf.Store3( vbIndex, asuint( cp ) );
		output_buf.Store( vbIndex + 12, col);
	}
}


