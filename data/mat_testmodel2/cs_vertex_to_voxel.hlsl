
cbuffer cbPerObject : register( b0 )
{
	float4   bb_box[2];//min max
	float4   bb_dd;
	int4     bb_num;
	float4   uv_scale;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer  input_buf  : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );

Texture2D	textureF : register( t1 );
SamplerState textureFSampler : register( s0 );

//-----------------------------
// Out
// uint color
// uint voxBit
//-----------------------------

void writeOne(uint offset, float3 pos, uint voxBit, float2 uv){
	uint vstride = 4 + 4; //c flag
	uint vbIndex = vstride * offset;

	float4 tcol = textureF.SampleLevel(textureFSampler, frac(uv * uv_scale.xy), 0);
	uint col = (uint)(tcol.x * 255.f) |
		((uint)(tcol.y * 255.f) << 8) |
		((uint)(tcol.z * 255.f) << 16) |
		((uint)(tcol.w * 255.f) << 24);

	//Interlocked
	output_buf.InterlockedMax( vbIndex, col);
	output_buf.InterlockedOr(vbIndex + 4,  voxBit);
}

float3 calcGPosToXyz(int3 pos){
	float3 ret;
	ret.x = bb_box[0].x + bb_dd.x * (float)pos.x;
	ret.y = bb_box[0].y + bb_dd.y * (float)pos.y;
	ret.z = bb_box[0].z + bb_dd.z * (float)pos.z;
	return ret;
}

int3 calcXyzToGPos(float3 pos){
	int3 ret;
	ret.x = (int)((pos.x - bb_box[0].x) / bb_dd.x);
	ret.y = (int)((pos.y - bb_box[0].y) / bb_dd.y);
	ret.z = (int)((pos.z - bb_box[0].z) / bb_dd.z);
	return ret;
}

void calcWrite(float3 pos, uint voxBit, float2 uv){

	int3 ret = calcXyzToGPos(pos);
	if( (ret.x < 0) || (ret.y < 0) || (ret.z < 0) ||
		(ret.x >= bb_num.x) || (ret.y >= bb_num.y) || (ret.z >= bb_num.z) ){
		
		return;
	}

	float3 wpos = calcGPosToXyz(ret);
	uint offset = (bb_num.x * bb_num.y) * ret.z + bb_num.x * ret.y + ret.x;
	writeOne(offset, wpos, voxBit, uv);
}

void writeBox(float3 pos[3], float2 uv[3], uint voxBit){

	int3 g0 = calcXyzToGPos(pos[0]);
	int3 g1 = calcXyzToGPos(pos[1]);
	int3 g2 = calcXyzToGPos(pos[2]);
	
	int3 gv_min = min(min(g0, g1), g2);
	int3 gv_max = max(max(g0, g1), g2);
	int3 gv = gv_max - gv_min;
	int n = max(max(gv.x, gv.y),gv.z) + 1;

	n = clamp(n, 1, bb_num.w);

	float3 fv1 = pos[1] - pos[0];
	float3 fv2 = pos[2] - pos[0];

	float3 wpos = pos[0];
	for(int e1=0;e1<n;e1++){
		float e1_r = (float)e1 / (float)n;
		float e2_r_max = 1.f - e1_r;
		for(int e2=0;e2<n;e2++){
			float e2_r = e2_r_max * (float)e2/(float)n;
			calcWrite(wpos + fv1 * e1_r + fv2 * e2_r,
					  voxBit,
					  uv[0] + uv[1] * e1_r +uv[2] * e2_r);
		}
	}
}

uint normalToBit(float3 normal){
	uint ret = 0;

	//-x 向きが重要
	normal = -normal;
	ret  = ((sign(normal.x) < 0)?1:2) << 1;
	
	return ret;
}

//-----------------------------
// In
// float3 pos
// float3 normal
// float2 uv
//-----------------------------

[numthreads(1, 1, 1)]

void CS( const CSInput input ) {
    int index = input.dispatch.z * 1 * 1 + input.dispatch.y * 1 + input.dispatch.x;
	uint vstride = 12 + 12 + 8; //xyz normal uv
	uint vbIndex = (vstride * 3) * index; //1/3個数で呼び出される

	float3 pos[3];
	pos[0] = asfloat(input_buf.Load3( vbIndex + 0 * vstride));
	pos[1] = asfloat(input_buf.Load3( vbIndex + 1 * vstride));
	pos[2] = asfloat(input_buf.Load3( vbIndex + 2 * vstride));

	float3 normal = 0;
	normal += asfloat(input_buf.Load3( vbIndex + 0 * vstride + 12));
	normal += asfloat(input_buf.Load3( vbIndex + 1 * vstride + 12));
	normal += asfloat(input_buf.Load3( vbIndex + 2 * vstride + 12));
	uint voxBit = normalToBit(normal) | 1;

	float2 uv[3];
	uv[0] = asfloat(input_buf.Load2( vbIndex + 0 * vstride + 24));
	uv[1] = asfloat(input_buf.Load2( vbIndex + 1 * vstride + 24));
	uv[2] = asfloat(input_buf.Load2( vbIndex + 2 * vstride + 24));

	uv[1] -= uv[0];
	uv[2] -= uv[0];

	writeBox(pos, uv, voxBit);
}


