
cbuffer cbPerObject : register( b0 )
{
	uint4     bb_num;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

//-----------------------------
// In
// uint color
// uint voxBit
//-----------------------------

RWByteAddressBuffer output_buf : register( u0 );
[numthreads(32, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	uint vstride = 4 + 4; //color flag
	uint bb_num_xy = bb_num.x * bb_num.y;
	
	const uint exist_flag = (1<<0);
	const uint slice_flag = (1<<8);

	if(index >= bb_num.x)return;

	//index max z
	uint x = index;
	uint y = bb_num.w;

	uint voxBitStart = 0;
	uint voxBitOffset = 4;
	uint idx = 0;

#if 1
	//信頼ない場所検索
	{
		uint voxBitBuf[3];
		uint voxBitBufIdx = 0;

		idx = (y * bb_num.x + x) * vstride;
		voxBitBuf[0] = output_buf.Load( idx + voxBitOffset);
		idx = (1 * bb_num_xy + y * bb_num.x + x) * vstride;
		voxBitBuf[1] = output_buf.Load( idx + voxBitOffset);
		voxBitBufIdx = 2;

		for(uint z=2;z<bb_num.z-1;++z){
			idx = (z * bb_num_xy + y * bb_num.x + x) * vstride;
			voxBitBuf[voxBitBufIdx] = output_buf.Load( idx + voxBitOffset);
			if(!(voxBitBuf[(voxBitBufIdx + 1)%3] & slice_flag) &&
			   !(voxBitBuf[voxBitBufIdx] & slice_flag) &&
				(voxBitBuf[(voxBitBufIdx + 2)%3] & slice_flag) ){

				//上下空白なら消す
				voxBitBuf[(voxBitBufIdx + 2)%3] = 0;
				idx = ((z-1) * bb_num_xy + y * bb_num.x + x) * vstride;
				output_buf.Store( idx + voxBitOffset, 0);
			}
			voxBitBufIdx = (voxBitBufIdx + 1) % 3;
		}
	}
#endif
	
#if 1
	{
		//断面の高さ
		uint left_idx = 0;
		bool flag = false;

		uint left_idx_b = 0;

		for(uint z=0;z<bb_num.z;++z){
			idx = (z * bb_num_xy + y * bb_num.x + x) * vstride;
			uint voxBit = output_buf.Load( idx + voxBitOffset);
			if(voxBit & slice_flag){

				if(!flag && (left_idx_b != 0) ){
					//exist to fillbox -> write
					for(uint lz=left_idx_b;lz<z;++lz){
						idx = (lz * bb_num_xy + y * bb_num.x + x) * vstride;
						output_buf.Store( idx + voxBitOffset, voxBit);
					}
				}

				voxBitStart = voxBit;
				left_idx = z + 1;
				flag = true;

				left_idx_b = 0;
				
			}else if(voxBit & exist_flag){
				flag = false;

				//exist -> write index
				left_idx_b = z + 1;
			}
			if(flag && !(voxBit & exist_flag)){
				output_buf.Store( idx + voxBitOffset, voxBitStart); 
			}
		}
		//検索失敗している
		if(flag && (left_idx != 0) ){
			for(uint z=left_idx;z<bb_num.z;++z){
				idx = (z * (bb_num.x * bb_num.y) + y * bb_num.x + x) * vstride;
				output_buf.Store( idx + voxBitOffset, 0);
			}
		}

	}
#endif
	
}


