
cbuffer cbPerObject : register( b0 )
{
	int4x4		    voxInfo;
	float4x4        bone[31];
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer  input_buf  : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );

[numthreads(256, 1, 1)]

void CS( const CSInput input ) {
    int index = input.dispatch.z * 256 * 1 + input.dispatch.y * 256 + input.dispatch.x;


	if(index >= voxInfo[0][0])return;
	
	uint vstride = 52;
	uint vbIndex = vstride * index;

	float3 pos        = asfloat(input_buf.Load3( vbIndex + 0));
	float3 normal     = asfloat(input_buf.Load3( vbIndex + 12));
	float2 uv         = asfloat(input_buf.Load2( vbIndex + 24));
	uint   weight_idx = asuint(input_buf.Load( vbIndex + 32));
	float4 weight     = asfloat(input_buf.Load4( vbIndex + 36));

	float3 wp = 0;
	float3 wn = 0;
	uint4 idx = uint4((weight_idx >>  0) & 0xff,
					  (weight_idx >>  8) & 0xff,
					  (weight_idx >> 16) & 0xff,
					  (weight_idx >> 24) & 0xff);
	
	wp += mul(bone[idx.x], float4(pos, 1)).xyz * weight.x;
	wp += mul(bone[idx.y], float4(pos, 1)).xyz * weight.y;
	wp += mul(bone[idx.z], float4(pos, 1)).xyz * weight.z;
	wp += mul(bone[idx.w], float4(pos, 1)).xyz * weight.w;

	wn += mul(bone[idx.x], float4(normal, 0)).xyz * weight.x;
	wn += mul(bone[idx.y], float4(normal, 0)).xyz * weight.y;
	wn += mul(bone[idx.z], float4(normal, 0)).xyz * weight.z;
	wn += mul(bone[idx.w], float4(normal, 0)).xyz * weight.w;
	wn = normalize( wn );

	
	vstride = 12 + 12 + 8;
	vbIndex = vstride * index;

	output_buf.Store3( vbIndex, asuint( wp ) );
	output_buf.Store3( vbIndex + 12, asuint( wn ) );
	output_buf.Store2( vbIndex + 12 + 12, asuint( uv ));
}


