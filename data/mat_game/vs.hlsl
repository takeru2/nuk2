
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj : packoffset( c0 );
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
};

VS_OUTPUT VS(
	in float4 pos	 : POSITION,
	in float4 color  : COLOR0
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.pos    = mul(matWorldViewProj, pos);
	output.color  = color;

	return output;
}
