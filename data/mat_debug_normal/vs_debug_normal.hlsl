
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj;
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
};

VS_OUTPUT VS(
	in float3 pos	 : POSITION,
	in float4 color  : COLOR
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.pos    = mul(matWorldViewProj, float4(pos, 1));
	output.color  = color;
	return output;
}
