
cbuffer cbPerObject : register( b0 )
{
	uint4  stream_param;
	float4 stream_param_float;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer   input_buf  : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );
RWByteAddressBuffer count_buf  : register( u1 );

//-----------------------------
// Out
// float3 pos
// float2 uv
// uint voxBit
//-----------------------------

[numthreads(256, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 256 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	if(index >= stream_param.x)return;

	uint strideSrc = stream_param.y;
	uint strideDst = 12 + 4;

	float3 pos    = asfloat(input_buf.Load3(strideSrc * index));
	float3 normal = asfloat(input_buf.Load3(strideSrc * index + 12));

	normal = normal * stream_param_float.x;
	
	uint vbIndex;
	uint write_index = index * 2;

	vbIndex = write_index * strideDst;
	output_buf.Store3( vbIndex, asuint( pos ) );
	output_buf.Store( vbIndex + 12, stream_param.z);

	vbIndex = (write_index + 1) * 16;
	output_buf.Store3( vbIndex, asuint( pos + normal ) );
	output_buf.Store( vbIndex + 12, stream_param.w);

}


