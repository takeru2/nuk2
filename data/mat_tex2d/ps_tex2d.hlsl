
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj : packoffset( c0 );
};

Texture2D	textureBase : register( t0 );
SamplerState textureBaseSampler : register( s0 );

struct PS_INPUT
{
    float4  position   : SV_POSITION;
	float4	color      : COLOR0;
	float2	uv         : TEXCOORD0;
};

float4 PS(PS_INPUT input) : SV_TARGET
{
	float4 output;
	output = input.color;
	output *= textureBase.Sample(textureBaseSampler, input.uv);

	return output;
}
