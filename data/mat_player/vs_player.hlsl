
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj : packoffset( c0 );
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
	float2	uv     : TEXCOORD0;
};

VS_OUTPUT VS(
	in float3 pos	 : POSITION,
	in float3 normal : NORMAL,
	in float2 uv     : TEXCOORD0
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.pos    = mul(matWorldViewProj, float4(pos, 1));
	output.color  = float4(1,1,1,1);
	output.uv     = uv;

	return output;
}
