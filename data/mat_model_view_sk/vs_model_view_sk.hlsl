
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj;
	matrix		matWorld;
	matrix      bone[30];
};

float4x4 IT_Matrix(float3 t, float3 b, float3 n) {
	float4x4 mat = float4x4(
		float4(t.x, t.y, t.z, 0),
		float4(b.x, b.y, b.z, 0),
		float4(n.x, n.y, n.z, 0),
		float4(0, 0, 0, 1));
	return transpose(mat);
}

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float2	uv     : TEXCOORD0;
	float3	vpos   : TEXCOORD1;
	float3	normal : TEXCOORD2;
	float3	tangent : TEXCOORD3;
};


VS_OUTPUT VS(
	in float3 pos	 : POSITION,
	in float3 normal : NORMAL,
	in float2 uv     : TEXCOORD0,
	in float3 tangent : TANGENT,
	in int4 bi       : BLENDINDICES,
	in float4 bw     : BLENDWEIGHT
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	float3 wp = 0;
	wp += mul(bone[bi.x], float4(pos, 1)).xyz * bw.x;
	wp += mul(bone[bi.y], float4(pos, 1)).xyz * bw.y;
	wp += mul(bone[bi.z], float4(pos, 1)).xyz * bw.z;
	wp += mul(bone[bi.w], float4(pos, 1)).xyz * bw.w;

	float3 wn = 0;
	wp += mul(bone[bi.x], float4(normal, 0)).xyz * bw.x;
	wp += mul(bone[bi.y], float4(normal, 0)).xyz * bw.y;
	wp += mul(bone[bi.z], float4(normal, 0)).xyz * bw.z;
	wp += mul(bone[bi.w], float4(normal, 0)).xyz * bw.w;

	float3 tn = 0;
	tn += mul(bone[bi.x], float4(tangent, 0)).xyz * bw.x;
	tn += mul(bone[bi.y], float4(tangent, 0)).xyz * bw.y;
	tn += mul(bone[bi.z], float4(tangent, 0)).xyz * bw.z;
	tn += mul(bone[bi.w], float4(tangent, 0)).xyz * bw.w;

	output.pos    = mul(matWorldViewProj, float4(wp, 1));
	output.uv     = uv;
	output.vpos   = mul(matWorld, float4(wp, 1)).xyz;
	output.normal = normalize( mul(matWorld, float4(wp,0)).xyz );
	output.tangent= normalize( mul(matWorld, float4(tn,0)).xyz );
	
	return output;
}
