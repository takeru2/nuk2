
cbuffer cbPerObject : register( b0 )
{
	float4      ambiColor;
	float4      lightDir;
	float4      lightColor;
	float4      cameraPos;
	float4      param; //param.x fresnel param.y pow
};

Texture2D	textureEmission : register( t0 );
SamplerState textureEmissionSampler : register( s0 );
Texture2D	textureAlbedo : register( t1 );
SamplerState textureAlbedoSampler : register( s1 );
Texture2D	textureMetal : register( t2 );
SamplerState textureMetalSampler : register( s2 );
Texture2D	textureNormal : register( t3 );
SamplerState textureNormalSampler : register( s3 );

// D（GGX）の項
float D_GGX(float3 H, float3 N, float Ro) {
	float NdotH = saturate(dot(H, N));
	float roughness = saturate( Ro );
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float t = ((NdotH * NdotH) * (alpha2 - 1.0) + 1.0);
	float PI = 3.1415926535897;
	return alpha2 / (PI * t * t);
}

// フレネルの項
float Flesnel(float3 V, float3 H, float Fl, float po) {
	float VdotH = saturate(dot(V, H));
	float F0 = saturate( Fl );
	float F = pow(1.0 - VdotH, po);
	F *= (1.0 - F0);
	F += F0;
	return F;
}

// G - 幾何減衰の項（クック トランスモデル）
float G_CookTorrance(float3 L, float3 V, float3 H, float3 N) {
	float NdotH = saturate(dot(N, H));
	float NdotL = saturate(dot(N, L));
	float NdotV = saturate(dot(N, V));
	float VdotH = saturate(dot(V, H));

	float NH2 = 2.0 * NdotH;
	float g1 = (NH2 * NdotV) / VdotH;
	float g2 = (NH2 * NdotL) / VdotH;
	float G = min(1.0, min(g1, g2));
	return G;
}

struct PS_INPUT
{
    float4  position   : SV_POSITION;
	float2	uv     : TEXCOORD0;
	float3	vpos   : TEXCOORD1;
	float3	normal : TEXCOORD2;
	float3	tangent : TEXCOORD3;
};

float4 PS(PS_INPUT input) : SV_TARGET
{
	float3 amissioncolor = textureEmission.Sample(textureEmissionSampler, input.uv).rgb;
	float3 albedo_color  = textureAlbedo.Sample(textureAlbedoSampler, input.uv).rgb;
	float4 metal_color   = textureMetal.Sample(textureMetalSampler, input.uv);

	float3 normal        = textureNormal.Sample(textureNormalSampler, input.uv).rgb;
	normal = 2 * normal - 1.f;
	float3x3 mat = float3x3(
		input.tangent,
		cross(input.normal, input.tangent),
		input.normal);
	normal = normalize(mul(transpose(mat), normal));
#if 0
	return float4(normal, 1.0);
#endif

#if 0	
	normal = input.normal;
#endif	
	// 環境光とマテリアルの色を合算
	float3 ambientLight = ambiColor.xyz * albedo_color.rgb;
	        
	// ワールド空間上のライト位置と法線との内積を計算
	float3 lightDirectionNormal = normalize(lightDir);
	
	float NdotL = saturate(dot(normal, lightDirectionNormal));

	// ワールド空間上の視点（カメラ）位置と法線との内積を計算
	float3 viewDirectionNormal = normalize(cameraPos.xyz - input.vpos);
	float NdotV = saturate(dot(normal, viewDirectionNormal));

	// ライトと視点ベクトルのハーフベクトルを計算
	float3 halfVector = normalize(lightDirectionNormal + viewDirectionNormal);

#if 0
	return float4(NdotL, NdotL, NdotL, 1.0);
#endif
#if 0
	return float4(NdotV, NdotV, NdotV, 1.0);
#endif

	
	// D_GGXの項
	float D = D_GGX(halfVector, normal, 1.f - metal_color.a);

	// Fの項
	float F = Flesnel(viewDirectionNormal, halfVector, param.x, param.y);

	// Gの項
	float G = G_CookTorrance(lightDirectionNormal, viewDirectionNormal,
							 halfVector, normal);

	// スペキュラおよびディフューズを計算
	float specularReflection = (D * F * G) / (4.0 * NdotV * NdotL + 0.000001);
	float3 diffuseReflection = lightColor.xyz * albedo_color.xyz * NdotL;

	return float4(ambientLight + diffuseReflection + specularReflection, 1.0);

}
