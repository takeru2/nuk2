
cbuffer cbPerObject : register( b0 )
{
	int4     clear_param; //x:max size y:stride z:clear_stride
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

RWByteAddressBuffer output_buf : register( u0 );

[numthreads(32, 1, 1)]
void CS( const CSInput input ) {
    int index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;
	if(index >= clear_param.x)return;

	uint vstride = clear_param.y;
	uint vbIndex = vstride * index;
	output_buf.Store3 ( vbIndex + clear_param.z, 0 ); //clear 64bit
}


