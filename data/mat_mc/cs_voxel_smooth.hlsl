
cbuffer cbPerObject : register( b0 )
{
	uint4 vox_info; //gridX gridY gridZ instNum
	uint4 vox_type;
	float4 vox_param[27]; 
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer   input_buf  : register( t0 );
RWByteAddressBuffer output_buf : register( u0 );

float getVoxel(uint3 xyz){
    uint x = xyz.x;
    uint y = xyz.y;
    uint z = xyz.z;
	uint idx = x + y * vox_info.x + z * (vox_info.x * vox_info.y);
	uint vbIndex = idx * 8;
	uint voxBit  = input_buf.Load( vbIndex + 4);
	if(voxBit & 1){
		return 1;
	}else{
		return 0;
	}
}

float getVoxelClamp(uint3 xyz){
    uint x = clamp(xyz.x, 0, vox_info.x - 1);
    uint y = clamp(xyz.y, 0, vox_info.y - 1);
    uint z = clamp(xyz.z, 0, vox_info.z - 1);
	uint idx = x + y * vox_info.x + z * (vox_info.x * vox_info.y);
	uint vbIndex = idx * 8;
	uint voxBit  = input_buf.Load( vbIndex + 4);
	if(voxBit & 1){
		return 1;
	}else{
		return 0;
	}
}

[numthreads(32, 1, 1)]
void CS( const CSInput input ) {
    uint index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	if(index >= vox_info.w)return;

	uint writeOffset = index * 4;
	uint3 _xyz;
	_xyz.z = index / (vox_info.x * vox_info.y);
	index -= _xyz.z * (vox_info.x * vox_info.y);
	_xyz.y = index / vox_info.x;
	_xyz.x = index - _xyz.y  * vox_info.x;

    float rate = 0;
	if(vox_type.x <= 0){
		rate = getVoxel(_xyz);
	}else{
		for(uint i=0;i<27;++i){
			rate += getVoxelClamp(_xyz + (uint3)vox_param[i].xyz) * vox_param[i].w;
		}
	}

    output_buf.Store(writeOffset, asuint(rate));
}


