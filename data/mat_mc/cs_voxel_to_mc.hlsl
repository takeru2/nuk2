
cbuffer cbPerObject : register( b0 )
{
	uint4 vox_info; //gridX gridY gridZ instNum
	float4 vox_param;
	float4 vox_scl;
	float4 vox_pos;
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

ByteAddressBuffer   input_buf  : register( t0 );
ByteAddressBuffer   tri_connection_buf  : register( t1 );
ByteAddressBuffer   cube_edge_flag_buf  : register( t2 );

RWByteAddressBuffer output_buf : register( u0 );
RWByteAddressBuffer count_buf  : register( u1 );

static const int2 edgeConnection[12] = {
	int2(0,1), int2(1,2), int2(2,3), int2(3,0),
	int2(4,5), int2(5,6), int2(6,7), int2(7,4),
	int2(0,4), int2(1,5), int2(2,6), int2(3,7)
};
static const float3 edgeDirection[12] = {
	float3(1.0, 0.0, 0.0),  float3(0.0, 1.0, 0.0),
	float3(-1.0, 0.0, 0.0), float3(0.0, -1.0, 0.0),
	float3(1.0, 0.0, 0.0),  float3(0.0, 1.0, 0.0),
	float3(-1.0, 0.0, 0.0), float3(0.0, -1.0, 0.0),
	float3(0.0, 0.0, 1.0),  float3(0.0, 0.0, 1.0),
	float3(0.0, 0.0, 1.0),  float3(0.0,  0.0, 1.0)
};
static const float3 vertexOffset[8] = {
	float3(0, 0, 0),float3(1, 0, 0),float3(1, 1, 0),float3(0, 1, 0),
	float3(0, 0, 1),float3(1, 0, 1),float3(1, 1, 1),float3(0, 1, 1)
};

//-----------------------------
// Out
// float3 pos
// float2 uv
// uint voxBit
//-----------------------------

float getVoxel(uint x, uint y, uint z){
	uint idx = x + y * vox_info.x + z * (vox_info.x * vox_info.y);
	uint offset = idx * 4;
	float value = asfloat(input_buf.Load( offset ));
	return value;
}

void getCubeVoxel(uint3 xyz, out float cube[8]){
	cube[0] = getVoxel(  xyz.x,   xyz.y,   xyz.z);
	cube[1] = getVoxel(xyz.x+1,   xyz.y,   xyz.z);
	cube[2] = getVoxel(xyz.x+1, xyz.y+1,   xyz.z);
	cube[3] = getVoxel(  xyz.x, xyz.y+1,   xyz.z);
	cube[4] = getVoxel(  xyz.x,   xyz.y, xyz.z+1);
	cube[5] = getVoxel(xyz.x+1,   xyz.y, xyz.z+1);
	cube[6] = getVoxel(xyz.x+1, xyz.y+1, xyz.z+1);
	cube[7] = getVoxel(  xyz.x, xyz.y+1, xyz.z+1);
}

float3 gridToPos(float3 pos){
	float3 cp = float3(pos.x - (float)(vox_info.x / 2),
					   pos.y - (float)(vox_info.y / 2),
					   pos.z - (float)(vox_info.z / 2));
	return cp * vox_scl.xyz + vox_pos.xyz;
}

float getOffset(float v1, float v2){
	float delta = v2 - v1;
	return (delta == 0.0) ? 0.5 : (vox_param.x - v1) / delta;
}

[numthreads(32, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	if(index >= vox_info.w)return;

	unsigned int strideDst = 12 + 12 + 4;
	
	uint3 _xyz;
	_xyz.z = index / (vox_info.x * vox_info.y);
	index -= _xyz.z * (vox_info.x * vox_info.y);
	_xyz.y = index / vox_info.x;
	_xyz.x = index - _xyz.y  * vox_info.x;

	if( (_xyz.x >= (vox_info.x - 1)) ||
		(_xyz.y >= (vox_info.y - 1)) ||
		(_xyz.z >= (vox_info.z - 1)) ){
		return;
	}

	float3 cp = gridToPos(float3((float)_xyz.x, (float)_xyz.y, (float)_xyz.z));
	
	float cube[8];
	getCubeVoxel(_xyz, cube);
	
	int flagIndex = 0;
	float3 edgeVertex[12];

	for(uint i = 0; i < 8; i++){
		if(cube[i] <= vox_param.x){
			flagIndex |= (1 << i);
		}
	}
	int edgeFlags = cube_edge_flag_buf.Load(flagIndex * 4);
	if(edgeFlags == 0)return;

	for(uint i = 0; i < 12; i++){
		if((edgeFlags & (1 << i)) != 0){
			float offset = getOffset(cube[edgeConnection[i].x],
									 cube[edgeConnection[i].y]);
			edgeVertex[i] = cp +
				vertexOffset[edgeConnection[i].x] *
				vox_scl.xyz * vox_param.y +
				edgeDirection[i] * offset * vox_scl.xyz * vox_param.y;
		}
	}
	uint col = 0xffffffff;
	unsigned int vbIndex = 0;
	
	for(uint i = 0; i < 5; i++) {
		int idx0 = tri_connection_buf.Load( (flagIndex * 16 + 3 * i) * 4 );
		if(idx0 < 0){
			return;
		}else{
			uint write_index;
			count_buf.InterlockedAdd(0, 3, write_index); //3 pos
			if(write_index >= vox_info.w)return;

			float3 normal0, normal1, normal2;
			
			float3 pos0 = edgeVertex[idx0];
			int idx1 = tri_connection_buf.Load( (flagIndex * 16 + 3 * i + 1) * 4 );
			float3 pos1 = edgeVertex[idx1];
			int idx2 = tri_connection_buf.Load( (flagIndex * 16 + 3 * i + 2) * 4 );
			float3 pos2 = edgeVertex[idx2];

			// �ʂ̖@��
			normal0 = normal1 = normal2 =
				normalize(cross(pos1 - pos0, pos2 - pos0));

			//Write
			vbIndex = strideDst * write_index;
			output_buf.Store3( vbIndex, asuint( pos0 ) );
			output_buf.Store3( vbIndex + 12, asuint( normal0 ) );
			output_buf.Store( vbIndex + 24, col);

			vbIndex = strideDst * (write_index + 1);
			output_buf.Store3( vbIndex, asuint( pos1 ) );
			output_buf.Store3( vbIndex + 12, asuint( normal1 ) );
			output_buf.Store( vbIndex + 24, col);

			vbIndex = strideDst * (write_index + 2);
			output_buf.Store3( vbIndex, asuint( pos2 ) );
			output_buf.Store3( vbIndex + 12, asuint( normal2 ) );
			output_buf.Store( vbIndex + 24, col);
		}
	}
}
