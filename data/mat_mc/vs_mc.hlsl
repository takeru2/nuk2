
cbuffer cbPerObject : register( b0 )
{
	matrix		matWorldViewProj;// : packoffset( c0 );
	float4      lv;
};

struct VS_OUTPUT {
	float4	pos	   : SV_POSITION;
	float4	color  : COLOR0;
};

VS_OUTPUT VS(
	in float3 pos	 : POSITION,
	in float3 normal : NORMAL,
	in float4 color  : COLOR
) {

	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.pos    = mul(matWorldViewProj, float4(pos, 1));
	output.color  = float4(1,1,1,1);
	output.color.rgb *= clamp(abs(dot(normal, lv.xyz)), 0.2, 1.0);

	return output;
}
