
cbuffer cbPerObject : register( b0 )
{
	uint4 vox_info; //gridX gridY gridZ instNum
	//x = gridX * 12
	//y = gridY * 12
	//z = gridZ * 12
};

struct CSInput
{
    uint3   groupThread  : SV_GroupThreadID;
    uint3   group        : SV_GroupID;
    uint    groupIndex   : SV_GroupIndex;
    uint3   dispatch     : SV_DispatchThreadID;
};

RWByteAddressBuffer output_buf : register( u0 );
RWByteAddressBuffer normal_tmp_buf : register( u1 );

[numthreads(32, 1, 1)]

void CS( const CSInput input ) {
    uint index = input.dispatch.z * 32 * 1 + input.dispatch.y * 1 + input.dispatch.x;

	if(index >= vox_info.w)return;

	uint vbIndex = index * (12 + 12 + 4) + 12 ;
	int3 inormal_idx = output_buf.Load3( vbIndex );
	uint bufferOffset = inormal_idx.z * (vox_info.x * vox_info.y) + inormal_idx.y * vox_info.x + inormal_idx.x;
	int3 inormal = normal_tmp_buf.Load3( bufferOffset * 12 );
	float3 normal = normalize(float3((float)inormal.x / 4096,
									 (float)inormal.y / 4096,
									 (float)inormal.z / 4096));

	output_buf.Store3( vbIndex, asuint( normal ));
}
