﻿
#include "gf.h"
#include "gf_input.h"
#include "gf_simplemodel.h"
#include "gf_rendertexture.h"

#include "game_common.h"

//-----------------------------------------------------------------------------
// Name : ModelView
// Desc :
//-----------------------------------------------------------------------------
class ModelView : public GF::ITask {
private:
	CameraControl mCam;
	DrawGrid mGrid;
	
	GF::IMaterial *pMaterial;
	GF::ISimpleModel *pModel;
	
	EDIT_PARAMS mEP;
	float scale = 0.001f;
	bool dispGrid = true;

	GF::PColor ColorAmbi   = GF::PColor(0x10, 0x10, 0x10, 0xff);
	GF::PColor ColorLight  = GF::PColor(0xe0, 0xe0, 0xe0, 0xff);
	float fresnel = 0.2f;
	float fresnelPow = 5.f;
	float lightDirX = 0;
	float animSpeed = 1.f;

	GF::PColor ColorBack = GF::PColor(0x10, 0x10, 0x10, 0xff);

	struct cbParamPS {
		Vector4     ambiColor;
		Vector4     lightDir;
		Vector4     lightColor;
		Vector4     cameraPos;
		Vector4     param; //param.x fresnel
	};
	cbParamPS mShaderParamPS;
public:
	ModelView(){
		//pMaterial = GF::IMaterial::GetMaterial("mat_model_view");
		pMaterial = GF::IMaterial::GetMaterial("mat_model_view_sk");

		GF::File modelFile;
		//modelFile.Load("model/ch_aodaruma_3D_sub.mdl");
		modelFile.Load("model/ch_a_aodaruma_walk.anim_def.mdl");

		GF::File animFile;
		animFile.Load("model/ch_a_aodaruma_walk.anim_def.anim");
		std::vector<const void*> anims;
		anims.push_back(animFile.GetData());

		pModel = GF::ISimpleModel::Create((const void*)modelFile.GetData(), anims);
		pModel->SetMaterial( pMaterial );
		pModel->SetMaterialParam(NULL, &mShaderParamPS);

		{
			EDIT_REGIST(mEP, "ModelView");
			EDIT_VALUE(mEP, scale);
			EDIT_VALUE(mEP, dispGrid);
			EDIT_VALUE(mEP, ColorAmbi);
			EDIT_VALUE(mEP, ColorLight);
			EDIT_VALUE(mEP, fresnel);
			EDIT_VALUE(mEP, fresnelPow);
			EDIT_VALUE(mEP, lightDirX);
			EDIT_VALUE(mEP, animSpeed);
			EDIT_VALUE(mEP, ColorBack);
		}
	}

	virtual ~ModelView(){
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
		mCam.Update();
	}

	virtual void PreDraw(void){
	}

	virtual void Draw(void){
		float clearColor[4];
		ColorBack.store(clearColor);
		GF::IDevice::GetInstance()->Clear(clearColor);

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//Grid Draw
		if(dispGrid)mGrid.Draw(m);

		{
			cbParamPS &cb = mShaderParamPS;
			memset(&cb, 0, sizeof(cb));
			cb.cameraPos = Vector4(Vector3(mCam.GetPos()), 1);
			ColorAmbi.store((float*)&cb.ambiColor);
			ColorLight.store((float*)&cb.lightColor);
			cb.param.setX( fresnel );
			cb.param.setY(fresnelPow);
			//			cb.lightDir = Vector4(mCam.GetViewDir(), 1);
			cb.lightDir = Vector4(Matrix3::rotationX((float)DEG2RAD(lightDirX)) * mCam.GetViewDir(), 1);
		}
		
		//Model Update
		pModel->SetAnimSpeed(animSpeed);
		pModel->UpdateAnim();
		pModel->Update(Matrix4::scale(Vector3(scale)));

		//Model Draw
		pModel->Draw(m);

	}
};

SELECT_TASK(ModelView);
