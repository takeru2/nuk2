﻿
#include "gf.h"
#include "gf_input.h"
#include "gf_simplemodel.h"
#include "gf_rendertexture.h"

#include "game_common.h"

#include "test_mc.h"

#define GRID_SHOW    (0)
#define TEST_MC      (1)

//-----------------------------------------------------------------------------
// Name : Test_CsVertexBox
// Desc :
//-----------------------------------------------------------------------------
class Test_CsVertexBox : public GF::ITask {
private:
	CameraControl mCam;
	DrawGrid mGrid;
	
	DebugLine mDebugLine;
	DebugTexture2D mDebugTex2D;

	GF::IMaterial *pMaterial;
	GF::ISimpleModel *pModel;

	struct Block {
		float x,y,z;
		unsigned int color;
	};
	struct Model_Skin {
		float x,y,z;
		float nx,ny,nz;
		float weight[4];
		unsigned int index4;
		float u,v;
	};
	struct Vtx {
		float x,y,z;
		float nx,ny,nz;
		float u,v;
	};
	struct Voxel {
		unsigned int color;
		unsigned int voxBit;
	};

	BoxMesh mBM;
	GF::IMaterial *pMaterialBox = NULL;

	GF::IPipeline *pipeline_SkinToVtx = NULL;
	GF::IPipeline *pipeline_VtxToVox  = NULL;
	GF::IPipeline *pipeline_VoxSlice  = NULL;
	GF::IPipeline *pipeline_VoxSliceFill = NULL;
	GF::IPipeline *pipeline_VoxToInst = NULL;
	GF::IPipeline *pipeline_InstClear = NULL;

	GF::IBuffer *pVertexBufferSkin  = NULL;
	GF::IBuffer *pVertexBufferVtx   = NULL;
	GF::IBuffer *pVertexBufferVoxel = NULL;
	GF::IBuffer *pVertexBufferInst  = NULL;
	GF::IBuffer *pVertexBufferCount = NULL;

	EDIT_PARAMS mEP;
	float sliceY = 114;
	float sliceYd = 0.5f;

	bool sliceAnim = true;
	bool dispBB = false;
	bool dispModel = false;
	bool dispMC = false;
	GF::PColor fillColorA  = GF::PColor(0xff, 0xff, 0xff, 0xff);
	GF::PColor fillColorB = GF::PColor(0xff, 0x80, 0xff, 0xff);
	GF::PColor fillColorC = GF::PColor(0x80, 0xff, 0xff, 0xff);
	GF::PColor sliceColor = GF::PColor(0x80, 0xff, 0x80, 0xff);

	float offsetX = -100;
	float offsetY = 0;
	float offsetZ = 0;
	bool anim = true;
	float animSpeed = 0.5f;

	float uv_scl = 4;
	float box_scl = 1.1f;

	unsigned int instDispMax = 100000;
	unsigned int instDispNum = 0;

	int vox_dim_x = 150;
	int vox_dim_y = 150;
	int vox_dim_z = 150;

	int vox_dim = 200;
	float vox_dim_size = 0;
	int vox_dim_prev = 0;
	int instNum;

	GF::ITexture *pTex_Trip = NULL;

	Point3 model_bb[2];
	Point3 model_cp;
	Vector3 model_size;

	bool sliceZTest = true;
	bool dispInst = true;
	bool calcInst = true;

#if (TEST_MC == 1)
	ITestMC *mc = NULL;
#endif

public:
	Test_CsVertexBox(){

		{
			EDIT_REGIST(mEP, "Grid");
			EDIT_VALUE(mEP, sliceY);
			EDIT_VALUE(mEP, sliceYd);
			EDIT_VALUE(mEP, sliceAnim);
			EDIT_VALUE(mEP, instDispMax);
			EDIT_VALUE(mEP, fillColorA);
			EDIT_VALUE(mEP, fillColorB);
			EDIT_VALUE(mEP, fillColorC);
			EDIT_VALUE(mEP, sliceColor);
			EDIT_VALUE(mEP, offsetX);
			EDIT_VALUE(mEP, offsetY);
			EDIT_VALUE(mEP, offsetZ);
			EDIT_VALUE(mEP, anim);
			EDIT_VALUE(mEP, animSpeed);
			EDIT_VALUE(mEP, dispBB);
			EDIT_VALUE(mEP, dispModel);
			EDIT_VALUE(mEP, dispMC);
			EDIT_VALUE(mEP, vox_dim);
			EDIT_VALUE(mEP, uv_scl);
			EDIT_VALUE(mEP, box_scl);
			EDIT_VALUE(mEP, sliceZTest);
			EDIT_VALUE(mEP, dispInst);
			EDIT_VALUE(mEP, calcInst);
		}

		mBM.Create(0.4f);

		pMaterial = GF::IMaterial::GetMaterial("mat_testmodel2");
		
		GF::File modelFile;
		modelFile.Load("model/test2.mdl");

		std::vector<const void*> anims;
		GF::File animFile;
		animFile.Load("model/test2.anim");
		anims.push_back(animFile.GetData());

		pModel = GF::ISimpleModel::Create((const void*)modelFile.GetData(), anims);
		pModel->SetMaterial( pMaterial );
		pMaterialBox = GF::IMaterial::GetMaterial("mat_mb");

		//モデルBBを求めておく
		pModel->Update(Matrix4::identity());
		pModel->GetBoundingBox(model_bb, true);

		//CS
		{ //Skin -> Triangle 
			const char *fname = "mat_testmodel2/cs_skin_vertex.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = 2048;
			pipeline_SkinToVtx = GF::IPipeline::GetPipeline(info);
		}

		{ //Triangle -> Voxel
			const char *fname = "mat_testmodel2/cs_vertex_to_voxel.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = sizeof(Vector4) * 5;
			pipeline_VtxToVox = GF::IPipeline::GetPipeline(info);
		}

		{ //Voxel -> Slice Voxel
			const char *fname = "mat_testmodel2/cs_voxel_slice.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = sizeof(Vector4) * 5;
			pipeline_VoxSlice = GF::IPipeline::GetPipeline(info);
		}
		{ // Slice Fill
			const char *fname = "mat_testmodel2/cs_voxel_slice_fill.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = sizeof(Vector4) * 5;
			pipeline_VoxSliceFill = GF::IPipeline::GetPipeline(info);
		}

		{ //Voxel -> Inst
			const char *fname = "mat_testmodel2/cs_voxel_to_inst.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = sizeof(Vector4) * 5;
			pipeline_VoxToInst = GF::IPipeline::GetPipeline(info);
		}

		{ //Clear Voxel
			const char *fname = "mat_testmodel2/cs_clear_voxel.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = sizeof(Vector4) * 5;
			pipeline_InstClear = GF::IPipeline::GetPipeline(info);
		}


		//out 10000 point
		unsigned int vs = pModel->GetVertexBuffer()->GetSize();
		pVertexBufferSkin = GF::IBuffer::CreateBuffer(vs);
		pVertexBufferSkin->CopyData(pModel->GetVertexBufferRaw(), vs);
		pVertexBufferVtx = GF::IBuffer::CreateBuffer(
			sizeof(Vtx) * (vs / sizeof(Model_Skin)));
		pVertexBufferVtx->CreateUAV();


		//Voxel Counter
		pVertexBufferCount = GF::IBuffer::CreateBuffer(16);
		pVertexBufferCount->CreateUAV();

		{
			GF::File texFile;
			texFile.Load("mat_testmodel2/trip128.DDS");
			pTex_Trip = GF::ITexture::CreateFromMemory(texFile.GetData(), texFile.GetSize());
		}

#if (TEST_MC == 1)
		mc = ITestMC::CreateInstance();
#endif

	}

	virtual ~Test_CsVertexBox(){
	}

	virtual void Release(void){
		SAFE_RELEASE(mc);
		delete this;
	}

	void resizeVox(void) {
		if (vox_dim_prev != vox_dim) {
			vox_dim_prev = vox_dim;

			SAFE_RELEASE(pVertexBufferVoxel);
			SAFE_RELEASE(pVertexBufferInst);

			//再構築
			{
				model_size = Vector3(model_bb[1].getX() - model_bb[0].getX(),
					model_bb[1].getY() - model_bb[0].getY(),
					model_bb[1].getZ() - model_bb[0].getZ());
				model_cp = Point3((model_bb[1].getX() + model_bb[0].getX()) * 0.5f,
					(model_bb[1].getY() + model_bb[0].getY()) * 0.5f,
					(model_bb[1].getZ() + model_bb[0].getZ()) * 0.5f);
				float size_max = Max(Max(model_size.getX(), model_size.getY()), model_size.getZ());
				vox_dim_size = size_max / (float)vox_dim;
				vox_dim_x = (int)(model_size.getX() / vox_dim_size);
				vox_dim_y = (int)(model_size.getY() / vox_dim_size);
				vox_dim_z = (int)(model_size.getZ() / vox_dim_size);
				instNum = vox_dim_x * vox_dim_y * vox_dim_z;
			}

			pVertexBufferInst = GF::IBuffer::CreateVertexBufferUAV(sizeof(Block), instNum);
			pVertexBufferVoxel = GF::IBuffer::CreateBuffer(sizeof(Voxel) * instNum);
			pVertexBufferVoxel->CreateUAV();
		}

	}

	virtual void Update(void){

		pModel->SetAnimSpeed( anim?animSpeed:0 );
		pModel->UpdateAnim();

		if (sliceAnim) {

			//スライス位置更新
			sliceY += sliceYd;
			if (sliceY > (float)vox_dim_y) {
				if (sliceYd > 0)sliceYd *= -1;
			}
			else if (sliceY < 0) {
				if (sliceYd < 0)sliceYd *= -1;
			}
		}
	}

	virtual void PreDraw(void){

		mCam.Update();

		Point3 bb[2];
		pModel->Update(Matrix4::identity());
		pModel->GetBoundingBox(bb, true);

		resizeVox();

		if (calcInst)
		{
			//Clear VOX
			{
				void *views[] = { NULL };
				void *uavs[] = { pVertexBufferVoxel->GetUAV() };
				struct cs_buffer {
					int clearParam[4];
				};
				cs_buffer cb;
				cb.clearParam[0] = instNum;
				cb.clearParam[1] = (int)sizeof(Voxel);
				cb.clearParam[2] = (int)sizeof(Voxel) - 8;
				pipeline_InstClear->SetConstantBuffer(&cb, sizeof(cb));
				pipeline_InstClear->Dispatch(views, 1, uavs, 1,
											 cb.clearParam[0] / 32, 1, 1);
			}
			//Clear Inst
			{
				void *views[] = { NULL };
				void *uavs[] = { pVertexBufferInst->GetUAV() };
				struct cs_buffer {
					int clearParam[4];
				};
				cs_buffer cb;
				cb.clearParam[0] = instDispMax;
				cb.clearParam[1] = (int)sizeof(Block);
				cb.clearParam[2] = (int)sizeof(Block) - 8;
				pipeline_InstClear->SetConstantBuffer(&cb, sizeof(cb));
				pipeline_InstClear->Dispatch(views, 1, uavs, 1,
											 cb.clearParam[0] / 32, 1, 1);
			}

			//Skin -> Vtx
			unsigned int vnum = pModel->GetVertexBuffer()->GetSize() / sizeof(Model_Skin);
			{
				void *views[] = { pVertexBufferSkin->GetSRV() };
				void *uavs[] = { pVertexBufferVtx->GetUAV() };

				unsigned int bone_num = pModel->UpdateBoneMatrix();
				Matrix4 *bone_matrix = pModel->GetBoneMatrix();
				int *num_ptr = (int*)bone_matrix;
				*num_ptr = vnum;
				pipeline_SkinToVtx->SetConstantBuffer(bone_matrix, 0);
				pipeline_SkinToVtx->Dispatch(
					views, 1, uavs, 1, (vnum / 256) + 1, 1, 1);
			}

			//Vtx -> Voxel
			{
				void *views[] = { pVertexBufferVtx->GetSRV(), pTex_Trip->GetSRV() };
				void *uavs[] = { pVertexBufferVoxel->GetUAV() };
				struct cs_buffer{
					Vector4 bb_box[2];
					Vector4 bb_dd;
					int     bb_num[4];
					Vector4 uv_scl;
				};
				cs_buffer cb;

				cb.bb_box[0] = Vector4(Vector3(model_bb[0]), 1);
				cb.bb_box[1] = Vector4(Vector3(model_bb[1]), 1);
				cb.bb_num[0] = vox_dim_x;
				cb.bb_num[1] = vox_dim_y;
				cb.bb_num[2] = vox_dim_z;
				cb.bb_num[3] = vox_dim;
				cb.bb_dd = Vector4(vox_dim_size, vox_dim_size, vox_dim_size, 1);
				cb.uv_scl = Vector4(uv_scl, uv_scl, 0, 0);
				
				pipeline_VtxToVox->SetConstantBuffer(&cb, sizeof(cb));
				pipeline_VtxToVox->Dispatch(views, 2, uavs, 1, vnum / 3, 1, 1);
			}

			//Voxel Slice
			{
				struct cs_buffer{
					int     bb_num[4];
				};
				cs_buffer cb;
				cb.bb_num[0] = vox_dim_x;
				cb.bb_num[1] = vox_dim_y;
				cb.bb_num[2] = vox_dim_z;
				cb.bb_num[3] = (int)sliceY;

				pipeline_VoxSlice->SetConstantBuffer(&cb, sizeof(cb));
				void *views[] = { NULL };
				void *uavs[] = { pVertexBufferVoxel->GetUAV() };
				//走査線を走らせる ２軸
				pipeline_VoxSlice->Dispatch(views, 1, uavs, 1, vox_dim_y * vox_dim_z / 32, 1, 1);
			}

			//Voxel SliceFill
			if(sliceZTest){
				struct cs_buffer {
					int     bb_num[4];
				};
				cs_buffer cb;
				cb.bb_num[0] = vox_dim_x;
				cb.bb_num[1] = vox_dim_y;
				cb.bb_num[2] = vox_dim_z;
				cb.bb_num[3] = (int)sliceY;

				pipeline_VoxSliceFill->SetConstantBuffer(&cb, sizeof(cb));
				void *views[] = { NULL };
				void *uavs[] = { pVertexBufferVoxel->GetUAV() };
				//走査線を走らせる X軸
				pipeline_VoxSliceFill->Dispatch(views, 1, uavs, 1, vox_dim_x / 32 + 1, 1, 1);
			}

			//Voxel -> Inst
			{
				//カウンター個数取得＆クリア
				int clearBuf[4] = {0};
				pVertexBufferCount->ReadData(&instDispNum, 0, 4);
				pVertexBufferCount->CopyData(clearBuf, sizeof(clearBuf));
				
				void *views[] = { pVertexBufferVoxel->GetSRV()};
				void *uavs[] = { pVertexBufferInst->GetUAV(), pVertexBufferCount->GetUAV()};
				
				struct cs_buffer{
					int     vox_info[4];
					int     colors[4];
					Vector4 scl;
					Vector4 pos;
				};
				cs_buffer cb;
				memset(&cb, 0, sizeof(cb));
				cb.vox_info[0] = vox_dim_x;
				cb.vox_info[1] = vox_dim_y;
				cb.vox_info[2] = vox_dim_z;
				cb.vox_info[3] = instNum;
				cb.colors[0] = fillColorA;
				cb.colors[1] = fillColorB;
				cb.colors[2] = fillColorC;
				cb.colors[3] = sliceColor;
				cb.scl = Vector4(vox_dim_size, vox_dim_size, vox_dim_size, 1);
				cb.pos = Vector4(model_cp.getX(), model_cp.getY(), model_cp.getZ(), 1);

				pipeline_VoxToInst->SetConstantBuffer(&cb, sizeof(cb));
				pipeline_VoxToInst->Dispatch(views, 1, uavs, 2,	instNum / 256, 1, 1);
			}
		}

#if (TEST_MC == 1)
		if (dispMC) {
			mc->Calc(pVertexBufferVoxel->GetSRV(),
				vox_dim_x, vox_dim_y, vox_dim_z, Vector3(model_cp), vox_dim_size);
		}
#endif
	}

	virtual void Draw(void){
		float clearColor[] = {0.f,0.f,0.0f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//Grid Draw
#if (GRID_SHOW == 1)
		mGrid.Draw(m);
#endif
		//Model Update
		Point3 bb[2];

		pModel->Update(Matrix4::identity());
		//Bounding Box
		if (dispBB) {
			mDebugLine.Begin();

			pModel->GetBoundingBox(bb, true);
			mDebugLine.SetBoundingBoxLines(bb[0], bb[1], GF::PColor(0, 0xff, 0, 0xff));

			pModel->GetBoundingBox(bb, false);
			mDebugLine.SetBoundingBoxLines(bb[0], bb[1], GF::PColor(0, 0xff, 0x40, 0xff));

			mDebugLine.End();
			mDebugLine.Draw(m);
		}

		//Model Draw
		if (dispModel) {
			pModel->Draw(m);
		}

		//Inst Cube
		if(dispInst){
			struct scb{
				Matrix4 m;
				Vector4 lv;
			};
			scb _scb;
			_scb.m = m;
			_scb.lv = Vector4(mCam.GetViewDir(), box_scl * vox_dim_size);
			pMaterialBox->Set(&_scb);

			GF::Primitive::DrawIndexedInstanced(
				mBM.pVertexBuffer,
				GF::Primitive::Vertex_P3_N3_C8_UV_Stride,
				6 * 4,
				mBM.pIndexBuffer,
				6 * 6,
				//pVertexBufferInst, 16, Min(instDispMax, instDispNum));
				pVertexBufferInst, 16, instDispMax);
		}

#if (TEST_MC == 1)
		if (dispMC) {
			mc->Draw(m, mCam.GetViewDir());
		}
#endif

	}
};

SELECT_TASK(Test_CsVertexBox);
