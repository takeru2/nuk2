﻿
#include "gf.h"
#include "gf_input.h"

#include "game_common.h"

struct Block {
	float x,y,z;
	unsigned int color;
};

//-----------------------------------------------------------------------------
// Name : Test_InstBox
// Desc :
//-----------------------------------------------------------------------------
class Test_InstBox : public GF::ITask {
private:
	GF::IPipeline *pipeline;
	GF::IPipeline *pipelineCS;
	GF::IPipeline *pipelineCS_T;
	GF::ITexture *pTex;

	unsigned int instNum;

	GF::IBuffer *pInstBuffer;
	GF::IBuffer *pInstBuffer2;
	GF::IBuffer *pVertexBuffer;
	GF::IBuffer *pIndexBuffer;
	void *cs_prog;
	float *vtx;

	int gx,gy,gz;
	Block *m_pBlock;

	CameraControl mCam;
	DrawGrid mGrid;

	BoxMesh mBM;

public:
	Test_InstBox(){

		mBM.Create(0.4f);
		{
			{
				GF::File shaderFile;
				shaderFile.Load("mat_test/vs_tun_i.hlsl");
				GF::IShader::CreateShader(
					GF::IShader::SHADER_VS, 
					shaderFile.GetName(),
					"VS",
					(const char*)shaderFile.GetData(), shaderFile.GetSize());
			}
			{
				GF::File shaderFile;
				shaderFile.Load("mat_test/ps_nt.hlsl");
				GF::IShader::CreateShader(
					GF::IShader::SHADER_PS, 
					shaderFile.GetName(),
					"PS",
					(const char*)shaderFile.GetData(), shaderFile.GetSize());
			}

			{
				GF::IPipeline::Info info;
				info.bs     = GF::IPipeline::BLEND_SA_IA;
				info.ds     = GF::IPipeline::DEPTH_E_WRITE_E;
				info.rs     = GF::IPipeline::FILL_S_CULL_B;
				info.ss[0]     = GF::IPipeline::ANISO_WRAP_WRAP;
				info.ss_num = 1;
				info.layout = GF::IPipeline::LAYOUT_P3_N3_C8_UV | GF::IPipeline::LAYOUT_INST_VEC;
				info.topology = GF::IPipeline::TOPOLOGY_TRIANGLELIST;
				info.vs     = GF::IShader::GetShaderID("mat_test/vs_tun_i.hlsl");
				info.ps     = GF::IShader::GetShaderID("mat_test/ps_nt.hlsl");
				info.constant_size[0] = 64 + 16;

				pipeline = GF::IPipeline::GetPipeline(info);
			}
		}

		//CS
		{
			GF::File shaderFile;
			shaderFile.Load("mat_test/cs_v4.hlsl");
			GF::IShader::CreateShader(
				GF::IShader::SHADER_CS, 
				shaderFile.GetName(),
				"CS",
				(const char*)shaderFile.GetData(), shaderFile.GetSize());

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID("mat_test/cs_v4.hlsl");
			pipelineCS = GF::IPipeline::GetPipeline(info);
		}

		//CS_T
		{
			GF::File shaderFile;
			shaderFile.Load("mat_test/cs_texsample.hlsl");
			GF::IShader::CreateShader(
				GF::IShader::SHADER_CS, 
				shaderFile.GetName(),
				"CS",
				(const char*)shaderFile.GetData(), shaderFile.GetSize());

			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID("mat_test/cs_texsample.hlsl");
			info.ss[0]     = GF::IPipeline::ANISO_CLAMP_CLAMP;
			info.ss_num = 1;
			pipelineCS_T = GF::IPipeline::GetPipeline(info);
		}

		{
			GF::File texFile;
			texFile.Load("mat_test/sample2.DDS");
			pTex = GF::ITexture::CreateFromMemory(texFile.GetData(), texFile.GetSize());
		}


		//100 x 100 x 100
		gx = gy = gz = 100;
		instNum = gx * gy * gz;
		pInstBuffer  = GF::IBuffer::CreateStructuredBuffer(sizeof(Block), instNum);

		m_pBlock = new Block[instNum];
		for(int z=0;z<gz;z++){
			float fz = (float)z - 0.5f*(float)gz;
			for(int y=0;y<gy;y++){
				float fy = (float)y - 0.5f*(float)gy;
				for(int x=0;x<gx;x++){
					int idx = x + y * gx + z * (gx * gy);

					float fx = (float)x - 0.5f*(float)gx;
					m_pBlock[idx].x = fx;
					m_pBlock[idx].y = fy;
					m_pBlock[idx].z = fz;

					GF::PColor col;
#if 1
					col = 
						(0x0000ffff * x / gx) | 
						((0x0000ffff * z / gz) << 16) ;
#else
					col.setHSVA(
						GF::Rand(),
						0.8f,
						0.8f, 
						1.f);

#endif
					m_pBlock[idx].color = col;
				}
			}
		}
		pVertexBuffer = GF::IBuffer::CreateVertexBufferUAV(16, instNum);

	}

	virtual ~Test_InstBox(){
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
		mCam.Update();
	}

	virtual void PreDraw(void){

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//更新かけるなら
		pInstBuffer->CopyData(m_pBlock, sizeof(Block) * instNum);

#if 1
		void *views[] = {pInstBuffer->GetSRV(), pTex->GetSRV()};
		void *uavs[]  = {pVertexBuffer->GetUAV()};
		pipelineCS_T->Dispatch(views, 2, uavs, 1, instNum / 100, 1, 1);
#else
		void *views[] = {pInstBuffer->GetSRV()};
		void *uavs[]  = {pVertexBuffer->GetUAV()};
		pipelineCS->Dispatch(views, 1, uavs, 1, instNum / 100, 1, 1);
#endif

	}

	virtual void Draw(void){
		float clearColor[] = {0,0,0.1f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//mGrid.Draw(m);

		struct scb{
			Matrix4 m;
			Vector4 lv;
		};
		scb _scb;
		_scb.m = m;
		_scb.lv = Vector4(mCam.GetViewDir());
		
		GF::Primitive::SetPipeline(pipeline, &_scb, sizeof(_scb), NULL);
#if 1
		GF::Primitive::DrawIndexedInstanced(mBM.pVertexBuffer,
											GF::Primitive::Vertex_P3_N3_C8_UV_Stride,
											6 * 4,
											mBM.pIndexBuffer, 
											6 * 6,
											pVertexBuffer, 16, instNum);

#else
		GF::Primitive::DrawIndexed(mBM.pVertexBuffer,
								   GF::Primitive::Vertex_P3_N3_C8_UV_Stride,
								   6 * 4,
								   mBM.pIndexBuffer, 
								   6 * 6);
#endif

	}

};

SELECT_TASK(Test_InstBox);
