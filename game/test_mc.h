﻿
#pragma once

#include "gf.h"

//-----------------------------------------------------------------------------
// Name : ITestMC
// Desc :
//-----------------------------------------------------------------------------
class ITestMC {
public:
	virtual ~ITestMC(){}
	virtual void Release(void) = 0;

	virtual void Calc(GF::ISRV *voxel, int gx, int gy, int gz, const Vector3 &cp, float gs) = 0;
	virtual void Draw(const Matrix4 &transform, const Vector3 &dir) = 0;

	static ITestMC *CreateInstance(void);
};
