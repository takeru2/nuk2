﻿
#include "gf.h"
#include "gf_input.h"
#include "gf_simplemodel.h"
#include "gf_rendertexture.h"

#include "game_common.h"

//-----------------------------------------------------------------------------
// Name : Test_Model
// Desc :
//-----------------------------------------------------------------------------
class Test_Model : public GF::ITask {
private:
	CameraControl mCam;
	DrawGrid mGrid;
	
	GF::IMaterial *pMaterial;
	GF::ISimpleModel *pModel;
	
public:
	Test_Model(){
		//pMaterial = GF::IMaterial::GetMaterial("mat_warumask");
		pMaterial = GF::IMaterial::GetMaterial("mat_testmodel");

		GF::File modelFile;
		//modelFile.Load("model/ch_a_warumask_01_walk.anim.mdl");
		//modelFile.Load("model/ch_a_freddy_13_fight.mdl");
		modelFile.Load("model/test.mdl");

		GF::File animFile;
		//animFile.Load("model/ch_a_warumask_01_walk.anim.anim");
		//animFile.Load("model/ch_a_freddy_13_fight.anim");
		animFile.Load("model/test.anim");

		std::vector<const void*> anims;
		anims.push_back(animFile.GetData());

		pModel = GF::ISimpleModel::Create((const void*)modelFile.GetData(), anims);
		pModel->SetMaterial( pMaterial );

	}

	virtual ~Test_Model(){
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
		mCam.Update();
	}

	virtual void PreDraw(void){
	}

	virtual void Draw(void){
		float clearColor[] = {0,0,0.1f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//Grid Draw
		mGrid.Draw(m);

		//Model Update
		pModel->UpdateAnim();
		pModel->Update(Matrix4::identity());

		//Model Draw
		pModel->Draw(m);

	}
};

SELECT_TASK(Test_Model);
