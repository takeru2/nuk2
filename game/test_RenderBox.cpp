﻿
#include "gf.h"
#include "gf_input.h"
#include "gf_simplemodel.h"
#include "gf_rendertexture.h"

#include "game_common.h"

#define MODEL_TYPE   (2)
#define DUMMY_SCALE  (0)
#define MODEL_SHOW   (0)
#define GRID_SHOW    (0)

//-----------------------------------------------------------------------------
// Name : Test_RenderBox
// Desc :
//-----------------------------------------------------------------------------
class Test_RenderBox : public GF::ITask {
private:
	CameraControl mCam;
	DrawGrid mGrid;
	
	DebugLine mDebugLine;
	DebugTexture2D mDebugTex2D;

	GF::IMaterial *pMaterial;
	GF::ISimpleModel *pModel;

	std::vector<GF::IRenderTexture*> vRenderTarget;


	GF::IBuffer *pInstBuffer;
	GF::IPipeline *pipelineCS;
	GF::IBuffer *pVertexBuffer;

	struct Block {
		float x,y,z;
		unsigned int color;
	};
	Block *m_pBlock;
	int instNum;

	BoxMesh mBM;

	GF::IMaterial *pMaterialBox;
	
public:
	Test_RenderBox(){
		mBM.Create(0.4f);

#if (MODEL_TYPE == 1)
		pMaterial = GF::IMaterial::GetMaterial("mat_warumask");
#elif (MODEL_TYPE == 2)
		pMaterial = GF::IMaterial::GetMaterial("mat_testmodel");
#else
		pMaterial = GF::IMaterial::GetMaterial("mat_player");
#endif
		
		GF::File modelFile;

#if (MODEL_TYPE == 1)
		modelFile.Load("model/ch_a_warumask_01_walk.anim.mdl");
#elif (MODEL_TYPE == 2)
		modelFile.Load("model/test.mdl");
#else
		modelFile.Load("model/ch_player.mdl");
#endif
		std::vector<const void*> anims;
#if (MODEL_TYPE == 1)
		GF::File animFile;
		animFile.Load("model/ch_a_warumask_01_walk.anim.anim");
		anims.push_back(animFile.GetData());
#endif
		pModel = GF::ISimpleModel::Create((const void*)modelFile.GetData(), anims);
		pModel->SetMaterial( pMaterial );

		for(int i=0;i<2;i++){
			GF::IRenderTexture *pRenderTarget = GF::IRenderTexture::Create(0, 100, 100);
			vRenderTarget.push_back( pRenderTarget );
		}

		//INST VOXEL
		{
			int gx = 100;
			int gy = 100;
			int gz = 100;
			instNum = gx * gy * gz;
			pInstBuffer  = GF::IBuffer::CreateStructuredBuffer(sizeof(Block), instNum);

			m_pBlock = new Block[instNum];
			for(int z=0;z<gz;z++){
				float fz = (float)z - 0.5f*(float)gz;
				for(int y=0;y<gy;y++){
					float fy = (float)y - 0.5f*(float)gy;
					for(int x=0;x<gx;x++){
						int idx = x + y * gx + z * (gx * gy);

						float fx = (float)x - 0.5f*(float)gx;
						m_pBlock[idx].x = fx;
						m_pBlock[idx].y = fy;
						m_pBlock[idx].z = fz;
						GF::PColor col;
						col = 
							(0x00ff * x / gx) | 
							((0x00ff * y / gy) << 8) | 
							((0x00ff * z / gz) << 16) ;
						m_pBlock[idx].color = col;
					}
				}
			}
			pVertexBuffer = GF::IBuffer::CreateVertexBufferUAV(16, instNum);

			//CS
			{
				GF::File shaderFile;
				shaderFile.Load("mat_mb/cs_mb_inst.hlsl");
				GF::IShader::CreateShader(
					GF::IShader::SHADER_CS, 
					shaderFile.GetName(),
					"CS",
					(const char*)shaderFile.GetData(), shaderFile.GetSize());

				GF::IPipeline::Info info;
				info.cs = GF::IShader::GetShaderID("mat_mb/cs_mb_inst.hlsl");
				info.ss_num = 4;
				for(int i=0;i<4;i++){
					info.ss[i] = GF::IPipeline::ANISO_CLAMP_CLAMP;
				}
				pipelineCS = GF::IPipeline::GetPipeline(info);
			}

			pInstBuffer->CopyData(m_pBlock, sizeof(Block) * instNum);
		}

		pMaterialBox = GF::IMaterial::GetMaterial("mat_mb");
	}

	virtual ~Test_RenderBox(){
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
		mCam.Update();
	}

	virtual void PreDraw(void){

		Point3 bb[2];
		pModel->Update(Matrix4::identity());
		pModel->GetBoundingBox(bb);

		{
			float _xyz = Max(
				Max(
					Abs(bb[1].getX() - bb[0].getZ()),
					Abs(bb[1].getY() - bb[0].getY())),
				Abs(bb[1].getZ() - bb[0].getZ()));

#if (DUMMY_SCALE == 1)
			//dymmy scale
			float _dymmy_scl = 6.f;
			_xyz *= _dymmy_scl;
#endif
			float _s = _xyz * 0.5f;
			Matrix4 om;
			Matrix4 vm;
			Vector3 cp;
			
			om = Matrix4::orthographic(-_s, _s, -_s, _s, -_xyz * 2, 0);
			cp = Vector3((bb[0].getX() + bb[1].getX()) * 0.5f,
						 (bb[0].getY() + bb[1].getY()) * 0.5f, 0);

			Matrix4 rY = Matrix4::rotationY((float)DEG2RAD(90.0f));

			//+Z
			{
				vRenderTarget[0]->Begin();
				vRenderTarget[0]->Clear(NULL, 1);
				cp.setZ(-_xyz * 0.5f);
				vm = Matrix4::lookAt(
					Point3(cp + Vector3(0, 0, 0)),
					Point3(cp + Vector3(0, 0, -1)),
					Vector3(0, 1, 0));
				pModel->Draw(om * vm * rY);
				vRenderTarget[0]->End();
			}

			//-z
			{
				vRenderTarget[1]->Begin();
				vRenderTarget[1]->Clear(NULL, 1);
				cp.setZ(_xyz * 0.5f);
				vm = Matrix4::lookAt(
					Point3(cp + Vector3(0, 0, 0)),
					Point3(cp + Vector3(0, 0, 1)),
					Vector3(0, 1, 0));
				pModel->Draw(om * vm * rY);
				vRenderTarget[1]->End();
			}
		}
#if 1
		void *views[] = {pInstBuffer->GetSRV(),
						 vRenderTarget[0]->GetSRV(),
						 vRenderTarget[0]->GetDepthSRV(),
						 vRenderTarget[1]->GetSRV(),
						 vRenderTarget[1]->GetDepthSRV()};
		void *uavs[]  = {pVertexBuffer->GetUAV()};
		pipelineCS->Dispatch(views, 5, uavs, 1, instNum / 100, 1, 1);
#endif
	}

	void drawRenderTexture(void){
		unsigned short w, h;
		GF::IDevice::GetInstance()->GetScreenSize(&w, &h);

		mDebugTex2D.Begin();
		mDebugTex2D.SetSprite(vRenderTarget[0]->GetSRV(),
							  Point3(100,100,0), 100, 100, 0xffffffff);
		mDebugTex2D.SetSprite(vRenderTarget[0]->GetDepthSRV(),
							  Point3(100,100 + 220,0), 100, 100, 0xffffffff);
		mDebugTex2D.SetSprite(vRenderTarget[1]->GetSRV(),
							  Point3(100 + 220,100,0), 100, 100, 0xffffffff);
		mDebugTex2D.SetSprite(vRenderTarget[1]->GetDepthSRV(),
							  Point3(100 + 220,100 + 220,0), 100, 100, 0xffffffff);
		mDebugTex2D.End();
		mDebugTex2D.Draw(Matrix4::orthographic(0, w, h, 0, -1, 1));
	}
	
	virtual void Draw(void){
		float clearColor[] = {0.1f,0.1f,0.2f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		Matrix4 m = Matrix4::identity();
		mCam.UpdateShaderMatrix(m);

		//Grid Draw
#if (GRID_SHOW == 1)
		mGrid.Draw(m);
#endif
		//Model Update
		Point3 bb[2];

		pModel->UpdateAnim();

		pModel->Update(Matrix4::identity());
		pModel->GetBoundingBox(bb);

		//Model Draw
#if (MODEL_SHOW == 1)
		pModel->Draw(m);
#endif
		//Bounding Box
		mDebugLine.Begin();
		mDebugLine.SetBoundingBoxLines(bb[0], bb[1], GF::PColor(0,0xff,0,0xff));
		mDebugLine.End();
		mDebugLine.Draw(m);

		//Render Texture Draw
		drawRenderTexture();

		//Inst Cube
#if 1
		{
			struct scb{
				Matrix4 m;
				Vector4 lv;
			};
			scb _scb;
			_scb.m = m;
			_scb.lv = Vector4(mCam.GetViewDir());
			pMaterialBox->Set(&_scb);
		
			GF::Primitive::DrawIndexedInstanced(
				mBM.pVertexBuffer,
				GF::Primitive::Vertex_P3_N3_C8_UV_Stride,
				6 * 4,
				mBM.pIndexBuffer, 
				6 * 6,
				pVertexBuffer, 16, instNum);
		}
#endif
	}
};

SELECT_TASK(Test_RenderBox);
