﻿

#include "gf.h"
#include "gf_editvalue.h"

class Test_2DTex : public GF::ITask {
private:
	GF::IBuffer *pVertexBuffer;
	GF::IBuffer *pIndexBuffer;
	GF::IMaterial *pMaterial;
	float mPx = 0;
	float mPy = 0;

	EDIT_PARAMS mEP;
public:
	Test_2DTex(){

		pMaterial = GF::IMaterial::GetMaterial("mat_game");

		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(0x100);
		pIndexBuffer = GF::IBuffer::CreateIndexBuffer(0x100);

		//QUAD
		{
			pIndexBuffer->Map();
			int *pIdx = (int*)pIndexBuffer->GetMapMemory();
			
			pIdx[0] = 0;
			pIdx[1] = 1;
			pIdx[2] = 2;
			pIdx[3] = 2;
			pIdx[4] = 3;
			pIdx[5] = 0;

			pIndexBuffer->Unmap();
		}

		EDIT_REGIST(mEP, "2DTex");
		EDIT_VALUE(mEP, mPx);
		EDIT_VALUE(mEP, mPy);
	}

	virtual ~Test_2DTex(){
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
	}

	virtual void Draw(void){
		float clearColor[] = {0,0,0.1f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		pVertexBuffer->Map();
		float *ptr = (float*)pVertexBuffer->GetMapMemory(
			GF::Primitive::Vertex_P3_C8_UV_Stride * 4);
		{
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx, mPy, 0, 0xffffffff, 0, 0);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx, mPy+100, 0, 0xffffffff, 0, 1);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx+100, mPy+100, 0, 0xffffffff, 1, 1);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx+100, mPy, 0, 0xffffffff, 1, 0);
		}
		pVertexBuffer->Unmap();

		Matrix4 m = Matrix4::identity();
		unsigned short w,h;
		GF::IDevice::GetInstance()->GetScreenSize(&w, &h);
        m = Matrix4::orthographic(0, w, h, 0, -2048, 2048);

		GF::Primitive::SetMaterial(pMaterial, &m);
		GF::Primitive::DrawIndexed(pVertexBuffer,
								   GF::Primitive::Vertex_P3_C8_UV_Stride, 4,
								   pIndexBuffer, 6);
	}

};

SELECT_TASK(Test_2DTex);
