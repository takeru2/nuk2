﻿

#include "gf.h"
#include "gf_input.h"
#include "gf_editvalue.h"

class TestSprite {

public:
	float px = 0;
	float py = 0;
	EDIT_PARAMS mEP;

public:
	TestSprite(){
		EDIT_REGIST(mEP, "TestSp");
		EDIT_VALUE(mEP, px);
		EDIT_VALUE(mEP, py);
	}
	~TestSprite(){
	}
};


class Test_2DTex_Edit : public GF::ITask {
private:
	GF::IBuffer *pVertexBuffer;
	GF::IBuffer *pIndexBuffer;
	GF::IMaterial *pMaterial;
	float mPx = 0;
	float mPy = 0;

	EDIT_PARAMS mEP;
	std::vector<TestSprite*> testSprites;
public:
	Test_2DTex_Edit(){

		pMaterial = GF::IMaterial::GetMaterial("mat_game");

		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(0x100);
		pIndexBuffer = GF::IBuffer::CreateIndexBuffer(0x100);

		//QUAD
		{
			pIndexBuffer->Map();
			int *pIdx = (int*)pIndexBuffer->GetMapMemory();
			
			pIdx[0] = 0;
			pIdx[1] = 1;
			pIdx[2] = 2;
			pIdx[3] = 2;
			pIdx[4] = 3;
			pIdx[5] = 0;

			pIndexBuffer->Unmap();
		}
		{
			EDIT_REGIST(mEP, "2DTex");
			EDIT_VALUE(mEP, mPx);
			EDIT_VALUE(mEP, mPy);
		}

		createSprite();
	}
	void createSprite(void) {
		for (int i = 0; i<4; i++) {
			TestSprite *p = new TestSprite();
			p->px = (float)i * 100;
			testSprites.push_back(p);
		}
	}
	void clearSprite(void){
		for(std::vector<TestSprite*>::iterator itr = testSprites.begin();
			itr != testSprites.end();++itr){
			delete *itr;
		}
		testSprites.clear();
	}

	virtual ~Test_2DTex_Edit(){
		clearSprite();
	}

	virtual void Release(void){
		delete this;
	}

	virtual void Update(void){
		if (GF::IInput::GetInstance()->Press(VK_LBUTTON)) {
			if (testSprites.size() < 10) {
				createSprite();
			}
		}
		else if (GF::IInput::GetInstance()->Press(VK_RBUTTON)) {
			clearSprite();
		}
	}

	virtual void Draw(void){
		float clearColor[] = {0,0,0.1f,1.f};
		GF::IDevice::GetInstance()->Clear(clearColor);

		pVertexBuffer->Map();
		float *ptr = (float*)pVertexBuffer->GetMapMemory(
			GF::Primitive::Vertex_P3_C8_UV_Stride * 4);
		{
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx, mPy, 0, 0xffffffff, 0, 0);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx, mPy+100, 0, 0xffffffff, 0, 1);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx+100, mPy+100, 0, 0xffffffff, 1, 1);
			GF::Primitive::SetVertex_P3_C8_UV(ptr, mPx+100, mPy, 0, 0xffffffff, 1, 0);
		}
		pVertexBuffer->Unmap();

		Matrix4 m = Matrix4::identity();
		unsigned short w,h;
		GF::IDevice::GetInstance()->GetScreenSize(&w, &h);
        m = Matrix4::orthographic(0, w, h, 0, -2048, 2048);

		for(std::vector<TestSprite*>::iterator itr = testSprites.begin();
			itr != testSprites.end();++itr){
			TestSprite *sp = *itr;
			Matrix4 tr = m * Matrix4::translation(Vector3(sp->px, sp->py, 0));
			GF::Primitive::SetMaterial(pMaterial, &tr);
			GF::Primitive::DrawIndexed(pVertexBuffer,
									   GF::Primitive::Vertex_P3_C8_UV_Stride, 4,
									   pIndexBuffer, 6);
		}
	}

};

SELECT_TASK(Test_2DTex_Edit);
