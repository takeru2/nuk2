﻿
#pragma once

#include "gf.h"
#include "gf_input.h"
#include "gf_simplemodel.h"
#include "gf_rendertexture.h"

//-----------------------------------------------------------------------------
// Name : CameraControl
// Desc :
//-----------------------------------------------------------------------------
class CameraControl {
public:
	float ax,ay;
	Matrix4 projM,viewM,viewIM;
	Matrix3 viewTM;
	Matrix4 vpM;
	Point3 eye,at;
	float dist;

	Vector3 dirVec;
	float degAng;
	float screenW, screenH;
	float spX,spY;
	float dist_max;
	float move_ang_max;
	float move_max;
public:
	CameraControl(){
		degAng = 45.f;
		at = Point3(0,0,0);
		eye = Point3(0, 10, -10);
		dist = 100.f;
		dist_max = 40000.f;
		move_ang_max = 20.f;
		move_max =0.002f;
	}
	~CameraControl(){
	}

	void updateMatrix(void){
		projM =  Matrix4::perspective((float)M_PI * degAng / 180.f,
									  screenW / screenH, 1.f, dist_max);
		viewM  = Matrix4::lookAt(eye, at, Vector3(0,1,0));
		viewTM = transpose(viewM.getUpper3x3());
		viewIM = inverse(viewM);
		{
			float px = spX;
			float py = spY;
			float w_h = 0.5f * screenW;
			float h_h = 0.5f * screenH;
			Vector3 sv( -((px / w_h) - 1.f) / projM.getCol0().getX(),
						((py / h_h) - 1.f) / projM.getCol1().getY(), 1.f);
			dirVec = normalize(viewIM * sv).getXYZ();
		}

		vpM = projM * viewM;
	}

	void Update(void){

		//screen
		unsigned short w,h;
		GF::IDevice::GetInstance()->GetScreenSize(&w, &h);
		screenW = (float)w;
		screenH = (float)h;

		//mouse
		float pvx,pvy;
		GF::IInput::GetInstance()->GetPositionInfo(&spX, &spY, &pvx, &pvy);

		
		Point3 _eye = eye;
		Point3 _at = at;

		Vector3 up(0,1,0);
		Vector3 dir = normalize(_at - _eye);
		Vector3 vx = cross(up, dir);
		Vector3 vy = cross(dir, vx);
		vx = normalize(vx);
		vy = normalize(vy);

		if(GF::IInput::GetInstance()->On(0xc1)){
			dist *= 0.9f;
			if(dist <= 1)dist = 1.f;
		}else if(GF::IInput::GetInstance()->On(0xc2)){
			dist *= 1.1f;
			if(dist > dist_max)dist = dist_max;
		}
		_eye = _at - dir * dist;

		{
			ax = Clamp(pvx, -move_ang_max, move_ang_max);
			ay = Clamp(pvy, -move_ang_max, move_ang_max);
		}

		if(GF::IInput::GetInstance()->On(VK_RBUTTON)){
			float rad = fabs(atanf(sqrtf(ax*ax + ay*ay)/100.f));
			if(rad > 0){
				Vector3 ov = cross(dir, -vx * ax  + -vy * ay);
				Matrix3 m3 = Matrix3::rotation(rad, normalize(ov));
				Point3 newEye = _at - m3 * (dir * dist);
				if(fabs(dot(normalize(newEye - _at), Vector3(0,1,0))) > 
				   cosf(move_ang_max * (float)M_PI / 180.f)){
				}else{
					_eye = newEye;
				}
			}
		}

		if(GF::IInput::GetInstance()->On(VK_LBUTTON)){
			Vector3 mvy = vy * ay;
			Vector3 mvx = vx * ax;
			_eye += (mvx + mvy) * dist * move_max;
			_at += (mvx + mvy) * dist * move_max;
		}
		eye = _eye;
		at = _at;
		at.setY(0);

		dist = length(Vector3(eye) - Vector3(at));

		updateMatrix();
	}

	void UpdateShaderMatrix(Matrix4 &tr){
		tr = vpM * tr;
	}
	const Vector3 GetViewDir(void){return viewIM.getCol2().getXYZ();}
	const Point3 &GetPos(void) { return eye; }

};

//-----------------------------------------------------------------------------
// Name : BoxMesh
// Desc :
//-----------------------------------------------------------------------------
class BoxMesh {
public:
	GF::IBuffer *pVertexBuffer;
	GF::IBuffer *pIndexBuffer;

public:
	BoxMesh(){
	}
	~BoxMesh(){
	}
	void Create(float scl){

		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(
			GF::Primitive::Vertex_P3_N3_C8_UV_Stride * 24);
		pIndexBuffer = GF::IBuffer::CreateIndexBuffer(6 * 6 * 4);

		//--------------------
		//        0---------------1    y
		//       /|              /|    |
		//      / |             / |    |
		//     /  |   F0       /  |    |
		//    /   |      f1   /   |    |
		//   /    |          /    |    |
		//  2-----+-------- 3     |     ---x
		//  | f3  4---------|-----5   /
		//  |    /          | F2 /   z
		//  |   /  F4       |   /   
		//  |  /      f5    |  /    
		//  | /             | /    
		//  |/              |/    
        //  6---------------7
		//--------------------
		Vector3 pos[8];
		pos[0] = Vector3(-scl,  scl, -scl); pos[1] = Vector3( scl,  scl, -scl);
		pos[2] = Vector3(-scl,  scl,  scl); pos[3] = Vector3( scl,  scl,  scl);
		pos[4] = Vector3(-scl, -scl, -scl); pos[5] = Vector3( scl, -scl, -scl);
		pos[6] = Vector3(-scl, -scl,  scl); pos[7] = Vector3( scl, -scl,  scl);

		Vector3 normal[6];
		normal[0] = Vector3( 0,  1,  0); normal[1] = Vector3( 0,  0, -1);
		normal[2] = Vector3( 1,  0,  0); normal[3] = Vector3(-1,  0,  0);
		normal[4] = Vector3( 0,  0,  1);normal[5] = Vector3( 0, -1,  0);

		//F0
		int vidx[] = {
			//CW
/*
			0,2,3,1,   0,
			0,1,5,4,   1,
			1,3,7,5,   2,
			2,0,4,6,   3,
			2,6,7,3,   4,
			4,5,7,6,   5,
*/
			//CCW
			0,1,3,2,   0,
			0,4,5,1,   1,
			1,5,7,3,   2,
			2,6,4,0,   3,
			2,3,7,6,   4,
			4,6,7,5,   5,
		};

		pVertexBuffer->Map();
		float *fp = (float*)pVertexBuffer->GetMapMemory();
		for(int i=0;i<6;i++){
			int _vidx = i * 5;
			int _nidx = i * 5 + 4;
			for(int n=0;n<4;n++){
				GF::Primitive::SetVertex_P3_N3_C8_UV(
					fp,
					pos[vidx[_vidx + n]].getX(),
					pos[vidx[_vidx + n]].getY(),
					pos[vidx[_vidx + n]].getZ(),
					normal[vidx[_nidx]].getX(),
					normal[vidx[_nidx]].getY(),
					normal[vidx[_nidx]].getZ(),
					0xffffffff,
					0, 0);
			}
		}
		pVertexBuffer->Unmap();

		pIndexBuffer->Map();
		int *ip = (int*)pIndexBuffer->GetMapMemory();
		for(int i=0;i<6;i++){
			int _vidx = i * 4;
			*ip++ = _vidx + 0;
			*ip++ = _vidx + 1;
			*ip++ = _vidx + 2;

			*ip++ = _vidx + 2;
			*ip++ = _vidx + 3;
			*ip++ = _vidx + 0;
		}
		pIndexBuffer->Unmap();

	}
};


//-----------------------------------------------------------------------------
// Name : DrawGrid
// Desc :
//-----------------------------------------------------------------------------
class DrawGrid {
private:
	GF::IPipeline *pipeline;
	GF::IBuffer *pVertexBuffer;
	int gx = 10;
	int gz = 10;
	bool makeVertex = false;
public:
	DrawGrid(){
		{
			GF::File shaderFile;
			shaderFile.Load("mat_grid/vs.hlsl");
			GF::IShader::CreateShader(
				GF::IShader::SHADER_VS, 
				shaderFile.GetName(),
				"VS",
				(const char*)shaderFile.GetData(), shaderFile.GetSize());
		}
		{
			GF::File shaderFile;
			shaderFile.Load("mat_grid/ps.hlsl");
			GF::IShader::CreateShader(
				GF::IShader::SHADER_PS, 
				shaderFile.GetName(),
				"PS",
				(const char*)shaderFile.GetData(), shaderFile.GetSize());
		}

		{
			GF::IPipeline::Info info;
			info.bs     = GF::IPipeline::BLEND_SA_IA;
			info.ds     = GF::IPipeline::DEPTH_E_WRITE_E;
			info.rs     = GF::IPipeline::FILL_S_CULL_N;
			info.layout = GF::IPipeline::LAYOUT_P3_C8;
			info.vs     = GF::IShader::GetShaderID("mat_grid/vs.hlsl");
			info.ps     = GF::IShader::GetShaderID("mat_grid/ps.hlsl");
			info.topology = GF::IPipeline::TOPOLOGY_LINELIST;
			pipeline = GF::IPipeline::GetPipeline(info);
		}
		
		int vnum = 2 * ((gx + 1) + (gz + 1));

		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(
			vnum * GF::Primitive::Vertex_P3_C8_Stride);
		
	}
	~DrawGrid(){
		pipeline->Release();
		pVertexBuffer->Release();
	}

	void Draw(Matrix4 &tr){
		if(!makeVertex){
			float y = 0;
			float s = 10;
			float sx = s * static_cast<float>( gx ) * 0.5f;
			float sz = s * static_cast<float>( gz ) * 0.5f;

			unsigned int color = GF::PColor(0xff,0xff,0xff,0xff);

			pVertexBuffer->Map();
			float *ptr = (float*)pVertexBuffer->GetMapMemory(
				GF::Primitive::Vertex_P3_C8_Stride * 2 * ((gx + 1) + (gz + 1)));

			for(int i=0;i<=gz;i++){
				GF::Primitive::SetVertex_P3_C8(ptr, -sx, y, -sz + s * static_cast<float>(i), color);
				GF::Primitive::SetVertex_P3_C8(ptr, +sx, y, -sz + s * static_cast<float>(i), color);
			}
			for(int i=0;i<=gx;i++){
				GF::Primitive::SetVertex_P3_C8(ptr, -sx + s * static_cast<float>(i), y, -sz, color);
				GF::Primitive::SetVertex_P3_C8(ptr, -sx + s * static_cast<float>(i), y, +sz, color);
			}
			pVertexBuffer->Unmap();
			makeVertex = true;
		}

		GF::Primitive::SetPipeline(pipeline, &tr, (unsigned int)sizeof(tr), NULL);
		GF::Primitive::Draw(pVertexBuffer, GF::Primitive::Vertex_P3_C8_Stride, 
							2 * ((gx + 1) + (gz + 1)));
	}
};

//-----------------------------------------------------------------------------
// Name : DebugLine
// Desc :
//-----------------------------------------------------------------------------
class DebugLine {
private:
	GF::IMaterial *pMaterial;
	GF::IBuffer *pVertexBuffer;
	unsigned int vertexNum = 0;
public:
	DebugLine(){
		pMaterial = GF::IMaterial::GetMaterial("mat_grid");
		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(0x10000);
	}
	
	~DebugLine(){
		pMaterial->Release();
		pVertexBuffer->Release();
	}

	void Begin(void){
		pVertexBuffer->Map();
		vertexNum = 0;
	}

	void End(void){
		pVertexBuffer->Unmap();
	}

	void SetLines(const Point3 &p0, const Point3 &p1, unsigned int c0, unsigned int c1){
		float *ptr = (float*)pVertexBuffer->GetMapMemory(GF::Primitive::Vertex_P3_C8_Stride * 2);
		GF::Primitive::SetVertex_P3_C8(ptr, p0.getX(), p0.getY(), p0.getZ(), c0);
		GF::Primitive::SetVertex_P3_C8(ptr, p1.getX(), p1.getY(), p1.getZ(), c1);
		vertexNum += 2;
	}
	void SetBoundingBoxLines(const Point3 &p0, const Point3 &p1, unsigned int color){
		//--------------------
		//        0---------------1    y
		//       /|              /|    |
		//      / |             / |    |
		//     /  |   F0       /  |    |
		//    /   |      f1   /   |    |
		//   /    |          /    |    |
		//  2-----+-------- 3     |     ---x
		//  | f3  4---------|-----5   /
		//  |    /          | F2 /   z
		//  |   /  F4       |   /   
		//  |  /      f5    |  /    
		//  | /             | /    
		//  |/              |/    
        //  6---------------7
		//--------------------
		Point3 pos[8];
		pos[0] = Point3(p0.getX(), p1.getY(), p0.getZ());
		pos[1] = Point3(p1.getX(), p1.getY(), p0.getZ());
		pos[2] = Point3(p0.getX(), p1.getY(), p1.getZ());
		pos[3] = Point3(p1.getX(), p1.getY(), p1.getZ());
		pos[4] = Point3(p0.getX(), p0.getY(), p0.getZ());
		pos[5] = Point3(p1.getX(), p0.getY(), p0.getZ());
		pos[6] = Point3(p0.getX(), p0.getY(), p1.getZ());
		pos[7] = Point3(p1.getX(), p0.getY(), p1.getZ());

		SetLines(pos[0], pos[1], color, color);
		SetLines(pos[0], pos[2], color, color);
		SetLines(pos[2], pos[3], color, color);
		SetLines(pos[1], pos[3], color, color);

		SetLines(pos[4], pos[5], color, color);
		SetLines(pos[4], pos[6], color, color);
		SetLines(pos[6], pos[7], color, color);
		SetLines(pos[5], pos[7], color, color);

		SetLines(pos[0], pos[4], color, color);
		SetLines(pos[1], pos[5], color, color);
		SetLines(pos[2], pos[6], color, color);
		SetLines(pos[3], pos[7], color, color);
	}
	
	void Draw(const Matrix4 &tr){
		GF::Primitive::SetMaterial(pMaterial, &tr);
		GF::Primitive::Draw(pVertexBuffer, GF::Primitive::Vertex_P3_C8_Stride, vertexNum);
	}
};

//-----------------------------------------------------------------------------
// Name : DebugTexture2D
// Desc :
//-----------------------------------------------------------------------------
class DebugTexture2D {
private:
	GF::IMaterial *pMaterial;
	GF::IBuffer *pVertexBuffer;
	struct Sprite {
		GF::ISRV *pSRV;
		unsigned int vtxOffset;
		unsigned int vtxNum;
	};
	std::vector<Sprite> vSprite;

public:
	DebugTexture2D(){
		pMaterial = GF::IMaterial::GetMaterial("mat_tex2d");
		pVertexBuffer = GF::IBuffer::CreateVertexBuffer(0x1000);
	}
	
	~DebugTexture2D(){
		pMaterial->Release();
		pVertexBuffer->Release();
	}

	void Begin(void){
		pVertexBuffer->Map();
		vSprite.clear();
	}

	void End(void){
		pVertexBuffer->Unmap();
	}

	void SetSprite(GF::ISRV *srv,
				   const Point3 &cp, float hw, float hh, unsigned int color){
		
		Sprite sp;
		sp.pSRV = srv;
		sp.vtxOffset = pVertexBuffer->GetMapMemoryOffset();
		
		float *ptr = (float*)pVertexBuffer->GetMapMemory(
			GF::Primitive::Vertex_P3_C8_UV_Stride * 6);
		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()-hw, cp.getY()-hh, cp.getZ(), color, 0, 0);
		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()-hw, cp.getY()+hh, cp.getZ(), color, 0, 1);
		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()+hw, cp.getY()+hh, cp.getZ(), color, 1, 1);

		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()+hw, cp.getY()+hh, cp.getZ(), color, 1, 1);
		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()+hw, cp.getY()-hh, cp.getZ(), color, 1, 0);
		GF::Primitive::SetVertex_P3_C8_UV(
			ptr,
			cp.getX()-hw, cp.getY()-hh, cp.getZ(), color, 0, 0);

		unsigned int ws = pVertexBuffer->GetMapMemoryOffset() - sp.vtxOffset;
		sp.vtxNum = ws / GF::Primitive::Vertex_P3_C8_UV_Stride;
		vSprite.push_back( sp );
	}

	void Draw(const Matrix4 &tr){
		for(std::vector<Sprite>::iterator itr = vSprite.begin();
			itr != vSprite.end();++itr){
			Sprite &sp = *itr;
			const GF::ISRV *srv[1] = {sp.pSRV};
			pMaterial->UpdateSRV(1, srv);
			GF::Primitive::SetMaterial(pMaterial, &tr);
			GF::Primitive::Draw(pVertexBuffer,
								GF::Primitive::Vertex_P3_C8_UV_Stride,
								sp.vtxOffset, sp.vtxNum);
		}
	}
};


//-----------------------------------------------------------------------------
// Name : DebugTexture2D
// Desc :
//-----------------------------------------------------------------------------
class DebugNormal {
private:
	GF::IMaterial *pMaterial;
	GF::IPipeline *pPipelineCS;
	GF::IBuffer *pVertexBuffer = NULL;

	unsigned int bufferSize = 0;
public:
	DebugNormal() {
		pMaterial = GF::IMaterial::GetMaterial("mat_debug_normal");
		{
			const char *fname = "mat_debug_normal/cs_debug_normal.hlsl";
			GF::IShader::CreateShader(GF::IShader::SHADER_CS, fname, "CS", fname);
			GF::IPipeline::Info info;
			info.cs = GF::IShader::GetShaderID(fname);
			info.constant_size[0] = 2048;
			pPipelineCS = GF::IPipeline::GetPipeline(info);
		}
	}

	~DebugNormal() {
		SAFE_RELEASE(pMaterial);
		SAFE_RELEASE(pPipelineCS);
		SAFE_RELEASE(pVertexBuffer);
	}

	void resize(unsigned int size){
		if(bufferSize < size){
			SAFE_RELEASE(pVertexBuffer);
			bufferSize = size;
			pVertexBuffer = GF::IBuffer::CreateBuffer(bufferSize);
			pVertexBuffer->CreateUAV();
		}
	}
	
	void Draw(const Matrix4 &transform, GF::ISRV *srv,
			  unsigned int vtxNum, unsigned int stride,
			  unsigned int color0, unsigned int color1, float length) {

		resize(vtxNum * stride);
		{
			void *views[] = { srv };
			void *uavs[] = { pVertexBuffer->GetUAV() };

			struct cs_buffer{
				unsigned int     param[4];
				float           fparam[4];
			};
			cs_buffer cb;
			cb.param[0] = vtxNum;
			cb.param[1] = stride;
			cb.param[2] = color0;
			cb.param[3] = color1;
			cb.fparam[0] = length;
			pPipelineCS->SetConstantBuffer(&cb, sizeof(cb));
			pPipelineCS->Dispatch(views, 1, uavs, 1, (vtxNum / 256) + 1, 1, 1);

			struct scb {
				Matrix4 m;
			};
			scb _scb;
			_scb.m = transform;
			GF::Primitive::SetMaterial(pMaterial, &_scb);
			GF::Primitive::Draw(pVertexBuffer, 16, vtxNum);
		}
	}
};

